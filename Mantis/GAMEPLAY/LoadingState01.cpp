#include "LoadingState01.h"
#include "AllGameStates.h"
#include "../FRAMEWORK/Input/RawInput.h"
#include "../GRAPHICS/D3D9GraphicsCore.h"
#include "../FRAMEWORK/GameFramework.h"
#include "../FRAMEWORK/Sound/3DSound.h"

void LoadingState01::InitializeState()
{

}

void LoadingState01::EnterState()
{
	Graphics->SetDrawColor(0, 0, 0);
	Graphics->SetSkyBoxRender(false);
	Graphics->SetGfxStatsRender(false);
}

void LoadingState01::LeaveState()
{

}

void LoadingState01::Update(float dt)
{
	static float te = 0.0f;
	te += dt;
	if(te >= 0.5f)
		Framework->getGSM()->LoadNextState();
}
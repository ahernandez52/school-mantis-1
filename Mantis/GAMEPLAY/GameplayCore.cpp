#include "GameplayCore.h"
#include "../FRAMEWORK/Managers/WorldManager.h"
#include "../AI/Messaging/PostOffice.h"
#include "Classes\Warp\WarpController.h"
#include "Classes\Quest\QuestSystem.h"
#include "../GRAPHICS/Objects/D3D9Object.h"
#include "../FRAMEWORK/Sound/Audio.h"

using namespace std;

GameplayCore::GameplayCore()
{
	m_Player = nullptr;
}

GameplayCore::~GameplayCore()
{
	Shutdown();
}

void GameplayCore::Startup()
{
	m_BattleSystem = new BattleSystem();
	m_WarpController = new WarpController();
	m_QuestSystem = new QuestSystem();
	m_QuestSystem->CreateQuestDialogWindow();
}

void GameplayCore::Shutdown()
{
	ReleasePlayerAudio();
	CleanUp();

	delete m_BattleSystem;
	delete m_WarpController;
	delete m_QuestSystem;
}

void GameplayCore::CleanUp()
{
	m_BattleSystem->m_ActiveProjectiles.clear();

	if(m_Player != nullptr)
	{
		m_Player->GetActor()->Gameplay = 0;
		delete m_Player;
		m_Player = nullptr;
	}

	m_NPCs.clear();
}

//Private Functions
//=======================
void GameplayCore::InterpretContacts()
{
	vector<pair<GameplayObject*, GameplayObject*>> Contacts;

	auto projectiles = m_BattleSystem->m_ActiveProjectiles;
	auto NPCs = m_NPCs;

	for(auto projectile : projectiles)
	{
		Vector3 midline(projectile->GetActor()->m_Position - m_Player->GetActor()->m_Position);
		float length = midline.LengthSq();

		float radii = (projectile->m_Radius + m_Player->m_Radius) * (projectile->m_Radius + m_Player->m_Radius);

		if(length < radii)
		{
			Contacts.push_back(make_pair(m_Player, projectile));

			Vector3 contactPoint = Vector3(m_Player->GetActor()->m_Position + midline * 0.5f);
			
			if(m_Player->m_CurrentShield > 0.0f)
			{
				m_BattleSystem->m_Particles->CreateEmitter("ShieldImpact", contactPoint, 2.0f);
				m_Player->HitShields(contactPoint - m_Player->GetActor()->m_Position);
				//RegisterShieldImpact(m_Player->m_ShieldMesh, contactPoint - m_Player->GetActor()->m_Position, 1.0f, 15.0f);
			}
		}
	}

	for(auto projectile : projectiles)
	{
		for(auto npc : m_NPCs)
		{
			Vector3 midline(projectile->GetActor()->m_Position - npc->GetActor()->m_Position);
			float length = midline.LengthSq();

			float radii = (projectile->m_Radius + npc->m_Radius) * (projectile->m_Radius + npc->m_Radius);

			if(length < radii)
			{
				Contacts.push_back(make_pair(npc, projectile));

				Vector3 contactPoint = Vector3(npc->GetActor()->m_Position + midline * 0.5f);

				if(npc->m_CurrentShield > 0.0f)
				{
					m_BattleSystem->m_Particles->CreateEmitter("ShieldImpact", contactPoint, 2.0f);
					npc->HitShields(contactPoint - npc->GetActor()->m_Position);
					//RegisterShieldImpact(npc->m_ShieldMesh, contactPoint - npc->GetActor()->m_Position, 1.0f, 17.0f);
				}
			}
		}
	}

	vector<GameplayObject*> actorsToDelete;

	for(auto pair : Contacts)
    {       
        if(pair.first->GetGameplayType() == PLAYER)
        {
			((Player*)(pair.first))->TakeDamage(((Projectile*)(pair.second))->Damage());
			
			m_BattleSystem->RetireProjectile(((Projectile*)(pair.second)));
        }

        if(pair.first->GetGameplayType() == NPC)
        {
            //Send message to AI
			if(! ((Ship*)(pair.first))->IsDead() )
			{
				Message* m = new Message(pair.first->GetActor()->ID, pair.first->GetActor()->ID, 0.0f, HIT);
				POST->AddMail(m);
			}

			((Ship*)(pair.first))->TakeDamage(((Projectile*)(pair.second))->Damage());


			m_BattleSystem->RetireProjectile(((Projectile*)(pair.second)));
        }
    }

	for( vector<Ship*>::iterator it = m_NPCs.begin();
			it != m_NPCs.end();
			++it)
	{
		if( (*it)->IsDead() )
		{
			//Delete the actor and remove him from the list
			Ship* temp = (*it);

			it = m_NPCs.erase(it);

			if(temp->GetActor() == m_Player->m_Target)
			{
				m_Player->m_Target = nullptr;
			}

			m_Player->m_Credits += 10000;

			POST->DeleteMessagesWithToID(temp->GetActor()->ID);

			WMI->DeRegisterGraphic(temp->m_ShieldMesh);
			delete temp->m_ShieldMesh;

			SCI->PlaySample3D("Assets/Sounds/Effects/explosion.wav", temp->GetActor()->m_Position);
			m_BattleSystem->m_Particles->CreateEmitter("Explosion1", temp->GetActor()->m_Position, 1.8f);

			WMI->DeleteActor(temp->GetActor());

			if(it == m_NPCs.end())
			{
				break;
			}
			if(it != m_NPCs.begin())
			{
				--it;
			}
		}
	}
}

void GameplayCore::RegisterShieldImpact(D3D9Object* impactedShields, Vector3 impact, float duration, float blastRadius)
{
	m_ShieldImpacts.push_back(ShieldImpact(impactedShields, impact, duration, blastRadius));
}

void GameplayCore::UpdateShieldImpacts(float dt)
{
	//for(auto impact : m_ShieldImpacts)
	//{
	//	
	//	impact.m_ImpactedShields->SetShieldContact(impact.m_Impact, impact.m_BlastRadius, D3DXVECTOR4(1.0, 0.0f, 0.0f, 0.0f));

	//	impact.m_BlastRadius *= impact.m_TimeLeft / impact.m_Duration;

	//	impact.m_TimeLeft -= dt;
	//}
	////for(int i = m_ShieldImpacts.size() - 1;
	////	i >= 0;
	////	--i)
	////{
	////	auto impact = m_ShieldImpacts.at(i);
	////	impact.m_TimeLeft -= dt;
	////	impact.m_ImpactedShields->SetShieldContact(impact.m_Impact, impact.m_BlastRadius, D3DXVECTOR4(1.0, 0.0f, 0.0f, 0.0f));

	////	impact.m_BlastRadius *= impact.m_TimeLeft / impact.m_Duration;

	////	if(impact.m_TimeLeft <= 0.0f)
	////	{
	////		m_ShieldImpacts.erase(m_ShieldImpacts.begin() + i);
	////	}
	////}
}
//***********************


//Public Functions
//=======================
void GameplayCore::Update(vector<ActorContactPair> contactingPairs, float dt)
{
	InterpretContacts();
	UpdateShieldImpacts(dt);

	if(m_Player != nullptr)
		m_Player->Update(dt);

	for(auto NPC : m_NPCs)
	{
		NPC->Update(dt);
	}

	m_BattleSystem->Update(dt);

	m_WarpController->Update(dt);
}

void GameplayCore::InitPlayer()
{
	Actor *playerActor = WMI->CreateActor("player", "player4", "militaryFighter4", "NULL");

	//playerActor->Gameplay = new Ship(playerActor);
	m_Player = new Player(playerActor);

	m_Player->m_Radius = 40.0f;	//BV

	m_Player->EquipWeapon(1, 0.25f, Vector3(10.0f, 0.0f, 0.0f));
	m_Player->EquipWeapon(1, 0.25f, Vector3(-10.0f, 0.0f, 0.0f));

	playerActor->Gameplay = m_Player;

	m_Player->m_ShieldMesh = new D3D9Object(playerActor, WMI->GetGraphicsSchematic("player4LP"));
	WMI->RegisterGraphicObject(m_Player->m_ShieldMesh);
	m_Player->m_ShieldMesh->SetEffect(FX_SHIELD);
	m_Player->m_ShieldColor = Vector4(0.0f, 0.0f, 1.0f, 1.0f);

	m_QuestSystem->m_QuestLog = m_Player->m_QuestLog;

	InitPlayerAudio();
}

void GameplayCore::InitPlayerAudio()
{
	//SCI->LoadSound("Assets/Sounds/Effects/shield.wav");
	//SCI->LoadSound("Assets/Sounds/Effects/hull.wav");

}

void GameplayCore::ReleasePlayerAudio()
{
	//SCI->ReleaseSound("Assets/Sounds/Effects/shield.wav");
	//SCI->ReleaseSound("Assets/Sounds/Effects/hull.wav");
	//SCI->ReleaseSound("Assets/Sounds/Effects/fire_palceholder.wav");
}

void GameplayCore::CreateNPC(Actor* actor, string type)
{
	//need a ship factory here
	Ship *newShip = new Ship(actor);
	actor->Gameplay = newShip;

	if(type == "pirate_1")
	{
		newShip->m_Acceleration	= 800.0f;
		newShip->m_MaxHull		= 100.0f;
		newShip->m_CurrentHull	= 100.0f;
		newShip->m_MaxShield	= 100.0f;
		newShip->m_CurrentShield= 100.0f;
		newShip->m_ShieldRechargeDelay	= 3.0f;
		newShip->m_ShieldRechargeRate	= 10.0f;
		newShip->m_TimeOutOfCombat	= newShip->m_ShieldRechargeDelay;
		newShip->m_MaxSpeed		= 160.0f;
		newShip->m_CurrentSpeed	= 0.0f;
	}

	newShip->m_Radius			= 30.0f;
	newShip->SetGameplayType(NPC);

	m_NPCs.push_back(newShip);
}

void GameplayCore::RemoveNPC(Ship* ship)
{
	for(vector<Ship*>::iterator it = m_NPCs.begin();
		it != m_NPCs.end();
		it++)
	{
		if((*it) == ship)
		{
			m_NPCs.erase(it);
			return;
		}
	}
}

//void GameplayCore::AddNPC(Ship* ship)
//{
//	m_NPCs.push_back(ship);
//}

Player* GameplayCore::GetPlayer()
{
	return m_Player;
}

vector<Ship*> GameplayCore::GetNPCs()
{
	return m_NPCs;
}
//***********************
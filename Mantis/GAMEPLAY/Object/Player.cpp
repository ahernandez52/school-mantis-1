#include "Player.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../FRAMEWORK/Input/RawInput.h"
#include "../../FRAMEWORK/GameFramework.h"

#include "PlayerController.h"
#include "../../FRAMEWORK/Sound/Audio.h"

#include "../../FRAMEWORK/Managers/WorldManager.h"

#include "../Classes/Quest/QuestLog.h"

Player::Player(Actor *OwningActor) : Ship(OwningActor)
{
	m_Actor = OwningActor;

	m_GameplayType = PLAYER;


	m_Acceleration			= 800.0f;
	m_MaxHull				= 100.0f;
	m_CurrentHull			= m_MaxHull;
	m_MaxShield				= 100.0f;
	m_CurrentShield			= m_MaxShield;
	m_ShieldRechargeDelay	= 3.0f;
	m_ShieldRechargeRate	= 10.0f;
	m_TimeOutOfCombat		= m_ShieldRechargeDelay;
	m_MaxSpeed		= 160.0f;
	m_CurrentSpeed	= 0.0f;

	m_Rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_CurrentVelocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_Steering = false;

	m_TotalExperience		= 0.0f;
	m_CurrentExperience		= 0.0f;
	m_ExperienceToLevel		= 0.0f;

	m_Level					= 0;
	m_Credits				= 0;

	m_Target				= nullptr;

	m_boosting = false;
	m_cruiseCharging = false;
	m_cruising = false;

	//m_Weapons.AddWeapon(1, .5f);
	//m_Weapons.AddWeapon(2, .5f);

	m_QuestLog = new QuestLog();
	m_QuestLog->CreateQuestLogWindow();

	//m_Target = WMI->CreateActor("NULL", "Sun", "NULL", "NULL");
	//m_Target->Graphics->m_scale = D3DXVECTOR3(5.0f, 5.0f, 5.0f);
}

Player::~Player()
{
	 delete m_QuestLog;
}

//Private Functions
//===============================
void Player::Input(float dt)
{
	//<WARNING>
	Quaternion tempQuat = m_Actor->m_Orientation;
	D3DXVECTOR3 forward = tempQuat.Forward();

	//Update Position
	float finalSpeed = m_CurrentSpeed;

	static float chargeTime = 5.0f;
	if(m_cruiseCharging)
	{
		chargeTime -= dt;

		if(chargeTime <= 0.0f)
		{
			m_cruising = true;
			m_cruiseCharging = false;
			chargeTime = 5.0f;
		}
	}

	if(m_cruising)
	{
		//m_CurrentSpeed += m_Acceleration * dt;
		//forward *= m_Acceleration * dt;
		finalSpeed = m_MaxSpeed * 2.2f;
	} 
	else if (m_boosting)
	{
		finalSpeed = m_MaxSpeed * 1.9f;
	}
	else 
	{
		finalSpeed = m_CurrentSpeed;
	}

	m_Actor->m_Position += (forward* finalSpeed) * dt;



	if(gRawInput->keyPressed("W"))
	{
		if(m_CurrentSpeed < m_MaxSpeed)
		{
			m_CurrentSpeed += m_Acceleration * dt;
			forward *= m_Acceleration * dt;
			//m_Actor->Physics->AddVelocity(forward);
		}
		else
		{
			m_CurrentSpeed = m_MaxSpeed;
		}
	}

	if(gRawInput->keyPressed("S"))
	{
		if(m_cruising)
		{
			StopCruise();
		}

		if(m_CurrentSpeed > -(m_MaxSpeed*2))
		{
			m_CurrentSpeed -= (m_Acceleration * 1.3f) * dt;
			forward *= -(m_Acceleration * 1.3f) * dt;
			//m_Actor->Physics->AddVelocity(forward);
		}
		else
		{
			m_CurrentSpeed = -(m_MaxSpeed / 2);
		}
	}


	if(gRawInput->mouseButtonHeld(1))
	{
		if(!(m_cruising || m_cruiseCharging))
		{
			D3DXVECTOR3 dir = ProjectedTarget - m_Actor->m_Position;
			D3DXVec3Normalize(&dir, &dir);

 			if (m_Weapons.Fire(m_Actor->m_Position, dir))
			{
				SCI->PlaySample3D("Assets/Sounds/Effects/fire_bolt_micro.wav", m_Actor->m_Position);
			}
		}
		else
		{
			SCI->PlaySample("Assets/Sounds/Effects/no_fire.wav");
		}
	}


	//m_Actor->Physics->SetAwake(true);
	//m_Actor->Physics->m_LinearVelocity = forward * m_CurrentSpeed;
	m_CurrentVelocity = forward * finalSpeed;

	if(gRawInput->keyPressed(VK_SPACE))
	{
		m_Steering = !m_Steering;
		//Play Sound

		if(!m_Steering)
		{
			//Re-orient ship

		}	
	}

	//if(gRawInput->keyPressed("X"))
	//{
	//	m_CurrentSpeed = 0;
	//}

	if(gRawInput->keyPressed("L"))
	{
		m_QuestLog->ToggleQuestLog();
	}
}

void Player::Clipping()
{
	
}

void Player::DidLevelUp()
{
	m_ExperienceToLevel = m_ExperienceToLevel * 1.3f;

	m_CurrentExperience = 0.0f;

	//check for talents
	//and rpg elements
}



//*******************************

//Public Functions
//===============================
void Player::Update(float dt)
{
	//process player input
	Input(dt);

	//Charge Shields
	ChargeShield(dt);
	UpdateShields(dt);

	//Update Weapon system
	m_Weapons.UpdateWeapons(dt);

	//Camera Post Process
	D3DXVECTOR3 camPos = m_Actor->m_Position;

	//<WARNING>
	Quaternion tempQuat = m_Actor->m_Orientation;

	D3DXVECTOR3 offset = tempQuat.Forward();
	D3DXVECTOR3 UPoffset = tempQuat.Up();
	UPoffset *= 30.0f;
	offset *= 120.0f;

	camPos -= offset;
	camPos += UPoffset;

	D3DXVECTOR3 lookAtPoint;
	lookAtPoint = camPos;
	lookAtPoint += offset/4;

	gCamera->lookAt(camPos, lookAtPoint, tempQuat.Up());

	//Calculate projected targeting point
	D3DXVECTOR3 originW(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dirW(0.0f, 0.0f, 0.0f);

	POINT s;
	GetCursorPos(&s);
	ScreenToClient(gd3dApp->getMainWnd(), &s);

	float w = (float)gd3dApp->GetScreenSize().x;
	float h = (float)gd3dApp->GetScreenSize().y;

	s.x = (s.x > w) ? (LONG)w : s.x;
	s.x = (s.x < 0) ? 0 : s.x;

	s.y = (s.y > h) ? (LONG)h : s.y;
	s.y = (s.y < 0) ? 0 : s.y;

	D3DXMATRIX proj = gCamera->proj();

	float x = (2.0f*s.x/w - 1.0f) / proj(0,0);
	float y = (-2.0f*s.y/h + 1.0f) / proj(1,1);

	// Build picking ray in view space.
	D3DXVECTOR3 origin(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 dir(x, y, 1.0f);

	// So if the view matrix transforms coordinates from 
	// world space to view space, then the inverse of the
	// view matrix transforms coordinates from view space
	// to world space.
	D3DXMATRIX invView;
	D3DXMatrixInverse(&invView, 0, &gCamera->view());

	// Transform picking ray to world space.
	D3DXVec3TransformCoord(&originW, &origin, &invView);
	D3DXVec3TransformNormal(&dirW, &dir, &invView);
	D3DXVec3Normalize(&dirW, &dirW);

	ProjectedTarget = (dirW * 1000.0f/*distance projected*/) + gCamera->pos();
	//m_Target->m_Position = ProjectedTarget;

	if(m_Steering)
	{
		static float SENSITVITY = 0.25f;
		float yaw = 0.0f;
		float pitch = 0.0f;
		float speed = 2.0f;

		D3DXVECTOR2 mouse((float)s.x, (float)s.y);
		D3DXVECTOR2 screenCenter(w/2.0f, h/2.0f);
		D3DXVECTOR2 screenReso(w, h);

		float MaxFromCenter = D3DXVec2Length(&(screenCenter - screenReso));

		//account for screen ratio
		MaxFromCenter *= 0.8f;

		float DistanceFromCenter = D3DXVec2Length(&(mouse - screenCenter));


		float speedCoef = DistanceFromCenter / MaxFromCenter;


		float XCoef = (mouse.x - screenCenter.x) / screenCenter.x;
		float YCoef = (mouse.y - screenCenter.y) / screenCenter.y;


		yaw += ((speed * XCoef) * dt);
 		pitch += ((speed * YCoef) * dt);

		if(pitch >= D3DX_PI*2)
			pitch = 0;

		if(yaw >= D3DX_PI*2)
			yaw = 0;

		m_Rotation.x = yaw * 0.4f;
		m_Rotation.y = pitch * 0.4f;

		D3DXQUATERNION s = D3DXQUATERNION(m_Rotation.y, m_Rotation.x, m_Rotation.z, 0);

		s *= m_Actor->m_Orientation;

		m_Actor->m_Orientation.w += s.w;
		m_Actor->m_Orientation.x += s.x;
		m_Actor->m_Orientation.y += s.y;
		m_Actor->m_Orientation.z += s.z;

		//Post Camera Process
		//======================================================
		float leanCoef = fabsf(mouse.x - screenCenter.x) / screenCenter.x;

		D3DXVECTOR3 sideOffset = tempQuat.Right();

		sideOffset *= (40.0f * leanCoef);

		float duckCoef = (screenCenter.y - mouse.y) / screenCenter.y;

		Vector3 ProjectedLook = ProjectedTarget;

		//TURN RIGHT
		if(mouse.x > screenCenter.x)
		{
			ProjectedLook -= sideOffset * 15.0f;
			camPos += sideOffset * 1.8f;
			camPos += (tempQuat.Up() * 15.0f) * leanCoef;
		}
		else //TURN LEFT
		{
			ProjectedLook += sideOffset * 15;
			camPos -= sideOffset * 2;
			camPos += (tempQuat.Up() * 15.0) * leanCoef;
		}

		//Pitch UP
		if(mouse.y < screenCenter.y)
		{
			ProjectedLook -= (tempQuat.Up() * 200.0) * duckCoef;
			camPos -= (tempQuat.Up() * 15.0) * duckCoef;
		}
		else //Pitch Down
		{
			ProjectedLook -= (tempQuat.Up() * 200.0) * duckCoef;
			camPos += (tempQuat.Up() * 65.0) * duckCoef;
		}

		gCamera->lookAt(camPos, ProjectedLook, tempQuat.Up());
	}
	else if (!m_Steering)
	{ 
		////Get the heading as a rotation axis
		//Quaternion orientation = m_Actor->m_Orientation;
		//Vector3 heading = orientation.Forward();

		//D3DXQUATERNION reference;
		//D3DXQuaternionRotationAxis(&reference, &heading, 0.0f);

		D3DXQuaternionSlerp(&m_Actor->m_Orientation, &m_Actor->m_Orientation, &D3DXQUATERNION(0, 0, 0, 1), dt);
	}

	if(gRawInput->mouseButtonHeld(0))
	{
		//m_Weapons.Fire(m_Actor->m_Position, 
	}

	Clipping();
}

void Player::BoostON()
{
	m_boosting = true;
}

void Player::BoostOFF()
{
	m_boosting = false;
}

void Player::StartCruise()
{
	if(!(m_cruising || m_cruiseCharging))
		SCI->PlaySample("Assets/Sounds/Effects/engine_start.wav", 1.0f);

	m_cruiseCharging = true;
}

void Player::StopCruise()
{
	if(m_cruising || m_cruiseCharging)
		SCI->PlaySample("Assets/Sounds/Effects/engine_stop.wav", 1.0f);

	m_cruising = false;
}

void Player::AddExperience(float value)
{
	m_CurrentExperience += value;
	m_TotalExperience += value;

	if(m_CurrentExperience >= m_ExperienceToLevel)
		DidLevelUp();
}
//*******************************
#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"
//#include "Ship.h"

//Utility functions
float MapToRange(float orientation)
{
	if (orientation > D3DX_PI)
		return orientation - 2 * D3DX_PI;
	if (orientation < D3DX_PI)
		return orientation + 2* D3DX_PI;
	return orientation;
}

D3DXVECTOR3 Transform(D3DXVECTOR3 vector, D3DXQUATERNION quat)
{
	//Convert the vector into a quaternion
	D3DXQUATERNION vectorAsQuat = D3DXQUATERNION(vector.x, vector.y, vector.z, 0);

	//Transform it
	vectorAsQuat = quat * vectorAsQuat * (-quat);

	//Normalize it
	//D3DXVec3Normalize(&vectorAsQuat);

	//Return transformed vector
	return D3DXVECTOR3(vectorAsQuat.x, vectorAsQuat.y, vectorAsQuat.z);
}

class PlayerController
{
private:

protected:
	
	D3DXVECTOR3 m_DesiredAngularVelocity;
	
	float 			m_MouseX;
	float			m_MouseY;
	
	USHORT 			m_VirtualKey;
	
	//Ship			m_Ship;

	D3DXVECTOR3		m_Target;
	D3DXVECTOR3		m_Position;
	D3DXQUATERNION	m_Orientation;

	float			m_MaxRotation;
	float			m_MaxAngularAccl;
	
public:

public:
	PlayerController(){}
	PlayerController(/*Ship ship, */D3DXVECTOR3 target, D3DXVECTOR3 position, D3DXQUATERNION orientation, float maxRotation, float maxAngularAccl)
	{
		//m_Ship				= ship;
		m_Target			= target;
		m_Position			= position;
		m_Orientation		= orientation;
		m_MaxRotation		= maxRotation;
		m_MaxAngularAccl	= maxAngularAccl;
	}
	
	~PlayerController(){}
	
	//D3DXVECTOR3 getRotation(float mx, float my)
	D3DXVECTOR3 getRotation()
	{
		//Create 3D point using ray casting here
		D3DXVECTOR3 target;
		
		//Get direction
		//m_Target = projected target
		//CURRENTPOSITION - Player actor position
		D3DXVECTOR3 direction = m_Target - m_Position;
		
		//Create rotation variable
		D3DXVECTOR3 rotation;
		
		//If facing make no change
		if (D3DXVec3LengthSq(&direction) < D3DX_16F_EPSILON)
		{
			rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			return rotation;
		}
		
		//Calculate the orientation change
		D3DXVec3Normalize(&direction, &direction);
		D3DXQUATERNION q = CalculateOrientation(direction);
		
		return Align3D(q);
	}
	
	D3DXQUATERNION CalculateOrientation(D3DXVECTOR3 vector)
	{
		D3DXVECTOR3 BaseZ;
		D3DXQUATERNION m_BaseOrientation(0.0f, 0.0f, 0.0f, 1.0f);

		//Get the base vector along Z axis
		BaseZ = Transform(D3DXVECTOR3(0, 0, 1), D3DXQUATERNION(0, 0, 0, 1));
		//BaseZ = Transform(QuatForwardVec(m_Orientation), m_Orientation);

		D3DXVECTOR3 dir = vector - m_Position;
		D3DXVec3Normalize(&dir, &dir);

		if (BaseZ == vector)
			return m_BaseOrientation;
	
		if (BaseZ == -vector)
		{
			D3DXQUATERNION bInv;
			D3DXQuaternionInverse(&bInv, &m_BaseOrientation);
			return bInv;
		}
		

		//Find the minimum rotation from base to target using vector product
		D3DXVECTOR3 change, localV;
	
		//Find the angle and axis
		D3DXVECTOR3 normal, normalB;
		D3DXVec3Normalize(&normal, &vector);
		D3DXVec3Normalize(&normalB, &BaseZ);
		float angle = acosf(D3DXVec3Dot(&BaseZ, &normal));

		D3DXVec3Cross(&change, &BaseZ, &vector);

		D3DXVECTOR3 b;
        D3DXVec3Cross(&b, &change, &normalB);

		D3DXVECTOR3 axis = change;

		D3DXVec3Normalize(&axis, &axis);

		//D3DXVECTOR3 axis = change / angle;

		

		//Pack the axis and angle into a quaternion
		return D3DXQUATERNION(sinf(angle/2)*axis.x, sinf(angle/2)*axis.y,
							  sinf(angle/2)*axis.z, cosf(angle/2));
	}
	
	D3DXVECTOR3 Align3D(D3DXQUATERNION t)
	{
		//Result q = s^-1 * t;  where s is the inverse of the actor's current orientation
		D3DXQUATERNION q, sInv;
		D3DXQuaternionInverse(&sInv, &m_Orientation);
		
		D3DXQuaternionNormalize(&t, &t);
		
		q = sInv * t;
		D3DXQuaternionNormalize(&q, &q);
		
		//Split quaternion into angle and axis
		
		//Double check for normalized float
		if (q.w > 1.0f || q.w < -1.0f)
		{
			q.w /= fabs(q.w);
		}
		
		//Angle
		float theta = 2 * acosf(q.w);

		//if (UndefinedResult(theta))
       // {
       //         theta = 0.0f;
       // }
		
		//Axis of rotation
		D3DXVECTOR3 a = D3DXVECTOR3((1/(sinf(theta/2)))*q.x, 
								    (1/(sinf(theta/2)))*q.y,
								    (1/(sinf(theta/2)))*q.z);
									
		D3DXVec3Normalize(&a, &a);

		//if (UndefinedResult(a))
       // {
        //        a = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
        //}
		
		//==========================================
		//		   Find the Rotation Vector
		//==========================================
	
		//Find angular acceleration
		float rotation = theta;
	
		rotation = MapToRange(rotation);

		float rotationSize = fabs(rotation);
	
		//Bail out if no change
		if (rotationSize < D3DX_16F_EPSILON)
		{
			rotation = 0.0f;
		}
	
		float targetRotation;
		float slowRadius = D3DX_PI / 2;  //ROTATION WILL SLOW IF ANGLE IS WITHIN THIS RADIUS

		//If we are tehre
		if (rotationSize < (D3DX_PI / 8))
        {
            targetRotation = 0.0f;
        }
	
		//Rotate at full speed
		if (rotationSize > slowRadius)
		{
			targetRotation = m_MaxRotation;	//ROTATE AT MAX SPEED IF OUTSIDE
		}												//OF SLOW RADIUS
	
		//Rotate slowly
		else
		{
			targetRotation = m_MaxRotation * rotationSize / slowRadius;
		}
	
		//Calculate final target rotation
		if (rotationSize != 0)
		{
			targetRotation *= rotation / rotationSize;
		}
	
		else
		{
			targetRotation = 0.0f;
		}

		//Debug
       // if (UndefinedResult(targetRotation))
       // {
        //        targetRotation = 0.0f;
        //}
	
		float steeringAngular = targetRotation;
		steeringAngular /= (float)0.3;					//WHERE 0.3 IS TIMETOTARGET
	
		//Normalize the value
		float angularAcceleration = abs(steeringAngular);
		if (angularAcceleration > m_MaxAngularAccl)
		{
			steeringAngular /= angularAcceleration;
			steeringAngular *= m_MaxAngularAccl;
		}
	
		//Set angular velocity
		float w = steeringAngular;

		//Debug
        //if (UndefinedResult(w))
        //{
        //        w = 0.0f;
        //}
	
		//Get the target angular velocity
		//D3DXVECTOR3 rot(a.x*w, a.y*w, a.z*w);
		return D3DXVECTOR3(a.x*w, a.y*w, a.z*w);
	
		////==========================================
		////		 Align to Rotation Axis Vector
		////==========================================
	
		////Get cross product of rotation vector and baseZorientation
		//D3DXVECTOR3 r, Zb, rotAxis;
		//D3DXQUATERNION baseOrientation = D3DXQUATERNION(0, 0, 0, 1);
		//Zb = Transform(D3DXVECTOR3(0, 0, 1), baseOrientation); //WHERE baseOrientation = [0, 0, 0, 1]
		//D3DXVec3Normalize(&Zb, &Zb);
		//D3DXVec3Normalize(&rotAxis, &rot);
		//D3DXVec3Cross(&r, &Zb, &rotAxis);
	
		//float angleOfRotation = asinf(D3DXVec3Length(&r));
	
		//D3DXVECTOR3 minAxisRotation;
		//minAxisRotation = r / angleOfRotation;
	
		//D3DXQUATERNION bInv, p;
		//D3DXQuaternionInverse(&bInv, &baseOrientation);	//Base orientation
	
		//p = D3DXQUATERNION(minAxisRotation.x*sinf(angleOfRotation/2),
		//					minAxisRotation.y*sinf(angleOfRotation/2),
		//					minAxisRotation.z*sinf(angleOfRotation/2),
		//					cosf(angleOfRotation/2));
		//				
		//D3DXQuaternionNormalize(&p, &p);
	
		//q = bInv * p;
	
		//if (sinf(angleOfRotation != 0))
		//{
		//	D3DXQuaternionNormalize(&q, &q);
		//	return q;
		//}
	
		//else
		//{
		//	D3DXVECTOR3 Zt = Transform(D3DXVECTOR3(0, 0, 1), q);
		//	D3DXVec3Normalize(&Zt, &Zt);
		//
		//	if (Zb == Zt)
		//	{
		//		return baseOrientation;
		//	}
		//	else
		//	{
		//		return -baseOrientation;
		//	}
		//}
	}
};
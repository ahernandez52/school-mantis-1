#pragma once

#include "GameplayObject.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"

class D3D9Object;

class Asteroid : public GameplayObject
{
public:

private:

public:

	Asteroid(){}
	Asteroid(Actor* owningActor) : GameplayObject(owningActor)
	{
		m_GameplayType = NO_TYPE;
	}

	~Asteroid(){}

	void Update(float dt) override {}
	
};
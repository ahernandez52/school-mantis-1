#include "../../FRAMEWORK/Objects/Actor.h"

GameplayObject::GameplayObject()
{
	m_GameplayType	= NO_TYPE;
	m_Faction			= UNDECLARED;

	m_Actor				= nullptr;
}

GameplayObject::GameplayObject(Actor* actor)
{
	m_GameplayType	= NO_TYPE;
	m_Faction			= UNDECLARED;

	m_Actor				= actor;
}

GameplayObject::~GameplayObject()
{

}

//====================
//	Set Functions
//====================

void GameplayObject::SetFaction(FACTION f)
{
	m_Faction = f;
}

void GameplayObject::SetGameplayType(int g)
{
	m_GameplayType = g;
}

//====================
//	Get Functions
//====================

Actor* GameplayObject::GetActor()
{
	return m_Actor;
}

FACTION GameplayObject::GetFaction()
{
	return m_Faction;
}

int GameplayObject::GetGameplayType()
{
	return m_GameplayType;
}
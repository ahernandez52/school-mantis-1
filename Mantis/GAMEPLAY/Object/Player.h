#pragma once
#include "Ship.h"
//#include "../Classes/Inventory/Inventory.h"

class QuestLog;
class SoundObject;

class Player : public Ship
{
private:

	//Actor	*m_Actor;


	
public:
	
	float			m_TotalExperience;
	float			m_CurrentExperience;
	float			m_ExperienceToLevel;

	int				m_Level;
	int				m_Credits;

	QuestLog		*m_QuestLog;
	//Inventory	   *m_Inventory;
	bool			m_Steering;

	Actor		   *m_Target;
	D3DXVECTOR3		ProjectedTarget;
	D3DXVECTOR3		m_CurrentVelocity;

	bool			m_boosting;
	bool			m_cruiseCharging;
	bool			m_cruising;
private:
	  
	void Input(float dt);
	void Clipping();
	void DidLevelUp();

	D3DXVECTOR3 m_Rotation;
	
public:

	Player(Actor *OwningActor);
	~Player();

	virtual void Update(float dt);

	void AddExperience(float value);

	void BoostON();
	void BoostOFF();
	void StartCruise();
	void StopCruise();

	Vector3 GetVelocity(void){return m_CurrentVelocity;}
};
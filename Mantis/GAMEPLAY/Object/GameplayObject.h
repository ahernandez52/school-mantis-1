#pragma once
#include "../../AI/AIObject/AIFaction.h"

/**	\brief Base GameplayObject class
 *	
 *	All gameplay objects derive from this base class.
 *	The class holds a pointer to it's owner, and has an identifier for what gameplay object it is and what faction it is under.
 */

class Actor;

class GameplayObject
{
protected:

	int			m_GameplayType;
	FACTION		m_Faction;

	Actor*		m_Actor;

public:

	GameplayObject();
	GameplayObject(Actor* actor);

	~GameplayObject();

	/**	
	 *	Pure virtual update method.
	 *	Derived classes must define their update method.
	 */
	virtual void	Update(float dt) = 0;

	/**	
	 *	Setter and getter functions.
	 */
	void			SetFaction(FACTION f);
	void			SetGameplayType(int g);

	Actor*			GetActor();

	FACTION			GetFaction();
	int				GetGameplayType();

	float		m_Radius;
};
#pragma once
#include "GameplayObject.h"
#include "../Classes/Weapons/WeaponSystem.h"

class SubModules;
class SoundObject;

class D3D9Object;

class Ship : public GameplayObject
{
public:

	float			m_MaxHull, m_CurrentHull;
	float			m_MaxShield, m_CurrentShield;

	float			m_ShieldRechargeRate, m_ShieldRechargeDelay;
	float			m_TimeOutOfCombat;
	bool			m_ShieldCharging;

	float			m_CurrentSpeed;
	float			m_MaxSpeed;
	float			m_Acceleration;
	
	WeaponSystem	m_Weapons;
	//SubModules		m_Modules;

	D3D9Object		*m_ShieldMesh;
	Vector3			m_ShieldImpactPoint;
	float			m_TotalShieldDecay;
	float			m_CurrentDecay;
	Vector4			m_ShieldColor;

private:


public:

	Ship(){}
	Ship(Actor* owningActor);

	~Ship();

	virtual void Update(float dt);

	//Weapon Systems
	//================================
	void RemoveWeapon(int position);

	bool EquipWeapon(int id, float cd, Vector3 offset);

	void Fire(D3DXVECTOR3 position, D3DXVECTOR3 direction);
	//********************************

	//Health / Shield
	//================================
	void ChargeShield(float dt);

	void TakeDamage(float amount);

	void HitShields(Vector3 impactPoint);
	void UpdateShields(float dt);

	bool IsDead();

	void SetHull(float hull);
	void SetShield(float shield);
	//================================
};
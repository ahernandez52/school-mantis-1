#include "Ship.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../../FRAMEWORK/Sound/Audio.h"

Ship::Ship(Actor* owningActor) : GameplayObject(owningActor)
{
	m_GameplayType = SHIP;

	m_Weapons = WeaponSystem(2);

	m_ShieldCharging = false;

	m_TotalShieldDecay = 1.0f;
	m_CurrentDecay = 1.0f;

	m_ShieldMesh = nullptr;
}


Ship::~Ship(void)
{
	if(m_ShieldMesh != nullptr)
		delete m_ShieldMesh;
}

void Ship::Update(float dt)
{
	ChargeShield(dt);

	UpdateShields(dt);

	m_Weapons.UpdateWeapons(dt);
}

bool Ship::EquipWeapon(int id, float cd, Vector3 offset)
{
	return m_Weapons.AddWeapon(id, cd, offset);
}

void Ship::RemoveWeapon(int position)
{
	m_Weapons.RemoveWeapon(position);
}

void Ship::Fire(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	if (m_Weapons.Fire(position, direction))
	{
  		SCI->PlaySample3D("Assets/Sounds/Effects/fire_gun6_rank1.wav", position);
	}
}

void Ship::ChargeShield(float dt)
{
	m_TimeOutOfCombat += dt;

	//If shield is full don't recharge
	if(m_CurrentShield == m_MaxShield)
	{
		return;
	}

	//If it's been out of combat for the required amount of time
	if(m_TimeOutOfCombat >= m_ShieldRechargeDelay)
	{
		m_CurrentShield += m_ShieldRechargeRate * dt;

		if (!m_ShieldCharging)
		{
			m_ShieldCharging = true;
			SCI->PlaySample3D("Assets/Sounds/Effects/shield_charge.mp3", m_Actor->m_Position);
		}

		//Be sure to clamp to Max shield
		if(m_CurrentShield > m_MaxShield)
		{
			m_CurrentShield = m_MaxShield;
		}
	}
}

void Ship::TakeDamage(float amount)
{
	//Set time since damage taken to zero
	m_TimeOutOfCombat = 0.0f;

	float shieldLoss = m_CurrentShield - amount;

	m_ShieldCharging = false;

	//Decrement Health if shield is empty
	if(shieldLoss < 0)
	{
		SCI->PlaySample3D("Assets/Sounds/Effects/detonation_ball02.wav", m_Actor->m_Position, 1.0f);
		m_CurrentShield = 0;
		m_CurrentHull -= abs(shieldLoss);
	}
	//Decrement Shield
	else
	{
		SCI->PlaySample3D("Assets/Sounds/Effects/shield.wav", m_Actor->m_Position);
		m_CurrentShield -= amount;
	}
}

void Ship::HitShields(Vector3 impactPoint)
{
	if(m_CurrentShield > 0.0f)
	{
		m_ShieldImpactPoint = impactPoint;
		m_CurrentDecay = 0.0f;
	}
}

void Ship::UpdateShields(float dt)
{
	if(m_CurrentDecay >= m_TotalShieldDecay) return;

	m_CurrentDecay += dt;

	float decayCoeff = m_CurrentDecay / m_TotalShieldDecay;

	m_ShieldMesh->SetShieldContact(m_ShieldImpactPoint, 15.0f, m_ShieldColor, 1 - decayCoeff);
}

bool Ship::IsDead()
{
	//Make sure you're dead
	return m_CurrentHull <= 0;
}

void Ship::SetHull(float hull)
{
	m_MaxHull = hull;
	m_CurrentHull = hull;
}

void Ship::SetShield(float shield)
{
	m_MaxShield = shield;
	m_CurrentShield = shield;
}
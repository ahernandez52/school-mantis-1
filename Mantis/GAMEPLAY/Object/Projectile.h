#pragma once
#include "GameplayObject.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"

class ParticleEmitter;

class Projectile : public GameplayObject
{
private:

	float		m_Speed;
	float		m_Damage;


	D3DXVECTOR3	m_Direction;
	
public:

	ParticleEmitter *m_Emitter;

	float		m_ActiveTime;
	float		m_MaxActiveTime;

	Projectile(Actor* owningActor);
	~Projectile();

	void Update(float dt);

	void SetInternals(float speed, 
					  float damage, 
					  D3DXVECTOR3 position,
					  D3DXVECTOR3 direction);

	float Damage();
};
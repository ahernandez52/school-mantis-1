#include "Projectile.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GRAPHICS/Particle/ParticleEmitter.h"


Projectile::Projectile(Actor* owningActor) : GameplayObject(owningActor)
{
	m_GameplayType = PROJECTILE;
	m_MaxActiveTime = 1.0f;
	m_ActiveTime = 0.0f;
}

Projectile::~Projectile()
{

}

//Private Functions
//=========================

//*************************


//Public Functions
//=========================
void Projectile::Update(float dt)
{
	m_Actor->m_Position += (m_Direction * m_Speed) * dt;
	m_Emitter->m_Particle_Pos = m_Actor->m_Position;

	m_ActiveTime += dt;
}

void Projectile::SetInternals(float speed, float damage, D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	m_Speed = speed;
	m_Damage = damage;

	m_Actor->m_Position = position;
	m_Direction = direction;
}

float Projectile::Damage()
{
	return m_Damage;
}
//*************************

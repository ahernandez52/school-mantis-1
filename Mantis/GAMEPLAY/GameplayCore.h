#pragma once
#include "Classes\Weapons\BattleSystem.h"
#include "Object\Player.h"

class D3D9GraphicsCore;
class WarpController;
class D3D9Object;
class QuestSystem;

struct ShieldImpact
{
	ShieldImpact(D3D9Object* impactedShields, Vector3 impact, float duration, float blastRadius)
	{
		m_ImpactedShields = impactedShields;
		m_Impact = impact;
		m_Duration = duration;
		m_BlastRadius = blastRadius;
		m_TimeLeft = duration;
	}
	D3D9Object *m_ImpactedShields;
	Vector3		m_Impact;
	float		m_Duration;
	float		m_TimeLeft;
	float		m_BlastRadius;
};

class GameplayCore
{
private:

	Player					   *m_Player;
	std::vector<Ship*>			m_NPCs;
	std::vector<ShieldImpact>	m_ShieldImpacts;

public:

	BattleSystem		*m_BattleSystem;
	WarpController		*m_WarpController;
	QuestSystem			*m_QuestSystem;

private:

	void InterpretContacts();

	void RegisterShieldImpact(D3D9Object* impactedShip, Vector3 impact, float duration, float blastRadius);
	void UpdateShieldImpacts(float dt);
	
public:

	GameplayCore();
	~GameplayCore();

	void				Startup();
	void				Shutdown();

	void				CleanUp();

	void				Update(std::vector<ActorContactPair> contactingPairs, float dt);
	
	void				InitPlayer();
	void				InitPlayerAudio();
	void				ReleasePlayerAudio();

	void				CreateNPC(Actor* actor, std::string type);
	void				RemoveNPC(Ship* npc);
	//void AddNPC(Ship* ship);

	Player*				GetPlayer();
	std::vector<Ship*>	GetNPCs();

	//==================
	void assignGraphics(ParticleManager *particleManager)
	{
		m_BattleSystem->m_Particles = particleManager;
	}
};
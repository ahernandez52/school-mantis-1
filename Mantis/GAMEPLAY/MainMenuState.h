#pragma once
#include "../FRAMEWORK/GSM/GameState.h"
#include "../GRAPHICS/Gui/MainMenu.h"
#include "../GRAPHICS/Gui/OptionsMenu.h"

class GameFramework;
class D3D9GraphicsCore;
class Actor;
class Formation;
class SoundObject;

struct patrolNode
{
	D3DXVECTOR3 pos;
};

class MainMenuState : public GameState
{
private:


private:

	Actor* sun;
	Actor* planet;
	Actor* station;

	Actor* Transport1;
	Actor* Transport2;
	Actor* Escort1;
	Actor* Escort2;
	Actor* Escort3;

	//Patrol nodes
	patrolNode A;
	patrolNode B;
	patrolNode C;
	patrolNode D;
	patrolNode E;
	patrolNode F;

public:


public:

	MainMenuState(){}
	~MainMenuState(){}

	void InitializeState();
	void LeaveState();
	void EnterState();
    void Update(float dt); 

	void NewGame();
	void Options();
	void QuitGame();

private:
	void SetUpShips(void);
	void UpdateShips(void);
	void BuildAsteroidField(void);
	void LoadSounds(void);
};


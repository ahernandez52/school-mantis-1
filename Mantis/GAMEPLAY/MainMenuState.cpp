#include "MainMenuState.h"
#include "FinalDemoState.h"
#include "LoadingState01.h"
#include "../FRAMEWORK/Managers/WorldManager.h"
#include "../FRAMEWORK/Utility/Camera.h"
#include "../FRAMEWORK/Input/RawInput.h"

#include "../AI/AIObject/ShipAI.h"
#include "../AI/Steering/Formation.h"
#include "../AI/Steering/SteeringBehavior.h"
#include "../GAMEPLAY/Object/Asteroid.h"

#include "../FRAMEWORK/Sound/Audio.h"

#include <chrono>
#include <random>

// Public Functions
//============================================

void MainMenuState::InitializeState()
{


	WMI->InitMemory();
	
	//Load audio
	LoadSounds();

	Graphics->SetSkyBoxRender(true);
	Graphics->SetGfxStatsRender(false);

	Framework->GetGui()->m_MainMenu->setVisible(true);
	Framework->GetGui()->m_Options->SetMainMenuFlag(true);

	GraphicSchematic graphic;
	graphic.fileName = "space_station_6";
	graphic.name = "station";
	WMI->RegisterGraphicSchematic(graphic);
	station = WMI->CreateActor("NULL", "station", "NULL", "NULL");

	station->Graphics->m_scale = D3DXVECTOR3(15, 15, 15);

	graphic.fileName = "planet_desert_1";
	graphic.name = "planet";
	WMI->RegisterGraphicSchematic(graphic);
	planet = WMI->CreateActor("NULL", "planet", "NULL", "NULL");

	graphic.fileName = "sun";
	graphic.name = "sun";
	WMI->RegisterGraphicSchematic(graphic);
	sun = WMI->CreateActor("NULL", "sun", "NULL", "NULL");

	graphic.fileName	= "Asteroid_1";
	graphic.name		= "Asteroid1";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_2";
	graphic.name		= "Asteroid2";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_3";
	graphic.name		= "Asteroid3";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_4";
	graphic.name		= "Asteroid4";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_5";
	graphic.name		= "Asteroid5";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "transport_3E";
	graphic.name		= "transport";
	WMI->RegisterGraphicSchematic(graphic);

	sun->m_Position = D3DXVECTOR3(-5000, 800, 5800);
	sun->Graphics->m_scale = D3DXVECTOR3(150, 150, 150);

	planet->Graphics->m_scale = D3DXVECTOR3(1200, 1200, 1200);
	planet->m_Position = D3DXVECTOR3(-2500.0f, 250.0f, -1000.0f);

	Transport1 = WMI->CreateActor("NULL", "transport", "NULL", "NULL");
	Transport1->m_Position = D3DXVECTOR3(200.0f, 400.0f, -600.0f);
	Transport1->Graphics->m_scale = D3DXVECTOR3(3.0f, 3.0f, 3.0f);
	D3DXQuaternionRotationYawPitchRoll(&Transport1->m_Orientation, D3DX_PI/2, 0.0f, 0.0f);

	Transport2 = WMI->CreateActor("NULL", "transport", "NULL", "NULL");
	Transport2->m_Position = D3DXVECTOR3(350.0f, 480.0f, -500.0f);
	Transport2->Graphics->m_scale = D3DXVECTOR3(3.0f, 3.0f, 3.0f);
	D3DXQuaternionRotationYawPitchRoll(&Transport2->m_Orientation, D3DX_PI/2, 0.0f, 0.0f);
	

	SetUpShips();

	gCamera->setMode(STANDSTILL);
	gCamera->setMode(FREEFORM);
	gCamera->lookAt(D3DXVECTOR3(1365.0f, 540.0f, -1240.0f), D3DXVECTOR3(-90.0f, 370.0f, -570.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));

	//Framework->GetGui()->m_MainMenu->setVisible(false);

	//Start music
	SCI->PlayMusic("Assets/Sounds/Music/background_01.mp3", true);

#ifdef _DEBUG
	//Do not build Asteroid Field in Debug!
#else
	BuildAsteroidField();
#endif
	
}

void MainMenuState::LeaveState()
{

	WMI->CleanUp();

	SCI->ReleaseSound("Assets/Sounds/Music/background_01.mp3");

	Framework->GetGui()->m_MainMenu->setVisible(false);
}

void MainMenuState::EnterState()
{
	//gCamera->pos().y = 20.0f;
}

void MainMenuState::Update(float dt)
{
	static float TE = 0.0f;
	TE += dt;

	//rotate planet
	static float planetRoation = 0.0f;
	float speed = 0.007f * dt;
	D3DXQuaternionRotationYawPitchRoll(&planet->m_Orientation, planetRoation, 0.0f, 0.0f);
	planetRoation += speed;

	station->m_Position = D3DXVECTOR3(0.0f, (cosf(TE)*5/3.0f), 0.0f);
	D3DXQuaternionRotationYawPitchRoll(&station->m_Orientation, cosf(TE)/500.0f + D3DX_PI, 0.0f, 0.0f);

	Transport1->m_Position.x += 5.0f * dt;
	Transport2->m_Position.x += 5.0f * dt;

	UpdateShips();

	if(gRawInput->keyPressed(VK_ESCAPE))
	{
		Framework->GetGui()->m_MainMenu->setVisible(true);
		Framework->GetGui()->m_Options->setVisible(false);
	}
}

//********************************************

void MainMenuState::NewGame()
{
	Framework->getGSM()->SetNextState(new FinalDemoState());
	
	Framework->getGSM()->LoadNextState();

}

void MainMenuState::Options()
{
	Framework->GetGui()->m_Options->setVisible(true);
	Framework->GetGui()->m_MainMenu->setVisible(false);

}

void MainMenuState::QuitGame()
{
	PostQuitMessage(0);
}

void MainMenuState::SetUpShips(void)
{
	//Temp constants for moving ships
	Vector3 OffsetOne(100, 50, 100);

	//Transport
	GraphicSchematic graphic;

	graphic.fileName = "transport_1E";
	graphic.name	 = "transport";
	WMI->RegisterGraphicSchematic(graphic);

	PhysicsSchematic physics;

	physics.Identifier = "heavy";
	physics.Mass	   = 10.0f;
	physics.Moveable   = true;

	WMI->RegisterPhysicsSchematic(physics);

	AISchematic ai;

	ai.Identifier = "transport";
	ai.Faction    = "PHALANX";
	ai.AIType     = "FRIGATE";

	WMI->RegisterAISchematic(ai);

	//Transport = WMI->CreateActor("transport", "transport", "heavy", "NULL");
	//Transport->Graphics->m_scale = D3DXVECTOR3(4, 4, 4);
	//Transport->m_Position = D3DXVECTOR3(-1440.0f, 300.0f, -525.0f);

	//Fighter escorts
	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "dark_fighter_3";	

	WMI->RegisterGraphicSchematic(graphic);

	physics.Identifier = "light";
	physics.Mass	   = 1.0f;
	physics.Moveable   = true;

	WMI->RegisterPhysicsSchematic(physics);

	ai.Identifier	   = "fighter";
	ai.Faction		   = "PHALANX";
	ai.AIType		   = "FIGHTER";

	WMI->RegisterAISchematic(ai);

	Escort1 = WMI->CreateActor("fighter", "fighter_1", "light", "NULL");
	Escort2 = WMI->CreateActor("fighter", "fighter_1", "light", "NULL");
	Escort3 = WMI->CreateActor("fighter", "fighter_1", "light", "NULL");


	//Patrol nodes
	A.pos = D3DXVECTOR3(-1150.0f, -300.0f, -600.0f);
	B.pos = D3DXVECTOR3(-1300.0f, 350.0f, 800.0f);
	C.pos = D3DXVECTOR3(-500.0f, -350.0f, 1450.0f);
	D.pos = D3DXVECTOR3(1300.0f, 350.0f, 750.0f);
	E.pos = D3DXVECTOR3(1300.0f, 500.0f, -1100.0f);
	F.pos = D3DXVECTOR3(-125.0f, 350.0f, -1500.0f);

	Escort1->m_Position = D.pos;
	Escort2->m_Position = D.pos - OffsetOne;
	Escort3->m_Position = D.pos + OffsetOne;

	((ShipAI*)Escort1->AI)->GetSteering()->SetTargetSeek(A.pos);

	//turn on Steering
	((ShipAI*)Escort1->AI)->GetSteering()->FaceForward3DOn();
	((ShipAI*)Escort1->AI)->GetSteering()->SeekOn();

	((ShipAI*)Escort2->AI)->GetSteering()->SetTargetSeek(A.pos - OffsetOne);

	//turn on Steering
	((ShipAI*)Escort2->AI)->GetSteering()->FaceForward3DOn();
	((ShipAI*)Escort2->AI)->GetSteering()->SeekOn();

	((ShipAI*)Escort3->AI)->GetSteering()->SetTargetSeek(A.pos + OffsetOne);

	//turn on Steering
	((ShipAI*)Escort3->AI)->GetSteering()->FaceForward3DOn();
	((ShipAI*)Escort3->AI)->GetSteering()->SeekOn();

	((ShipAI*)Escort3->AI)->GetSteering()->ObstacleAvoidanceOn();

}

void MainMenuState::UpdateShips(void)
{
	//Temp constants for moving ships
	Vector3 OffsetOne(100, 50, 100);

	//Location based target changes
	ShipAI* esc1 = (ShipAI*)Escort1->AI;

	D3DXVECTOR3 distance = esc1->GetSteering()->GetTargetSeek() - esc1->GetPosition();
	if (fabs(D3DXVec3Length(&distance)) < 75.0f)
	{
		if (esc1->GetSteering()->GetTargetSeek() == A.pos)
			esc1->GetSteering()->SetTargetSeek(E.pos);

		else if (esc1->GetSteering()->GetTargetSeek() == E.pos)
			esc1->GetSteering()->SetTargetSeek(D.pos);

		else if (esc1->GetSteering()->GetTargetSeek() == D.pos)
			esc1->GetSteering()->SetTargetSeek(C.pos);

		else if (esc1->GetSteering()->GetTargetSeek() == C.pos)
			esc1->GetSteering()->SetTargetSeek(B.pos);

		else if (esc1->GetSteering()->GetTargetSeek() == B.pos)
			esc1->GetSteering()->SetTargetSeek(A.pos);

		else if (esc1->GetSteering()->GetTargetSeek() == F.pos)
			esc1->GetSteering()->SetTargetSeek(A.pos);
	}

	//Location based target changes
	ShipAI* esc2 = (ShipAI*)Escort2->AI;

	distance = esc2->GetSteering()->GetTargetSeek() - esc2->GetPosition();
	if (fabs(D3DXVec3Length(&distance)) < 75.0f)
	{
		if (esc2->GetSteering()->GetTargetSeek() == A.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(E.pos - OffsetOne);

		else if (esc2->GetSteering()->GetTargetSeek() == E.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(D.pos - OffsetOne);

		else if (esc2->GetSteering()->GetTargetSeek() == D.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(C.pos - OffsetOne);

		else if (esc2->GetSteering()->GetTargetSeek() == C.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(B.pos - OffsetOne);

		else if (esc2->GetSteering()->GetTargetSeek() == B.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(A.pos - OffsetOne);

		else if (esc2->GetSteering()->GetTargetSeek() == F.pos - OffsetOne)
			esc2->GetSteering()->SetTargetSeek(A.pos - OffsetOne);
	}

	//Location based target changes
	ShipAI* esc3 = (ShipAI*)Escort3->AI;

	distance = esc3->GetSteering()->GetTargetSeek() - esc3->GetPosition();
	if (fabs(D3DXVec3Length(&distance)) < 75.0f)
	{
		if (esc3->GetSteering()->GetTargetSeek() == A.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(E.pos + OffsetOne);

		else if (esc3->GetSteering()->GetTargetSeek() == E.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(D.pos + OffsetOne);

		else if (esc3->GetSteering()->GetTargetSeek() == D.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(C.pos + OffsetOne);

		else if (esc3->GetSteering()->GetTargetSeek() == C.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(B.pos + OffsetOne);

		else if (esc3->GetSteering()->GetTargetSeek() == B.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(A.pos + OffsetOne);

		else if (esc3->GetSteering()->GetTargetSeek() == F.pos + OffsetOne)
			esc3->GetSteering()->SetTargetSeek(A.pos + OffsetOne);
	}
}

void MainMenuState::BuildAsteroidField()
{
	Actor *newActor;

	string Asteroids[] = {"Asteroid1",
						  "Asteroid2",
						  "Asteroid3",
						  "Asteroid4",
						  "Asteroid5"};

	//Generate Asteroid Field Positions
	//std::mt19937 rand((int)std::chrono::system_clock::now().time_since_epoch().count());
	std::mt19937 rand(654564);
	std::uniform_int_distribution<int> Placement(-150,150);
	std::uniform_int_distribution<int> asteroid(0,4);
	std::uniform_int_distribution<int> Scale(2,6);
	std::uniform_real_distribution<float> Rotation(0, 359);

	vector<Vector3> RandPositions;

	Vector3 center(-2500.0f, 250.0f, -1000.0f);
	float radius = 3000.0f;
	float distance = 0.0f;

	float x, y, z;

	float theta = 0.0f;
	float frequency = 0.09f;
	float spread = 250.0f;
	int density = 6;

	Vector3 ringPoint;
	Vector3 asteroidPoint;

	while(theta <= D3DX_PI * 2)
	{
		x = cos(theta) * 1450.0f;
		z = sin(theta) * 1450.0f;
		y = cos(theta) * 450.0f;

		ringPoint = Vector3(x, y, z);

		for(int i = 0; i < density; i++)
		{
			asteroidPoint = Vector3(Placement(rand), Placement(rand), Placement(rand));

			RandPositions.push_back(asteroidPoint + ringPoint + center);
		}

		theta += frequency;
	}

	//Assign remaining position to actors
	float scale;
	Quaternion newRotation;

	for(auto position : RandPositions)
	{
		newActor = WMI->CreateActor("NULL", Asteroids[asteroid(rand)], "NULL", "NULL");

		scale = (float)Scale(rand);
		newRotation.BuildRotYawPitchRoll(Rotation(rand), Rotation(rand), Rotation(rand));

		newActor->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);
		newActor->m_Orientation = newRotation;
		newActor->m_Position = position;

		newActor->Gameplay = new Asteroid(newActor);
	}
}

void MainMenuState::LoadSounds(void)
{
	//SCI->LoadMusic("Assets/Sounds/Music/background.mp3", true);
	SCI->LoadMusic("Assets/Sounds/Music/background_01.mp3", true);
}
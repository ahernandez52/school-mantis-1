#include "TriggerVolumes.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include <algorithm>

TriggerVolumes::TriggerVolumes():GameplayObject()
{
	m_Name = "null";
	m_TriggerType=TT_NoType;
	m_Shape=TS_NoShape;
	m_Radius = 0.0f;
	m_HalfSizes = D3DXVECTOR3(0.0f,0.0f,0.0f);
	m_Effect = [](Actor* actr){};
}
TriggerVolumes::TriggerVolumes(Actor* actr,std::string name):GameplayObject(actr)
{
	m_Name = name;
	m_TriggerType=TT_NoType;
	m_Shape=TS_NoShape;
	m_Radius = 0.0f;
	m_HalfSizes = D3DXVECTOR3(0.0f,0.0f,0.0f);
	m_Effect = [](Actor* actr){};
}
TriggerVolumes::~TriggerVolumes()
{
	m_TriggerRegistry.clear();
}

bool TriggerVolumes::IsInsideVolume(D3DXVECTOR3& pos)
{
	switch (m_Shape)
	{
	case TS_NoShape:
		{
			return false;
			break;
		}
	case TS_Sphere:
		{
			D3DXVECTOR3 temp = this->GetActor()->m_Position - pos;
			return D3DXVec3LengthSq(&temp)<(m_Radius*m_Radius);
			break;
		}
	case TS_AABB:
		{
			float minX,maxX,minY,maxY,minZ,maxZ;
			minX=this->GetActor()->m_Position.x-m_HalfSizes.x;
			maxX=this->GetActor()->m_Position.x+m_HalfSizes.x;
			minY=this->GetActor()->m_Position.y-m_HalfSizes.y;
			maxY=this->GetActor()->m_Position.y+m_HalfSizes.y;
			minZ=this->GetActor()->m_Position.z-m_HalfSizes.z;
			maxZ=this->GetActor()->m_Position.z+m_HalfSizes.z;
			if(pos.x<minX||pos.x>maxX)
			{
				return false;
				break;
			}
			if (pos.y<minY||pos.y>maxY)
			{
				return false;
				break;
			}
			if (pos.z<minZ||pos.z>maxZ)
			{
				return false;
				break;
			}
			return true;
			break;
		}
	default:
		return false;
		break;
	}
}
void TriggerVolumes::TakeSchematic(TriggerSchematic schematic)
{
	m_Name = schematic.schematicName;
	m_TriggerType=schematic.triggerType;
	m_Shape=schematic.shape;
	switch(m_Shape)
	{
	case TS_NoShape:
		{
			break;
		}
	case TS_Sphere:
		{
			m_Radius=schematic.radius;
			break;
		}
	case  TS_AABB:
		{
			m_HalfSizes=schematic.halfSizes;
			break;
		}
	default:
		{
			break;
		}
	}
}
void TriggerVolumes::SetEffect(std::function<void (Actor*)> effect)
{
	m_Effect = effect;
}
void TriggerVolumes::Update(Actor* Acter)
{
	switch (m_TriggerType)
	{
	case TT_NoType:
		{
			break;
		}
	case TT_OnceLifeTime:
		{
			if(IsInsideVolume(Acter->m_Position))
			{
				auto it= std::find(begin(m_TriggerRegistry),end(m_TriggerRegistry),Acter);
				if(it==end(m_TriggerRegistry))
				{
					m_Effect(Acter);
					m_TriggerRegistry.push_back(Acter);
					break;
				}
				break;
			}
			break;
		}
	case TT_OncePerEntry:
		{
			if(IsInsideVolume(Acter->m_Position))
			{
				auto it= std::find(begin(m_TriggerRegistry),end(m_TriggerRegistry),Acter);
				if(it==end(m_TriggerRegistry))
				{
					m_Effect(Acter);
					m_TriggerRegistry.push_back(Acter);
					break;
				}
				break;
			}
			else
			{
				auto it= std::find(begin(m_TriggerRegistry),end(m_TriggerRegistry),Acter);
				if(it!=end(m_TriggerRegistry))
				{
					std::vector<Actor*> temp;
					for (auto actr : m_TriggerRegistry)
					{
						if (actr!=Acter)
						{
							temp.push_back(actr);
						}
					}
					m_TriggerRegistry.clear();
					m_TriggerRegistry=temp;
					break;
				}
				break;
			}
		}
	case TT_Continuous:
		{
			if(IsInsideVolume(Acter->m_Position))
			{
				m_Effect(Acter);
				m_TriggerRegistry.push_back(Acter);
				break;
			}
			break;
		}
	default:
		{
			break;
		}		
	}
}

TriggerVolumeRegistry::TriggerVolumeRegistry()
{

}
TriggerVolumeRegistry::~TriggerVolumeRegistry()
{
	ClearAllRegistries();
}
void TriggerVolumeRegistry::UpdateSingleEntity(Actor* acter)
{
	for(auto reg : m_TriggerVolumeRegistry)
	{
		reg.second->Update(acter);
	}
}
void TriggerVolumeRegistry::UpdateVectorOfEntities(std::vector<Actor*>& acters)
{
	for(auto reg : m_TriggerVolumeRegistry)
	{
		for(auto actr : acters)
		{
			reg.second->Update(actr);
		}
	}
}
void TriggerVolumeRegistry::ClearSchematics()
{
	m_TriggerVolumeSchematicRegistry.clear();
}
void TriggerVolumeRegistry::ClearVolumes()
{
	for (auto reg :m_TriggerVolumeRegistry)
	{
		delete reg.second;
		reg.second=nullptr;
	}
	m_TriggerVolumeRegistry.clear();
}
void TriggerVolumeRegistry::ClearAllRegistries()
{
	ClearSchematics();
	ClearVolumes();
}
TriggerVolumes* TriggerVolumeRegistry::RegisterNewTriggerVolume(std::string name,TriggerSchematic schem,Actor* actr,std::function<void (Actor*)> effect)
{
	auto ein = m_TriggerVolumeRegistry.find(name);
	if (ein!=end(m_TriggerVolumeRegistry))
	{
		return ein->second;
	}

	auto it = m_TriggerVolumeSchematicRegistry.find(schem.schematicName);
	if (it==end(m_TriggerVolumeSchematicRegistry))
	{
		m_TriggerVolumeSchematicRegistry.insert(std::make_pair(schem.schematicName,schem));
		TriggerVolumes* v = new TriggerVolumes(actr,name);
		v->TakeSchematic(schem);
		v->SetEffect(effect);
		m_TriggerVolumeRegistry.insert(std::make_pair(name,v));
		return v;
	}
	else
	{
		TriggerVolumes* v = new TriggerVolumes(actr,name);
		v->TakeSchematic(it->second);
		v->SetEffect(effect);
		m_TriggerVolumeRegistry.insert(std::make_pair(name,v));
		return v;
	}
}
TriggerVolumes* TriggerVolumeRegistry::GetVolumeByName(std::string name)
{
	auto it = m_TriggerVolumeRegistry.find(name);
	if (it==end(m_TriggerVolumeRegistry))
	{
		return nullptr;
	}
	else
	{
		return it->second;
	}
}
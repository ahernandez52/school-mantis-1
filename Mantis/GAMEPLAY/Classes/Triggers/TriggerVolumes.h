#pragma once

#include <functional>
#include "../../../FRAMEWORK/Utility/MantisUtil.h"
#include "../../Object/GameplayObject.h"

enum TriggerShape
{
	TS_NoShape=-1,
	TS_Sphere,
	TS_AABB,
};

enum TriggerType
{
	TT_NoType=-1,
	TT_OnceLifeTime,
	TT_OncePerEntry,
	TT_Continuous,
};

struct TriggerSchematic{
	std::string		schematicName;
	int				triggerType;
	int				shape;
	float			radius;
	D3DXVECTOR3		halfSizes;
};

class TriggerVolumes : public GameplayObject
{
private:

	std::string						m_Name;
	int								m_TriggerType;
	int								m_Shape;
	float							m_Radius;
	D3DXVECTOR3						m_HalfSizes;
	std::function<void (Actor*)>	m_Effect;
	std::vector<Actor*>				m_TriggerRegistry;

private:

	bool IsInsideVolume(D3DXVECTOR3& pos);

public:

	TriggerVolumes();
	TriggerVolumes(Actor*,std::string);
	~TriggerVolumes();

	void Update(float dt){} // do nothing

	void TakeSchematic(TriggerSchematic schematic);
	void SetEffect(std::function<void (Actor*)>);
	void Update(Actor* Acter);

};

class TriggerVolumeRegistry
{
private:

	std::map<std::string,TriggerSchematic>	m_TriggerVolumeSchematicRegistry;
	std::map<std::string,TriggerVolumes*>	m_TriggerVolumeRegistry;

public:

	TriggerVolumeRegistry();
	~TriggerVolumeRegistry();

	void UpdateSingleEntity(Actor* acter);
	void UpdateVectorOfEntities(std::vector<Actor*>& acters);
	void ClearSchematics();
	void ClearVolumes();
	void ClearAllRegistries();

	TriggerVolumes* RegisterNewTriggerVolume(std::string,TriggerSchematic,Actor*,std::function<void (Actor*)>);
	TriggerVolumes* GetVolumeByName(std::string);
};

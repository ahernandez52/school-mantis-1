#include "WarpPortal.h"
#include "../../../FRAMEWORK/Objects/Actor.h"
#include "../../../AI/Utility/AIUtility.h"
#include "../../../FRAMEWORK/Managers/WorldManager.h"

#include <random>
#include <chrono>

WarpPortal::WarpPortal() : GameplayObject()
{
	//Parent variables

	m_GameplayType = WARP_PORTAL;
	

	//This object's variables

	m_Active = false;
	m_Unidirectional = true;

	m_Speed = 0;
	m_Strength = 0;
	m_TriggerRadius = 0;

	m_Direction = D3DXVECTOR3(0.0f,1.0f,0.0f);

	m_ActorsInTransit.clear();

	m_SpinMesh = true;
	m_SpinSpeed = D3DX_PI / 128;

	std::mt19937 rand((int)std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<float> U(0.0f, 2 * D3DX_PI);

	D3DXQUATERNION spin;
	D3DXQuaternionRotationAxis(&spin, &GetDirection(), U(rand));
	m_Actor->m_Orientation *= spin;
}

WarpPortal::WarpPortal(Actor* actor) : GameplayObject(actor)
{
	//Parent variables

	m_GameplayType = WARP_PORTAL;

	//This object's variables

	m_Active = false;
	m_Unidirectional = true;

	m_Speed = 0;
	m_Strength = 0;
	m_TriggerRadius = 0;

	m_Direction = D3DXVECTOR3(0.0f,1.0f,0.0f);

	m_ActorsInTransit.clear();

	m_SpinMesh = true;
	m_SpinSpeed = D3DX_PI / 128;

	std::mt19937 rand((int)std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<float> U(0.0f, 2 * D3DX_PI);

	D3DXQUATERNION spin;
	D3DXQuaternionRotationAxis(&spin, &GetDirection(), U(rand));
	m_Actor->m_Orientation *= spin;
}

WarpPortal::~WarpPortal()
{
	m_ActorsInTransit.clear();
}

//====================
//	Public Member Functions
//====================

void WarpPortal::Update(float dt)
{
	//Is the portal allowed to spin?
	if( IsSpinning() )
	{
		SpinMesh(dt);
	}

	//if the portal is not active, leave function
	if(! IsActive() )
	{
		return;
	}

	//check for actors near the portal
	CheckActors();

	//throw any currently in transit actors
	ThrowActors(dt);
	
	//drop actors from the transit vector if they get too far
	DropActors();
}

void WarpPortal::AttachActor(Actor* actor)
{
	m_ActorsInTransit.push_back(actor);
}

void WarpPortal::ReleaseActor(Actor* actor)
{
	for(std::vector<Actor*>::iterator it = m_ActorsInTransit.begin(); it != m_ActorsInTransit.end(); ++it)
	{
		if( *it = actor )
		{
			m_ActorsInTransit.erase(it);
			return;
		}
	}
}

//====================
//	Private Member Functions
//====================

inline Actor* __GetClosestShip(D3DXVECTOR3 point)
{
	std::vector<Actor*> ships = WMI->GetActors();

	if(ships.size() == 0)
	{
		return nullptr;
	}

	Actor* closestShip = ships.front();

	for(auto ship : ships)
	{
		if(ship->Gameplay == nullptr)
		{
			continue;
		}

		if(ship->Gameplay->GetGameplayType() == PLAYER || ship->Gameplay->GetGameplayType() == NPC)
		{
			if( D3DXVec3LengthSq(&( closestShip->m_Position - point )) > D3DXVec3LengthSq(&( ship->m_Position - point )) )
			{
				closestShip = ship;
			}
		}
	}

	return closestShip;
}

void WarpPortal::CheckActors()
{
	Actor* a = __GetClosestShip(m_Actor->m_Position);

	//if there are no ships
	if(a == nullptr)
	{
		return;
	}

	//if the distance between the actor and warp portal is too far
	if(D3DXVec3LengthSq(&(a->m_Position - m_Actor->m_Position)) > m_TriggerRadius * m_TriggerRadius)
	{
		return;
	}
	
	//make sure the ship is facing the correct direction if the portal is unidirectional
	if(IsOneWay())
	{
		Quaternion quat = a->m_Orientation;
		D3DXVECTOR3 shipHeading = quat.Forward();
		//D3DXVec3Normalize(&shipHeading, &QuatForwardVec(a->m_Orientation));
		//D3DXVec3Normalize(&shipHeading, &a->Physics->m_LinearVelocity);

		//if the ship is not facing the correct direction
		if(D3DXVec3Dot(&m_Direction, &shipHeading) < 0)
		{
			return;
		}
	}

	//make sure we're not adding a ship twice
	for(Actor* ship : m_ActorsInTransit)
	{
		if(ship == a)
		{
			return;
		}
	}

	//finally add the ship
	AttachActor(a);
}

void WarpPortal::ThrowActors(float dt)
{
	for(Actor* a : m_ActorsInTransit)
	{
		//static
		a->m_Position += (m_Direction * m_Speed) * dt;

		//physics
		//a->Physics->AddForce((m_Direction * m_Speed) * dt);
	}
}

void WarpPortal::DropActors()
{
	for(unsigned i = 0; i < m_ActorsInTransit.size(); ++i)
	{
		if(D3DXVec3LengthSq(&(m_ActorsInTransit[i]->m_Position - m_Actor->m_Position)) >= m_Strength * m_Strength)
		{
			m_ActorsInTransit.erase(m_ActorsInTransit.begin() + i);
		}
	}
}

void WarpPortal::SpinMesh(float dt)
{
	D3DXQUATERNION spin;
	D3DXQuaternionRotationAxis(&spin, &GetDirection(), m_SpinSpeed * dt);
	m_Actor->m_Orientation *= spin;
}

//====================
//	Set Utility Functions
//====================

void WarpPortal::SetActive(bool b)
{
	m_Active = b;
}

void WarpPortal::SetOneWay(bool b)
{
	m_Unidirectional = b;
}

void WarpPortal::SetSpeed(float speed)
{
	m_Speed = speed;
}

void WarpPortal::SetStrength(float strength)
{
	m_Strength = strength;
}

void WarpPortal::SetTriggerRadius(float radius)
{
	m_TriggerRadius = radius;
}

void WarpPortal::SetDirection(D3DXVECTOR3 dir)
{
	D3DXVec3Normalize(&m_Direction, &dir);

	float angle;
	D3DXVECTOR3 origin(0.0f,1.0f,0.0f), cross;
	D3DXQUATERNION q;

	D3DXVec3Cross(&cross, &origin, &m_Direction);
	angle = asin(D3DXVec3Length(&cross));

	D3DXQuaternionRotationAxis(&q, &cross, angle);
	m_Actor->m_Orientation = q;

	/*D3DXQUATERNION q90;

	D3DXQuaternionRotationAxis(&q90, &m_Direction, D3DX_PI / 2);
	m_Actor->m_Orientation *= q90;*/
}

void WarpPortal::SetDirection(float x, float y, float z)
{
	D3DXVec3Normalize(&m_Direction, &D3DXVECTOR3(x,y,z));

	float angle;
	D3DXVECTOR3 origin(0.0f,1.0f,0.0f), cross;
	D3DXQUATERNION q;

	D3DXVec3Cross(&cross, &origin, &m_Direction);
	angle = asin(D3DXVec3Length(&cross));

	D3DXQuaternionRotationAxis(&q, &cross, angle);
	m_Actor->m_Orientation = q;

	/*D3DXQUATERNION q90;

	D3DXQuaternionRotationAxis(&q90, &m_Direction, D3DX_PI / 2);
	m_Actor->m_Orientation *= q90;*/
}

void WarpPortal::SetSpin(bool b)
{
	m_SpinMesh = b;
}

void WarpPortal::SetSpinSpeed(float spinSpeed)
{
	m_SpinSpeed = spinSpeed;
}

//====================
//	Get Utility Functions
//====================

bool WarpPortal::IsActive()
{
	return m_Active;
}

bool WarpPortal::IsOneWay()
{
	return m_Unidirectional;
}

int WarpPortal::GetID()
{
	return m_Actor->ID;
}

float WarpPortal::GetSpeed()
{
	return m_Speed;
}

float WarpPortal::GetStrength()
{
	return m_Strength;
}

float WarpPortal::GetTriggerRadius()
{
	return m_TriggerRadius;
}

D3DXVECTOR3 WarpPortal::GetDirection()
{
	//make sure the direction is normalized before returning
	D3DXVec3Normalize(&m_Direction, &m_Direction);
	return m_Direction;
}

std::vector<Actor*> WarpPortal::GetActorsInTransit()
{
	return m_ActorsInTransit;
}

bool WarpPortal::IsSpinning()
{
	return m_SpinMesh;
}

float WarpPortal::GetSpinSpeed()
{
	return m_SpinSpeed;
}

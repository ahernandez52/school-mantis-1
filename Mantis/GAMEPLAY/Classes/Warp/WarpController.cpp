#include "WarpController.h"
#include "../../../FRAMEWORK/Managers/WorldManager.h"

WarpController::WarpController()
{
}

WarpController::~WarpController()
{
	for(WarpPortal* portal : m_Portals)
	{
		WarpPortal* temp = portal;
		portal = nullptr;
		delete temp;
	}

	m_Portals.clear();

	for(WarpLane* lane : m_Lanes)
	{
		WarpLane* temp = lane;
		lane = nullptr;
		delete temp;
	}

	m_Lanes.clear();
}

void WarpController::Update(float dt)
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->Update(dt);
	}

	for(WarpLane* lane : m_Lanes)
	{
		lane->Update(dt);
	}
}

void WarpController::CreateWarpLane(D3DXVECTOR3 start, D3DXVECTOR3 finish)
{
	WarpLane* lane = new WarpLane(start, finish);

	m_Lanes.push_back(lane);
}

void WarpController::CreateWarpLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd)
{
	WarpLane* lane = new WarpLane(xStart, yStart, zStart, xEnd, yEnd, zEnd);

	m_Lanes.push_back(lane);
}

void WarpController::CreateWarpPortal(D3DXVECTOR3 pos, D3DXVECTOR3 dir, float strength, float speed)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void WarpController::CreateWarpPortal(float x, float y, float z, D3DXVECTOR3 dir, float strength, float speed)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void WarpController::CreateWarpPortalSystem(D3DXVECTOR3 pos, D3DXVECTOR3 dir, float strength, float speed)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalSystem";
	graphic.fileName	= "space_portal_2";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void WarpController::CreateWarpPortalSystem(float x, float y, float z, D3DXVECTOR3 dir, float strength, float speed)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalSystem";
	graphic.fileName	= "space_portal_2";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

bool WarpController::ActivatePortal(int ID)
{
	//search the stray portals
	for(WarpPortal* portal : m_Portals)
	{
		if(portal->GetID() == ID)
		{
			portal->SetActive(true);
			return true;
		}
	}

	//search the lanes
	for(WarpLane* lane : m_Lanes)
	{
		if(lane->ActivatePortal(ID))
		{
			return true;
		}
	}

	return false;
}

void WarpController::ActivateALLPortals()
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->SetActive(true);
	}

	for(WarpLane* lane : m_Lanes)
	{
		lane->ActivateALLPortals();
	}
}

bool WarpController::DeActivatePortal(int ID)
{
	//search the stray portals
	for(WarpPortal* portal : m_Portals)
	{
		if(portal->GetID() == ID)
		{
			portal->SetActive(false);
			return true;
		}
	}

	//search the lanes
	for(WarpLane* lane : m_Lanes)
	{
		if(lane->DeactivatePortal(ID))
		{
			return true;
		}
	}

	return false;
}

void WarpController::DeActivateALLPortals()
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->SetActive(false);
	}

	for(WarpLane* lane : m_Lanes)
	{
		lane->DeactivateALLPortals();
	}
}
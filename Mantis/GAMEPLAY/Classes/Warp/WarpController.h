#pragma once

#include "WarpLane.h"

/**	\brief The WarpController Class handles all the WarpLane objects and stray WarpPortal objects.
 *	
 *	The WarpController class updates all Lanes and stray Portals that are not part of a Lane.
 */

class WarpController
{
public:
	//public member variables

private:
	//private member variables

	std::vector<WarpPortal*> m_Portals;
	std::vector<WarpLane*> m_Lanes;

public:
	//public member functions

	WarpController();

	~WarpController();

	/**	
	 *	Updates all the Lanes and stay Portals not in a Lane.
	 */
	void Update(float dt);

	/**	
	 *	Creates a WarpLane from one point to another.
	 */
	void CreateWarpLane(D3DXVECTOR3 start, D3DXVECTOR3 finish);
	void CreateWarpLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd);

	/**	
	 *	Creates a stray WarpPortal object at position with given direction, strength, and speed.
	 */
	void CreateWarpPortal(D3DXVECTOR3 pos, D3DXVECTOR3 dir, float strength, float speed);
	void CreateWarpPortal(float x, float y, float z, D3DXVECTOR3 dir, float strength, float speed);

	/**	
	 *	Creates a System portal that is intended to be a gateway between levels. (NYI)
	 */
	void CreateWarpPortalSystem(D3DXVECTOR3 pos, D3DXVECTOR3 dir, float strength, float speed);
	void CreateWarpPortalSystem(float x, float y, float z, D3DXVECTOR3 dir, float strength, float speed);

private:
	//private member functions

public:
	/**	
	 *	Utility functions.
	 */

	bool ActivatePortal(int ID);
	void ActivateALLPortals();

	bool DeActivatePortal(int ID);
	void DeActivateALLPortals();

};
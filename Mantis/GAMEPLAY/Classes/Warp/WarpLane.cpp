#include "WarpLane.h"
#include "../../../FRAMEWORK/Managers/WorldManager.h"

WarpLane::WarpLane()
{
}

WarpLane::WarpLane(D3DXVECTOR3 start, D3DXVECTOR3 end)
{
	FillLane(start, end);
}

WarpLane::WarpLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd)
{
	FillLane(xStart, yStart, zStart, xEnd, yEnd, zEnd);
}

WarpLane::~WarpLane()
{
	for(WarpPortal* portal : m_Portals)
	{
		WarpPortal* temp = portal;
		portal = nullptr;
		delete temp;
	}

	m_Portals.clear();
}

void WarpLane::Update(float dt)
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->Update(dt);
	}
}

bool WarpLane::ActivatePortal(int ID)
{
	for(WarpPortal* portal : m_Portals)
	{
		if(portal->GetID() == ID)
		{
			portal->SetActive(true);
			return true;
		}
	}
	return false;
}

void WarpLane::ActivateALLPortals()
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->SetActive(true);
	}
}

bool WarpLane::DeactivatePortal(int ID)
{
	for(WarpPortal* portal : m_Portals)
	{
		if(portal->GetID() == ID)
		{
			portal->SetActive(false);
			return true;
		}
	}
	return false;
}

void WarpLane::DeactivateALLPortals()
{
	for(WarpPortal* portal : m_Portals)
	{
		portal->SetActive(false);
	}
}

void WarpLane::FillLane(D3DXVECTOR3 start, D3DXVECTOR3 end)
{
	D3DXVECTOR3 dir = end - start;
	float dist = D3DXVec3Length(&dir);
	float maxPortalDist = 10000.0f;
	float maxSpeed = 2500.0f;
	float travelDist = 0;

	//get the number of portals between the end nodes
	int numPortals = (int)(dist / maxPortalDist);

	//if it's 0 make sure we don't divide by zero
	if(numPortals == 0)
	{
		travelDist = dist;
	}
	else
	{
		travelDist = dist / numPortals;
	}
	D3DXVec3Normalize(&dir, &dir);
	D3DXVECTOR3 newPos = start;

	CreatePortalAnchor(start, travelDist, maxSpeed, dir);

	for(int x = 0; x < numPortals; ++x)
	{
		newPos += dir * travelDist;
		CreatePortalConnector(newPos, travelDist, maxSpeed, dir);
	}

	CreatePortalAnchor(end, 750, 100, dir);
}

void WarpLane::FillLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd)
{
	D3DXVECTOR3 start = D3DXVECTOR3(xStart, yStart, zStart);
	D3DXVECTOR3 end = D3DXVECTOR3(xEnd, yEnd, zEnd);

	FillLane(start, end);
}

WarpPortal* WarpLane::CreatePortalAnchor(D3DXVECTOR3 pos, float strength, float speed, D3DXVECTOR3 dir)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);

	return w;
}

WarpPortal* WarpLane::CreatePortalConnector(D3DXVECTOR3 pos, float strength, float speed, D3DXVECTOR3 dir)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalConnector";
	graphic.fileName =	"space_portal_1";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalConnector", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	m_Portals.push_back(w);

	w->SetActive(true);

	w->SetDirection(dir);
	w->SetStrength(strength);
	w->SetSpeed(speed);

	w->SetSpin(true);

	float scale = 60.0f / 7.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 7.5f);

	return w;
}
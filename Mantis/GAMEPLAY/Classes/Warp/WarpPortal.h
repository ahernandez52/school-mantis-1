#pragma once

#include "../../Object/GameplayObject.h"
#include "../../../FRAMEWORK/Utility/MantisUtil.h"


/**	\brief WarpPortal Class
 *	
 *	The WarpPortal class derives from the GameplayObject class.
 *	It is designed to "throw" an actor a certain distance as a certain speed in one direction.
 */

class WarpPortal : public GameplayObject
{
private:
	//private member variables

	bool m_Active;
	bool m_Unidirectional;

	float m_Speed;
	float m_Strength;
	float m_TriggerRadius;

	D3DXVECTOR3 m_Direction;
	
	std::vector<Actor*> m_ActorsInTransit;


	//variables used to spin the mesh
	bool m_SpinMesh;
	float m_SpinSpeed;

public:
	//public member functions

	WarpPortal();
	WarpPortal(Actor* actor);

	~WarpPortal();

	/**	\brief Warp Portal Update Method
	 *	
	 *	The Update function handles "throwing" any actors currently warping. It also handles spinning the mesh.
	 */
	void Update(float dt) override;

	/**	
	 *	Add the specified actor to the list of actors being warped.
	 *	Does not "double-up" on actors if the actor is already in the list.
	 */
	void AttachActor(Actor* actor);

	/**	
	 *	Removes the specified actor from the list of actors being warped.
	 */
	void ReleaseActor(Actor* actor);

private:
	//private member functions

	/**	\brief Checks actors in the Update method.
	 *	
	 *	Goes through the current ships out on the field and checks to see if they need to be added to the warping list.
	 */
	void CheckActors();

	/**	\brief Throws actors in warp in the Update method.
	 *	
	 *	Iterates through the list of actors currently in warp and applies a static position change to the actor.
	 */
	void ThrowActors(float dt);

	/**	\brief Checks for out of range actors in the Update method.
	 *	
	 *	Removes any actors that are out of range of the warp portal from the warp list.
	 */
	void DropActors();

	/**	\brief Spins the mesh.
	 *	
	 *	Helper function that spins the mesh.
	 */
	void SpinMesh(float dt);

public:

	/**	
	 *	Setter Utility Functions
	 */
	void SetActive(bool b);
	void SetOneWay(bool b);

	void SetSpeed(float speed);
	void SetStrength(float strength);
	void SetTriggerRadius(float radius);

	void SetDirection(D3DXVECTOR3 dir);
	void SetDirection(float x, float y, float z);

	void SetSpin(bool b);
	void SetSpinSpeed(float spinSpeed);

	/**	
	 *	Getter Utility Functions
	 */
	bool IsActive();
	bool IsOneWay();

	int GetID();

	float GetSpeed();
	float GetStrength();
	float GetTriggerRadius();

	D3DXVECTOR3 GetDirection();

	std::vector<Actor*> GetActorsInTransit();

	bool IsSpinning();
	float GetSpinSpeed();

};
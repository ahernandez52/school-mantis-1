#pragma once

#include "WarpPortal.h"

/**	\brief WarpLane Class
 *	
 *	The WarpLane class manages all the WarpPortal objects in a lane.
 *	Lanes are simply a list of linked portals.
 */

class WarpLane
{
private:
	//private member variables

	std::vector<WarpPortal*> m_Portals;

public:
	//public member functions

	D3DXVECTOR3 m_StartPoint;
	D3DXVECTOR3 m_EndPoint;

public:
	//public member functions

	WarpLane();
	WarpLane(D3DXVECTOR3 start, D3DXVECTOR3 end);
	WarpLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd);

	~WarpLane();

	/**	
	 *	Updates all the warp portals in the lane.
	 */
	void Update(float dt);

	/**	
	 *	Activates the specified warp portal.
	 */
	bool ActivatePortal(int ID);

	/**	
	 *	Activates all portals in the lane.
	 */
	void ActivateALLPortals();

	/**	
	 *	Deactivates the specified warp portal.
	 */
	bool DeactivatePortal(int ID);

	/**	
	 *	Deactivates all portals in the lane.
	 */
	void DeactivateALLPortals();

private:
	//private member functions

	/**	\brief Helper function for the constructor.
	 *	
	 *	FillLane method creates WarpPortal objects and places them equidistant from each other based on a base WarpPortal strength.
	 *	Will only create as many WarpPortal objects as necessary to get from one point to another.
	 */
	void FillLane(D3DXVECTOR3 start, D3DXVECTOR3 end);
	void FillLane(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd);

	/**	\brief Helper function that creates an Anchor portal for the FillLane method.
	 *	
	 *	CreatePortalAnchor creates an "Anchor" WarpPortal object for the beginning and end of a lane.
	 */
	WarpPortal* CreatePortalAnchor(D3DXVECTOR3 pos, float strength, float speed, D3DXVECTOR3 dir);

	/**	\brief Helper function that creates a Connector portal for the FillLane method.
	 *	
	 *	CreatePortalConnector creates a "Connector" WarpPortal object for the mid section of a lane.
	 */
	WarpPortal* CreatePortalConnector(D3DXVECTOR3 pos, float strength, float speed, D3DXVECTOR3 dir);

};
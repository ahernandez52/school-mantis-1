#pragma once
#include <vector>
#include <map>
#include <string>
#include <list>
#include "../../Object/Projectile.h"

class Actor;

class ParticleManager;

struct ProjectileDef
{
	float			m_Speed;
	float			m_Damage;
	std::string		m_Name;
	std::string		m_FireSound;
	std::string		m_HitSound;
};

class BattleSystem
{
private:

	std::map<std::string, ProjectileDef>	m_ProjectileDefinitions;

	std::vector<Actor*>						m_InactiveProjectiles;
	//std::list<Projectile*>					m_ActiveProjectiles;
	

	float									m_DefragFrequency;

	int										m_StorageIncrement;
	int										m_Increments;
	
	bool									m_Activity;

private:

	void DefragProjectiles(float dt);
	void ExpandMemory();
	void PrimeBattleSystem();

	void UpdateProjectiles(float dt);

public:
	std::vector<Projectile*>						m_ActiveProjectiles;

	ParticleManager *m_Particles;

public:

	BattleSystem();
	~BattleSystem();

	void Update(float dt);

	//Registration Functions
	void CreateProjectile(int id, D3DXVECTOR3 pos, D3DXVECTOR3 dir);
	void RetireProjectile(Projectile* projectile);
};
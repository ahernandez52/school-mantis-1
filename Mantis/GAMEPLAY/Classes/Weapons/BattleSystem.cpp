#include "BattleSystem.h"
#include "../../FRAMEWORK/Utility/JSONParser.h"
#include "../../../FRAMEWORK/Managers/WorldManager.h"

#include "../../../GRAPHICS/Particle/ParticleManager.h"

using namespace std;

BattleSystem::BattleSystem()
{
	PrimeBattleSystem();
}

BattleSystem::~BattleSystem()
{
}

//Private Functions
//==========================
void BattleSystem::DefragProjectiles(float dt)
{
	static float ElapsedTime = 0.0f;

	//If there is any activity reset counter to 0
	if(m_ActiveProjectiles.size() > 0) 
	{
		ElapsedTime = 0.0f;
		return;
	}

	//if we have not incremented memory abort
	if(m_Increments == 1) 
		return;

	if(m_Increments == 0) 
		return;

	//if not enough time has elapsed abort and increment elapsed time with dt
	if(ElapsedTime < m_DefragFrequency) 
	{
		ElapsedTime += dt;
		return;
	}

	//Perform a defrag on projectiles if we've incremented
	//and enough time has elapsed
	//and there is no projectile activity

	for(vector<Actor*>::iterator projIT = m_InactiveProjectiles.begin() + (m_Increments * m_StorageIncrement);
		projIT != m_InactiveProjectiles.end();
		projIT++)
	{
		delete *projIT;
		m_InactiveProjectiles.erase(projIT); //might not need this step
	}

	m_Increments--;

	int newSize = m_Increments ? 1 : m_Increments; //calculate new size

	m_InactiveProjectiles.resize(newSize * m_StorageIncrement); //shrink container
	m_ActiveProjectiles.reserve(newSize * m_StorageIncrement);

	ElapsedTime = 0.0f;
}

void BattleSystem::ExpandMemory()
{
	m_Increments++;

	m_ActiveProjectiles.reserve(m_Increments * m_StorageIncrement);
	m_InactiveProjectiles.reserve(m_Increments * m_StorageIncrement);

	//Actor *tempActor;
	//Projectile *tempProjectile;
	//for(int i = 0; i < m_StorageIncrement; i++)
	//{

	//}
}

void BattleSystem::PrimeBattleSystem()
{
	m_Activity = false;

	//Read in Battle System Variables
	JSONParser parser;
	parser.OpenFile("Assets/Gameplay/Projectiles/BattleSystemVars.json");

	JSONObject sysVar = parser.GetParsedObject();

	m_DefragFrequency = (float)atof(sysVar.m_variables["defragFrequency"].c_str());
	m_StorageIncrement = atoi(sysVar.m_variables["defragIncrement"].c_str());
	m_Increments = 0;

	//Read in Projectile Definitons
	JSONParser parser2;
	parser2.OpenFile("Assets/Gameplay/Projectiles/ProjectileDefinitions.json");

	JSONObject projectiles = parser2.GetParsedObject();

	for(auto projectile : projectiles.m_arrays["Projectiles"].m_values)
	{
		ProjectileDef tempProjectileDef;

		tempProjectileDef.m_Speed = (float)atof(projectile.m_variables["speed"].c_str());
		tempProjectileDef.m_Damage = (float)atof(projectile.m_variables["damage"].c_str());
		tempProjectileDef.m_Name = projectile.m_variables["name"];
		tempProjectileDef.m_FireSound = projectile.m_variables["fireSound"];
		tempProjectileDef.m_HitSound = projectile.m_variables["hitSound"];

		m_ProjectileDefinitions.insert(make_pair(tempProjectileDef.m_Name, tempProjectileDef));
	}
}

void BattleSystem::UpdateProjectiles(float dt)
{
	//for(auto it = m_ActiveProjectiles.rbegin();
	//	it != m_ActiveProjectiles.rend();
	//	it++)
	//{
	//	(*it)->Update(dt);

	//	if((*it)->m_ActiveTime >= (*it)->m_MaxActiveTime)
	//	{
	//		Actor *temp = (*it)->GetActor();
	//		m_ActiveProjectiles.erase(it.base());
	//		WMI->DeleteActor(temp);
	//	}
	//}

	for(int i = m_ActiveProjectiles.size()-1;
		i >= 0;
		i--)
	{
		m_ActiveProjectiles.at(i)->Update(dt);

		if(m_ActiveProjectiles.at(i)->m_ActiveTime >= m_ActiveProjectiles.at(i)->m_MaxActiveTime)
		{
			m_Particles->RemoveEmitter(m_ActiveProjectiles.at(i)->m_Emitter);

			Actor *temp = m_ActiveProjectiles.at(i)->GetActor();
			m_ActiveProjectiles.erase(m_ActiveProjectiles.begin()+i);
			
			WMI->DeleteActor(temp);
		}
	}
}
//**************************


//Public Functions
//==========================
void BattleSystem::Update(float dt)
{
	DefragProjectiles(dt);

	UpdateProjectiles(dt);
}

void BattleSystem::CreateProjectile(int id, D3DXVECTOR3 pos, D3DXVECTOR3 dir)
{
	//if(m_ActiveProjectiles.size() == (m_Increments * m_StorageIncrement))
	//{
	//	ExpandMemory();
	//}

	Actor *tempActor;
	tempActor = WMI->CreateActor("NULL", "NULL", "light", "NULL");

	Projectile *newProjectile = new Projectile(tempActor);
	tempActor->Gameplay = newProjectile;

	newProjectile->SetInternals(2000.0f, 5.0f, pos, dir);
	newProjectile->m_Radius = 5.0f;

	switch(id)
	{
		case 1:
			newProjectile->m_Emitter = m_Particles->CreateEmitter("PlayerGun");
			break;

		case 2:
			newProjectile->m_Emitter = m_Particles->CreateEmitter("PirateGun");
			break;
	}
	

	m_ActiveProjectiles.push_back(newProjectile);
}

void BattleSystem::RetireProjectile(Projectile* projectile)
{
	for(int i = m_ActiveProjectiles.size()-1;
		i >= 0;
		i--)
	{
		if(m_ActiveProjectiles.at(i) == projectile)
		{
			Actor *temp = m_ActiveProjectiles.at(i)->GetActor();
			m_ActiveProjectiles.erase(m_ActiveProjectiles.begin()+i);
			m_Particles->RemoveEmitter(projectile->m_Emitter);

			WMI->DeleteActor(temp);
		}
	}
}
//**************************
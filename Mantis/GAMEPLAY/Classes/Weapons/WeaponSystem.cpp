#include "WeaponSystem.h"
#include "../../../FRAMEWORK/Managers/WorldManager.h"
#include "../../../FRAMEWORK/Sound/Audio.h"

#define DEFAULT_SHIP_SLOTS 3;

WeaponSystem::WeaponSystem()
{
	m_WeaponSlots = DEFAULT_SHIP_SLOTS;
	
	m_Weapons.reserve(m_WeaponSlots);

	InitAudio();
}

WeaponSystem::WeaponSystem(int slots)
{
	m_WeaponSlots = slots;

	m_Weapons.reserve(slots);
}

WeaponSystem::~WeaponSystem()
{
	//ReleaseAudio();
}

void WeaponSystem::InitAudio(void)
{
	//SCI->LoadSound("Assets/Sounds/Effects/fire_placeholder.wav");
}

void WeaponSystem::ReleaseAudio(void)
{
	//SCI->ReleaseSound("Assets/Sounds/Effects/fire_placeholder.wav");
}

//Private Functions
//================================
void WeaponSystem::UpdateTimers(float dt)
{
	for(auto &weapon : m_Weapons)
	{
		if(weapon.m_Status == COOLING)
		{

			weapon.m_CD -= dt;

			if(weapon.m_CD <= 0.0f)
			{
				weapon.m_Status = READY;
				weapon.m_CD = weapon.m_CooldownTime;
			}
		}
	}
}
//********************************


//Public Functions
//================================
bool WeaponSystem::AddWeapon(int id, float cd, Vector3 offset)
{
	if(m_Weapons.size() >= (unsigned)m_WeaponSlots)
		return false;

	m_Weapons.push_back(Weapon(id, cd, m_Weapons.size(), offset));

	return false;
}

void WeaponSystem::RemoveWeapon(int position)
{
	for(std::vector<Weapon>::iterator it = m_Weapons.begin();
		it != m_Weapons.end();
		it++)
	{
		if(it->m_TurretPosition == position)
		{
			m_Weapons.erase(it);
			//Successful remove sound <SOUND>
			return;
		}
	}
	//Failure Sound <SOUND>
}

void WeaponSystem::UpdateWeapons(float dt)
{
	UpdateTimers(dt);
}

bool WeaponSystem::Fire(D3DXVECTOR3 shipPosition, D3DXVECTOR3 direction)
{
	bool fired = false;

	for(auto &weapon : m_Weapons)
	{
		if(weapon.m_Status == READY)
		{
			weapon.m_Status = COOLING;

			D3DXVECTOR3 projectileSpawn = (shipPosition + weapon.m_Offset) + (direction * 55);

			WMI->SpawnProjectile(weapon.m_ID, projectileSpawn, direction);

			fired = true;
		}
	}

	return fired;
}
//********************************

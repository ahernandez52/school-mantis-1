#pragma once
#include <vector>
#include "../../../FRAMEWORK/Utility/MantisUtil.h"

enum Weapon_Status
{
	READY = 0,
	COOLING,
	OFFLINE,
};

struct Weapon{
	int				m_ID;
	int				m_TurretPosition; //NYI
	Weapon_Status	m_Status;
	float			m_CooldownTime;
	float			m_CD;
	Vector3			m_Offset;
	Weapon(int id, float cd, int position, Vector3 offset)
	{
		m_ID			= id;
		m_CooldownTime	= cd;
		m_CD			= cd;
		m_Status		= READY;
		m_Offset		= offset;
	}
};

class Actor;

class WeaponSystem
{
private:

	std::vector<Weapon>	m_Weapons;

	int					m_WeaponSlots;

private:

	void UpdateTimers(float dt);

public:
	WeaponSystem();
	WeaponSystem(int slots);
	~WeaponSystem();

	void InitAudio(void);
	void ReleaseAudio(void);

	bool AddWeapon(int id, float cd, Vector3 offset);
	void RemoveWeapon(int position);

	void UpdateWeapons(float dt);
	bool Fire(D3DXVECTOR3 shipPosition, D3DXVECTOR3 direction);
};

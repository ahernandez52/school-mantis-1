#pragma once

#include "Quest.h"
#include "../../../GRAPHICS/Gui/QuestDialog.h"
#include "QuestLog.h"
#include <map>

/**	\brief QuestSystem Class
 *	
 *	The QuestSystem object holds all quests found in the current area.
 *	It loads the quests from a JSON file and holds a pointer to a CEGUI window for the Dialog box to Accept / Decline a quest.
 */
class QuestSystem
{
public:

	//std::vector<Quest*> m_Quests;

	std::map<std::string, Quest*> m_AvailableQuests;
	std::map<std::string, Quest*> m_UnavailableQuests;
	std::map<std::string, Quest*> m_Quests;

	QuestDialog* m_QuestDialogWindow;

	QuestLog* m_QuestLog;

public:
	QuestSystem();

	~QuestSystem();

	/**	
	 *	Loads quests from a JSON file.
	 */
	void LoadQuests(std::string fileName);

	/**	
	 *	Utility function that creates a CEGUI window.
	 */
	void CreateQuestDialogWindow();

	/**	
	 *	Utility function that applies a quest's items to the Dialog window.
	 */
	void ApplyQuestToDialogWindow(Quest* quest);

	/**	
	 *	Utility function to show the Dialog window.
	 */
	void OpenQuestDialogWindow();

	/**	
	 *	Utility function to hide the Dialog window.
	 */
	void CloseQuestDialogWindow();
};
#include "QuestLog.h"
#include "Quest.h"
#include "../../../GRAPHICS/Gui/QuestLogWindow.h"

QuestLog::QuestLog()
{
	m_QuestLogWindow = nullptr;
}

QuestLog::~QuestLog()
{
	if(m_QuestLogWindow != nullptr)
	{
		delete m_QuestLogWindow;
	}
}

void QuestLog::AddQuest(Quest* quest)
{
	for(auto q : m_vActiveQuests)
	{
		if(q == quest)
		{
			return;
		}
	}

	m_mActiveQuests.insert(std::pair<std::string, Quest*>(quest->m_Name, quest));
	m_vActiveQuests.push_back(quest);

	if(m_QuestLogWindow->isVisible())
	{
		m_QuestLogWindow->DePopulateQuestLog();
		m_QuestLogWindow->PopulateQuestLog();
	}
}

void QuestLog::AbandonQuest(Quest* quest)
{
	m_mActiveQuests.erase(m_mActiveQuests.find(quest->m_Name));

	std::vector<Quest*>::iterator it;

	for(it = m_vActiveQuests.begin(); it != m_vActiveQuests.end(); ++it)
	{
		if((*it) == quest)
		{
			break;
		}
	}

	m_vActiveQuests.erase(it);

	if(m_QuestLogWindow->isVisible())
	{
		m_QuestLogWindow->DePopulateQuestLog();
		m_QuestLogWindow->PopulateQuestLog();
	}
}

void QuestLog::CompleteQuest(Quest* quest)
{
	m_mActiveQuests.erase(m_mActiveQuests.find(quest->m_Name));
	m_mCompletedQuests.insert(std::pair<std::string, Quest*>(quest->m_Name, quest));

	std::vector<Quest*>::iterator it;

	for(it = m_vActiveQuests.begin(); it != m_vActiveQuests.end(); ++it)
	{
		if((*it) == quest)
		{
			break;
		}
	}

	m_vActiveQuests.erase(it);

	m_vCompletedQuests.push_back(quest);
}

void QuestLog::CreateQuestLogWindow()
{
	m_QuestLogWindow = new QuestLogWindow(this);
	m_QuestLogWindow->Init();
	m_QuestLogWindow->setVisible(false);
}

void QuestLog::ToggleQuestLog()
{
	m_QuestLogWindow->ToggleVisible();
}
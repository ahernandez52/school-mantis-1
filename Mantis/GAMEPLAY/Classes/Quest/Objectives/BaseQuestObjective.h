#pragma once

#include <string>


/**	
 *	Enumeration for the type of Objective.
 */
enum ObjectiveType
{
	NO_OBJECTIVE = 0,
	KILL,
	COLLECT
};


/**	\brief BaseQuestObjective Class
 *	
 *	The BaseQuestObjective Class is the base object for all quest objectives.
 */
class BaseQuestObjective
{
public:
	ObjectiveType m_Type;

public:
	BaseQuestObjective();

	~BaseQuestObjective();

	virtual void Update(float dt);

	virtual bool IsComplete();

	/**
	 *	Base Utility Function for getting the generic Objective string.
	 */
	virtual std::string GetString();

	/**
	 *	Base Utility Function for getting the player's progress Objective string.
	 */
	virtual std::string GetProgressString();
	
};
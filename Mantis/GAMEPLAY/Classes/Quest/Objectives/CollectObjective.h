#pragma once

#include <string>
#include "BaseQuestObjective.h"


/**	\brief CollectObjective Class
 *	
 *	Keeps track of how many items of a certain item the Player has collected.
 */
class CollectObjective : public BaseQuestObjective
{
public:
	int m_PlayerProgress;
	int m_NumToCollect;
	std::string m_Name;

public:
	CollectObjective();
	CollectObjective(int num, std::string name);

	~CollectObjective();

	void Update(float dt) override;

	bool IsComplete() override;

	/**
	 *	Utility Function for getting the generic bjective string.
	 */
	std::string GetString() override;

	/**
	 *	Utility Function for getting the player's objective progress string.
	 */
	std::string GetProgressString() override;
};
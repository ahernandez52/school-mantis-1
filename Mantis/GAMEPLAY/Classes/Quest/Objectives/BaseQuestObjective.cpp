#include "BaseQuestObjective.h"

BaseQuestObjective::BaseQuestObjective()
{
	m_Type = NO_OBJECTIVE;
}

BaseQuestObjective::~BaseQuestObjective()
{
}

void BaseQuestObjective::Update(float dt)
{
}

bool BaseQuestObjective::IsComplete()
{
	return false;
}

std::string BaseQuestObjective::GetString()
{
	return "";
}

std::string BaseQuestObjective::GetProgressString()
{
	return "";
}
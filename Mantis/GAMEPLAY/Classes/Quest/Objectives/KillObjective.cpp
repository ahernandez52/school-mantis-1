#include "KillObjective.h"
#include <sstream>

KillObjective::KillObjective()
{
	m_Type = KILL;

	m_Name = "";
	m_NumToKill = 0;
	m_PlayerProgress = 0;
}

KillObjective::KillObjective(int num, std::string name)
{
	m_Type = KILL;

	m_Name = name;
	m_NumToKill = num;
	m_PlayerProgress = 0;
}

KillObjective::~KillObjective()
{
}

void KillObjective::Update(float dt)
{
}

bool KillObjective::IsComplete()
{
	return m_PlayerProgress >= m_NumToKill;
}

std::string KillObjective::GetString()
{
	std::string objective;

	objective = "Kill ";

	objective += std::to_string(m_NumToKill);

	objective += " " + m_Name;

	return objective;
}

std::string KillObjective::GetProgressString()
{
	std::string objective;

	objective += std::to_string(m_PlayerProgress);

	objective += "/";
	
	objective += std::to_string(m_NumToKill);

	objective += " " + m_Name;

	return objective;
}
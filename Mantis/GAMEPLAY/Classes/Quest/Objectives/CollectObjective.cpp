#include "CollectObjective.h"
#include <sstream>

CollectObjective::CollectObjective()
{
	m_Type = COLLECT;

	m_Name = "";
	m_NumToCollect = 0;
	m_PlayerProgress = 0;
}

CollectObjective::CollectObjective(int num, std::string name)
{
	m_Type = COLLECT;

	m_Name = name;
	m_NumToCollect = num;
	m_PlayerProgress = 0;
}

CollectObjective::~CollectObjective()
{
}

void CollectObjective::Update(float dt)
{
}

bool CollectObjective::IsComplete()
{
	return m_PlayerProgress >= m_NumToCollect;
}

std::string CollectObjective::GetString()
{
	std::string objective;

	objective = "Collect ";

	objective += std::to_string(m_NumToCollect);

	objective += " " + m_Name;

	return objective;
}

std::string CollectObjective::GetProgressString()
{
	std::string objective;

	objective += std::to_string(m_PlayerProgress);

	objective += "/";

	objective += std::to_string(m_NumToCollect);

	objective += " " + m_Name;

	return objective;
}
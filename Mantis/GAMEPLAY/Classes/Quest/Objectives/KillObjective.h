#pragma once

#include <string>
#include "BaseQuestObjective.h"


/**	\brief KillObjective Class
 *	
 *	Keeps track of how many kills of a certain enemy the Player has killed.
 */
class KillObjective : public BaseQuestObjective
{
public:
	int m_PlayerProgress;
	int m_NumToKill;
	std::string m_Name;

public:
	KillObjective();
	KillObjective(int num, std::string name);

	~KillObjective();

	void Update(float dt) override;

	bool IsComplete() override;

	/**
	 *	Utility Function for getting the generic objective string.
	 */
	std::string GetString() override;

	/**
	 *	Utility Function for getting the player's objective progress string.
	 */
	std::string GetProgressString() override;
};
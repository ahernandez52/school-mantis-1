#include "Quest.h"
#include "Objectives\CollectObjective.h"
#include "Objectives\KillObjective.h"
#include "Rewards\ExperienceReward.h"
#include "Rewards\ItemReward.h"
#include "Rewards\MoneyReward.h"
#include "Rewards\ReputationReward.h"

Quest::Quest()
{
}

Quest::Quest(std::string name, std::string body)
{
	m_Name = name;
	m_Body = body;
}

Quest::~Quest()
{
	for(BaseQuestObjective* o : m_Objectives)
	{
		BaseQuestObjective* temp = o;
		o = nullptr;
		delete temp;
	}

	m_Objectives.clear();

	for(BaseQuestReward* r : m_Rewards)
	{
		BaseQuestReward* temp = r;
		r = nullptr;
		delete temp;
	}

	m_Rewards.clear();
}

void Quest::SetName(std::string name)
{
	m_Name = name;
}

void Quest::SetBody(std::string body)
{
	m_Body = body;
}

void Quest::AddNewQuestObjective(ObjectiveType type, std::string name, int num)
{
	switch(type)
	{
	case KILL:
		{
			KillObjective* temp = new KillObjective(num, name);
			m_Objectives.push_back(temp);
			break;
		}
	case COLLECT:
		{
			CollectObjective* temp = new CollectObjective(num, name);
			m_Objectives.push_back(temp);
			break;
		}

		break;
	}
}

void Quest::AddNewQuestReward(RewardType type, int num)
{
	switch(type)
	{
	case MONEY:
		{
			MoneyReward* temp = new MoneyReward(num);
			m_Rewards.push_back(temp);
			break;
		}
	case EXPERIENCE:
		{
			ExperienceReward* temp = new ExperienceReward(num);
			m_Rewards.push_back(temp);
			break;
		}
	case REPUTATION:
		{
			ReputationReward* temp = new ReputationReward(num);
			m_Rewards.push_back(temp);
			break;
		}
	case ITEM:
		{
			ItemReward* temp = new ItemReward(num);
			m_Rewards.push_back(temp);
			break;
		}

		break;
	}
}

std::string Quest::GetName()
{
	return m_Name;
}

std::string Quest::GetBody()
{
	return m_Body;
}

std::string Quest::GetObjectiveString()
{
	std::string objectives;

	objectives = "Objectives: ";

	for(auto objective : m_Objectives)
	{
		objectives += "\n";

		objectives += "\t" + objective->GetString();
	}

	return objectives;
}

std::string Quest::GetObjectiveProgressString()
{
	std::string objectives;

	objectives = "Objectives: ";

	for(auto objective : m_Objectives)
	{
		objectives += "\n";

		objectives += "\t" + objective->GetProgressString();
	}

	return objectives;
}

std::string Quest::GetRewardString()
{
	std::string rewards;

	rewards = "Rewards: ";

	for(auto reward : m_Rewards)
	{
		rewards += "\n";

		rewards += "\t" + reward->GetString();
	}

	return rewards;
}

std::vector<BaseQuestObjective*> Quest::GetObjectives()
{
	return m_Objectives;
}

std::vector<BaseQuestReward*> Quest::GetRewards()
{
	return m_Rewards;
}
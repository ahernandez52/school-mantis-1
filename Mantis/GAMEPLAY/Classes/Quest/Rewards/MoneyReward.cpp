#include "MoneyReward.h"
#include <sstream>

MoneyReward::MoneyReward()
{
	m_Type = MONEY;

	m_Amount = 0;
}

MoneyReward::MoneyReward(int amount)
{
	m_Type = MONEY;

	m_Amount = amount;
}

MoneyReward::~MoneyReward()
{
}

std::string MoneyReward::GetString()
{
	std::string reward;

	reward += std::to_string(m_Amount);

	reward += " Credits";

	return reward;
}
#include "ItemReward.h"
#include <sstream>

ItemReward::ItemReward()
{
	m_Type = ITEM;

	m_ItemID = -1;
}

ItemReward::ItemReward(int itemID)
{
	m_Type = ITEM;

	m_ItemID = itemID;
}

ItemReward::~ItemReward()
{
}

std::string ItemReward::GetString()
{
	std::string reward;

	reward += "Item ";

	reward += std::to_string(m_ItemID);

	return reward;
}
#pragma once

#include "BaseQuestReward.h"


/**	\brief ExperienceReward Class
 *	
 *	Gives the player an amount of experience as a reward.
 */
class ExperienceReward : public BaseQuestReward
{
public:
	int m_Amount;

public:
	ExperienceReward();
	ExperienceReward(int amount);

	~ExperienceReward();

	/**	
	 *	Utility function that returns a string which defines the Reward.
	 */
	std::string GetString() override;
};
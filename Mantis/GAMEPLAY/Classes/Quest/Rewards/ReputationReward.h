#pragma once

#include "BaseQuestReward.h"


/**	\brief ReputationReward Class
 *	
 *	Gives the player an amount of reputation as a reward.
 */
class ReputationReward : public BaseQuestReward
{
public:
	int m_Amount;

public:
	ReputationReward();
	ReputationReward(int amount);

	~ReputationReward();

	/**	
	 *	Utility function that returns a string which defines the Reward.
	 */
	std::string GetString() override;
};
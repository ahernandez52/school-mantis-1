#pragma once

#include <string>

/**	
 *	Enumeration for the type of reward.
 */
enum RewardType
{
	NO_REWARD = 0,
	MONEY,
	EXPERIENCE,
	REPUTATION,
	ITEM
};

/**	\brief BaseQuestReward Class
 *	
 *	The BaseQuestReward Class is the base object for all quest rewards.
 */
class BaseQuestReward
{
public:
	RewardType m_Type;

public:
	BaseQuestReward();

	~BaseQuestReward();

	/**	
	 *	Base utility function.
	 */
	virtual std::string GetString();
};
#pragma once

#include "BaseQuestReward.h"


/**	\brief MoneyReward Class
 *	
 *	Gives the player an amount of credits as a reward.
 */
class MoneyReward : public BaseQuestReward
{
public:
	int m_Amount;

public:
	MoneyReward();
	MoneyReward(int amount);

	~MoneyReward();

	/**	
	 *	Utility function that returns a string which defines the Reward.
	 */
	std::string GetString() override;
};
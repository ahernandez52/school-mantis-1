#include "ReputationReward.h"
#include <sstream>

ReputationReward::ReputationReward()
{
	m_Type = REPUTATION;

	m_Amount = 0;
}

ReputationReward::ReputationReward(int amount)
{
	m_Type = REPUTATION;

	m_Amount = amount;
}

ReputationReward::~ReputationReward()
{
}

std::string ReputationReward::GetString()
{
	std::string reward;

	reward += std::to_string(m_Amount);

	reward += " Reputation";

	return reward;
}
#pragma once

#include "BaseQuestReward.h"


/**	\brief ItemReward Class
 *	
 *	Gives the player an item as a reward.
 */
class ItemReward : public BaseQuestReward
{
public:
	int m_ItemID;

public:
	ItemReward();
	ItemReward(int itemID);

	~ItemReward();

	/**	
	 *	Utility function that returns a string which defines the Reward.
	 */
	std::string GetString() override;
};
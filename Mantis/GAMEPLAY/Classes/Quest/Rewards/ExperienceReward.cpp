#include "ExperienceReward.h"
#include <sstream>

ExperienceReward::ExperienceReward()
{
	m_Type = EXPERIENCE;

	m_Amount = 0;
}

ExperienceReward::ExperienceReward(int amount)
{
	m_Type = EXPERIENCE;

	m_Amount = amount;
}

ExperienceReward::~ExperienceReward()
{
}

std::string ExperienceReward::GetString()
{
	std::string reward;

	reward += std::to_string(m_Amount);

	reward += " Experience";

	return reward;
}
#include "QuestSystem.h"
#include <assert.h>
#include "../../../FRAMEWORK/Managers/WorldManager.h"
#include "../../../FRAMEWORK/Utility/JSONParser.h"

QuestSystem::QuestSystem()
{
	m_QuestDialogWindow = nullptr;
}

QuestSystem::~QuestSystem()
{
	if(m_QuestDialogWindow)
	{
		delete m_QuestDialogWindow;
	}
}

void QuestSystem::LoadQuests(std::string fileName)
{
	JSONParser parser;

	int error = parser.OpenFile(fileName);

	switch(error)
	{
	case 1:
		{
			MessageBox(0, "Quests Invalid!", 0, 0);
			PostQuitMessage(0);
			break;
		}

	case 2:
		{
			MessageBox(0, "Quests Not Found!", 0, 0);
			PostQuitMessage(0);
			break;
		}

		break;
	}

	JSONObject quests = parser.GetParsedObject();

	for(auto questName : quests.m_arrays.at("Quests").m_values)
	{
		JSONParser parser2;

		int error2 = parser2.OpenFile("Assets/Quests/" + questName.m_variables.at("QuestName") + ".json");

		switch(error2)
		{
		case 1:
			{
				MessageBox(0, "QuestFile Invalid!", 0, 0);
				PostQuitMessage(0);
				break;
			}

		case 2:
			{
				MessageBox(0, "QuestFile Not Found!", 0, 0);
				PostQuitMessage(0);
				break;
			}

			break;
		}

		JSONObject quest = parser2.GetParsedObject();

		//string for use in extracting data
		std::string name, description, objectiveType, objectiveNum, objectiveName, rewardType, rewardNum;
		int num;
		ObjectiveType otype;
		RewardType rtype;

		name = quest.m_variables["Name"];
		description = quest.m_variables["Description"];

		Quest* NewQuest = new Quest(name, description);

		for(auto objective : quest.m_arrays.at("Objectives").m_values)
		{
			objectiveType = objective.m_variables["Type"];
			objectiveNum = objective.m_variables["Number"];
			objectiveName = objective.m_variables["Name"];

			std::stringstream(objectiveNum) >> num;

			if(objectiveType == "1")
			{
				otype = KILL;
			}
			else if(objectiveType == "2")
			{
				otype = COLLECT;
			}

			NewQuest->AddNewQuestObjective(otype, objectiveName, num);
		}

		for(auto reward : quest.m_arrays.at("Rewards").m_values)
		{
			rewardType = reward.m_variables["Type"];
			rewardNum = reward.m_variables["Number"];

			std::stringstream(rewardNum) >> num;

			if(rewardType == "1")
			{
				rtype = MONEY;
			}
			else if(rewardType == "2")
			{
				rtype = EXPERIENCE;
			}
			else if(rewardType == "3")
			{
				rtype = REPUTATION;
			}
			else if(rewardType == "4")
			{
				rtype = ITEM;
			}

			NewQuest->AddNewQuestReward(rtype, num);
		}

		//save quest
		m_Quests.insert(std::pair<std::string, Quest*>(NewQuest->m_Name, NewQuest));
	}

}

void QuestSystem::CreateQuestDialogWindow()
{
	m_QuestDialogWindow = new QuestDialog(this);
	m_QuestDialogWindow->Init();
	m_QuestDialogWindow->setVisible(false);
}

void QuestSystem::ApplyQuestToDialogWindow(Quest* quest) 
{
	m_QuestDialogWindow->SetQuest(quest);
}

void QuestSystem::OpenQuestDialogWindow()
{
	m_QuestDialogWindow->setVisible(true);
}

void QuestSystem::CloseQuestDialogWindow()
{
	m_QuestDialogWindow->setVisible(false);

	m_QuestDialogWindow->RemoveQuest();
}
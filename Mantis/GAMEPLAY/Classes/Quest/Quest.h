#pragma once

#include <string>
#include <vector>
#include "Objectives\BaseQuestObjective.h"
#include "Rewards\BaseQuestReward.h"

/**	\brief Quest Class
 *	
 *	Quest class holds the quest name, quest body, and objectives and rewards
 */
class Quest
{
public:
	//public member variables
	std::string m_Name;
	std::string m_Body;
	std::vector<BaseQuestObjective*> m_Objectives;
	std::vector<BaseQuestReward*> m_Rewards;

	//NPC* m_QuestGiver;
	//NPC* m_QuestReceiver;

private:
	//private member variables

public:
	//public member functions

	Quest();
	Quest(std::string name, std::string body);

	~Quest();

private:
	//private member functions

public:
	/**	
	 *	Adds a new quest objective to the Quest.
	 */
	void AddNewQuestObjective(ObjectiveType type, std::string name, int num);

	/**	
	 *	Adds a new quest objective to the Quest.
	 *	Treats input "int num" as itemID for an item type, and as an amount for everything else.
	 */
	void AddNewQuestReward(RewardType type, int num);
	
	/**	
	 *	Getters and Setters
	 */

	void SetName(std::string name);
	void SetBody(std::string body);

	std::string GetName();
	std::string GetBody();
	std::string GetObjectiveString();
	std::string GetObjectiveProgressString();
	std::string GetRewardString();

	std::vector<BaseQuestObjective*> GetObjectives();
	std::vector<BaseQuestReward*> GetRewards();

};
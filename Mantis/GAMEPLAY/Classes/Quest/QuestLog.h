#pragma once

#include <map>
#include <vector>

class Quest;
class QuestLogWindow;

/**	\brief QuestLog Class
 *	
 *	The QuestLog keep track of all the player's current active and completed quests.
 *	The QuestLog has it's own CEGUI window to display quests.
 */
class QuestLog
{
public:

	std::map<std::string, Quest*> m_mActiveQuests;
	std::vector<Quest*> m_vActiveQuests;

	std::map<std::string, Quest*> m_mCompletedQuests;
	std::vector<Quest*> m_vCompletedQuests;

	Quest* m_CurrQuest;

	QuestLogWindow* m_QuestLogWindow;

public:

	QuestLog();
	~QuestLog();

	/**	
	 *	Add a Quest to the QuestLog
	 */
	void AddQuest(Quest* quest);

	/**	
	 *	Abandon a Quest in the log.
	 */
	void AbandonQuest(Quest* quest);

	/**	
	 *	Complete a Quest in the log.
	 */
	void CompleteQuest(Quest* quest);

	/**	
	 *	Utility function for creating the CEGUI window for the QuestLog object.
	 */
	void CreateQuestLogWindow();

	/**	
	 *	Utility function for toggling the CEGUI window.
	 */
	void ToggleQuestLog();
};
/**
 * QuestDemo -	This demo will be used to demostrate the
 *					Quest System to be used in Gameplay.
 *
 */

#pragma once 

#include "../../FRAMEWORK/GSM/GameState.h"
#include "..\..\FRAMEWORK\Utility\MantisUtil.h"

#include "../Classes/Quest/Quest.h"
#include "../Classes/Quest/QuestSystem.h"
#include "../Classes/Quest/QuestLog.h"

#include "../Classes/Warp/WarpPortal.h"
#include "../Classes/Warp/WarpLane.h"
#include "../Classes/Warp/WarpController.h"

#include "../Object/Player.h"

class GameFramework;
class D3D9GraphicsCore;
class Actor;

class QuestDemo : public GameState
{
private:

	//State specific private members
	int		m_TemplateFighterID;

	Actor* currActor;
	Actor* lastActor;

	QuestSystem qs;
	QuestLog ql;

	std::vector<WarpPortal*> portals;
	std::vector<WarpLane*> lanes;
	WarpController warpSys;

	Actor* player;

public:

	//State specific public members

public:

	//State related public methods
	QuestDemo();

	~QuestDemo(void);

	void InitializeState(void) override;
	void EnterState(void) override;
	void LeaveState(void) override;

	void Update(float dt);

public:
		
	//State specific public methods

protected:

	//State specific protected methods

private:

	//State specific private methods

	void SetUpScene(void);

	//player creation
	Actor* CreatePlayer(float x, float y, float z);

	//ship creation
	Actor* CreatePhalanxFighter(D3DXVECTOR3 pos);
	Actor* CreatePhalanxFighter(float x, float y, float z);

	Actor* CreateOrderFighter(D3DXVECTOR3 pos);
	Actor* CreateOrderFighter(float x, float y, float z);


	Actor* CreatePhalanxFrigate(float x, float y, float z);

	Actor* CreatePhalanxCruiser(float x, float y, float z);

	Actor* CreateFreighter5(float x, float y, float z);

	Actor* CreateBattleShip(float x, float y, float z);

	//asteroid creation
	void CreateAsteroid(D3DXVECTOR3 pos);
	void CreateAsteroid(float x, float y, float z);
	void CreateAsteroid(D3DXVECTOR3 pos, float radius);
	void CreateAsteroid(float x, float y, float z, float radius);

	//so tired of these fucking directx units in my dragonx sandwich
	void CreateHalfUnitSphere(D3DXVECTOR3 pos);
	void CreateHalfUnitSphere(float x, float y, float z);

	//warp portal creation
	void CreateWarpPortalConnector(D3DXVECTOR3 pos);
	void CreateWarpPortalConnector(float x, float y, float z);

	void CreateWarpPortalAnchor(D3DXVECTOR3 pos);
	void CreateWarpPortalAnchor(float x, float y, float z);

	void CreateWarpPortalSystem(D3DXVECTOR3 pos);
	void CreateWarpPortalSystem(float x, float y, float z);

	//warp lane creation
	void CreateWarpLane(D3DXVECTOR3 start, D3DXVECTOR3 finish);
	void CreateWarpLane(float xStart, float yStart, float zStart, float xFinish, float yFinish, float zFinish);



	void ActorFaceDirection(Actor* actor, D3DXVECTOR3 dir);
};
#include "Inventorydemo.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../../FRAMEWORK/Utility/Camera.h"
#include "../../FRAMEWORK/Factories/LevelFactory.h"
#include "../../FRAMEWORK/Input/RawInput.h"
#include "../../GRAPHICS/D3D9GraphicsCore.h"
#include "../../GAMEPLAY/Object/Player.h"
#include "../../FRAMEWORK/Sound/3DSound.h"


// Public Functions
//============================================
void InventoryDemo::InitializeState()
{
	LevelFactory level;
	gCamera->setSpeed(500);
	//gCamera->setMode(FREEFORM);
	gCamera->lookAt(D3DXVECTOR3(50.0f, 100.0f, -50.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	//gCamera->setInput(PC_MOUSEKEYBOARD);
	//gCamera->setMode(FIXED_ROTATION);

	//Ideally want to just have level name and factory addes the folder location
	//level.Build("Assets/Levels/FinalDemo.json");

	GraphicSchematic graphic;
	graphic.Shader		= "UberMain";				
	graphic.fileName	= "battleship_1";		
	graphic.name		= "BattleShip";				
	WMI->RegisterGraphicSchematic(graphic);		

	graphic.fileName	= "battleship_2";		
	graphic.name		= "BattleShip1";				
	WMI->RegisterGraphicSchematic(graphic);	

	graphic.fileName	= "sun2";
	graphic.name		= "Sun";
	WMI->RegisterGraphicSchematic(graphic);

	Actor* tempActor;

	tempActor = WMI->CreateActor("NULL", "Sun", "NULL", "NULL");
	tempActor->m_Position = D3DXVECTOR3(-3000, 0, 0);
	tempActor->Graphics->m_scale = D3DXVECTOR3(1000, 1000, 1000);
	tempActor->Graphics->m_bIsSun = true;

	m_Player = new Player(tempActor);
}

void InventoryDemo::LeaveState()
{
	WMI->CleanUp();
}

void InventoryDemo::EnterState()
{
	Graphics->SetSkyBoxRender(true);
}

void InventoryDemo::Update(float dt)
{
	if(gRawInput->keyPressed("D"))
	{
		m_test.DisplayInventory();
		//m_Player->m_Inventory->DisplayInventory();
	}
	if(gRawInput->keyPressed("O"))
	{
		m_test.AddItem();
		//m_Player->m_Inventory->AddItem();
	}

}


//********************************************


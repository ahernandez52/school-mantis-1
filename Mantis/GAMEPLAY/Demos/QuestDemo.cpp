#include "QuestDemo.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "../../FRAMEWORK/Utility/Camera.h"
#include "../../FRAMEWORK/Input/RawInput.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../../AI/Influence/InfluenceSystem.h"
#include "../../AI/AIObject/ShipAI.h"
#include "../../AI/Steering/SteeringBehavior.h"
#include "../../AI/Messaging/PostOffice.h"

#include <random>
#include <chrono>

QuestDemo::QuestDemo()
{
}

QuestDemo::~QuestDemo(void)
{
	//Deallocate any state specific allocations
}


// Public Functions
//============================================
void QuestDemo::InitializeState(void)
{
	WMI->InitMemory();

	SetUpScene();
}

void QuestDemo::EnterState(void)
{
	Framework->onLostDevice();
	Framework->onResetDevice();
}

void QuestDemo::LeaveState(void)
{
	WMI->CleanUp();
}

void QuestDemo::Update(float dt)
{
	warpSys.Update(dt);

	for(auto p : portals)
	{
		p->Update(dt);
	}

	if(gRawInput->keyPressed("I"))
	{
		qs.OpenQuestDialogWindow();
		qs.ApplyQuestToDialogWindow(qs.m_Quests.at("Hello World!"));
	}
	if(gRawInput->keyPressed("O"))
	{
		qs.OpenQuestDialogWindow();
		qs.ApplyQuestToDialogWindow(qs.m_Quests.at("Bug Collection"));
	}

	if(gRawInput->keyPressed("L"))
	{
		ql.ToggleQuestLog();
	}

	if(gRawInput->keyPressed(VK_UP))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(0,0,1);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_DOWN))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(0,0,-1);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_LEFT))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(-1,0,0);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_RIGHT))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(1,0,0);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}

	if(gRawInput->keyPressed("R"))
	{
		currActor->m_Position = D3DXVECTOR3(0,0,0);
		gCamera->lookAt(D3DXVECTOR3(0,0,-5), D3DXVECTOR3(0,0,0), D3DXVECTOR3(0,1,0));
		gCamera->pos() = D3DXVECTOR3(0,0,-5);
	}

	if(gRawInput->keyPressed("D"))
	{
		warpSys.DeActivatePortal(2);
	}
}

void QuestDemo::SetUpScene(void)
{
	gCamera->setSpeed(20);
	gCamera->setMode(FREEFORM);
	gCamera->setInput(XBOX_CONTROLLER);

	Graphics->SetSkyBoxRender(true);

	CreateBattleShip(20,0,0);
	CreatePhalanxCruiser(20,0,-8);
	CreateFreighter5(20,0,-14);
	CreatePhalanxFrigate(20,0,-19);
	CreatePhalanxFighter(20,0,-22);

	qs.m_QuestLog = &ql;
	//qs.LoadQuests("Assets/Levels/ALPHA.json");
	//qs.LoadQuests("Assets/Quests/QUESTS.json");
	qs.LoadQuests("Assets/Quests/MultipleQuests.json");
	
	qs.CreateQuestDialogWindow();

	ql.CreateQuestLogWindow();
	
	warpSys.CreateWarpLane(D3DXVECTOR3(0,0,25), D3DXVECTOR3(0,0,500));

	CreateWarpPortalSystem(0,0,-50);
	CreateWarpPortalConnector(-25,0,0);

	CreateOrderFighter(0,0,0);
}

Actor* QuestDemo::CreatePlayer(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "dark_fighter_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "sun2";
	graphic.name		= "Sun";
	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("NULL", "fighter_1", "Light", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	
	//float scale = 1.0f / 60.0f;
	//a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	a->Gameplay = new Player(a);

	player = a;
	return a;
}

Actor* QuestDemo::CreatePhalanxFighter(D3DXVECTOR3 pos)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "military_fighter_3";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FIGHTER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("PHALANX_FIGHTER", "fighter_1", "Light", "NULL");
	a->m_Position = pos;
	m_TemplateFighterID = a->ID;
	
	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreatePhalanxFighter(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "military_fighter_3";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FIGHTER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("NULL", "fighter_1", "Light", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;
	
	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreateOrderFighter(D3DXVECTOR3 pos)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_2";					//specify identifier
	graphic.fileName = "dark_fighter_2";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "ORDER_FIGHTER";
	ai.Faction		= "ORDER";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("ORDER_FIGHTER", "fighter_2", "Light", "NULL");
	a->m_Position = pos;
	m_TemplateFighterID = a->ID;

	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreateOrderFighter(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_2";					//specify identifier
	graphic.fileName = "dark_fighter_5";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "ORDER_FIGHTER";
	ai.Faction		= "ORDER";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("ORDER_FIGHTER", "fighter_2", "Light", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreatePhalanxFrigate(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_frigate";					//specify identifier
	graphic.fileName = "frigate_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "frigate";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 2.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FRIGATE";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FRIGATE";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("NULL", "phalanx_frigate", "frigate", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 80.0f;
	//scale up to correct size
	scale *= 5.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreatePhalanxCruiser(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_cruiser";					//specify identifier
	graphic.fileName = "battle_cruiser_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "cruiser";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 8.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_CRUISER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "CRUISER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "phalanx_cruiser", "cruiser", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 90.0f;
	//scale up
	scale *= 14.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreateFreighter5(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "freighter_5";					//specify identifier
	graphic.fileName = "transport_1E";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "freighter5";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 1.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_CRUISER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "CRUISER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "freighter_5", "freighter5", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 100.0f;
	//scale up
	scale *= 12.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* QuestDemo::CreateBattleShip(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_battleship_1";					//specify identifier
	graphic.fileName = "space_battleship_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "phalanx_battleship_1";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 8.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "phalanx_battleship_1", "phalanx_battleship_1", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 114.0f;
	//scale up
	scale *= 20.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

void QuestDemo::CreateAsteroid(D3DXVECTOR3 pos)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = 4;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = pos;
	asteroid->Graphics->m_scale = D3DXVECTOR3(2,2,2);
}

void QuestDemo::CreateAsteroid(float x, float y, float z)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = 4;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(x, y, z);
	asteroid->Graphics->m_scale = D3DXVECTOR3(2,2,2);
}

void QuestDemo::CreateAsteroid(D3DXVECTOR3 pos, float radius)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = radius;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(pos);
	asteroid->Graphics->m_scale = D3DXVECTOR3(radius/2, radius/2, radius/2);
}

void QuestDemo::CreateAsteroid(float x, float y, float z, float radius)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = radius;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(x,y,z);
	asteroid->Graphics->m_scale = D3DXVECTOR3(radius/2, radius/2, radius/2);
}

void QuestDemo::CreateHalfUnitSphere(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name = "HalfUnitSphere";
	graphic.fileName = "Sphere_halfUnit";
	graphic.Shader = "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* actor = WMI->CreateActor("NULL", "HalfUnitSphere", "NULL", "NULL");
	actor->m_Position = pos;
}

void QuestDemo::CreateHalfUnitSphere(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name = "HalfUnitSphere";
	graphic.fileName = "Sphere_halfUnit";
	graphic.Shader = "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* actor = WMI->CreateActor("NULL", "HalfUnitSphere", "NULL", "NULL");
	actor->m_Position = D3DXVECTOR3(x,y,z);
}


void QuestDemo::CreateWarpPortalConnector(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalConnector";
	graphic.fileName =	"space_portal_1";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalConnector", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 7.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 7.5f);
}

void QuestDemo::CreateWarpPortalConnector(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalConnector";
	graphic.fileName =	"space_portal_1";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalConnector", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(-1,0,0);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 7.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 7.5f);
}

void QuestDemo::CreateWarpPortalAnchor(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void QuestDemo::CreateWarpPortalAnchor(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(100.0f);
	w->SetSpeed(60.0f);

	w->SetSpin(true);

	float scale = 1.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void QuestDemo::CreateWarpPortalSystem(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalSystem";
	graphic.fileName =	"space_portal_2";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 4.5f);
}

void QuestDemo::CreateWarpPortalSystem(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalSystem";
	graphic.fileName =	"space_portal_2";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 4.5f);
}

void QuestDemo::CreateWarpLane(D3DXVECTOR3 start, D3DXVECTOR3 finish)
{
	WarpLane* wl = new WarpLane(start, finish);
	lanes.push_back(wl);
}

void QuestDemo::CreateWarpLane(float xStart, float yStart, float zStart, float xFinish, float yFinish, float zFinish)
{
	WarpLane* wl = new WarpLane(xStart, yStart, zStart, xFinish, yFinish, zFinish);
	lanes.push_back(wl);
}

void QuestDemo::ActorFaceDirection(Actor* actor, D3DXVECTOR3 dir)
{
	//D3DXVECTOR3 origHeading(-1,0,0);
	//D3DXVECTOR3 cross;
	//float angle;
	//D3DXQUATERNION quat, origQuat(0,0,0,1);
	//actor->m_Orientation = origQuat;

	//angle = acos(D3DXVec3Dot(&origHeading, &dir));
	//D3DXVec3Cross(&cross, &origHeading, &dir);
	////cross = D3DXVECTOR3(0,1,0);

	//D3DXQuaternionRotationAxis(&quat, &cross, angle);

	//actor->m_Orientation = quat;
	//((ShipAI*)(actor->AI))->SetMovementSphere(dir);


	if(dir == D3DXVECTOR3(-1,0,0))
	{
		D3DXQUATERNION quat(0,0,0,1);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(1,0,0))
	{
		D3DXVECTOR3 axis(0,-1,0);
		float angle = D3DX_PI;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(0,0,1))
	{
		D3DXVECTOR3 axis(0,1,0);
		float angle = D3DX_PI / 2;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(0,0,-1))
	{
		D3DXVECTOR3 axis(0,1,0);
		float angle = D3DX_PI * 3 / 2;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
}
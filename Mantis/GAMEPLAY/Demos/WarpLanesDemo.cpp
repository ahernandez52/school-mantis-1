#include "WarpLanesDemo.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "../../FRAMEWORK/Utility/Camera.h"
#include "../../FRAMEWORK/Input/RawInput.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../../AI/Influence/InfluenceSystem.h"
#include "../../AI/AIObject/ShipAI.h"
#include "../../AI/Steering/SteeringBehavior.h"
#include "../../AI/Messaging/PostOffice.h"

#include <random>
#include <chrono>

WarpLanesDemo::WarpLanesDemo(GameFramework* framework)
{
	m_Framework = framework;
}

WarpLanesDemo::WarpLanesDemo(GameFramework* framework, D3D9GraphicsCore* graphicsInterface)
{
	m_Framework = framework;
	m_GraphicsCore = graphicsInterface;

	//Allocate any state specific allocations
}

WarpLanesDemo::~WarpLanesDemo(void)
{
	//Deallocate any state specific allocations
}


// Public Functions
//============================================
void WarpLanesDemo::InitializeState(void)
{
	WMI->InitMemory();

	SetUpScene();
}

void WarpLanesDemo::LeaveState(void)
{
	WMI->CleanUp();

	//If influence has been used... Clear it
	ISI->ClearInfluence();
}

void WarpLanesDemo::Update(float dt)
{
	gCamera->SnapToLookAtPoint(currActor->m_Position);

	for(auto p : portals)
	{
		p->Update(dt);
	}

	for(auto wl : lanes)
	{
		wl->Update(dt);
	}

	warpSys.Update(dt);

	if(gRawInput->keyPressed(VK_UP))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(0,0,1);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_DOWN))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(0,0,-1);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_LEFT))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(-1,0,0);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}
	if(gRawInput->keyPressed(VK_RIGHT))
	{
		D3DXVECTOR3 normal = D3DXVECTOR3(1,0,0);
		D3DXVec3Normalize(&normal, &normal);

		
		ActorFaceDirection(currActor, normal);
		currActor->m_Position += normal;
	}

	if(gRawInput->keyPressed("R"))
	{
		currActor->m_Position = D3DXVECTOR3(0,0,0);
	}
}

void WarpLanesDemo::SetUpScene(void)
{
	gCamera->setSpeed(40);
	//gCamera->setMode(FREEFORM);
	//gCamera->setInput(XBOX_CONTROLLER);

	//CreateWarpLane(D3DXVECTOR3(0,0,25), D3DXVECTOR3(0,0,500));
	warpSys.CreateWarpLane(D3DXVECTOR3(0,0,25), D3DXVECTOR3(0,0,500));

	warpSys.DeActivatePortal(2);

	CreateOrderFighter(0,0,0);

	//CreateHalfUnitSphere(0,0,0 + 25);
	//CreateHalfUnitSphere(0,0,-1 + 25);
	//CreateHalfUnitSphere(0,0,-2 + 25);
	//CreateHalfUnitSphere(0,0,-3 + 25);
	//CreateHalfUnitSphere(0,0,-4 + 25);
	//CreateHalfUnitSphere(0,0,-5 + 25);
	//CreateHalfUnitSphere(0,0,-6 + 25);
	//CreateHalfUnitSphere(1,0,0 + 25);
	//CreateHalfUnitSphere(2,0,0 + 25);
	//CreateHalfUnitSphere(3,0,0 + 25);
	//CreateHalfUnitSphere(4,0,0 + 25);
	//CreateHalfUnitSphere(5,0,0 + 25);
	//CreateHalfUnitSphere(6,0,0 + 25);
	//CreateHalfUnitSphere(0,1,0 + 25);
	//CreateHalfUnitSphere(0,2,0 + 25);
	//CreateHalfUnitSphere(0,3,0 + 25);
	//CreateHalfUnitSphere(0,4,0 + 25);
	//CreateHalfUnitSphere(0,5,0 + 25);
	//CreateHalfUnitSphere(0,6,0 + 25);

}

void WarpLanesDemo::CreateWarpPortalConnector(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalConnector";
	graphic.fileName =	"space_portal_1";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalConnector", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 7.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 7.5f);
}

void WarpLanesDemo::CreateWarpPortalConnector(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalConnector";
	graphic.fileName =	"space_portal_1";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalConnector", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 7.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 7.5f);
}

void WarpLanesDemo::CreateWarpPortalAnchor(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void WarpLanesDemo::CreateWarpPortalAnchor(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name		= "WarpPortalAnchor";
	graphic.fileName	= "space_portal_3";
	graphic.Shader		= "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalAnchor", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(100.0f);
	w->SetSpeed(60.0f);

	w->SetSpin(true);

	float scale = 1.0f / 6.0f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 6.0f);
}

void WarpLanesDemo::CreateWarpPortalSystem(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalSystem";
	graphic.fileName =	"space_portal_2";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = pos;

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 4.5f);
}

void WarpLanesDemo::CreateWarpPortalSystem(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name =		"WarpPortalSystem";
	graphic.fileName =	"space_portal_2";
	graphic.Shader =	"UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* a = WMI->CreateActor("NULL", "WarpPortalSystem", "NULL", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);

	WarpPortal* w = new WarpPortal(a);
	a->Gameplay = w;
	portals.push_back(w);

	w->SetActive(true);
	w->SetFaction(TAAC);

	w->SetDirection(0,0,1);
	w->SetStrength(1000.0f);
	w->SetSpeed(200.0f);

	w->SetSpin(true);

	float scale = 1.0f / 4.5f;
	scale *= 6.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);

	w->SetTriggerRadius(scale * 4.5f);
}

void WarpLanesDemo::CreateWarpLane(D3DXVECTOR3 start, D3DXVECTOR3 finish)
{
	WarpLane* wl = new WarpLane(start, finish);
	lanes.push_back(wl);
}

void WarpLanesDemo::CreateWarpLane(float xStart, float yStart, float zStart, float xFinish, float yFinish, float zFinish)
{
	WarpLane* wl = new WarpLane(xStart, yStart, zStart, xFinish, yFinish, zFinish);
	lanes.push_back(wl);
}

Actor* WarpLanesDemo::CreatePhalanxFighter(D3DXVECTOR3 pos)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "military_fighter_3";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FIGHTER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("PHALANX_FIGHTER", "fighter_1", "Light", "NULL");
	a->m_Position = pos;
	m_TemplateFighterID = a->ID;
	
	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreatePhalanxFighter(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_1";					//specify identifier
	graphic.fileName = "military_fighter_3";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FIGHTER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("PHALANX_FIGHTER", "fighter_1", "Light", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;
	
	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreateOrderFighter(D3DXVECTOR3 pos)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_2";					//specify identifier
	graphic.fileName = "dark_fighter_2";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "ORDER_FIGHTER";
	ai.Faction		= "ORDER";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("ORDER_FIGHTER", "fighter_2", "Light", "NULL");
	a->m_Position = pos;
	m_TemplateFighterID = a->ID;

	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreateOrderFighter(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "fighter_2";					//specify identifier
	graphic.fileName = "dark_fighter_5";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "Light";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 0.5f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "ORDER_FIGHTER";
	ai.Faction		= "ORDER";
	ai.AIType		= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("ORDER_FIGHTER", "fighter_2", "Light", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	float scale = 1.0f / 60.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreatePhalanxFrigate(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_frigate";					//specify identifier
	graphic.fileName = "frigate_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "frigate";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 2.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_FRIGATE";
	ai.Faction		= "PHALANX";
	ai.AIType		= "FRIGATE";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	Actor* a = WMI->CreateActor("PHALANX_FRIGATE", "phalanx_frigate", "frigate", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 80.0f;
	//scale up to correct size
	scale *= 5.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreatePhalanxCruiser(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_cruiser";					//specify identifier
	graphic.fileName = "battle_cruiser_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "cruiser";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 8.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_CRUISER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "CRUISER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "phalanx_cruiser", "cruiser", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 90.0f;
	//scale up
	scale *= 14.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreateFreighter5(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "freighter_5";					//specify identifier
	graphic.fileName = "transport_1E";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "freighter5";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 1.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//AI schematic
	AISchematic ai;

	ai.Identifier	= "PHALANX_CRUISER";
	ai.Faction		= "PHALANX";
	ai.AIType		= "CRUISER";

	WMI->RegisterAISchematic(ai);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "freighter_5", "freighter5", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 100.0f;
	//scale up
	scale *= 12.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

Actor* WarpLanesDemo::CreateBattleShip(float x, float y, float z)
{
	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "phalanx_battleship_1";					//specify identifier
	graphic.fileName = "space_battleship_1";				//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Physics schematic
	PhysicsSchematic physics;

	physics.Identifier = "phalanx_battleship_1";
	physics.Type = 1;							// (1 is sphere type)
	physics.Mass = 150.0f;						// Mass
	physics.Moveable = true;					// Can object move?
	physics.Radius = 8.0f;						// Bounding volume radius
												// battle cruiser is 57.0f

	WMI->RegisterPhysicsSchematic(physics);

	//Create actor using schematics
	//Actor* a = WMI->CreateActor("PHALANX_CRUISER", "phalanx_cruiser", "cruiser", "NULL");
	Actor* a = WMI->CreateActor("NULL", "phalanx_battleship_1", "phalanx_battleship_1", "NULL");
	a->m_Position = D3DXVECTOR3(x,y,z);
	m_TemplateFighterID = a->ID;

	//scale down to fighter size
	float scale = 1.0f / 114.0f;
	//scale up
	scale *= 20.0f;
	a->Graphics->m_scale = D3DXVECTOR3(scale,scale,scale);

	lastActor = currActor;
	currActor = a;
	return a;
}

void WarpLanesDemo::CreateAsteroid(D3DXVECTOR3 pos)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = 4;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = pos;
	asteroid->Graphics->m_scale = D3DXVECTOR3(2,2,2);
}

void WarpLanesDemo::CreateAsteroid(float x, float y, float z)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = 4;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(x, y, z);
	asteroid->Graphics->m_scale = D3DXVECTOR3(2,2,2);
}

void WarpLanesDemo::CreateAsteroid(D3DXVECTOR3 pos, float radius)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = radius;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(pos);
	asteroid->Graphics->m_scale = D3DXVECTOR3(radius/2, radius/2, radius/2);
}

void WarpLanesDemo::CreateAsteroid(float x, float y, float z, float radius)
{
	//Physics schematic
	PhysicsSchematic physics;

	//Physics schematic
	physics.Identifier = "Asteroid";
	physics.Type = 1;
	physics.Mass = 200.0f;
	physics.Radius = radius;

	WMI->RegisterPhysicsSchematic(physics);

	//Graphic schematic
	GraphicSchematic graphic;

	graphic.name = "Asteroid";					//specify identifier
	graphic.fileName = "crystal_1";		//specify folder name
	graphic.Shader = "UberShaderOne";			//specify shader to use (NYI)

	WMI->RegisterGraphicSchematic(graphic);

	//Create astroid using schematics
	Actor* asteroid = WMI->CreateActor("NULL", "Asteroid", "Asteroid", "NULL");
	asteroid->m_Position = D3DXVECTOR3(x,y,z);
	asteroid->Graphics->m_scale = D3DXVECTOR3(radius/2, radius/2, radius/2);
}

void WarpLanesDemo::CreateHalfUnitSphere(D3DXVECTOR3 pos)
{
	GraphicSchematic graphic;

	graphic.name = "HalfUnitSphere";
	graphic.fileName = "Sphere_halfUnit";
	graphic.Shader = "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* actor = WMI->CreateActor("NULL", "HalfUnitSphere", "NULL", "NULL");
	actor->m_Position = pos;
}

void WarpLanesDemo::CreateHalfUnitSphere(float x, float y, float z)
{
	GraphicSchematic graphic;

	graphic.name = "HalfUnitSphere";
	graphic.fileName = "Sphere_halfUnit";
	graphic.Shader = "UberShaderOne";

	WMI->RegisterGraphicSchematic(graphic);

	Actor* actor = WMI->CreateActor("NULL", "HalfUnitSphere", "NULL", "NULL");
	actor->m_Position = D3DXVECTOR3(x,y,z);
}




void WarpLanesDemo::ActorFaceDirection(Actor* actor, D3DXVECTOR3 dir)
{
	//D3DXVECTOR3 origHeading(-1,0,0);
	//D3DXVECTOR3 cross;
	//float angle;
	//D3DXQUATERNION quat, origQuat(0,0,0,1);
	//actor->m_Orientation = origQuat;

	//angle = acos(D3DXVec3Dot(&origHeading, &dir));
	//D3DXVec3Cross(&cross, &origHeading, &dir);
	////cross = D3DXVECTOR3(0,1,0);

	//D3DXQuaternionRotationAxis(&quat, &cross, angle);

	//actor->m_Orientation = quat;
	//((ShipAI*)(actor->AI))->SetMovementSphere(dir);


	if(dir == D3DXVECTOR3(-1,0,0))
	{
		D3DXQUATERNION quat(0,0,0,1);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(1,0,0))
	{
		D3DXVECTOR3 axis(0,-1,0);
		float angle = D3DX_PI;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(0,0,1))
	{
		D3DXVECTOR3 axis(0,1,0);
		float angle = D3DX_PI / 2;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
	else if(dir == D3DXVECTOR3(0,0,-1))
	{
		D3DXVECTOR3 axis(0,1,0);
		float angle = D3DX_PI * 3 / 2;
		D3DXQUATERNION quat(0,0,0,1);

		//D3DXQuaternionRotationAxis(&quat, &axis, angle);
		D3DXQuaternionRotationYawPitchRoll(&quat, angle, 0, 0);

		actor->m_Orientation = quat;
	}
}

#pragma once
#include "../FRAMEWORK/GSM/GameState.h"

class LoadingState01 : public GameState
{
private:

public:

	void InitializeState();
	void EnterState();
	void LeaveState();
	void Update(float dt);
};
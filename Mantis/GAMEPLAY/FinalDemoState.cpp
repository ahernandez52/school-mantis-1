#include "FinalDemoState.h"
#include "AllGameStates.h"

#include "../GRAPHICS/D3D9GraphicsCore.h"
#include "../GRAPHICS/Gui/DeathSplash.h"
#include "../GRAPHICS/Gui/HUD.h"
#include "../GRAPHICS/Gui/PauseMenu.h"
#include "../GRAPHICS/Gui/OptionsMenu.h"

#include "../FRAMEWORK/Factories/LevelFactory.h"
#include "../FRAMEWORK/Input/RawInput.h"
#include "../FRAMEWORK/Managers/WorldManager.h"
#include "../FRAMEWORK/Sound/Audio.h"
#include "../FRAMEWORK/Utility/Camera.h"

#include "../GAMEPLAY/Object/Asteroid.h"
#include "../GAMEPLAY/Object/Player.h"

#include "../AI/AIObject/ShipAI.h"
#include "../AI/Messaging/PostOffice.h"
#include "../AI/States/FighterAttack.h"
#include "../AI/States/FighterEvade.h"
#include "../AI/States/FighterMove.h"
#include "../AI/Steering/Formation.h"
#include "../AI/Steering/SteeringBehavior.h"

#include <chrono>
#include <random>


// Public Functions
//============================================
void FinalDemoState::InitializeState()
{
	WMI->InitMemory();

	Gameplay->CleanUp();
	Framework->GetGui()->m_HUD->ClearHUD();

	LoadSounds();
	LoadSchematics();
	BuildWorld();

	//gCamera->setMode(FREEFORM);

	Graphics->SetGfxStatsRender(false);

	//Init player and store Player Actor
	Gameplay->InitPlayer();
	Player* player = Gameplay->GetPlayer();
	PlayerActor = player->GetActor();

	//Attatch Player to AI states
	FighterAttack::Instance()->SetPlayer(PlayerActor);
	FighterEvade::Instance()->SetPlayer(PlayerActor);
	FighterMove::Instance()->SetPlayer(PlayerActor);

	//Set GUI Functions
	Framework->GetGui()->m_Options->SetMainMenuFlag(false);
	Framework->GetGui()->m_HUD->setVisible(true);
	Framework->GetGui()->m_HUD->SetPlayer(Gameplay->GetPlayer());

	m_WaveNumber = 0;

	LoadSounds();
	SCI->PlayMusic("Assets/Sounds/Music/demoMusic.mp3", true);
	Framework->GetGui()->m_HUD->SetWaveNumber(m_WaveNumber);
}

void FinalDemoState::LeaveState()
{
	SCI->ReleaseSound("Assets/Sounds/Music/demoMusic.mp3");

	Gameplay->CleanUp();
	WMI->CleanUp();
	POST->ClearPostOffice();
	Framework->GetGui()->m_PauseMenu->setVisible(false);
	Framework->GetGui()->m_HUD->setVisible(false);
	Framework->SetPause(false);
}

void FinalDemoState::EnterState()
{

}

void FinalDemoState::Update(float dt)
{
	UpdateStaticObjects(dt);


	

	if(gRawInput->keyPressed("H"))
	{
		Graphics->ReloadParticles();
		Gameplay->m_BattleSystem->m_Particles = Graphics->m_ParticleMan;
	}

	if(gRawInput->keyPressed("G"))
	{
		//explosion 1.8
		//shield 2.0
		//ParticleEmitter *temp = Graphics->CreateEmitter("ShieldImpact", 2.0f);
		//temp->m_Position = D3DXVECTOR3(0.0f, 0.0f, 50.0f);
		//WMI->SpawnProjectile(1, D3DXVECTOR3(0.0f, 0.0f, 100.0f), D3DXVECTOR3(1.0f, 0.0f, 0.0f));
	}

	if(!Framework->isPaused())
	{
		if(gRawInput->keyPressed("E"))
		{
			if(Gameplay->GetPlayer()->m_cruising)
				Gameplay->GetPlayer()->StopCruise();
			else
				Gameplay->GetPlayer()->StartCruise();
		}

		//if(gRawInput->keyPressed("Q"))
		//{
		//	Gameplay->GetPlayer()->BoostON();
		//}
		//else
		//{
		//	Gameplay->GetPlayer()->BoostOFF();
		//}
		
		if(gRawInput->keyReleased("R"))
		{
			Framework->GetGui()->m_HUD->SelectTarget();
		}

		if(	static_cast<Player*>(PlayerActor->Gameplay)->IsDead() )
		{
			Framework->GetGui()->m_DeathSplash->ShowDeathSplash();
		}

		if(gRawInput->keyReleased("1"))
		{
			for(auto NPC : Gameplay->GetNPCs())
			{
				NPC->TakeDamage(10000);
			}
		}
	}

	if(gRawInput->keyPressed(VK_ESCAPE) && !Framework->GetGui()->m_DeathSplash->isVisible())
	{
		if(Framework->GetGui()->m_Options->isVisible())
		{
			Framework->GetGui()->m_Options->setVisible(false);
			Framework->GetGui()->m_PauseMenu->setVisible(true);
		}
		else
		{
			Framework->GetGui()->m_HUD->ToggleVisible();
			Framework->GetGui()->m_PauseMenu->ToggleVisible();
			Framework->TogglePause();
		}
	}

	//If no active npc's spawn a wave
	if (Gameplay->GetNPCs().size() == 0)
	{
		static float CountDown = 10.0f;
		CountDown -= dt;

		Framework->GetGui()->m_HUD->UpdateWaveMarker(CountDown);

		if(CountDown <= 0.0)
		{
			CreateWave();
			CountDown = 10.0f;
			Framework->GetGui()->m_HUD->SetWaveNumber(m_WaveNumber);
		}
	}
}
//********************************************

void FinalDemoState::PauseGame()
{
	Framework->SetPause(true);
	Framework->GetGui()->m_PauseMenu->setVisible(true);
	Framework->GetGui()->m_HUD->setVisible(false);
}

void FinalDemoState::ResumeGame()
{
	Framework->SetPause(false);
	Framework->GetGui()->m_PauseMenu->setVisible(false);
	Framework->GetGui()->m_HUD->setVisible(true);
}

void FinalDemoState::Options()
{
	Framework->GetGui()->m_Options->setVisible(true);
	Framework->GetGui()->m_PauseMenu->setVisible(false);
}

void FinalDemoState::QuitMenu()
{
	Framework->getGSM()->SetNextState(new MainMenuState());

	Framework->getGSM()->LoadNextState();
}

void FinalDemoState::QuitDesktop()
{
	PostQuitMessage(0);
}


//==============================================================
//
//	Actor and Ship Creation Functions
//
//==============================================================

void FinalDemoState::UpdateStaticObjects(float dt)
{
	static float planetRoation = 0.0f;
	float speed = 0.007f * dt;
	D3DXQuaternionRotationYawPitchRoll(&planet->m_Orientation, planetRoation, 0.0f, 0.0f);
	planetRoation += speed;
}

void FinalDemoState::CreateWave(void)
{
	m_WaveNumber++;

	CreatePirates(m_WaveNumber);
}

void FinalDemoState::CreatePirates(int amount)
{
	std::mt19937 rand((int)std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> X(-6000,6000);
	std::uniform_int_distribution<int> Y(-700, 700);
	std::uniform_int_distribution<int> Z(-5000,5000);
	std::uniform_int_distribution<int> ship(0,5);

	std::string Ships[] = {"pirate1",
						   "pirate2",
						   "pirate3",
						   "pirate4",
						   "pirate5",
						   "pirate6"};

	std::string ShipsLP[] = {"pirate1LP",
						     "pirate2LP",
						     "pirate3LP",
						     "pirate4LP",
						     "pirate5LP",
						     "pirate6LP"};

	for(int x = 0; x < amount; ++x)
	{
		int shipPick = ship(rand);
		string shipChoice = "pirate";

		shipChoice += to_string(shipPick);

		int i = ship(rand);
		//Actor* a = WMI->CreateActor("fighter", shipChoice, "light", "NULL");
		Actor* a = WMI->CreateActor("fighter", Ships[i], "light", "NULL");
		Gameplay->CreateNPC(a, "pirate_1");
		a->m_Position = D3DXVECTOR3((float)X(rand), (float)Y(rand), (float)Z(rand));
		((ShipAI*)a->AI)->ChangeState(FighterAttack::Instance());

		((Ship*)(a->Gameplay))->m_ShieldMesh = new D3D9Object(a, WMI->GetGraphicsSchematic(ShipsLP[i]));
		((Ship*)(a->Gameplay))->m_ShieldMesh->SetEffect(FX_SHIELD);
		((Ship*)(a->Gameplay))->m_ShieldColor = Vector4(1.0f, 0.0f, 0.0f, 1.0f);
		WMI->RegisterGraphicObject(((Ship*)(a->Gameplay))->m_ShieldMesh);

		((Ship*)(a->Gameplay))->EquipWeapon(2, 0.25f, Vector3(0.0f, 0.0f, 0.0f));
		((Ship*)(a->Gameplay))->SetHull(40.0f);
		((Ship*)(a->Gameplay))->SetShield(20.0f);

	}
}

void FinalDemoState::LoadSchematics()
{
	//graphics
	GraphicSchematic graphic;

	graphic.fileName	= "sun2";
	graphic.name		= "Sun";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "space_station_6";
	graphic.name		= "station";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_1";
	graphic.name		= "Asteroid1";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_2";
	graphic.name		= "Asteroid2";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_3";
	graphic.name		= "Asteroid3";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_4";
	graphic.name		= "Asteroid4";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Asteroid_5";
	graphic.name		= "Asteroid5";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "planet_desert_1";
	graphic.name		= "planet";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_1";
	graphic.name		= "pirate1";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_1_LP";
	graphic.name		= "pirate1LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_2";
	graphic.name		= "pirate2";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_2_LP";
	graphic.name		= "pirate2LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_3";
	graphic.name		= "pirate3";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_3_LP";
	graphic.name		= "pirate3LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_4";
	graphic.name		= "pirate4";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_4_LP";
	graphic.name		= "pirate4LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_5";
	graphic.name		= "pirate5";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_5_LP";
	graphic.name		= "pirate5LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "dark_fighter_6";
	graphic.name		= "pirate6";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Dark_Fighter_6_LP";
	graphic.name		= "pirate6LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName	= "Sphere_halfUnit";
	graphic.name		= "sphere";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName = "military_fighter_4";
	graphic.name = "player4";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName = "Military_Fighter_4_LP";
	graphic.name = "player4LP";
	WMI->RegisterGraphicSchematic(graphic);

	graphic.fileName = "transport_2E";
	graphic.name = "transport";
	WMI->RegisterGraphicSchematic(graphic);

	//graphic.name = "Mfighter_4_LP";					
	//graphic.fileName = "Military_Fighter_4_LP";					
	//WMI->RegisterGraphicSchematic(graphic);

	//Physics
	PhysicsSchematic physics;

	physics.Identifier	= "light";
	physics.Type		= 1;
	physics.Mass		= 1.0f;
	physics.Moveable	= true;
	physics.Radius		= 30.0f;

	WMI->RegisterPhysicsSchematic(physics);

	physics.Identifier	= "militaryFighter4";
	physics.Type		= 1;							
	physics.Mass		= 1.0f;						
	physics.Moveable	= true;	
	physics.Radius		= 30.0f;
	//physics.Mesh=	WMI->GetGraphicsSchematic("Mfighter_4_LP").mesh;
	//physics.MeshName = "militaryfighter1.1,1,1";
	//physics.HalfExtents = D3DXVECTOR3(1.0f,1.0f,1.0f);

	WMI->RegisterPhysicsSchematic(physics);

	//AI
	AISchematic ai;

	ai.Identifier		= "fighter";
	ai.Faction			= "PHALANX";
	ai.AIType			= "FIGHTER";

	WMI->RegisterAISchematic(ai);

	ai.Identifier		= "player";
	ai.Faction			= "UNDECLARED";
	ai.AIType			= "PLAYER";

	WMI->RegisterAISchematic(ai);

}


inline float Distance(Vector3 source, Vector3 target)
{
	Vector3 midline = source - target;
	return fabs(midline.Length());
}

void FinalDemoState::BuildWorld(void)
{
	Actor* newActor;

	newActor = WMI->CreateActor("NULL", "Sun", "NULL", "NULL");
	newActor->m_Position = D3DXVECTOR3(500.0f, 400, -17000.0f);
	newActor->Graphics->m_scale = D3DXVECTOR3(150.0f, 150.0f, 150.0f);

	station = WMI->CreateActor("NULL", "station", "NULL", "NULL");
	station->Graphics->m_scale = D3DXVECTOR3(15, 15, 15);
	station->m_Position = D3DXVECTOR3(2000.0f, 0.0f, -800.0f);
	Quaternion rotation;
	rotation.BuildRotYawPitchRoll(D3DXToRadian(-50), 0, 0);
	station->m_Orientation = rotation;

	planet = WMI->CreateActor("NULL", "planet", "NULL", "NULL");
	planet->Graphics->m_scale = D3DXVECTOR3(4000,4000,4000);
	planet->m_Position = D3DXVECTOR3(-12000,0,0);

#ifdef _DEBUG
	//Do not build Asteroid Field in Debug!
#else
	BuildAsteroidField();
#endif
}

void FinalDemoState::BuildAsteroidField()
{
	Actor *newActor;

	string Asteroids[] = {"Asteroid1",
						  "Asteroid2",
						  "Asteroid3",
						  "Asteroid4",
						  "Asteroid5"};

	//Generate Asteroid Field Positions
	//std::mt19937 rand((int)std::chrono::system_clock::now().time_since_epoch().count());
	std::mt19937 rand(654564);
	std::uniform_int_distribution<int> X(-7000,7000);
	std::uniform_int_distribution<int> Y(-700,700);
	std::uniform_int_distribution<int> Z(-6000,6000);
	std::uniform_int_distribution<int> asteroid(0,4);
	std::uniform_int_distribution<int> Scale(12,34);
	std::uniform_real_distribution<float> Rotation(0, 359);

	vector<Vector3> RandPositions;

	Vector3 center(0.0f, 0.0f, 0.0f);
	float radius = 3000.0f;
	float distance = 0.0f;

	//Generate box of asteroids
	for(int i = 0; i < 280; ++i)
	{
		RandPositions.push_back(Vector3((float)X(rand), (float)Y(rand), (float)Z(rand)));
	}

	//Remove center for field of battle
	for(vector<Vector3>::iterator it = RandPositions.begin();
		it != RandPositions.end();
		++it)
	{
		distance = Distance(center, (*it));
		if(distance < radius)
		{
			it = RandPositions.erase(it);

			if(it != RandPositions.begin())
				--it;
		}
	}

	//Assign remaining position to actors
	float scale;
	Quaternion newRotation;

	for(auto position : RandPositions)
	{
		newActor = WMI->CreateActor("NULL", Asteroids[asteroid(rand)], "NULL", "NULL");

		scale = (float)Scale(rand);
		newRotation.BuildRotYawPitchRoll(Rotation(rand), Rotation(rand), Rotation(rand));

		newActor->Graphics->m_scale = D3DXVECTOR3(scale, scale, scale);
		newActor->m_Orientation = newRotation;
		newActor->m_Position = position;

		newActor->Gameplay = new Asteroid(newActor);
	}
}

void FinalDemoState::LoadSounds(void)
{
	//SCI->LoadSound("Assets/Sounds/Effects/fire_placeholder.wav");
	//SCI->LoadSound("Assets/Sounds/Effects/shield.wav");
	//SCI->LoadSound("Assets/Sounds/Effects/hull.wav");
	//
	SCI->LoadMusic("Assets/Sounds/Music/demoMusic.mp3", true);
}

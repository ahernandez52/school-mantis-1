#pragma once
#include "../FRAMEWORK/GSM/GameState.h"
#include "Classes/Warp/WarpController.h"
#include "Classes\Quest\QuestSystem.h"
#include "MainMenuState.h"


class FinalDemoState : public GameState
{
private:

	Actor* PlayerActor;

	Actor* lastActor;
	Actor* currActor;

	int m_WaveNumber;

public:

	Actor* station;
	Actor* planet;
	

public:

	FinalDemoState(){}
	~FinalDemoState(){}

	//virtual ~GraphicsDemoState();

	void InitializeState();
	void LeaveState();
	void EnterState();
    void Update(float dt); 

	void PauseGame();
	void ResumeGame();
	void Options();
	void QuitMenu();
	void QuitDesktop();

private:

	void UpdateStaticObjects(float dt);

	//Wave creation
	void CreateWave(void);
	
	//Pirate Creation
	void CreatePirates(int amount);

	//World Creation Functions
	void LoadSchematics();
	void BuildWorld();

	void BuildAsteroidField();

	//Load sound assets
	void LoadSounds(void);
	
};


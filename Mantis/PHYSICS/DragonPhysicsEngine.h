#pragma once

#include "Objects/PhysicsObject.h"
#include <vector>
#include "Contacts/DragonCollisionEngine.h"
#include "Forces/DragonForceEngine.h"

class DragonPhysicsEngine
{
private:
	bool m_CollisionDetectionOn;
	bool m_CollisionResolutionOn;
	DragonForceEngine* m_DragonForceEngine;
	DragonCollisionEngine* m_CollisionEngine;
public:
	DragonPhysicsEngine();
	~DragonPhysicsEngine();
	inline void TurnOffCollisionDetection()
	{
		m_CollisionDetectionOn=false;
	}
	inline void TurnOnCollisionDetection()
	{
		m_CollisionDetectionOn=true;
	}
	inline void TurnCollisionResolutionOff()
	{
		m_CollisionResolutionOn=false;
	}
	inline void TurnCollisionResolutionOn()
	{
		m_CollisionResolutionOn=true;
	}
	void Update(real dt);
	void ResolveContacts(real dt);	
	inline DragonForceEngine* GetDragonForceEngine()
	{
		return m_DragonForceEngine;
	}
	std::vector<ActorContactPair> GetContactPairs();
	std::vector<DragonContact*> GetContactsByPairOfEntities(PhysicsObject* obj1, PhysicsObject* obj2);		
	std::vector<DragonXVector3*> GetVertexPositionsFromMesh(ID3DXMesh* mesh);		
};

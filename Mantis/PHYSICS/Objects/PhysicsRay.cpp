#include "PhysicsRay.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../Utility/DragonEnums.h"

PhysicsRay::PhysicsRay(Actor* actr):PhysicsObject(actr,999999.9f)
{
	SetPhysicsType(COT_Game);
	SetPrimitiveType(PT_Ray);
	SetInverseInertiaTensor(DragonXMatrix().GetInverse());
}
PhysicsRay::~PhysicsRay()
{

}
DragonXVector3 PhysicsRay::GetNormal()
{
	return DragonXVector3(2.0f*(GetOrientation().x*GetOrientation().z+GetOrientation().w*GetOrientation().y),
		2.0f*(GetOrientation().y*GetOrientation().z-GetOrientation().w*GetOrientation().x),
		1.0f-2.0f*(GetOrientation().x*GetOrientation().x+GetOrientation().y*GetOrientation().y));
}
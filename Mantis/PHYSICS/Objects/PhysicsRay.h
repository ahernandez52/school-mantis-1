#pragma once

#include "PhysicsObject.h"

class PhysicsRay : public PhysicsObject
{
private:

public:
	PhysicsRay(Actor* actr);
	~PhysicsRay();
	DragonXVector3 GetNormal();
};
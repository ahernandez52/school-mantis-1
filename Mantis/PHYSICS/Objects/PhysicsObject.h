#pragma once
#include "../Utility/DragonXMath.h"
#include <vector>

class Actor;
class DragonCollisionMesh;

// the class mantis physics can be renamed for use in any other physics engine just be sure to change it everywhere
// this is the base physics object that will represent the physics properties

class PhysicsObject
{
public:

	int m_PhysicsType;
	int m_PrimitiveType;

	bool m_CanSleep;
	bool m_IsAwake;
	bool m_IsCollisionResolutionOn;
	bool m_IsCollisionDetectionOn;	

	real m_InverseMass;
	real m_LinearDampening;
	real m_AngularDampening;
	real m_Mass;
	real m_Motion;	
	real m_Radius;
	real m_DistanceFromOrigin;

	std::string m_CollisionMeshName;

	Actor* m_Actor;

	DragonXVector3 m_Position;
	DragonXVector3 m_LinearVelocity;
	DragonXVector3 m_RotationalVelocity;	
	DragonXVector3 m_LastFrameAccel;
	DragonXVector3 m_LinearForceAccum;
	DragonXVector3 m_AngularForceAccum;
	DragonXVector3 m_LinearAccel;	
	DragonXVector3 m_PlaneNormal;
	DragonXVector3 m_HalfExtents;

	DragonXMatrix m_InverseInertiaTensor;
	DragonXMatrix m_InverseInertiaTensorWorld;
	DragonXMatrix m_TransformMatrix;

	DragonXQuaternion m_QuatRot;

	DragonCollisionMesh* m_CollisionMesh;
private:
	void SetInverseInertiaTensor();
public:	
	PhysicsObject() {}
	PhysicsObject(Actor* actr, PhysicsSchematic schematic);

	virtual~PhysicsObject()
	{
	}	

	void Update(real dt);
	void UpdateMatricies();

	//Forces
	void AddForce(const DragonXVector3 &force);
	void AddForceAtPoint(const DragonXVector3 &force, const DragonXVector3 &point);
	void AddForceAtBodyPoint(const DragonXVector3 &force, const DragonXVector3 &point);
	void AddTorque(const DragonXVector3 &torque);
	void AddRotation(const DragonXVector3 &deltaRotation);
	void AddVelocity(const DragonXVector3 &deltaVelocity);

	void ClearAccumulators();	

	//Sleeping
	void SetAwake(const bool awake=true);
	void SetCanSleep(const bool canSleep=true);

	//Mass
	real GetMass();
	void SetMass(real mass);
	void SetInverseMass(real InvMass);
	bool HasFiniteMass();

	//Position
	void SetPosition(DragonXVector3& v);

	//Orientation
	void SetOrientation(const DragonXQuaternion &orientation);

	//Points
	DragonXVector3 GetPointInLocalSpace(const DragonXVector3 &point) const;
	DragonXVector3 GetPointInWorldSpace(const DragonXVector3 &point) const;
	DragonXVector3 GetDirectionInLocalSpace(const DragonXVector3 &direction) const;
	DragonXVector3 GetDirectionInWorldSpace(const DragonXVector3 &direction) const;

	// special case
	DragonXVector3 GetNormal();
};

#include "PhysicsObject.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../Utility/DragonEnums.h"
#include "../Utility/DragonPhysicsManager.h"
#include <cassert>

real sleepEpsilon=0.05f;
const real REAL_MAX = 99999999.9f;

/**
* Inline function that creates a transform matrix from a
* position and m_QuatRot.
*/
inline void _calculateTransformMatrix(DragonXMatrix &TransformMatrix,const DragonXVector3 &position,const DragonXQuaternion &m_QuatRot)
{
	TransformMatrix._11 = 1-2*m_QuatRot.y*m_QuatRot.y-2*m_QuatRot.z*m_QuatRot.z;
	TransformMatrix._12 = 2*m_QuatRot.x*m_QuatRot.y -2*m_QuatRot.w*m_QuatRot.z;
	TransformMatrix._13 = 2*m_QuatRot.x*m_QuatRot.z +2*m_QuatRot.w*m_QuatRot.y;
	TransformMatrix._14 = position.x;

	TransformMatrix._21 = 2*m_QuatRot.x*m_QuatRot.y +2*m_QuatRot.w*m_QuatRot.z;
	TransformMatrix._22 = 1-2*m_QuatRot.x*m_QuatRot.x-2*m_QuatRot.z*m_QuatRot.z;
	TransformMatrix._23 = 2*m_QuatRot.y*m_QuatRot.z -2*m_QuatRot.w*m_QuatRot.x;
	TransformMatrix._24 = position.y;

	TransformMatrix._31 = 2*m_QuatRot.x*m_QuatRot.z -2*m_QuatRot.w*m_QuatRot.y;
	TransformMatrix._32 = 2*m_QuatRot.y*m_QuatRot.z +2*m_QuatRot.w*m_QuatRot.x;
	TransformMatrix._33 = 1-2*m_QuatRot.x*m_QuatRot.x-2*m_QuatRot.y*m_QuatRot.y;
	TransformMatrix._34 = position.z;
}

PhysicsObject::PhysicsObject(Actor* actr, PhysicsSchematic schematic)
{
	m_Actor = actr;
	m_Position=actr->m_Position;
	m_QuatRot=actr->m_Orientation;

	m_PrimitiveType=schematic.Type;
	if(!schematic.Moveable)
	{
		m_PhysicsType=COT_World;
		m_InverseMass=0.0f;
		m_Mass=9999999999.9f;
	}
	else
	{
		m_PhysicsType=COT_Game;
		m_InverseMass=1.0f/schematic.Mass;
		m_Mass=schematic.Mass;
	}
	switch(m_PrimitiveType)
	{
	case PT_Sphere:
		{
			m_Radius=schematic.Radius;
			break;
		}
	case PT_OBB:
		{
			m_HalfExtents=schematic.HalfExtents;
			m_Radius=m_HalfExtents.GetMagnitude();
			break;
		}
	case PT_Plane:
		{
			m_DistanceFromOrigin=schematic.DistanceFromOrigin;
			m_PlaneNormal=schematic.PlaneNormalDirection;
		}
	case PT_Mesh:
		{
			m_CollisionMeshName=schematic.MeshName;
			m_CollisionMesh = DPMI->RegisterNewCollisionMesh(schematic.Mesh,schematic.MeshName,schematic.HalfExtents);
			m_HalfExtents=m_CollisionMesh->m_HalfSizes;
			m_Radius=m_CollisionMesh->m_Radius;
		}
	default:
		{
			break;
		}
	}
	m_LinearVelocity= DragonXVector3(0.0f,0.0f,0.0f);
	m_LinearForceAccum= DragonXVector3(0.0f,0.0f,0.0f);
	m_AngularForceAccum=DragonXVector3(0.0f,0.0f,0.0f);
	m_RotationalVelocity=DragonXVector3(0.0f,0.0f,0.0f);
	m_LastFrameAccel=DragonXVector3(0.0f,0.0f,0.0f);
	m_LinearAccel= DragonXVector3(0.0f,0.0f,0.0f);	
		
	m_LinearDampening=0.99f;
	m_AngularDampening=0.01f;
	m_Motion=0.0f;
	
	m_IsAwake=true;
	m_CanSleep=true;
	m_IsCollisionResolutionOn=true;
	m_IsCollisionDetectionOn=true;
	SetInverseInertiaTensor();
}
// Private Functions
//=================================
void PhysicsObject::SetInverseInertiaTensor()
{
	switch (m_PrimitiveType)
	{
	case PT_Sphere:
		{
			DragonXMatrix m = DragonXMatrix();
			D3DXMatrixIdentity(&m);
			real i = m_Mass*0.4f*(m_Radius*m_Radius);
			m._11=i;
			m._22=i;
			m._33=i;
			D3DXMatrixInverse(&m,NULL,&m);
			m_InverseInertiaTensor= m;
			break;
		}
	case PT_OBB:
		{
			DragonXMatrix m = DragonXMatrix();
			D3DXMatrixIdentity(&m);
			m._11=(real)0.08334*m_Mass*((m_HalfExtents.y*m_HalfExtents.y)+(m_HalfExtents.z*m_HalfExtents.z));
			m._22=(real)0.08334*m_Mass*((m_HalfExtents.x*m_HalfExtents.x)+(m_HalfExtents.z*m_HalfExtents.z));
			m._33=(real)0.08334*m_Mass*((m_HalfExtents.x*m_HalfExtents.x)+(m_HalfExtents.y*m_HalfExtents.y));
			m_InverseInertiaTensor= m.GetInverse();
			break;
		}
	case PT_Ray:
		{
			DragonXMatrix m = DragonXMatrix();
			m_InverseInertiaTensor=m.GetInverse();
			break;
		}
	case PT_Mesh:
		{
			DragonXMatrix m = DragonXMatrix();
			D3DXMatrixIdentity(&m);
			m._11=(real)0.2f*m_Mass*((m_HalfExtents.y*m_HalfExtents.y)+(m_HalfExtents.z*m_HalfExtents.z));
			m._22=(real)0.2f*m_Mass*((m_HalfExtents.x*m_HalfExtents.x)+(m_HalfExtents.z*m_HalfExtents.z));
			m._33=(real)0.2f*m_Mass*((m_HalfExtents.x*m_HalfExtents.x)+(m_HalfExtents.y*m_HalfExtents.y));
			m_InverseInertiaTensor= m.GetInverse();
			break;
		}
	default:
		{
			break;
		}
		break;
	}
}
//********************************************


// Public Functions
// =================================
void PhysicsObject::Update(real dt)
{
	m_Position = m_Actor->m_Position;
	m_QuatRot = m_Actor->m_Orientation;
	m_QuatRot.Normalize();

	if(!m_IsAwake)
	{
		m_Actor->m_Position=m_Position;
		m_Actor->m_Orientation=m_QuatRot;
		return;
	}

	// Calculate linear acceleration from force inputs.
	m_LastFrameAccel=m_LinearAccel;
	m_LastFrameAccel+=m_LinearForceAccum*m_InverseMass;	

	// Calculate angular acceleration from torque inputs.
	DragonXVector3 angularAcceleration=m_InverseInertiaTensorWorld.Transform(m_AngularForceAccum);

	// Adjust velocities
	// Update linear velocity from both acceleration and impulse.
	m_LinearVelocity+=m_LastFrameAccel*dt;

	// Update angular velocity from both acceleration and impulse.
	m_RotationalVelocity+=angularAcceleration*dt;

	// Impose drag.
	m_LinearVelocity*=pow(m_LinearDampening,dt);
	m_RotationalVelocity*=pow(m_AngularDampening,dt);

	// Adjust positions
	// Update linear position.
	m_Position+=m_LinearVelocity*dt;	

	// Update angular position.
	m_QuatRot.AddScaledVectorDrX(m_RotationalVelocity,dt);	

	// Normalise the m_QuatRot, and update the matrices with the new
	// position and m_QuatRot
	UpdateMatricies();

	// Clear accumulators.
	ClearAccumulators();

	// Update the kinetic energy store, and possibly put the body to
	// sleep.
	if(m_CanSleep)
	{
		real currentMotion=m_LinearVelocity*m_LinearVelocity+m_RotationalVelocity*m_RotationalVelocity;

		real bias=(real)pow(0.5,dt);
		m_Motion=bias*m_Motion+(1-bias)*currentMotion;

		if(m_Motion<sleepEpsilon) 
		{
			SetAwake(false);
		}
		else if(m_Motion>10*sleepEpsilon)
		{
			m_Motion=10*sleepEpsilon;
		}
	}
	// finally update the actors position and orientation
	m_Actor->m_Position=m_Position;
	m_Actor->m_Orientation=m_QuatRot;
}
void PhysicsObject::UpdateMatricies()
{
	m_QuatRot.Normalize();
	// Calculate the transform matrix for the body.
	_calculateTransformMatrix(m_TransformMatrix, m_Position, m_QuatRot);
	// here i create a temporary matrix that i will use for concatinating my matricies
	// so that i can obtain my inverse oh tensor in world space
	DragonXMatrix m;
	// what im doing here is basically creating a pure rotation matrix from my transform matrix by obtaining each axis of rotation 
	m.SetComponents(m_TransformMatrix.GetAxisVector(0),m_TransformMatrix.GetAxisVector(1),m_TransformMatrix.GetAxisVector(2));	
	// not order matters so first we get our local tensor to rotate correctly in local space although only temporarily we then multiply that result by the non inverse of the pure
	// rotation matrix which results in our tensor in world space 
	m_InverseInertiaTensorWorld= m * m_InverseInertiaTensor * m.GetInverse();
}
void PhysicsObject::AddForce(const DragonXVector3 &force)
{
	m_LinearForceAccum += force;
	SetAwake(true);
}
void PhysicsObject::AddForceAtBodyPoint(const DragonXVector3 &force,const DragonXVector3 &point)
{
	// Convert to coordinates relative to center of mass.
	DragonXVector3 pt = GetPointInWorldSpace(point);
	AddForceAtPoint(force, pt);
}
void PhysicsObject::AddForceAtPoint(const DragonXVector3 &force,const DragonXVector3 &point)
{
	// Convert to coordinates relative to center of mass.
	DragonXVector3 pt = point;
	pt -= m_Position;

	m_LinearForceAccum += force;
	m_AngularForceAccum += pt % force;

	SetAwake(true);
}
void PhysicsObject::AddTorque(const DragonXVector3 &torque)
{
	m_AngularForceAccum += torque;
	SetAwake(true);
}
void PhysicsObject::AddRotation(const DragonXVector3 &deltaRotation)
{
	m_RotationalVelocity += deltaRotation;
}
void PhysicsObject::AddVelocity(const DragonXVector3 &deltaVelocity)
{
	m_LinearVelocity += deltaVelocity;
}
void PhysicsObject::ClearAccumulators()
{
	m_LinearForceAccum.Clear();
	m_AngularForceAccum.Clear();
}

void PhysicsObject::SetAwake(const bool awake)
{
	if(awake) 
	{
		m_IsAwake= true;
		// Add a bit of motion to avoid it falling asleep immediately.
		m_Motion = sleepEpsilon*2.0f;
	}
	else
	{
		m_IsAwake = false;
		m_LinearVelocity.Clear();
		m_RotationalVelocity.Clear();
	}
}
void PhysicsObject::SetCanSleep(const bool canSleep)
{
	m_CanSleep = canSleep;
	if (!canSleep && !m_IsAwake) 
	{
		SetAwake();
	}
}
void PhysicsObject::SetMass(const real mass)
{
	assert(mass != 0);
	m_InverseMass = ((real)1.0)/mass;
}
void PhysicsObject::SetInverseMass(const real inverseMass)
{
	m_InverseMass = inverseMass;
	if (m_InverseMass == 0) {
		m_Mass= REAL_MAX;
	}
}
void PhysicsObject::SetPosition(DragonXVector3& v)
{
	m_Position=v;
	m_Actor->m_Position=m_Position;
}
void PhysicsObject::SetOrientation(const DragonXQuaternion &orientation)
{
	m_QuatRot = orientation;
	m_QuatRot.Normalize();
	m_Actor->m_Orientation=m_QuatRot;
}

bool PhysicsObject::HasFiniteMass()
{
	return m_InverseMass > 0.0f;
}

real PhysicsObject::GetMass()
{
	if (m_InverseMass == 0) {
		return REAL_MAX;
	} else {
		return ((real)1.0)/m_InverseMass;
	}
}

DragonXVector3 PhysicsObject::GetPointInLocalSpace(const DragonXVector3 &point) const
{
	return m_TransformMatrix.TransformInverse(point);
}
DragonXVector3 PhysicsObject::GetPointInWorldSpace(const DragonXVector3 &point) const
{
	return m_TransformMatrix.Transform(point);
}
DragonXVector3 PhysicsObject::GetDirectionInLocalSpace(const DragonXVector3 &direction) const
{
	return m_TransformMatrix.TransformInverseDirection(direction);
}
DragonXVector3 PhysicsObject::GetDirectionInWorldSpace(const DragonXVector3 &direction) const
{
	return m_TransformMatrix.TransformDirection(direction);
}
DragonXVector3 PhysicsObject::GetNormal()
{
	if(m_PrimitiveType==PT_Plane)
	{
		return m_PlaneNormal;
	}
	else if(m_PrimitiveType==PT_Ray)
	{
		return DragonXVector3(2.0f*(m_QuatRot.x*m_QuatRot.z+m_QuatRot.w*m_QuatRot.y),
			2.0f*(m_QuatRot.y*m_QuatRot.z-m_QuatRot.w*m_QuatRot.x),
			1.0f-2.0f*(m_QuatRot.x*m_QuatRot.x+m_QuatRot.y*m_QuatRot.y));
	}
	else
	{
		return DragonXVector3();
	}
}
//************************************************
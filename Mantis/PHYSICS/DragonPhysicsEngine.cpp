#include "DragonPhysicsEngine.h"
#include "../../GRAPHICS/Utility/D3D9Vertex.h"
#include "../FRAMEWORK/Managers/WorldManager.h"

DragonPhysicsEngine::DragonPhysicsEngine()
{
	m_CollisionDetectionOn=true;
	m_CollisionResolutionOn=true;
	m_CollisionEngine=new DragonCollisionEngine();
	m_DragonForceEngine=new DragonForceEngine();
}
DragonPhysicsEngine::~DragonPhysicsEngine()
{
	delete m_CollisionEngine;
	delete m_DragonForceEngine;
}

// Public Functions
//======================================
void DragonPhysicsEngine::Update(real dt)
{
	//Get physics objects from memory
	auto entities = WMI->GetPhysicsObjects();

	if(entities.size() == 0) return;

	m_DragonForceEngine->UpdateForces(dt);
	for(const auto PhysicsObject : entities)
	{
		PhysicsObject->Update(dt);
	}
	if(m_CollisionDetectionOn)
	{
		m_CollisionEngine->GenerateContacts(entities);
	}

	ResolveContacts(dt);
}
void DragonPhysicsEngine::ResolveContacts(real dt)
{
	if(m_CollisionResolutionOn)
	{
		m_CollisionEngine->ResolveContacts(dt);
	}
}
std::vector<ActorContactPair> DragonPhysicsEngine::GetContactPairs()
{
	std::vector<ActorContactPair> pairVector;

	for(auto pairs : m_CollisionEngine->GetGenerator()->GetContactRegistry()->GetContactPairs())
	{
		ActorContactPair temp;
		temp.Actor1 = pairs->m_Obj1->m_Actor;
		temp.Actor2 = pairs->m_Obj2->m_Actor;

		pairVector.push_back(temp);
	}

	return pairVector;
}
std::vector<DragonContact*> DragonPhysicsEngine::GetContactsByPairOfEntities(PhysicsObject* obj1, PhysicsObject* obj2)
{
	std::vector<DragonContact*> ret;
	if(obj1->m_PrimitiveType==PT_Ray||obj2->m_PrimitiveType==PT_Ray)
	{
		for(const auto DragonContact : m_CollisionEngine->GetGenerator()->GetContactRegistry()->GetRayContacts())
		{
			if(DragonContact->GetEntity(0)==obj1||DragonContact->GetEntity(1)==obj1)
			{
				if(DragonContact->GetEntity(0)==obj2||DragonContact->GetEntity(1)==obj2)
				{
					ret.push_back(DragonContact);
				}
			}
		}
	}
	else
	{
		for(const auto DragonContact : m_CollisionEngine->GetGenerator()->GetContactRegistry()->GetListOfContacts())
		{
			if(DragonContact->GetEntity(0)==obj1||DragonContact->GetEntity(1)==obj1)
			{
				if(DragonContact->GetEntity(0)==obj2||DragonContact->GetEntity(1)==obj2)
				{
					ret.push_back(DragonContact);
				}
			}
		}
	}
	return ret;
}
std::vector<DragonXVector3*> DragonPhysicsEngine::GetVertexPositionsFromMesh(ID3DXMesh* mesh)
{
	std::vector<DragonXVector3*> vPos;
	LPDIRECT3DVERTEXBUFFER9 pVBuf;
	if (SUCCEEDED(mesh->GetVertexBuffer(&pVBuf)))
	{
		VertexPNT *pVert;
		if (SUCCEEDED(pVBuf->Lock(0,0,(void **) &pVert,D3DLOCK_DISCARD)))
		{
			DWORD numVerts=mesh->GetNumVertices();
			for (int i=0;(unsigned)i<numVerts;i++)
			{
				DragonXVector3* v = new DragonXVector3();
				*v=pVert[i].pos;
				vPos.push_back(v);
			}
			pVBuf->Unlock();
		}
		pVBuf->Release();
	}
	return vPos;
}
//***************************************************

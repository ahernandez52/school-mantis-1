#pragma once

#include "../Utility/DragonXMath.h"
#include "../Utility/DragonEnums.h"
#include <vector>
#include "../Contacts/DragonContactGenerator.h"

class HydraOctree
{
protected:
	// class level static to keep the tree from going to deep
	static int m_CurrentNodeLevel;	
	// the number of objects in the leaf
	int	m_NumObjects;	
	// bool to tell whether or not its a node or leaf true 4 node false 4 leaf
	bool m_IsNode;	
	// diameter of the leaf
	real m_Diameter;		
	// center of the leaf
	DragonXVector3 m_Center;	
	// entities inthe leaf
	std::vector<PhysicsObject*>	m_Entities;		
	// array of 8 nodes
	HydraOctree* m_HydraNodes[8];
private:

public:
	HydraOctree()//default constructor sets everything to null or 0
	{
		m_NumObjects=0;
		m_Center=DragonXVector3(0.0f,0.0f,0.0f);
		m_Diameter=0.0f;
		ZeroMemory(m_HydraNodes, sizeof(m_HydraNodes));
	}
	~HydraOctree();
	// releases the tree deleting all the arrays and partitions so that memory is freed up before recreating the tree
	void Release();
	// creates the tree
	void Create(std::vector<PhysicsObject*>& entities,int numEntities);		
	// sets the data for a node
	void SetData(std::vector<PhysicsObject*>& entities,int numEntities);		
	// creates a node
	void CreateNode(std::vector<PhysicsObject*>& entities,int numEntities,DragonXVector3& center, real diameter);	
	// creates end node
	void CreateNodeEnd(std::vector<PhysicsObject*>& entities,int numEntities,bool* pBools,DragonXVector3& center,real diameter,int numEntitiesInLeaf,int whichNode);
	// sets the node
	void SetNode(std::vector<PhysicsObject*>& entities, int numEntities);		
	// called for collision detection
	void GetContacts(DragonContactGenerator& contactGen, HydraOctree* pNode);
	// helper function calculates the center of the new node
	DragonXVector3 GetNodeCenter(DragonXVector3& currentCenter, real diameter, int whichNode);
};
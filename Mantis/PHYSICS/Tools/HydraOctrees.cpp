#include "HydraOctrees.h"

int HydraOctree::m_CurrentNodeLevel = 0;

HydraOctree::~HydraOctree()
{
	Release();
}

// Public Functions
//==========================================
//call release when your done with  the hydra system so that you arent holding all these trees in memory
void HydraOctree::Release()
{
	if(m_IsNode)
	{
		m_Entities.clear();
	}
	for(int i=0; i<8; i++)
	{
		if(m_HydraNodes[i])
		{
			m_HydraNodes[i]->Release();
			delete m_HydraNodes[i];
			m_HydraNodes[i]=NULL;
		}
	}
}
//call create to set up the tree
void HydraOctree::Create(std::vector<PhysicsObject*>& entities,int numEntities)
{
	m_NumObjects=numEntities;
	SetData(entities,numEntities);
	CreateNode(entities,numEntities,m_Center,m_Diameter);// recursion begins here
}
//sets up the center of the tree and its range
void HydraOctree::SetData(std::vector<PhysicsObject*>& entities,int numEntities)
{
	// if theres no entities theres no need to make a tree
	if(entities.size()==0||numEntities==0)
	{
		return;
	}
	// find the center of the first node
	for(const auto PhysicsObject : entities)
	{
		if(PhysicsObject->m_PhysicsType!=COT_World)
		{
			m_Center.x = m_Center.x + PhysicsObject->m_Position.x;
			m_Center.y = m_Center.y + PhysicsObject->m_Position.y;
			m_Center.z = m_Center.z + PhysicsObject->m_Position.z;
		}
	}
	//center of the tree or um cube well the tree is in a cube shape
	m_Center.x=m_Center.x/(real)numEntities;
	m_Center.y=m_Center.y/(real)numEntities;
	m_Center.z=m_Center.z/(real)numEntities;

	// ok now we need the diameter or i guess sorta distance from the center of the tree cube thing to determine which entities go inside it
	real wMax = 0.0f;//initialize local variable to zero we will use these to figure out our diameter
	real hMax = 0.0f;
	real dMax = 0.0f;
	// now we make some mini variables that we'll be using to help find the afore mentioned diameter
	real lWidth = 0.0f;
	real lHeight = 0.0f;
	real lDepth = 0.0f;

	// now we find the diameter
	for(const auto PhysicsObject : entities)
	{
		if(PhysicsObject->m_PhysicsType!=COT_World)
		{
			lWidth = (real)abs(PhysicsObject->m_Position.x-m_Center.x);
			lHeight = (real)abs(PhysicsObject->m_Position.y-m_Center.y);
			lDepth = (real)abs(PhysicsObject->m_Position.z-m_Center.z);

			if(lWidth>wMax)
			{
				wMax=lWidth;
			}
			if(lHeight>hMax)
			{
				hMax=lHeight;
			}
			if(lDepth>dMax)
			{
				dMax=lDepth;
			}
		}
	}
	//ok we now have the largest radii ? any way radius whatever we need to x by 2 for diameter we all know math
	wMax*=2;
	hMax*=2;
	dMax*=2;
	// ok now lets see which one is biggest which will be our winner for largest diameter
	m_Diameter=wMax;
	if(hMax>m_Diameter)
	{
		m_Diameter=hMax;
	}
	if(dMax>m_Diameter)
	{
		m_Diameter=dMax;
	}
	// ok thats all for this method which means that this node has a center and diameter based on the vector or list of entities given
}
// creates nodes, this is called automatically when we need to go a node deeper
void HydraOctree::CreateNode(std::vector<PhysicsObject*>& entities,int numEntities,DragonXVector3& center, real diameter)
{
	m_Center=center;
	m_Diameter=diameter;

	//now for some important notes on recursion
	// one it means it calls itself
	// this is great for automated type stuff makes our lives easier
	// only one minor issue it usually doesn't know anything about the outside world or what we tell it and passing in a limit might get messy
	// so the hydra solution is a static variable the current node level tells the tree where it is as far as levels
	// now that is not its limit however
	int iMaxNodes = 50;
	// that is change it for different results
	// ill experiment around with it but that is where we set the number of levels
	// for examples sake 1 is 8 subdivisions of the original entities list 2 is 64 yes its a lot but that just means a lot less checks in
	// collision detection which those can get fairly cpu intensive even with just sphere sphere
	// now to avoid unnecessary recursion we need  a certain number of objects to exist before the system will bother with recursion
	// that number is set below
	int iMinNumberOfObjects = 64;//64 gives me a decent frame rate with friction and looks smoothest

	// now lets see what we are going to do
	if((numEntities<iMinNumberOfObjects)||(m_CurrentNodeLevel>=iMaxNodes))//if its a node
	{
		SetNode(entities,numEntities);
		m_IsNode=true;
	}
	else//not a node its a leaf or branch or head of hydra point is it holds more data
	{
		//oh umm this is where we sorta sort the entities just telling ya
		m_IsNode=false;//obviously not a node its a leaf or whatever you wanna call it
		// ok now this is the clever soring bit
		// we are going to make 8 bool pointer arrays these are the same size numbers wise as the number of entities
		//we have going into this node ok well its a leaf now but the point is we are going to check
		//where each entity needs to go and use bools to say yes you go there or no you don't
		// now why bools and then later
		// its easy really sometimes you might have entities that are going to be needing
		// to be in two or more umm sub division thingys because we are dealing with things
		// not particles which means they have radii which could cross the arbitrary divisions
		// of the octree system so
		// to eliminate the possibility of not detecting
		// collisions that should be detected we use bools and pool the entities based on bools and possible collisions
		bool* pBoolArray1 = new bool[numEntities];
		bool* pBoolArray2 = new bool[numEntities];
		bool* pBoolArray3 = new bool[numEntities];
		bool* pBoolArray4 = new bool[numEntities];
		bool* pBoolArray5 = new bool[numEntities];
		bool* pBoolArray6 = new bool[numEntities];
		bool* pBoolArray7 = new bool[numEntities];
		bool* pBoolArray8 = new bool[numEntities];
		//gotta make sure theres nothing in these arrays
		// also side not sets all to null
		// or zero which when read as a boolean is false
		// so this sets these arrays to being full of falses
		ZeroMemory(pBoolArray1, numEntities);
		ZeroMemory(pBoolArray2, numEntities);
		ZeroMemory(pBoolArray3, numEntities);
		ZeroMemory(pBoolArray4, numEntities);
		ZeroMemory(pBoolArray5, numEntities);
		ZeroMemory(pBoolArray6, numEntities);
		ZeroMemory(pBoolArray7, numEntities);
		ZeroMemory(pBoolArray8, numEntities);

		// Loop through all our entities, and allocate to the appropriate
		// cube area.
		int i=0;
		for(const auto PhysicsObject : entities)
		{
			if(PhysicsObject->m_PhysicsType==COT_World&&PhysicsObject->m_PrimitiveType==PT_Plane)
			{
				if((m_Center*PhysicsObject->GetNormal())-m_Diameter<PhysicsObject->m_DistanceFromOrigin)
				{
					pBoolArray1[i] = true;
					pBoolArray2[i] = true;
					pBoolArray3[i] = true;
					pBoolArray4[i] = true;
					pBoolArray5[i] = true;
					pBoolArray6[i] = true;
					pBoolArray7[i] = true;
					pBoolArray8[i] = true;
				}
			}
			else if (PhysicsObject->m_PrimitiveType==PT_Ray)// note ray optimization in octrees is untested 
			{
				DragonXVector3 m = PhysicsObject->m_Position-m_Center;
				real c = m*m-m_Diameter*m_Diameter;
				if(c<=0.0f)
				{				
					pBoolArray1[i] = true;
					pBoolArray2[i] = true;
					pBoolArray3[i] = true;
					pBoolArray4[i] = true;
					pBoolArray5[i] = true;
					pBoolArray6[i] = true;
					pBoolArray7[i] = true;
					pBoolArray8[i] = true;
				}
				else if(((m*PhysicsObject->GetNormal())*(m*PhysicsObject->GetNormal())-c)>=0.0f)
				{
					pBoolArray1[i] = true;
					pBoolArray2[i] = true;
					pBoolArray3[i] = true;
					pBoolArray4[i] = true;
					pBoolArray5[i] = true;
					pBoolArray6[i] = true;
					pBoolArray7[i] = true;
					pBoolArray8[i] = true;
				}
			}
			else
			{
				// TOP_FRONT_LEFT
				if((PhysicsObject->m_Position.y+PhysicsObject->m_Radius>=m_Center.y)&&(PhysicsObject->m_Position.x-PhysicsObject->m_Radius<=m_Center.x)&&(PhysicsObject->m_Position.z-PhysicsObject->m_Radius<=m_Center.z))
				{
					pBoolArray1[i] = true;
				}
				// TOP_FRONT_RIGHT
				if((PhysicsObject->m_Position.y+PhysicsObject->m_Radius>=m_Center.y)&&(PhysicsObject->m_Position.x+PhysicsObject->m_Radius>=m_Center.x)&&(PhysicsObject->m_Position.z-PhysicsObject->m_Radius<=m_Center.z))
				{
					pBoolArray2[i] = true;
				}
				// TOP_BACK_LEFT
				if((PhysicsObject->m_Position.y+PhysicsObject->m_Radius>=m_Center.y)&&(PhysicsObject->m_Position.x-PhysicsObject->m_Radius<=m_Center.x)&&(PhysicsObject->m_Position.z+PhysicsObject->m_Radius>=m_Center.z))
				{
					pBoolArray3[i] = true;
				}
				// TOP_BACK_RIGHT
				if((PhysicsObject->m_Position.y+PhysicsObject->m_Radius>=m_Center.y)&&(PhysicsObject->m_Position.x+PhysicsObject->m_Radius>=m_Center.x)&&(PhysicsObject->m_Position.z+PhysicsObject->m_Radius>=m_Center.z))
				{
					pBoolArray4[i] = true;
				}

				// BOTTOM_FRONT_LEFT
				if((PhysicsObject->m_Position.y-PhysicsObject->m_Radius<=m_Center.y)&&(PhysicsObject->m_Position.x-PhysicsObject->m_Radius<=m_Center.x)&&(PhysicsObject->m_Position.z-PhysicsObject->m_Radius<=m_Center.z))
				{
					pBoolArray5[i] = true;
				}
				// BOTTOM_FRONT_RIGHT
				if((PhysicsObject->m_Position.y-PhysicsObject->m_Radius<=m_Center.y)&&(PhysicsObject->m_Position.x+PhysicsObject->m_Radius>=m_Center.x)&&(PhysicsObject->m_Position.z-PhysicsObject->m_Radius<=m_Center.z))
				{
					pBoolArray6[i] = true;
				}
				// BOTTOM_BACK_LEFT
				if((PhysicsObject->m_Position.y-PhysicsObject->m_Radius<=m_Center.y)&&(PhysicsObject->m_Position.x-PhysicsObject->m_Radius<=m_Center.x)&&(PhysicsObject->m_Position.z+PhysicsObject->m_Radius>=m_Center.z))
				{
					pBoolArray7[i] = true;
				}
				// BOTTOM_BACK_RIGHT
				if((PhysicsObject->m_Position.y-PhysicsObject->m_Radius<=m_Center.y)&&(PhysicsObject->m_Position.x+PhysicsObject->m_Radius>=m_Center.x)&&(PhysicsObject->m_Position.z+PhysicsObject->m_Radius>=m_Center.z))
				{
					pBoolArray8[i] = true;
				}
			}
			i++;
		}
		// now we need 2 count how many entities are in each partition this is used for early outs in this recursive process
		int iCount1 = 0;
		int iCount2 = 0;
		int iCount3 = 0;
		int iCount4 = 0;
		int iCount5 = 0;
		int iCount6 = 0;
		int iCount7 = 0;
		int iCount8 = 0;

		for(int i=0; i<numEntities; i++)
		{
			if(pBoolArray1[i]==true)
			{
				iCount1++;
			}
			if(pBoolArray2[i]==true)
			{
				iCount2++;
			}
			if(pBoolArray3[i]==true)
			{
				iCount3++;
			}
			if(pBoolArray4[i]==true)
			{
				iCount4++;
			}
			if(pBoolArray5[i]==true)
			{
				iCount5++;
			}
			if(pBoolArray6[i]==true)
			{
				iCount6++;
			}
			if(pBoolArray7[i]==true)
			{
				iCount7++;
			}
			if(pBoolArray8[i]==true)
			{
				iCount8++;
			}
		}
		// now we make each of the nodes or leafy bits or oh whatever this is where you split up the data
		CreateNodeEnd(entities,numEntities,pBoolArray1,m_Center,m_Diameter,iCount1,Top_Front_Left);
		CreateNodeEnd(entities,numEntities,pBoolArray2,m_Center,m_Diameter,iCount2,Top_Front_Right);
		CreateNodeEnd(entities,numEntities,pBoolArray3,m_Center,m_Diameter,iCount3,Top_Back_Left);
		CreateNodeEnd(entities,numEntities,pBoolArray4,m_Center,m_Diameter,iCount4,Top_Back_Right);

		CreateNodeEnd(entities,numEntities,pBoolArray5,m_Center,m_Diameter,iCount5,Bottom_Front_Left);
		CreateNodeEnd(entities,numEntities,pBoolArray6,m_Center,m_Diameter,iCount6,Bottom_Front_Right);
		CreateNodeEnd(entities,numEntities,pBoolArray7,m_Center,m_Diameter,iCount7,Bottom_Back_Left);
		CreateNodeEnd(entities,numEntities,pBoolArray8,m_Center,m_Diameter,iCount8,Bottom_Back_Right);
		delete pBoolArray1;
		delete pBoolArray2;
		delete pBoolArray3;
		delete pBoolArray4;
		delete pBoolArray5;
		delete pBoolArray6;
		delete pBoolArray7;
		delete pBoolArray8;
	}
}
//when a new node is created this sets up all its data
void HydraOctree::CreateNodeEnd(std::vector<PhysicsObject*>& entities,int numEntities,bool* pBools,DragonXVector3& center,real diameter,int numEntitiesInNode,int whichNode)
{
	// is there any point in making a node with nothing in it
	if(numEntitiesInNode==0)
	{
		return;// no
	}
	// so we dont

	// now just make a temporary container that will be used to hold the entities that are going into this new tree
	std::vector<PhysicsObject*> tempEntities;
	//now lets check which of the entities that were passed in need to go into this tree
	int i = 0;// an initialized counter for use with the bool pointer to help us sort
	for(const auto PhysicsObject : entities)
	{
		if(pBools[i])
		{
			tempEntities.push_back(PhysicsObject);
		}
		i++;
	}
	// heres where we create a new node
	m_HydraNodes[whichNode] = new HydraOctree;

	// figure out the new center for this new node
	DragonXVector3 vNewCenter = GetNodeCenter(center,diameter,whichNode);

	m_CurrentNodeLevel++;
	//make a new node and pass in details
	m_HydraNodes[whichNode]->CreateNode(tempEntities,numEntitiesInNode,vNewCenter,diameter/2);

	m_CurrentNodeLevel--;
}
// sets up the node assigning its entities into its vector this basically is the end of the reccursion
void HydraOctree::SetNode(std::vector<PhysicsObject*>& entities, int numEntities)
{
	m_IsNode = true;
	m_NumObjects = numEntities;
	m_Entities.clear();
	m_Entities = entities;
}
// call this to generate contacts using the partitioned space
void HydraOctree::GetContacts(DragonContactGenerator& contactGen, HydraOctree* pNode)
{
	if (pNode==NULL)
	{
		return;
	}
	if (pNode->m_IsNode)
	{
		contactGen.GenerateContacts(pNode->m_Entities);
	}
	else
	{
		GetContacts(contactGen,pNode->m_HydraNodes[Top_Front_Left]);
		GetContacts(contactGen,pNode->m_HydraNodes[Top_Front_Right]);
		GetContacts(contactGen,pNode->m_HydraNodes[Top_Back_Left]);
		GetContacts(contactGen,pNode->m_HydraNodes[Top_Back_Right]);

		GetContacts(contactGen,pNode->m_HydraNodes[Bottom_Front_Left]);
		GetContacts(contactGen,pNode->m_HydraNodes[Bottom_Front_Right]);
		GetContacts(contactGen,pNode->m_HydraNodes[Bottom_Back_Left]);
		GetContacts(contactGen,pNode->m_HydraNodes[Bottom_Back_Right]);
	}
}
//helper function calculates the center of the new node
DragonXVector3 HydraOctree::GetNodeCenter(DragonXVector3& currentCenter, real diameter, int whichNode)
{
	//ok first off we make our new center n set it 2 overs
	DragonXVector3 vNewCenter = DragonXVector3(0.0f,0.0f,0.0f);
	// now based on which part of the tree we are generating the center for we take the current center + or - the diameter over 4 that
	// gives us a center thats well centered in which ever octant? we need a center for
	switch(whichNode)
	{
	case Top_Front_Left:      // 0
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y + diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Top_Front_Right:     // 1
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y + diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Top_Back_Left:       // 2
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y + diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Top_Back_Right:      // 3
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y + diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Bottom_Front_Left:   // 4
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y - diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Bottom_Front_Right:  // 5
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y - diameter/4, currentCenter.z + diameter/4 );
			break;
		}
	case Bottom_Back_Left:    // 6
		{
			vNewCenter = DragonXVector3( currentCenter.x - diameter/4, currentCenter.y - diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	case Bottom_Back_Right:   // 7
		{
			vNewCenter = DragonXVector3( currentCenter.x + diameter/4, currentCenter.y - diameter/4, currentCenter.z - diameter/4 );
			break;
		}
	default:
		{
			break;
		}
	}
	return vNewCenter;
}
//*********************************************************
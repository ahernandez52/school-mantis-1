#pragma once

#include "../Utility/DragonEnums.h"
#include <vector>
#include "../Utility/DragonXMath.h"

class DragonTriangle;
class DragonSector;

class MeshOctreePartitioner
{
protected:
	static int						g_CurrentNodeLevel;	// class level variable used to keep from going too deep	
	std::vector<DragonTriangle*>	m_Entities;			// vector of entities in the tree
	DragonXVector3					m_Center;				// center of the tree

	bool							m_IsNode;				// bool to tell whether or not its a node or leaf true 4 node false 4 leaf
	MeshOctreePartitioner*			m_HydraNodes[8];		// the 8 node array used if its a node
private:

public:
	int								m_NumObjects;			// the number of entities in the tree
	DragonXVector3					m_HalfExtents;			// half extents
public:

	MeshOctreePartitioner();
	~MeshOctreePartitioner();
	void Release();
	void Create(std::vector<DragonTriangle*>& entities,int numEntities);		//call create to set up the tree
	void SetData(std::vector<DragonTriangle*>& entities,int numEntities);		//sets up the center of the tree and its range
	void CreateNode(std::vector<DragonTriangle*>& entities,int numEntities,DragonXVector3& center, DragonXVector3& HalfExtents);	// creates nodes
	void CreateNodeEnd(std::vector<DragonTriangle*>& entities,int numEntities,bool* pBools,DragonXVector3& center,DragonXVector3& HalfExtents,int numEntitiesInLeaf,int whichNode);//when a new node is created this sets up all its data
	DragonXVector3 GetNodeCenter(DragonXVector3& currentCenter, DragonXVector3& HalfExtents, int whichNode);			//helper function calculates the center of the new node
	void SetNode(std::vector<DragonTriangle*>& entities, int numEntities);		// sets up the node assigning its entities into its vector
	void GetSectors(std::vector<DragonSector*>& mesh, MeshOctreePartitioner* pNode);	// call this to generate contacts using the partitioned space
};
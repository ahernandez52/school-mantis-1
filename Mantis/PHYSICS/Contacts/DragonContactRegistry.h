#pragma once

#include "DragonContact.h"
#include "DragonContactResolver.h"
#include "../Utility/DragonPhysicsManager.h"
#include <vector>

//this class will hold the master list of contacts 
// as well as enable contacts to be added manually be it cable or rod 
// or whatever
// it will also hold a simler list that just has two entities per element this is the registry 
// its purpose is to identify and hold all entities that are currently in contact these are DragonicPairs
struct DragonicPair
{
	int m_ContactregistryType;// this is simply natural contact or if not then it will be handled differently
	int m_CollisionType;//this will be set by the collision detector to allow for no checking of object types for contact generation
	PhysicsObject* m_Obj1;//just the objects in the pair
	PhysicsObject* m_Obj2;
};

class DragonContactRegistry
{
private:
	unsigned int m_Limit;
	real m_Restitution;
	// vector of pairs, this is a master list of pairs
	std::vector<DragonicPair*> m_ContactPairs;
	// vector of contacts, master list of contacts
	std::vector<DragonContact*> m_Contacts;
	// vector of ray contacts though they use the same 
	// contact structure as the normal contacts the
	// data means different things and is not meant to
	// be resolved these are purely for gameplay as rays have no mass
	std::vector<DragonContact*> m_RayContacts;
private:
	inline void CleanPairs()
	{
		std::vector<DragonicPair*> keep;
		for(const auto DragonicPair : m_ContactPairs)
		{
			if(DragonicPair->m_Obj1!=nullptr&&DragonicPair->m_Obj2!=nullptr)
			{
				keep.push_back(DragonicPair);
			}
		}
		m_ContactPairs.clear();
		m_ContactPairs=keep;
	}
	inline void CleanContacts()
	{
		std::vector<DragonContact*> keep;
		for(const auto DragonContact : m_Contacts)
		{
			if(DragonContact->GetEntity(0)!=nullptr&&DragonContact->GetEntity(1)!=nullptr)
			{
				keep.push_back(DragonContact);
			}
		}
		m_Contacts.clear();
		m_Contacts=keep;
	}
	inline void CleanRayContacts()
	{
		std::vector<DragonContact*> keep;
		for(const auto DragonContact : m_RayContacts)
		{
			if(DragonContact->GetEntity(0)!=nullptr&&DragonContact->GetEntity(1)!=nullptr)
			{
				keep.push_back(DragonContact);
			}
		}
		m_RayContacts.clear();
		m_RayContacts=keep;
	}
public:
	// default constructor
	DragonContactRegistry();
	//destructor
	~DragonContactRegistry();
	// quick test for sector ray
	bool SectorRayCollisionDetection(DragonXVector3& spherePos,real rad,DragonXVector3& rayOrigin,DragonXVector3& rayDirection);
	// this clears n deletes the vector of contacts
	void ClearContacts();
	// this clears n deletes the contactPairs
	void ClearPairs();
	// clears the ray contacts 
	inline void ClearRayContacts()
	{
		for(const auto DragonContact : m_RayContacts)
		{
			delete DragonContact;
		}
		m_RayContacts.clear();
	}
	// this will generate the contacts for each pair
	void GenerateContact(int,PhysicsObject*,PhysicsObject*);
	// these are methods for generating contacts all done internally
	// this is for sphere sphere
	void GenerateContactSphereSphere(PhysicsObject* obj1,PhysicsObject* obj2);
	// plane sphere
	void GenerateContactSpherePlane(PhysicsObject* obj1,PhysicsObject* obj2);
	// sphere obb
	void GenerateContactSphereOBB(PhysicsObject* obj1,PhysicsObject* obj2);
	// obb plane
	void GenerateContactOBBPlane(PhysicsObject* obj1,PhysicsObject* obj2);
	// obb obb 
	void GenerateContactOBBOBB(PhysicsObject* obj1,PhysicsObject* obj2);
	// ray sphere
	void GenerateContactRaySphere(PhysicsObject* obj1,PhysicsObject* obj2);
	// ray obb
	void GenerateContactRayObb(PhysicsObject* obj1,PhysicsObject* obj2);
	// and some helpers for obb obb 
	// this generates the contact for point face
	void FillPointFaceBoxBox(PhysicsObject &one,PhysicsObject &two,const DragonXVector3 &toCentre,unsigned best,real pen);
	// obb helper	
	void ObbObbContactGen(PhysicsObject &one,PhysicsObject &two);
	// mesh sphere
	void GenerateContactMeshSphere(PhysicsObject* obj1,PhysicsObject* obj2);
	// sphere tri
	void GenerateContactSphereTri(PhysicsObject* mesh,PhysicsObject* sphere,DragonTriangle* tri);
	// gen contact mesh obb
	void GenerateContactMeshObb(PhysicsObject* obj1,PhysicsObject* obj2);
	// obb tri
	void GenerateContactObbTri(PhysicsObject* mesh,PhysicsObject* obb,DragonTriangle* tri);
	// gen contact mesh mesh
	void GenerateContactsMeshMesh(PhysicsObject* obj1,PhysicsObject* obj2);
	// gen contacts tri tri
	void GenerateContactsTriTri(PhysicsObject* mesh1,DragonTriangle* tri1,PhysicsObject* mesh2,DragonTriangle* tri2);
	
	// gen contact mesh ray
	void GenerateContactsMeshRay(PhysicsObject* obj1,PhysicsObject* obj2);
	// tri ray contact gen
	void GenerateContactsRayTri(PhysicsObject* mesh, DragonTriangle* tri,PhysicsObject* ray);
	// this class holds the master list of contacts so we will just take in a contact resolver 
	// and after it has resolved the contacts we will update the list
	// removing the fixed contacts and keeping the not fixed ones
	void ResolveContacts(DragonContactResolver*,real);
	// this will update the contact list
	void UpdateContactList(real);
	// we dont want multiple pairs that contain the same entities so
	// we use an add method to check the pair against the current list and only add it 
	// if no matching pair is found  natural meaning this will
	// be used by the collision detector we may wish to add other non natural pairs
	// such as rods which will need to be done dealt with differently
	int AddNaturalContactPair(int,PhysicsObject*, PhysicsObject*);
	// this is used for adding ray contacts
	int AddRayContactPair(int,PhysicsObject*,PhysicsObject*);
	// get contact pairs
	inline std::vector<DragonicPair*> GetContactPairs()
	{		
		CleanPairs();
		return m_ContactPairs;
	}
	// get the contacts
	inline std::vector<DragonContact*> GetListOfContacts()
	{
		CleanContacts();
		return m_Contacts;
	}	
	// get ray contacts
	inline std::vector<DragonContact*> GetRayContacts()
	{
		CleanRayContacts();
		return m_RayContacts;
	}
};
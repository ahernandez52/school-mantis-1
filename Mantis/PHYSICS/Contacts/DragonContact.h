#pragma once

#include "../Objects/PhysicsObject.h"

class DragonContact
{
private:
	// the coefficient of restitution or bounciness of the collision between 0.0 and 1.0 , 1.0 means no energy is lost 0.0 means all energy is lost
	// if you want to be funny for a bit till it crashes use greater than 1.0 which means collisions generate energy
	real m_Restitution;
	// the friction that will be used for the game
	real m_Friction;
	// holds depth of penetration
	real m_Penetration;
	// this holds the change in velocity required for the contact to be resolved
	// basically the top half of the impulse calculation formula
	real m_DesiredDeltaVelocity;
	// this is an array of base game pointers it holds the 2 entities that are contacting
	PhysicsObject* m_EntitiesContacted[2];
	// 3d vector that holds the contact point
	DragonXVector3 m_ContactPoint;
	// the contact normal
	DragonXVector3 m_ContactNormal;
	// this is the closing velocity or the combined velocity of the objects
	DragonXVector3 m_ContactVelocity;	
	// holds world space position of the point of contact relative to the center of each body
	DragonXVector3 m_RelitiveContactPosition[2];
	// this is a transform matrix that converts from contact coordinates to world and back
	DragonXMatrix m_ContactToWorld;
public:
	// constructor sets up some base things for use later
	DragonContact();
	// destructor what do you think its used 4
	~DragonContact();	
	// performs an inertia weighted penetration resolution  of this contact
	void ApplyPositionChange(DragonXVector3 linearChange[2],DragonXVector3 angularChange[2],real penetration);
	// this calculates and applies impulse based collision resolution to the velocities of the objects 
	void ApplyVelocityChange(DragonXVector3 velocityChange[2],	DragonXVector3 angularChange[2]);
	// creates an orthanormal contact basis that mostly just helps in friction but has some other less important uses
	// though its primary function is to change between world and contact coordinates
	void CalculateContactBasis();
	// calculates internal data this is usually called automatically before the resolver resolves this contact
	// there for it should not need to be called manually
	void CalculateInternals(real dt);
	/**
	* Calculates and sets the internal value for the desired delta
	* velocity. aka the expected velocity change
	*/
	void CalculateDesiredDeltaVelocity(real dt);	
	// basically if you collide a sleeping object wake it up but if your also asleep don't bother
	void MatchAwakeState();
	// reverses the contacting bodies so body 0 and 1 in the array are swapped
	// you should calculate the internals again after doing this as its not done automatically
	void SwapBodies();	
	// this is called after the constructor to set various data like the contacting bodies and the properties associated with the collision like restitution or bounciness
	void SetContactData(PhysicsObject* entity1,PhysicsObject* entity2,real restituion);
	// set contact normal
	inline void SetContactNormal(DragonXVector3& v)
	{
		m_ContactNormal=v;
	}
	// set penitration
	inline void SetPenitrationDepth(real p)
	{
		m_Penetration=p;
	}
	// set contact point 
	inline void SetContactPoint(DragonXVector3& v)
	{
		m_ContactPoint=v;
	}
	// update the contact velocity
	inline void UpdateContactVelocity(DragonXVector3& v)
	{
		m_ContactVelocity+=v;
	}
	// update penitration depth
	inline void UpdatePenitrationDepth(real p)
	{
		m_Penetration+=p;
	}
	// get desired delta vel
	inline real GetDesiredDeltaVelocity()
	{
		return m_DesiredDeltaVelocity;
	}
	// get penitration
	inline real GetPenitration()
	{
		return m_Penetration;
	}
	// get entity by index
	inline PhysicsObject* GetEntity(int idx)
	{
		return m_EntitiesContacted[idx];
	}
	// get contact basis matrix
	inline DragonXMatrix GetContactToWorldMatrix()
	{
		return m_ContactToWorld;
	}
	// get point of contact
	inline DragonXVector3 GetContactPoint()
	{
		return m_ContactPoint;
	}
	// get contact normal
	inline DragonXVector3 GetContactNormal()
	{
		return m_ContactNormal;
	}
	// get reletive contact pos
	inline DragonXVector3 GetReletiveContactPosIdx(unsigned idx)
	{
		return m_RelitiveContactPosition[idx];
	}
	// calculates and returns the velocity of the contact point on the given body
	DragonXVector3 CalculateLocalVelocity(unsigned entityIndex, real dt);
	// calc frictionless impluse faster but not very realistic looking
	DragonXVector3 CalculateFrictionlessImpulse(DragonXMatrix *inverseInertiaTensor);
	// calculate friction impulse more tedius and more calculations but it will look better
	DragonXVector3 CalculateFrictionImpulse(DragonXMatrix* inverseInertiaTensor);	
};

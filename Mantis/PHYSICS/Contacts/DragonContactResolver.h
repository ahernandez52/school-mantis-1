#pragma once

#include "DragonContact.h"
#include <vector>

class DragonContactResolver
{
private:
	//number of iterations to perform when resolving velocity
	unsigned m_VelocityIterations;
	// number of iterations to perform when resolving position
	unsigned m_PositionIterations;
	// stores the number of velocity iterations used in the last resolve call
	unsigned m_VelocityIterationsUsed;
	//ditto for position or penetration
	unsigned m_PositionIterationsUsed;
	// keeps track of weather the internal values are valid basically the above variables being kept in check
	bool m_ValidSettings;
	// to reduce instability velocities less than this are considered 0 good starting point is 0.01
	real m_VelocityEpsilon;
	// to avoid instability penetrations smaller than this are considered zero good starting point is 0.01.
	real m_PositionEpsilon;
private:
	/**
	* Sets up contacts ready for processing. This makes sure their
	* internal data is configured correctly and the correct set of bodies
	* is made alive.
	*/
	void PrepareContacts(std::vector<DragonContact*>& contacts,real dt);
	/**
	* Resolves the velocity issues with the given array of constraints,
	* using the given number of iterations.
	*/
	void AdjustVelocities(std::vector<DragonContact*>& contacts,real dt);
	/**
	* Resolves the positional issues with the given array of constraints,
	* using the given number of iterations.
	*/
	void AdjustPositions(std::vector<DragonContact*>& contacts,real dt);
public:
	//default constructor sets the internal values like epsilon and iterations automatically to predetermined values
	DragonContactResolver();
	// destructor theres not much to say the resolver doesn't really have anything important in it well pointers wise
	~DragonContactResolver();
	/**
	* Sets the number of iterations for each resolution stage.
	*/
	void SetIterations(unsigned velocityIterations,unsigned positionIterations);
	/**
	* Sets the number of iterations for both resolution stages.
	*/
	void SetIterations(unsigned iterations);
	/**
	* Sets the tolerance value for both velocity and position.
	*/
	void SetEpsilon(real velocityEpsilon,real positionEpsilon);
	// the main iterative contact resolver algorithm 
	void ResolveContacts( std::vector<DragonContact*>& contacts,real dt);
	// returns true if the resolver has valid settings and is ready to go
	bool IsValid();
	inline real GetEpsilon()
	{
		return (m_VelocityEpsilon+m_PositionEpsilon)/(real)2.0f;
	}	
};

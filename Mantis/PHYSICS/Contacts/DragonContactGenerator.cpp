#include "DragonContactGenerator.h"
#include "../Objects/PhysicsObject.h"
#include "../Utility/DragonEnums.h"
#include <cassert>

const static real REAL_MAX=9999999.99f;// this is a static const used locally though it is technically a global variable or rather a global const so yes its global scope but its unmodifyable

//these are local static inline helper functions un declared un the header
// --------------------------------------------------
// BEGIN LOCAL HELPER FUNCTIONS
// --------------------------------------------------

// this is a helper function that basically performs the dot product of the vector 3 axis passed in with the half sizes of the box passed in
// the dot product resultant being the projection of one vector 3 or in this case the axis of one box on to the axis of the box thats passed in
// the return value is the result of these dot products
static inline real TransformToAxis( PhysicsObject &box, const DragonXVector3 &axis)
{
	return box.m_HalfExtents.x * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(0)) +
		box.m_HalfExtents.y * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(1)) +
		box.m_HalfExtents.z * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(2));
}
// this is a helper function i wrote for use with kdops but as they are still in developement its currently unused
static inline void ClostestPtPointOBB(DragonXVector3& p,PhysicsObject* obb, DragonXVector3& q)
{
	DragonXVector3 d = p - obb->m_Position;
	q = obb->m_Position;
	DragonXVector3 he = obb->m_HalfExtents;
	//for each obb axis

	real dist = d * obb->m_TransformMatrix.GetAxisVector(0);
	// if the dist is further than the box extents clamp it
	if(dist> he.x)
	{
		dist = he.x;
	}
	if(dist < -he.x)
	{
		dist = -he.x;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(0);

	dist = d * obb->m_TransformMatrix.GetAxisVector(1);
	// if the dist is further than the box extents clamp it
	if(dist> he.y)
	{
		dist = he.y;
	}
	if(dist < -he.y)
	{
		dist = -he.y;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(1);

	dist = d * obb->m_TransformMatrix.GetAxisVector(2);
	// if the dist is further than the box extents clamp it
	if(dist> he.z)
	{
		dist = he.z;
	}
	if(dist < -he.z)
	{
		dist = -he.z;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(2);
}

/**
* This function checks if the two boxes overlap
* along the given axis. The final parameter toCentre
* is used to pass in the vector between the boxes center
* points, to avoid having to recalculate it each time.
*/
static inline bool OverlapOnAxis(PhysicsObject &one,PhysicsObject &two,const DragonXVector3 &axis,const DragonXVector3 & toCentre)
{
	// project the half size of one onto axis
	real oneProject = TransformToAxis(one,axis);
	real twoProject = TransformToAxis(two,axis);

	// project this onto the axis
	real dist = (real)abs(toCentre * axis);

	// check for overlap
	return (dist <= oneProject + twoProject);
}
// obb obb collision test is done here do to its use of a large number of helper functions and so to keep the other code cleaner
// macro deffinition to make code easier to read
#define TEST_OVERLAP(axis) OverlapOnAxis(one,two,(axis),toCentre)
bool ObbObbTest(PhysicsObject &one,PhysicsObject &two)
{
	DragonXVector3 toCentre = two.m_Position - one.m_Position;

	return(
		// check box ones axis first
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(0)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(1)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(2)) &&

		// now twos
		TEST_OVERLAP(two.m_TransformMatrix.GetAxisVector(0)) &&
		TEST_OVERLAP(two.m_TransformMatrix.GetAxisVector(1)) &&
		TEST_OVERLAP(two.m_TransformMatrix.GetAxisVector(2)) &&

		// and now the cross products
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(0)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(1)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(2)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(0)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(1)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(2)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(0)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(1)) &&
		TEST_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(2))
		);
}
#undef TEST_OVERLAP

static inline real GetMax(real zed, real ein, real zwei)
{
	real ret=-9999999999.9f;
	if(zed>ret)
	{
		ret=zed;
	}
	if(ein>ret)
	{
		ret = ein;
	}
	if(zwei>ret)
	{
		ret = zwei;
	}
	return ret;
}

static inline real GetMin(real zed, real ein, real zwei)
{
	real ret=9999999999.9f;
	if(zed<ret)
	{
		ret=zed;
	}
	if(ein<ret)
	{
		ret = ein;
	}
	if(zwei<ret)
	{
		ret = zwei;
	}
	return ret;
}

static inline real GetMax(real zed, real ein)
{
	if(zed>ein)
	{
		return zed;
	}
	else
	{
		return ein;
	}
}

static inline real GetMin(real zed, real ein)
{
	if(zed<ein)
	{
		return zed;
	}
	else
	{
		return ein;
	}
}

static inline real DistPointPlane(DragonXVector3& p, DragonXVector3& planeNormal,real planeDist)
{
	return p*planeNormal-planeDist;
}

// --------------------------------------------------
// END LOCAL HELPER FUNCTIONS
// --------------------------------------------------

DragonContactGenerator::DragonContactGenerator()
{
	m_Limit=250;//Anthony said a 250 contact limit
	m_NumPairs=0;
	m_Tolerence=0.0f;
	m_Registry = new DragonContactRegistry();
}
DragonContactGenerator::DragonContactGenerator(int limit)
{
	m_Limit=limit;
	m_NumPairs=0;
	m_Tolerence=0.0f;
	m_Registry = new DragonContactRegistry();
}
DragonContactGenerator::~DragonContactGenerator()
{
	delete m_Registry;
}
// Public Functions
//==========================================
void DragonContactGenerator::SetLimit(int limit)
{
	m_Limit=limit;
}
void DragonContactGenerator::GenerateContacts(std::vector<PhysicsObject*>& entities)
{
	for(int i = 0;(unsigned)i<entities.size()-1;i++)
	{
		for(int j = i+1;(unsigned)j<entities.size();j++)
		{
			if(m_NumPairs<m_Limit)
			{
				if(entities[i]->m_IsCollisionDetectionOn&&entities[j]->m_IsCollisionDetectionOn)
				{
					switch(CollisionType(entities[i],entities[j]))
					{
					case CT_SphereSphere:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								m_NumPairs+=m_Registry->AddNaturalContactPair(CT_SphereSphere,entities[i],entities[j]);
							}
							break;
						}
					case CT_SpherePlane:
						{
							if(SpherePlaneCollisionTest(entities[i],entities[j]))
							{
								m_NumPairs+=m_Registry->AddNaturalContactPair(CT_SpherePlane,entities[i],entities[j]);
							}
							break;
						}
					case CT_SphereOBB:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								if(SphereOBBCollisionTest(entities[i],entities[j]))
								{
									m_NumPairs+=m_Registry->AddNaturalContactPair(CT_SphereOBB,entities[i],entities[j]);
								}
							}
							break;
						}
					case CT_ObbPlane:
						{
							if(SpherePlaneCollisionTest(entities[i],entities[j]))
							{
								if(ObbPlaneCollisionTest(entities[i],entities[j]))
								{
									m_NumPairs+=m_Registry->AddNaturalContactPair(CT_ObbPlane,entities[i],entities[j]);
								}
							}
							break;
						}
					case CT_ObbObb:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								if(ObbObbCollisionTest(entities[i],entities[j]))
								{
									m_NumPairs+=m_Registry->AddNaturalContactPair(CT_ObbObb,entities[i],entities[j]);
								}
							}
							break;
						}	
					case CT_RaySphere:
						{
							if(RaySphereCollisionTest(entities[i],entities[j]))
							{
								m_NumPairs+=m_Registry->AddRayContactPair(CT_RaySphere,entities[i],entities[j]);
							}
							break;
						}
					case CT_RayOBB:
						{
							if(RaySphereCollisionTest(entities[i],entities[j]))
							{
								if(RayObbCollisionTest(entities[i],entities[j]))
								{
									m_NumPairs+=m_Registry->AddRayContactPair(CT_RayOBB,entities[i],entities[j]);
								}
							}
							break;
						}
					case CT_MeshSphere:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								if (SphereOBBCollisionTest(entities[i],entities[j]))
								{
									if (MeshSphereCollisionTest(entities[i],entities[j]))
									{
										m_NumPairs+=m_Registry->AddNaturalContactPair(CT_MeshSphere,entities[i],entities[j]);
									}
								}
							}
							break;
						}
					case CT_MeshOBB:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								if (ObbObbCollisionTest(entities[i],entities[j]))
								{
									if (MeshObbCollisionTest(entities[i],entities[j]))
									{
										m_NumPairs+=m_Registry->AddNaturalContactPair(CT_MeshOBB,entities[i],entities[j]);
									}
								}
							}
							break;
						}
					case CT_MeshMesh:
						{
							if(SphereSphereCollisionTest(entities[i],entities[j]))
							{
								if(ObbObbCollisionTest(entities[i],entities[j]))
								{
									if(MeshMeshCollisionTest(entities[i],entities[j]))
									{										
										m_NumPairs+=m_Registry->AddNaturalContactPair(CT_MeshMesh,entities[i],entities[j]);
									}
								}
							}
							break;
						}
					case CT_MeshRay:
						{
							if(RaySphereCollisionTest(entities[i],entities[j]))
							{
								if(RayObbCollisionTest(entities[i],entities[j]))
								{
									if(MeshRay(entities[i],entities[j]))
									{										
										m_NumPairs+=m_Registry->AddRayContactPair(CT_MeshRay,entities[i],entities[j]);
									}
								}
							}
							break;
						}
					default:
						{
							break;
						}
					}
				}
			}
		}
	}
}
int DragonContactGenerator::GetLimit()
{
	return m_Limit;
}
int DragonContactGenerator::CollisionType(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if((obj1->m_PhysicsType==COT_World)&&(obj2->m_PhysicsType==COT_World))
	{
		return -1;
	}
	if((!obj1->m_IsAwake)&&(!obj2->m_IsAwake))
	{
		return -1;
	}
	else if((obj1->m_PrimitiveType==PT_Sphere)&&(obj2->m_PrimitiveType==PT_Sphere))
	{
		return CT_SphereSphere;
	}
	else if((obj1->m_PrimitiveType==PT_Sphere)&&(obj2->m_PrimitiveType==PT_Plane)||(obj1->m_PrimitiveType==PT_Plane)&&(obj2->m_PrimitiveType==PT_Sphere))
	{
		return CT_SpherePlane;
	}
	else if((obj1->m_PrimitiveType==PT_Sphere)&&(obj2->m_PrimitiveType==PT_OBB)||(obj1->m_PrimitiveType==PT_OBB)&&(obj2->m_PrimitiveType==PT_Sphere))
	{
		return CT_SphereOBB;
	}
	else if((obj1->m_PrimitiveType==PT_OBB)&&(obj2->m_PrimitiveType==PT_Plane)||(obj1->m_PrimitiveType==PT_Plane)&&(obj2->m_PrimitiveType==PT_OBB))
	{
		return CT_ObbPlane;
	}
	else if((obj1->m_PrimitiveType==PT_OBB)&&(obj2->m_PrimitiveType==PT_OBB))
	{
		return CT_ObbObb;
	}
	else if((obj1->m_PrimitiveType==PT_Ray)&&(obj2->m_PrimitiveType==PT_Sphere)||(obj1->m_PrimitiveType==PT_Sphere)&&(obj2->m_PrimitiveType==PT_Ray))
	{
		return CT_RaySphere;
	}
	else if((obj1->m_PrimitiveType==PT_Ray)&&(obj2->m_PrimitiveType==PT_OBB)||(obj1->m_PrimitiveType==PT_OBB)&&(obj2->m_PrimitiveType==PT_Ray))
	{
		return CT_RayOBB;
	}
	else if((obj1->m_PrimitiveType==PT_Mesh)&&(obj2->m_PrimitiveType==PT_Plane)||(obj1->m_PrimitiveType==PT_Plane)&&(obj2->m_PrimitiveType==PT_Mesh))
	{
		return CT_ObbPlane;// mesh plane will be treated as a obb plane
	}
	else if((obj1->m_PrimitiveType==PT_Sphere)&&(obj2->m_PrimitiveType==PT_Mesh)||(obj1->m_PrimitiveType==PT_Mesh)&&(obj2->m_PrimitiveType==PT_Sphere))
	{
		return CT_MeshSphere;
	}
	else if((obj1->m_PrimitiveType==PT_OBB)&&(obj2->m_PrimitiveType==PT_Mesh)||(obj1->m_PrimitiveType==PT_Mesh)&&(obj2->m_PrimitiveType==PT_OBB))
	{
		return CT_MeshOBB;
	}
	else if((obj1->m_PrimitiveType==PT_Mesh)&&(obj2->m_PrimitiveType==PT_Mesh))
	{
		return CT_MeshMesh;
	}
	else if((obj1->m_PrimitiveType==PT_Ray)&&(obj2->m_PrimitiveType==PT_Mesh)||(obj1->m_PrimitiveType==PT_Mesh)&&(obj2->m_PrimitiveType==PT_Ray))
	{
		return CT_MeshRay;
	}
	else
	{
		return -1;
	}
}
bool DragonContactGenerator::SphereSphereCollisionTest(DragonXVector3 pos1,real radius1,DragonXVector3 pos2, real radius2)
{
	return (pos1-pos2).GetMagSquared() < (radius1+radius2)*(radius1+radius2);
}
bool DragonContactGenerator::SphereSphereCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	return (obj1->m_Position - obj2->m_Position).GetMagSquared() <((obj1->m_Radius+obj2->m_Radius)*(obj1->m_Radius+obj2->m_Radius));
}
// collision utility
bool DragonContactGenerator::SpherePlaneCollisionTest(DragonXVector3 pos,real radius,DragonXVector3 normal,real distFromOrigin)
{
	return (normal*pos-radius)<=(distFromOrigin);
}
// internal collision test
bool DragonContactGenerator::SpherePlaneCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(!obj1->m_PrimitiveType==PT_Plane)
	{
		if((obj2->GetNormal() * obj1->m_Position - obj1->m_Radius) <= obj2->m_DistanceFromOrigin)
		{
			real dist = obj2->GetNormal() * obj1->m_Position - obj2->m_DistanceFromOrigin;
			if(dist*dist>obj1->m_Radius*obj1->m_Radius)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		if((obj1->GetNormal() * obj2->m_Position - obj2->m_Radius) <= obj1->m_DistanceFromOrigin)
		{
			real dist = obj1->GetNormal() * obj2->m_Position - obj1->m_DistanceFromOrigin;
			if(dist*dist>obj2->m_Radius*obj2->m_Radius)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
}
// sphere obb
bool DragonContactGenerator::SphereOBBCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Sphere)//if object 1 is the sphere
	{
		DragonXVector3 center = obj1->m_Position;
		DragonXVector3 relCenter = obj2->m_TransformMatrix.TransformInverse(center);
		if(abs(relCenter.x) - obj1->m_Radius > obj2->m_HalfExtents.x ||
			abs(relCenter.y) - obj1->m_Radius > obj2->m_HalfExtents.y ||
			abs(relCenter.z) - obj1->m_Radius > obj2->m_HalfExtents.z)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else //obj2 is the sphere and obj1 is the obb
	{
		DragonXVector3 center = obj2->m_Position;
		DragonXVector3 relCenter = obj1->m_TransformMatrix.TransformInverse(center);
		if(abs(relCenter.x) - obj2->m_Radius > obj1->m_HalfExtents.x ||
			abs(relCenter.y) - obj2->m_Radius > obj1->m_HalfExtents.y ||
			abs(relCenter.z) - obj2->m_Radius > obj1->m_HalfExtents.z)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
//obb plane
bool DragonContactGenerator::ObbPlaneCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType!=PT_Plane)
	{
		real projectedRadius = TransformToAxis(*obj1,obj2->GetNormal());
		real boxDistance = (obj2->GetNormal() * obj1->m_Position) - projectedRadius;
		if(boxDistance>= obj2->m_DistanceFromOrigin-m_Tolerence)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else//else obj2 is the obb and obj1 is the plane
	{
		real projectedRadius = TransformToAxis(*obj2,obj1->GetNormal());
		real boxDistance = (obj1->GetNormal() * obj2->m_Position) - projectedRadius;
		if(boxDistance>= obj1->m_DistanceFromOrigin-m_Tolerence)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
// obb obb
bool DragonContactGenerator::ObbObbCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	return ObbObbTest(*obj1,*obj2);
}
// ray sphere
bool DragonContactGenerator::RaySphereCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if (obj1->m_PrimitiveType==PT_Ray)//if obj1 is the ray
	{
		DragonXVector3 m=obj1->m_Position-obj2->m_Position;
		real b = m*obj1->GetNormal();
		real c = m*m-obj2->m_Radius*obj2->m_Radius;

		if (c>0.0f&&b>0.0f)
		{
			return false;
		}

		real discr = b*b-c;
		if(discr<0.0f)
		{
			return false;
		}
		else
		{
			return true;
		}
	} 
	else// obj2 is the ray
	{
		DragonXVector3 m=obj2->m_Position-obj1->m_Position;
		real b = m*obj2->GetNormal();
		real c = (m*m)-(obj1->m_Radius*obj1->m_Radius);

		if (c>0.0f&&b>0.0f)
		{
			return false;
		}

		real discr = b*b-c;
		if(discr<0.0f)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
// ray obb
bool DragonContactGenerator::RayObbCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	real EPSILON = 0.001f;
	if (obj1->m_PrimitiveType==PT_Ray)//if obj1 is ray
	{
		real tmin=-999999999.9f;
		real tmax=9999999999.9f;
		DragonXVector3 p = obj2->GetPointInLocalSpace(obj1->m_Position);
		DragonXVector3 d = obj2->GetDirectionInLocalSpace(obj1->GetNormal());
		if(abs(d.x)<EPSILON)// for x
		{
			if(p.x<-obj2->m_HalfExtents.x||p.x>obj2->m_HalfExtents.x)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.x;
			real t1 = (-obj2->m_HalfExtents.x - p.x)*ood;
			real t2 = (obj2->m_HalfExtents.x - p.x)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		if(abs(d.y)<EPSILON)// for y
		{
			if(p.y<-obj2->m_HalfExtents.y||p.y>obj2->m_HalfExtents.y)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.y;
			real t1 = (-obj2->m_HalfExtents.y - p.y)*ood;
			real t2 = (obj2->m_HalfExtents.y - p.y)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		if(abs(d.z)<EPSILON)// for z
		{
			if(p.z<-obj2->m_HalfExtents.z||p.z>obj2->m_HalfExtents.z)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.z;
			real t1 = (-obj2->m_HalfExtents.z - p.z)*ood;
			real t2 = (obj2->m_HalfExtents.z - p.z)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		return true;
	} 
	else// obj2 is the ray
	{
		real tmin=-999999999.9f;
		real tmax=9999999999.9f;
		DragonXVector3 p = obj1->GetPointInLocalSpace(obj2->m_Position);
		DragonXVector3 d = obj1->GetDirectionInLocalSpace(obj2->GetNormal());
		if(abs(d.x)<EPSILON)// for x
		{
			if(p.x<-obj1->m_HalfExtents.x||p.x>obj1->m_HalfExtents.x)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.x;
			real t1 = (-obj1->m_HalfExtents.x - p.x)*ood;
			real t2 = (obj1->m_HalfExtents.x - p.x)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		if(abs(d.y)<EPSILON)// for y
		{
			if(p.y<-obj1->m_HalfExtents.y||p.y>obj1->m_HalfExtents.y)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.y;
			real t1 = (-obj1->m_HalfExtents.y - p.y)*ood;
			real t2 = (obj1->m_HalfExtents.y - p.y)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		if(abs(d.z)<EPSILON)// for z
		{
			if(p.z<-obj1->m_HalfExtents.z||p.z>obj1->m_HalfExtents.z)
			{
				return false;
			}
		}
		else
		{
			real ood = 1.0f / d.z;
			real t1 = (-obj1->m_HalfExtents.z - p.z)*ood;
			real t2 = (obj1->m_HalfExtents.z - p.z)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return false;
			}
		}
		return true;
	}
}
// mesh sphere test
bool DragonContactGenerator::MeshSphereCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)// if obj1 is the mesh
	{
		for (auto DragonSector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereCollisionTest(obj1->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj2->m_Position,obj2->m_Radius))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleSphereCollisionTest(DragonTriangle,obj1,obj2))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	else // otherwise obj2 is the mesh
	{
		for (auto DragonSector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereCollisionTest(obj2->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj1->m_Position,obj1->m_Radius))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleSphereCollisionTest(DragonTriangle,obj2,obj1))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
}
bool DragonContactGenerator::TriangleSphereCollisionTest(DragonTriangle* tri,PhysicsObject* mesh,PhysicsObject* sphere)
{
	DragonXVector3 p = ClosestPtPointTriangle(sphere->m_Position,mesh->GetPointInWorldSpace(tri->GetVertA()),mesh->GetPointInWorldSpace(tri->GetVertB()),mesh->GetPointInWorldSpace(tri->GetVertC()));
	p-=sphere->m_Position;
	if (p*p<=sphere->m_Radius*sphere->m_Radius)
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool DragonContactGenerator::MeshObbCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)//if obj1 is the mesh
	{
		for (auto DragonSector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereCollisionTest(obj1->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj2->m_Position,obj2->m_Radius))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleSphereCollisionTest(DragonTriangle,obj1,obj2))
					{
						if(TriangleObbCollisionTest(DragonTriangle,obj1,obj2))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	else// obj2 is the mesh
	{
		for (auto DragonSector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereCollisionTest(obj2->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj1->m_Position,obj1->m_Radius))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleSphereCollisionTest(DragonTriangle,obj2,obj1))
					{
						if(TriangleObbCollisionTest(DragonTriangle,obj2,obj1))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
bool DragonContactGenerator::TriangleObbCollisionTest(DragonTriangle* tri,PhysicsObject* mesh, PhysicsObject* obb)
{
	DragonXVector3 vertA = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertA()));
	DragonXVector3 vertB = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertB()));
	DragonXVector3 vertC = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertC()));
	DragonXVector3 norm = tri->GetNormal(vertA,vertB,vertC);

	DragonXVector3 f0=vertB-vertA;
	//f0.Invert();
	DragonXVector3 f1=vertC-vertB;
	//f1.Invert();
	DragonXVector3 f2=vertA-vertC;
	//f2.Invert();

	// cross product axis tests
	DragonXVector3 axis = obb->m_TransformMatrix.GetAxisVector(0)%f0;
	real p0 = vertA*axis;
	real p1 = vertB*axis;
	real p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(0)%f1;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(0)%f2;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(1)%f0;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(1)%f1;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(1)%f2;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(2)%f0;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(2)%f1;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	axis = obb->m_TransformMatrix.GetAxisVector(2)%f2;
	p0 = vertA*axis;
	p1 = vertB*axis;
	p2 = vertC*axis;
	if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))>TransformToAxis(*obb,axis))
	{
		return false;
	}

	if(GetMax(vertA.x,vertB.x,vertC.x)<-1.0f*obb->m_HalfExtents.x||GetMin(vertA.x,vertB.x,vertC.x)>obb->m_HalfExtents.x)
	{
		return false;
	}

	if(GetMax(vertA.y,vertB.y,vertC.y)<-1.0f*obb->m_HalfExtents.y||GetMin(vertA.y,vertB.y,vertC.y)>obb->m_HalfExtents.y)
	{
		return false;
	}

	if(GetMax(vertA.z,vertB.z,vertC.z)<-1.0f*obb->m_HalfExtents.z||GetMin(vertA.z,vertB.z,vertC.z)>obb->m_HalfExtents.z)
	{
		return false;
	}

	if((real)abs(norm*obb->GetPointInLocalSpace(obb->m_Position))-(norm*vertA)<=TransformToAxis(*obb,norm))
	{
		return true;// note seemed to work better with out the transformation of the normal maybe     NOTE i think i need to transform the normals to mesh local direction then world direction
	}
	else
	{
		return false;
	}
}

bool DragonContactGenerator::MeshMeshCollisionTest(PhysicsObject* obj1,PhysicsObject* obj2)
{
	for(auto mesh1sector : obj1->m_CollisionMesh->m_DragonSectors)
	{
		for(auto mesh2sector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereCollisionTest(obj1->GetPointInWorldSpace(mesh1sector->m_Position),mesh1sector->m_Radius,obj2->GetPointInWorldSpace(mesh2sector->m_Position),mesh2sector->m_Radius))
			{
				for(auto tri1 : mesh1sector->m_SectorTris)
				{
					for(auto tri2 : mesh2sector->m_SectorTris)
					{
						if(TestTriTri(tri1,obj1,tri2,obj2))
						{
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}
bool DragonContactGenerator::TestTriVertsAgainstPlane(DragonXVector3& vertA,DragonXVector3& vertB,DragonXVector3& vertC,DragonXVector3& planeNormal,real planeDist,real& d0,real& d1,real& d2)
{
	d0 = DistPointPlane(vertA,planeNormal,planeDist);
	d1 = DistPointPlane(vertB,planeNormal,planeDist);
	d2 = DistPointPlane(vertC,planeNormal,planeDist);

	if(d0>0&&d1>0&&d2>0)
	{
		return false;
	}
	else if(d0<0&&d1<0&&d2<0)
	{
		return false;
	}
	else
	{
		return true;
	}
}
bool DragonContactGenerator::TestTriTri(DragonTriangle* tri1,PhysicsObject* mesh1,DragonTriangle* tri2,PhysicsObject* mesh2)
{
	//create local temp verts 4 each triangle in world space
	// tri 1
	DragonXVector3 tri1A = mesh1->GetPointInWorldSpace(tri1->GetVertA());
	DragonXVector3 tri1B = mesh1->GetPointInWorldSpace(tri1->GetVertB());
	DragonXVector3 tri1C = mesh1->GetPointInWorldSpace(tri1->GetVertC());
	//tri2
	DragonXVector3 tri2A = mesh2->GetPointInWorldSpace(tri2->GetVertA());
	DragonXVector3 tri2B = mesh2->GetPointInWorldSpace(tri2->GetVertB());
	DragonXVector3 tri2C = mesh2->GetPointInWorldSpace(tri2->GetVertC());
	DragonXVector3 tri1Norm = tri1->GetNormal(tri1A,tri1B,tri1C);
	DragonXVector3 tri2Norm = tri2->GetNormal(tri2A,tri2B,tri2C);
	real du0=0.0f;
	real du1=0.0f;
	real du2=0.0f;
	real dv0=0.0f;
	real dv1=0.0f;
	real dv2=0.0f;

	if(!TestTriVertsAgainstPlane(tri1A,tri1B,tri1C,tri2Norm,tri2Norm*tri2C,du0,du1,du2))
	{
		return false;
	}
	
	if(!TestTriVertsAgainstPlane(tri2A,tri2B,tri2C,tri1Norm,tri1Norm*tri1C,dv0,dv1,dv2))
	{
		return false;
	}
	// now compute the edges
	DragonXVector3 tri1ba = tri1B-tri1A;
	DragonXVector3 tri1cb = tri1C-tri1B;
	DragonXVector3 tri1ac = tri1A-tri1C;
	//tri2
	DragonXVector3 tri2ba = tri2B-tri2A;
	DragonXVector3 tri2cb = tri2C-tri2B;
	DragonXVector3 tri2ac = tri2A-tri2C;

	real pu0,pu1,pu2,pv0,pv1,pv2;

	// 9 cross product axis tests
	// set 1
	DragonXVector3 testAxis = tri1ba%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1ba%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1ba%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}
	//set2 
	testAxis = tri1cb%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1cb%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1cb%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}
	// set3
	testAxis = tri1ac%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1ac%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}

	testAxis = tri1ac%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)<GetMin(pu0,pu1,pu2))	
		{
			return false;
		}
		if(GetMin(pv0,pv1,pv2)>GetMax(pu0,pu1,pu2))	
		{
			return false;
		}
	}
	return true;
}

bool DragonContactGenerator::MeshRay(PhysicsObject* obj1, PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)// if obj1 is the mesh
	{
		for (auto DragonSector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SectorRayCollisionDetection(obj1->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj2->m_Position,obj2->GetNormal()))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleRayCollisionTest(obj1->GetPointInWorldSpace(DragonTriangle->GetVertA()),obj1->GetPointInWorldSpace(DragonTriangle->GetVertB()),obj1->GetPointInWorldSpace(DragonTriangle->GetVertC()),obj2->m_Position,obj2->GetNormal()))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	else // otherwise obj2 is the mesh
	{
		for (auto DragonSector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SectorRayCollisionDetection(obj2->GetPointInWorldSpace(DragonSector->m_Position),DragonSector->m_Radius,obj1->m_Position,obj1->GetNormal()))
			{
				for (auto DragonTriangle : DragonSector->m_SectorTris)
				{
					if (TriangleRayCollisionTest(obj2->GetPointInWorldSpace(DragonTriangle->GetVertA()),obj2->GetPointInWorldSpace(DragonTriangle->GetVertB()),obj2->GetPointInWorldSpace(DragonTriangle->GetVertC()),obj1->m_Position,obj1->GetNormal()))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
}

bool DragonContactGenerator::SectorRayCollisionDetection(DragonXVector3& spherePos,real rad,DragonXVector3& rayOrigin,DragonXVector3& rayDirection)
{
	DragonXVector3 m = rayOrigin-spherePos;
	real c = (m*m)-(rad*rad);

	if(c<=0.0f)
	{
		return true;
	}

	real b = m*rayDirection;

	if(b>0.0f)
	{
		return false;
	}

	real disc = b*b-c;

	if(disc < 0.0f)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool DragonContactGenerator::TriangleRayCollisionTest(DragonXVector3& triVertA,DragonXVector3& triVertB,DragonXVector3& triVertC,DragonXVector3& rayPos,DragonXVector3& rayDir)
{
	DragonXVector3 u = triVertB-triVertA;
	DragonXVector3 v = triVertC-triVertA;
	DragonXVector3 n = u%v;

	DragonXVector3 w0 = rayPos-triVertA;
	real a = -1.0f*(n*w0);
	real b = n*rayDir;
	if((real)abs(b)<0.00005f)
	{
		if(a!=0)
		{
			return false;
		}
	}
	real r = a/b;
	if(r<0.0f)
	{
		return false;
	}

	DragonXVector3 I = rayPos + r * rayDir;
	real uu,uv,vv,wu,wv,d;
	uu = u*u;
	uv=u*v;
	vv=v*v;
	DragonXVector3 w = I - triVertA;
	wu=w*u;
	wv=w*v;
	d = uv*uv-uu*vv;

	real s,t;
	s = (uv*wv-vv*wu)/d;
	if(s<0.0f||s>1.0f)
	{
		return false;
	}
	t = (uv*wu-uu*wv)/d;
	if(t<0.0f||(s+t)>1.0f)
	{
		return false;
	}
	return true;
}
// closest point on tri abc to point p
DragonXVector3 DragonContactGenerator::ClosestPtPointTriangle(DragonXVector3& p,DragonXVector3& a,DragonXVector3& b,DragonXVector3& c)
{
	// check if p is in vertex region outside a
	DragonXVector3 ab = b-a;
	DragonXVector3 ac = c-a;
	DragonXVector3 ap = p-a;
	real d1 = ab*ap;
	real d2 = ac*ap;
	if(d1<=0.0f&&d2<=0.0f)
	{
		return a;
	}

	// check b
	DragonXVector3 bp = p-b;
	real d3 = ab*bp;
	real d4=ac*bp;
	if(d3>=0.0f&&d4<=d3)
	{
		return b;
	}

	// check edge region ab
	real vc = d1*d4-d3*d2;
	if(vc<=0.0f&&d1>=0.0f&&d3<=0.0f)
	{
		real v = d1/(d1-d3);
		return a+v*ab;
	}

	// check c
	DragonXVector3 cp = p-c;
	real d5=ab*cp;
	real d6=ac*cp;
	if(d6>=0.0f&&d5<=d6)
	{
		return c;
	}

	// check p on ac
	real vb = d5*d2-d1*d6;
	if(vb<=0.0f&&d2>=0.0f&&d6<=0.0f)
	{
		real w = d2/(d2-d6);
		return a+w*ac;
	}

	// check p on bc
	real va = d3*d6-d5*d4;
	if(va<=0.0f&&(d4-d3)>=0.0f&&(d5-d6)>=0.0f)
	{
		real w = (d4-d3)/((d4-d3)+(d5-d6));
		return b + w*(c-b);
	}

	// p is in the face region
	real denom = 1.0f/(va+vb+vc);
	real v = vb*denom;
	real w = vc * denom;
	return a+ab*v+ac*w;
}
std::vector<DragonContact*> DragonContactGenerator::GetContacts()
{
	return m_Registry->GetListOfContacts();
}
//********************************************************
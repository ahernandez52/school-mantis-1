// dragon collision engine .h
#pragma once
// includes
#include "DragonContactResolver.h"
#include "../Tools/HydraOctrees.h"
#include <vector>

class DragonCollisionEngine
{
private:// private member variables
	// this is a pointer to the contact resolver class 
	// it resolves contacts generated by the collision detector / contact generator
	DragonContactResolver* m_ContactResolver;
	// this is the collision detector / contact generator it does both or rather this is a pointer to it 
	DragonContactGenerator* m_ContactGenerator;
	// this is a pointer to the parent node of the octree 
	HydraOctree* m_HydraSpaceOptimization;
public:// public holds ctor and functions
	// default constructor theres no need for a parameterized one
	DragonCollisionEngine();
	// destructor needed to delete pointers and eliminate memory leaks
	// also note its not virtual as no class inherits from this one
	~DragonCollisionEngine();	
	// begin functions defined in .cpp
	// this will generate contacts from a list of entities
	void GenerateContacts(std::vector<PhysicsObject*>& entities);
	// this can be used to resolve the internal and currently un used vector of contacts
	void ResolveContacts(real dt);	
	// this returns the internal list of contacts however because the collision engine doesn't stare the contacts at the moment 
	// this is un used but can be used if found needed
	std::vector<DragonContact*> GetContactList();
	// this is currently used for debugging but can and will likely be used for game play aswell
	// this will perform collision detection and contact generation on the given list of entities 
	// then return the vector of contacts to be used by other things in my case i render contacts via contact point and normal 
	// but other uses may be found one theory is to use these in conjunction with trigger volumes that is 
	// keep a list of trigger volumes and their associated triggered effects to be applied or perhaps only send a message when a contact with a trigger volume is 
	// detected and returned
	std::vector<DragonContact*> GetListOfContactsFromList(std::vector<PhysicsObject*>& entities);	
	// end functions defined in .cpp

	// begin functions defined in header
	// gets a pointer to the octree object
	inline HydraOctree* GetHydra()
	{
		return m_HydraSpaceOptimization;
	}
	// gets a pointer to the contact resolver
	inline DragonContactResolver* GetResolver()
	{
		return m_ContactResolver;
	}
	// gets a pointer to the contact generator 
	inline DragonContactGenerator* GetGenerator()
	{
		return m_ContactGenerator;
	}
	// end functions defined in header
};

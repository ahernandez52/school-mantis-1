#include "DragonContactResolver.h"
#include <algorithm>

DragonContactResolver::DragonContactResolver()
{
	SetIterations(5,5);
	SetEpsilon(0.01f, 0.01f);
}
DragonContactResolver::~DragonContactResolver()
{
}

// Private Functions
//==============================
void DragonContactResolver::PrepareContacts(std::vector<DragonContact*>& contacts,real dt)
{
	for(const auto DragonContact: contacts)
	{
		DragonContact->CalculateInternals(dt);
	}
}
void DragonContactResolver::AdjustVelocities(std::vector<DragonContact*>& contacts, real dt)
{
	DragonXVector3 velocityChange[2], rotationChange[2];
	DragonXVector3 deltaVel;
	// iteratively handle impacts in order of severity.
	m_VelocityIterationsUsed = 0;
	while (m_VelocityIterationsUsed < m_VelocityIterations)
	{
		// Find contact with maximum magnitude of probable velocity change.
		auto itr = std::max_element(begin(contacts),end(contacts),[&](DragonContact* a,DragonContact* b){return a->GetDesiredDeltaVelocity()<b->GetDesiredDeltaVelocity();});
		if(itr.operator*()->GetDesiredDeltaVelocity()<m_VelocityEpsilon)
		{
			break;
		}
		// Match the awake state at the contact
		itr.operator*()->MatchAwakeState();
		// Do the resolution on the contact that came out top.
		itr.operator*()->ApplyVelocityChange(velocityChange, rotationChange);
		// With the change in velocity of the two bodies, the update of
		// contact velocities means that some of the relative closing
		// velocities need recomputing.
		for(auto DragonContact : contacts)
		{
			// Check each body in the contact
			for (unsigned b = 0; b < 2; b++) 
			{
				if (DragonContact->GetEntity(b)->m_PhysicsType)
				{
					// Check for a match with each body in the newly
					// resolved contact
					for (unsigned d = 0; d < 2; d++)
					{
						if (DragonContact->GetEntity(b)==itr.operator*()->GetEntity(d))
						{
							deltaVel = velocityChange[d] + rotationChange[d] % DragonContact->GetReletiveContactPosIdx(b);
							// The sign of the change is negative if we're dealing
							// with the second body in a contact.
							DragonContact->UpdateContactVelocity(DragonContact->GetContactToWorldMatrix().TransformTranspose(deltaVel)* (real)(b?-1:1));
							DragonContact->CalculateDesiredDeltaVelocity(dt);
						}
					}
				}
			}
		}
		m_VelocityIterationsUsed++;
	}
}
void DragonContactResolver::AdjustPositions(std::vector<DragonContact*>& contacts,real dt)
{
	DragonXVector3 linearChange[2], angularChange[2];
	DragonXVector3 deltaPosition;
	// iteratively resolve interpenetration in order of severity.
	m_PositionIterationsUsed = 0;
	while (m_PositionIterationsUsed < m_PositionIterations)
	{
		// Find biggest penetration
		auto itr = std::max_element(begin(contacts),end(contacts),[&](DragonContact* a, DragonContact* b){return a->GetPenitration()<b->GetPenitration();});
		if(itr.operator*()->GetPenitration()<m_PositionEpsilon)
		{
			break;
		}
		// Match the awake state at the contact
		itr.operator*()->MatchAwakeState();
		// Resolve the penetration.
		itr.operator*()->ApplyPositionChange(linearChange,angularChange,itr.operator*()->GetPenitration());
		// Again this action may have changed the penetration of other
		// bodies, so we update contacts.
		for(auto DragonContact : contacts)
		{
			// Check each body in the contact
			for (unsigned b = 0; b < 2; b++)
			{
				if (DragonContact->GetEntity(b)->m_PhysicsType)
				{
					// Check for a match with each body in the newly
					// resolved contact
					for (unsigned d = 0; d < 2; d++)
					{
						if (DragonContact->GetEntity(b) == itr.operator*()->GetEntity(d))
						{
							deltaPosition = linearChange[d] + angularChange[d]%DragonContact->GetReletiveContactPosIdx(b);
							// The sign of the change is positive if we're
							// dealing with the second body in a contact
							// and negative otherwise (because we're
							// subtracting the resolution)..
							DragonContact->UpdatePenitrationDepth(deltaPosition*DragonContact->GetContactNormal()*(b?1:-1));
						}
					}
				}
			}
		}
		m_PositionIterationsUsed++;
	}
}
//****************************************

// Public Functions
//=================================
void DragonContactResolver::SetIterations(unsigned iterations)
{
	SetIterations(iterations, iterations);
}
void DragonContactResolver::SetIterations(unsigned velocityIterations, unsigned positionIterations)
{
	m_VelocityIterations = velocityIterations;
	m_PositionIterations = positionIterations;
}
void DragonContactResolver::SetEpsilon(real velocityEpsilon,real positionEpsilon)
{
	m_VelocityEpsilon = velocityEpsilon;
	m_PositionEpsilon = positionEpsilon;
}
void DragonContactResolver::ResolveContacts(std::vector<DragonContact*>& contacts,real dt)
{
	// Make sure we have something to do.
	if (contacts.size() == 0)
	{
		return;
	}
	if (!IsValid())// if not valid
	{
		return;// dont resolve
	}
	// Prepare the contacts for processing
	PrepareContacts(contacts,dt);
	// Resolve the interpenetration problems with the contacts.
	AdjustPositions(contacts,dt);
	// Resolve the velocity problems with the contacts.
	AdjustVelocities(contacts,dt);
}
bool DragonContactResolver::IsValid()
{
	return (m_VelocityIterations > 0) &&
		(m_PositionIterations > 0) &&
		(m_PositionEpsilon >= 0.01f) &&
		(m_VelocityEpsilon >= 0.01f);
}
//********************************************

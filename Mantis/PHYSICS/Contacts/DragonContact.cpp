#include "DragonContact.h"
#include "../Objects/PhysicsObject.h"
#include <cassert>
#include "../Utility/DragonEnums.h"

DragonContact::DragonContact()
{
	DragonXMatrix m;
	D3DXMatrixIdentity(&m);
	m_ContactToWorld=m;
	m_Friction=0.5f;
}
DragonContact::~DragonContact()// nothing to delete the only pointers are to entities which will be deleted on their own
{
}
// Public Functions
//=======================================
void DragonContact::ApplyPositionChange(DragonXVector3 linearChange[2],DragonXVector3 angularChange[2],	real penetration)
{
	// ok here is the position or interpenitration fix method
	// first a few local variables to make our life easier
	const real angularLimit = (real)0.01f;
	real angularMove[2];
	real linearMove[2];
	real totalInertia = 0;
	real linearInertia[2];
	real angularInertia[2];
	// ok step one of interpenitration fixes with rotation is to ge the total inertia
	// and while we are at it we can obtain and hold on to various important parts of the inertia for use in a bit
	for (unsigned i = 0; i < 2; i++) if (m_EntitiesContacted[i]->m_PhysicsType)
	{
		// first up we are gunnaa cache the tensors
		DragonXMatrix inverseInertiaTensor;
		inverseInertiaTensor=m_EntitiesContacted[i]->m_InverseInertiaTensorWorld;
		// same as before we add up all the inertia
		DragonXVector3 angularInertiaWorld = m_RelitiveContactPosition[i] % m_ContactNormal;
		angularInertiaWorld = inverseInertiaTensor.TransformDirection(angularInertiaWorld);
		angularInertiaWorld = angularInertiaWorld % m_RelitiveContactPosition[i];
		angularInertia[i] = angularInertiaWorld * m_ContactNormal;
		linearInertia[i] = m_EntitiesContacted[i]->m_InverseMass;
		// Keep track of the total inertia from all components
		// we will need this in a moment
		totalInertia += linearInertia[i] + angularInertia[i];
		// We break the loop here so that the totalInertia value is
		// completely calculated (by both iterations) before
		// continuing.
	}
	// Loop through again calculating and applying the changes
	for (unsigned i = 0; i < 2; i++) if (m_EntitiesContacted[i]->m_PhysicsType)
	{
		// The linear and angular movements required are in proportion to
		// the two inverse inertias.
		// this lil bit of code sign basically says
		// if i ==0 sign = 1 if i!=0 sign = -1
		// what that means for us is
		//we dont have to worry about the multiplication of everything by -1 this will do it for us
		real sign =(i == 0)?(real)1:-1;
		angularMove[i] = sign * penetration * (angularInertia[i] / totalInertia);
		linearMove[i] = sign * penetration * (linearInertia[i] / totalInertia);

		// this next 20 or so lines is an over rotation fix based on the object's mass and inertia tensor
		DragonXVector3 proj = m_RelitiveContactPosition[i];
		proj.operator+=(m_ContactNormal*(m_RelitiveContactPosition[i]*-1.0f*m_ContactNormal));

		real maxMag= angularLimit * proj.GetMagnitude();

		if(angularMove[i] < -maxMag)
		{
			real totalMove = angularMove[i] + linearMove[i];
			angularMove[i] = -maxMag;
			linearMove[i]= totalMove-angularMove[i];
		}
		if(angularMove[i] > maxMag)
		{
			real totalMove = angularMove[i] + linearMove[i];
			angularMove[i] = maxMag;
			linearMove[i]= totalMove-angularMove[i];
		}
		// in case unclear end over rotation fix

		if (angularMove[i] == 0)
		{
			// Easy case - no angular movement means no rotation.
			angularChange[i].Clear();
		}
		else
		{
			// ok for rotation fixes first we determine what direction we want to rotate
			// we obtain this by crossing the contact point by the contact normal this way we rotate
			// in a direction perpendicular to the direction of the contact and the contact point itself relative
			// to the object we are rotating
			DragonXVector3 targetAngularDirection = m_RelitiveContactPosition[i]%m_ContactNormal;
			// so that we want to ratate based on the tensor so that it rotates based on physics
			// so we need to transform the direction based on the tensor
			DragonXMatrix inverseInertiaTensor =  m_EntitiesContacted[i]->m_InverseInertiaTensorWorld;
			// ok now we need to rotate in this direction by the amount we determined above which is based
			// on the angular inertia and the total inertia and the penitration depth
			angularChange[i] = 	inverseInertiaTensor.TransformDirection(targetAngularDirection) * angularMove[i];
		}
		// Velocity change is easier - it is just the linear movement
		// along the DragonContact normal.
		linearChange[i] = m_ContactNormal * linearMove[i];
		// Now we can start to apply the values we've calculated.
		// Apply the linear movement
		m_EntitiesContacted[i]->SetPosition(m_EntitiesContacted[i]->m_Position+linearChange[i]);
		// And the change in orientation
		// note this is done in world so we convert back using rotation part of the transform matrix
		DragonXQuaternion q=m_EntitiesContacted[i]->m_QuatRot;
		q.AddScaledVectorDrX(m_EntitiesContacted[i]->GetDirectionInLocalSpace(angularChange[i]),(real)1.0f);
		q.Normalize();
		m_EntitiesContacted[i]->SetOrientation(q);
		// We need to calculate the derived data for any m_EntitiesContacted that is
		// asleep, so that the changes are reflected in the object's
		// data. Otherwise the resolution will not change the position
		// of the object, and the next collision detection round will
		// have the same penetration.
		if (!m_EntitiesContacted[i]->m_IsAwake)
		{
			m_EntitiesContacted[i]->UpdateMatricies();
		}
	}
}
void DragonContact::ApplyVelocityChange(DragonXVector3 velocityChange[2],DragonXVector3 rotationChange[2])
{
	//chache a copy of each entities invese inertia tensor in world space
	DragonXMatrix inverseInertiaTensor[2];
	inverseInertiaTensor[0]=m_EntitiesContacted[0]->m_InverseInertiaTensorWorld;
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// if the second body is not a world entity
		// note i use body and entity interchangeably body is just so much easier to type
		inverseInertiaTensor[1]=m_EntitiesContacted[1]->m_InverseInertiaTensorWorld;
	}
	// heres is just a local vector 3 we'll use him in a bit
	DragonXVector3 impulseDragonContact;
	// we are in space so we will be using a frictionless environment
	//impulseDragonContact = calculateFrictionlessImpulse(inverseInertiaTensor);  NOTE: this is for frictionless collisions if we want them
	impulseDragonContact = CalculateFrictionImpulse(inverseInertiaTensor);
	DragonXVector3 impulse = m_ContactToWorld.TransformDirection(impulseDragonContact);
	// Split in the impulse into linear and rotational components
	// here we calculate the impulsive torque which is the part of the impulse that will be used for rotational velocity
	DragonXVector3 impulsiveTorque = m_RelitiveContactPosition[0]%impulse;
	// the rotational velocity will need to be transformed by the inverse inertia tensor
	// note i use transform direction as the inertia tensor is a rotation matrix essentially
	// a 3x3 being stored in a 4x4 directx matrix so to avoid numerical issues such as negitive zeros
	// i apply a transform that only uses the directional or rotational part of the matrix also has a few less calculations
	rotationChange[0] = inverseInertiaTensor[0].TransformDirection(impulsiveTorque);
	// just to be sure lets zero this one out
	velocityChange[0].Clear();
	// linear impulse calculation easy imulse times nverse mass times contact normal direction
	velocityChange[0] = impulse * m_EntitiesContacted[0]->m_InverseMass;

	// Apply the changes
	// gotta apply em or they aren't really very useful
	// but anyway easy update for the linear velocity
	m_EntitiesContacted[0]->AddVelocity(velocityChange[0]);
	// now the rotational velocity is a bit tricky as it ends up being applied to the orientation in the for of a quaternion
	// in local space which means we need to convert the impulse from world space to local so that when the quaternion
	// updates it will be updating correctly
	m_EntitiesContacted[0]->AddRotation(m_EntitiesContacted[0]->GetDirectionInLocalSpace(rotationChange[0]));
	// now we check if the other body is a world object if it is we skip it
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// now the impulse will be reversed for object two as its impulse will be aplied in the opposite direction gotta conserve those laws of physics
		impulse*=-1.0f;
		// we do the same things as above only this time to the other entity
		DragonXVector3 impulsiveTorque = m_RelitiveContactPosition[1]%impulse;
		rotationChange[1] = inverseInertiaTensor[1].TransformDirection(impulsiveTorque);
		velocityChange[1].Clear();
		velocityChange[1]= impulse * m_EntitiesContacted[1]->m_InverseMass;

		// And apply them.
		m_EntitiesContacted[1]->AddVelocity(velocityChange[1]);
		m_EntitiesContacted[1]->AddRotation(m_EntitiesContacted[1]->GetDirectionInLocalSpace(rotationChange[1]));	
	}
}
/*
* Constructs an arbitrary orthonormal basis for the DragonContact. This is
* stored as a 3x3 matrix, where each vector is a column (in other
* words the matrix transforms DragonContact space into world space). The x
* direction is generated from the DragonContact normal, and the y and z
* directions are set so they are at right angles to it.
*/
inline void DragonContact::CalculateContactBasis()
{
	// this is primarily used for friction in the case of our frictionless envirnment it has limited uses mostly used in
	// the contact resolver during its iteration s to update velosities between resolutions
	DragonXVector3 DragonContactTangent[2];
	// Check whether the Z-axis is nearer to the X or Y axis
	if ((real)abs(m_ContactNormal.x) > (real)abs(m_ContactNormal.y))
	{
		// Scaling factor to ensure the results are normalized
		const real s = (real)1.0f/(real)sqrt(m_ContactNormal.z*m_ContactNormal.z +
			m_ContactNormal.x*m_ContactNormal.x);
		// The new X-axis is at right angles to the world Y-axis
		DragonContactTangent[0].x=m_ContactNormal.z*s;
		DragonContactTangent[0].y= 0.0f;
		DragonContactTangent[0].z= -m_ContactNormal.x*s;
		// The new Y-axis is at right angles to the new X- and Z- axes
		DragonContactTangent[1].x= m_ContactNormal.y*DragonContactTangent[0].x;
		DragonContactTangent[1].y=m_ContactNormal.z*DragonContactTangent[0].x -
			m_ContactNormal.x*DragonContactTangent[0].z;
		DragonContactTangent[1].z=-m_ContactNormal.y*DragonContactTangent[0].x;
	}
	else
	{
		// Scaling factor to ensure the results are normalized
		const real s = (real)1.0/(real)sqrt(m_ContactNormal.z*m_ContactNormal.z +
			m_ContactNormal.y*m_ContactNormal.y);
		// The new X-axis is at right angles to the world X-axis
		DragonContactTangent[0].x=(0);
		DragonContactTangent[0].y=-m_ContactNormal.z*s;
		DragonContactTangent[0].z=m_ContactNormal.y*s;

		// The new Y-axis is at right angles to the new X- and Z- axes
		DragonContactTangent[1].x= m_ContactNormal.y*DragonContactTangent[0].z -
			m_ContactNormal.z*DragonContactTangent[0].y;
		DragonContactTangent[1].y=-m_ContactNormal.x*DragonContactTangent[0].z;
		DragonContactTangent[1].z=m_ContactNormal.x*DragonContactTangent[0].y;
	}
	// Make a matrix from the three vectors.
	//this will create the orthanormal basis matrix as a 4 x 4 with the three orthoganal vectors taking up the
	// first 3 rows n columns and spot 44is a 1 because of the identity matrix created in the constructor
	m_ContactToWorld.SetComponents(m_ContactNormal,DragonContactTangent[0],DragonContactTangent[1]);
}
void DragonContact::CalculateInternals(real dt)
{
	// this will calculate some internal values for use in the contact resolution
	// Check if the first object is NULL, and swap if it is.
	if (!m_EntitiesContacted[0]->m_PhysicsType)
	{
		// this will swap object 1 with object 2
		// this is helpful in that if object 1 is a world object it will be moved to object 2
		// and then checks will keep the world object
		// now at place 2 from being used or updated in the resolution
		SwapBodies();
	}
	assert(m_EntitiesContacted[0]->m_PhysicsType&&"We have a contact generated between two world objects this should not be possible.");

	// this will create the orhanormal basis which can be used for friction
	CalculateContactBasis();
	// Store the relative position of the DragonContact relative to each entity
	// this is done by subrtracting the objects position from the contact position
	m_RelitiveContactPosition[0] = m_ContactPoint - m_EntitiesContacted[0]->m_Position;
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// the first of many checks to see if obj2 is a world body if its not then lets get its relitive contact position too
		m_RelitiveContactPosition[1] = m_ContactPoint - m_EntitiesContacted[1]->m_Position;
	}
	// Find the relative velocity of the bodies at the DragonContact point.
	// this calculates the closing velocity for each body at the point of contact
	m_ContactVelocity = CalculateLocalVelocity(0, dt);
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// and then subjtracts one from the other so we have a change in velocity
		m_ContactVelocity -= CalculateLocalVelocity(1, dt);
	}
	// and finally calcute expected change in velocity
	CalculateDesiredDeltaVelocity(dt);
}
void DragonContact::CalculateDesiredDeltaVelocity(real dt)
{
	const static real velocityLimit = (real).5f;

	// stacking fix remove excess velocity induced by forces
	real velFromAcc = 0.0f;

	// calclate acceleration induced velocity
	if(m_EntitiesContacted[0]->m_IsAwake)
	{
		velFromAcc+=m_EntitiesContacted[0]->m_LastFrameAccel * dt * m_ContactNormal;
	}

	// check if body 2 is a game object if not is it awake
	if(m_EntitiesContacted[1]->m_PhysicsType&&m_EntitiesContacted[1]->m_IsAwake)
	{
		velFromAcc-=m_EntitiesContacted[1]->m_LastFrameAccel * dt * m_ContactNormal;
	}

	// If the velocity is very slow, limit the restitution
	// they basically stopped moving so lets just let em settle down
	real rest = m_Restitution;
	if ((real)abs(m_ContactVelocity.GetMagSquared()) < velocityLimit)
	{
		rest = (real)0.0f;
	}
	// impulse only occurs in the direction of the contact normal we also want to remove any excess velocity from acceleration due to forces
	// so we remove it the use the restitution
	m_DesiredDeltaVelocity= -m_ContactVelocity.x - rest * (m_ContactVelocity.x - velFromAcc);
}
// this will wake up a sleeping object if one of the 2 objects are asleep
void DragonContact::MatchAwakeState()
{
	// Collisions with the world never cause an entity to wake up
	if (m_EntitiesContacted[1]->m_PhysicsType==COT_World) return;
	bool EntitiesContacted0awake = m_EntitiesContacted[0]->m_IsAwake;
	bool EntitiesContacted1awake = m_EntitiesContacted[1]->m_IsAwake;
	// Wake up only the sleeping one
	if (EntitiesContacted0awake ^ EntitiesContacted1awake)
	{
		if (EntitiesContacted0awake)
		{
			m_EntitiesContacted[1]->SetAwake(true);
		}
		else
		{
			m_EntitiesContacted[0]->SetAwake(true);
		}
	}
}
/*
* Swaps the bodies in the current DragonContact, so m_EntitiesContacted 0 is at m_EntitiesContacted 1 and
* vice versa. This also changes the direction of the DragonContact normal,
* but doesn't update any calculated internal data. If you are calling
* this method manually, then call calculateInternals afterwards to
* make sure the internal data is up to date.
*/
void DragonContact::SwapBodies()
{
	m_ContactNormal.Invert();
	PhysicsObject *temp = m_EntitiesContacted[0];
	m_EntitiesContacted[0] = m_EntitiesContacted[1];
	m_EntitiesContacted[1] = temp;
}
// this sets the contact data allowing the use of the default constructor making contact creation easier
void DragonContact::SetContactData(PhysicsObject* entity1,PhysicsObject* entity2,real restituion)
{
	m_EntitiesContacted[0]=entity1;
	m_EntitiesContacted[1]=entity2;
	m_Restitution=restituion;
}
DragonXVector3 DragonContact::CalculateLocalVelocity(unsigned entityIndex, real dt)
{
	// just for ease of use create a pointer to the entity we wanna use
	PhysicsObject *thisEntitiesContacted = m_EntitiesContacted[entityIndex];
	// Work out the velocity of the DragonContact point.
	// this will determine the velocity of the point
	// this will be used in the impulse calculation
	// note we need our rotational velocity in world space so we apply transform direction
	DragonXVector3 velocity = thisEntitiesContacted->GetDirectionInWorldSpace(thisEntitiesContacted->m_RotationalVelocity)%m_RelitiveContactPosition[entityIndex];

	// above will only get us the rotational velocity at the point
	// for impulse to work we need the velocity of the center of mass as well
	// which for us will just be the velocity of the object
	velocity += thisEntitiesContacted->m_LinearVelocity;

	// now for our friction and resting contacts to work properly
	// we need to move our velocity into contact coordinates
	// using our contact to world orthonormal basis
	DragonXVector3 contactVel = m_ContactToWorld.TransformTranspose(velocity);

	// ok now lets account for the acceleration from forces
	DragonXVector3 accVel = thisEntitiesContacted->m_LastFrameAccel * dt;

	//and again move to contact coordinates
	accVel = m_ContactToWorld.TransformTranspose(accVel);

	// we are only interested in the accel in the planar directions so not the contact normal direction
	// so we set the x component of the accel to 0
	accVel.x=0.0f;

	// combine the planar accel with contact velocity
	// if theres enough friction they will be removed
	contactVel+=accVel;

	// we have what we cam for return it
	return contactVel;
}
inline DragonXVector3 DragonContact::CalculateFrictionlessImpulse(DragonXMatrix *inverseInertiaTensor)
{
	// here we calculate the impulse
	// we use a vector for the return type as a vector3 impulse is used in friction impulse calculations so we
	// leave it as a vector 3 for now but may change it to just scalar impulse if its decided friction is
	// useless, and this is just a local variable to help make our lives just a bit easier
	DragonXVector3 impulseDragonContact;
	// ok this is going to basically be an iterative inertia adding up thingy
	// we will start by calculating the inertia for body 1
	// first up relitive contact position cross contact normal
	DragonXVector3 deltaVelWorld = m_RelitiveContactPosition[0] % m_ContactNormal;
	// transform by the inverse inertia tensor which is a mouthful
	// ill just call it tensor
	deltaVelWorld = inverseInertiaTensor[0].TransformDirection(deltaVelWorld);
	// cross with the relitive contact position again
	deltaVelWorld = deltaVelWorld % m_RelitiveContactPosition[0];
	// then dot with contact normal
	real deltaVelocity = deltaVelWorld * m_ContactNormal;
	// lastly we add in the linear component of the inertia with the inverse mass
	deltaVelocity += m_EntitiesContacted[0]->m_InverseMass;
	// Check if body 2 is a world object
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// yup ok add in its stuff too
		// Go through the same transformation sequence again
		DragonXVector3 deltaVelWorld = m_RelitiveContactPosition[1] % m_ContactNormal;
		deltaVelWorld = inverseInertiaTensor[1].TransformDirection(deltaVelWorld);
		deltaVelWorld = deltaVelWorld % m_RelitiveContactPosition[1];
		deltaVelocity += deltaVelWorld * m_ContactNormal;
		deltaVelocity += m_EntitiesContacted[1]->m_InverseMass;
	}
	//now the finale divide expected change in velocity by the very long divisor of inertia
	// again we use a vector 3 in case we need to switch to friction based impulse
	// but 4 now ill just set x to impulse n use it
	impulseDragonContact.x=(m_DesiredDeltaVelocity / deltaVelocity);
	impulseDragonContact.y=(0);
	impulseDragonContact.z=(0);
	return impulseDragonContact;
}
inline DragonXVector3 DragonContact::CalculateFrictionImpulse(DragonXMatrix * inverseInertiaTensor)
{
	DragonXVector3 impulseDragonContact;
	real inverseMass = m_EntitiesContacted[0]->m_InverseMass;
	// The equivalent of a cross product in matrices is multiplication
	// by a skew symmetric matrix - we build the matrix for converting
	// between linear and angular quantities.
	DragonXMatrix impulseToTorque;
	impulseToTorque.SetSkewSymmetric(m_RelitiveContactPosition[0]);
	// Build the matrix to convert DragonContact impulse to change in velocity
	// in world coordinates.
	DragonXMatrix deltaVelWorld = impulseToTorque;
	deltaVelWorld *= inverseInertiaTensor[0];
	deltaVelWorld *= impulseToTorque;
	deltaVelWorld *= -1;
	// Check if we need to add m_EntitiesContacted two's data
	if (m_EntitiesContacted[1]->m_PhysicsType)
	{
		// Set the cross product matrix
		impulseToTorque.SetSkewSymmetric(m_RelitiveContactPosition[1]);
		// Calculate the velocity change matrix
		DragonXMatrix deltaVelWorld2 = impulseToTorque;
		deltaVelWorld2 *= inverseInertiaTensor[1];
		deltaVelWorld2 *= impulseToTorque;
		deltaVelWorld2 *= -1;
		// Add to the total delta velocity.
		deltaVelWorld += deltaVelWorld2;
		// Add to the inverse mass
		inverseMass += m_EntitiesContacted[1]->m_InverseMass;
	}
	// Do a change of basis to convert into DragonContact coordinates.
	DragonXMatrix deltaVelocity = m_ContactToWorld.Transpose();
	deltaVelocity *= deltaVelWorld;
	deltaVelocity *= m_ContactToWorld;
	// Add in the linear velocity change
	deltaVelocity._11 += inverseMass;
	deltaVelocity._22 += inverseMass;
	deltaVelocity._33 += inverseMass;
	// Invert to get the impulse needed per unit velocity
	DragonXMatrix impulseMatrix = deltaVelocity.GetInverse();
	// Find the target velocities to kill
	DragonXVector3 velKill(m_DesiredDeltaVelocity,
		-m_ContactVelocity.y,
		-m_ContactVelocity.z);
	// Find the impulse to kill target velocities
	impulseDragonContact = impulseMatrix.TransformDirection(velKill);
	// Check for exceeding friction
	real planarImpulse = (real)sqrt(
		impulseDragonContact.y*impulseDragonContact.y +
		impulseDragonContact.z*impulseDragonContact.z
		);
	if (planarImpulse > impulseDragonContact.x * m_Friction)//this friction would be static friction below it would be dynamic friction
	{
		// We need to use dynamic friction
		impulseDragonContact.y=( impulseDragonContact.y/ planarImpulse);
		impulseDragonContact.z=( impulseDragonContact.z/ planarImpulse);
		impulseDragonContact.x=( deltaVelocity._11 +
			deltaVelocity._12*m_Friction*impulseDragonContact.y +
			deltaVelocity._13*m_Friction*impulseDragonContact.z);
		impulseDragonContact.x=( m_DesiredDeltaVelocity / impulseDragonContact.x);
		impulseDragonContact.y=( impulseDragonContact.y * m_Friction * impulseDragonContact.x);
		impulseDragonContact.z=( impulseDragonContact.z * m_Friction * impulseDragonContact.x);
	}
	return impulseDragonContact;
}
//**************************************************
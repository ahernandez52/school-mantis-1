#include "DragonContactRegistry.h"
#include "../Utility/DragonEnums.h"
#include <cassert>

const static real REAL_MAX=9999999.99f;// this is a static const used locally though it is technically a global variable or rather a global const so yes its global scope but its unmodifyable

//---------------------------------------------------------------
//  LOCAL STATIC INLINE HELPER FUNCTIONS
//---------------------------------------------------------------

// helper functions
// this is a helper function that basically performs the dot product of the vector 3 axis passed in with the half sizes of the box passed in
// the dot product resultant being the projection of one vector 3 or in this case the axis of one box on to the axis of the box thats passed in
// the return value is the result of these dot products
static inline real TransformToAxis( PhysicsObject &box, const DragonXVector3 &axis)
{
	return box.m_HalfExtents.x * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(0)) +
		box.m_HalfExtents.y * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(1)) +
		box.m_HalfExtents.z * (real)abs(axis * box.m_TransformMatrix.GetAxisVector(2));
}
// checks if the 2 obbs overlap along the given axis toCentre is just passed to avoid recalculating it
static inline real PenetrationOnAxis(PhysicsObject &one,PhysicsObject &two,const DragonXVector3 &axis,const DragonXVector3 &toCentre)
{
	// Project the half-size of one onto axis
	real oneProject = TransformToAxis(one, axis);
	real twoProject = TransformToAxis(two, axis);

	// Project this onto the axis
	real distance = (real)abs(toCentre * axis);

	// Return the overlap (i.e. positive indicates
	// overlap, negative indicates separation).
	return oneProject + twoProject - distance;
}
static inline bool TryAxis(PhysicsObject &one,PhysicsObject &two,DragonXVector3& axis,const DragonXVector3& toCentre,unsigned index,real& smallestPenetration,unsigned &smallestCase)
{
	// Make sure we have a normalized axis, and don't check almost parallel axes
	if (axis.GetMagSquared() < 0.0001)
	{
		return true;
	}
	axis.Normalize();

	real penetration = PenetrationOnAxis(one, two, axis, toCentre);

	if (penetration < 0)
	{
		return false;
	}
	if (penetration < smallestPenetration) 
	{
		smallestPenetration = penetration;
		smallestCase = index;
	}
	return true;
}
static inline DragonXVector3 ContactPoint(const DragonXVector3 &pOne,const DragonXVector3 &dOne,real oneSize,const DragonXVector3 &pTwo,const DragonXVector3 &dTwo,real twoSize,bool useOne)
{
	DragonXVector3 toSt, cOne, cTwo;
	real dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
	real denom, mua, mub;

	smOne = dOne.GetMagSquared();
	smTwo = dTwo.GetMagSquared();
	dpOneTwo = dTwo * dOne;

	toSt = pOne - pTwo;
	dpStaOne = dOne * toSt;
	dpStaTwo = dTwo * toSt;

	denom = smOne * smTwo - dpOneTwo * dpOneTwo;

	// Zero denominator indicates parallel lines
	if ((real)abs(denom) < 0.0001f) 
	{
		return useOne?pOne:pTwo;
	}

	mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
	mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

	// If either of the edges has the nearest point out
	// of bounds, then the edges aren't crossed, we have
	// an edge-face contact. Our point is on the edge, which
	// we know from the useOne parameter.
	if (mua > oneSize ||
		mua < -oneSize ||
		mub > twoSize ||
		mub < -twoSize)
	{
		return useOne?pOne:pTwo;
	}
	else
	{
		cOne = pOne + dOne * mua;
		cTwo = pTwo + dTwo * mub;
		return cOne * 0.5 + cTwo * 0.5;
	}
}
static inline bool SphereSphereIntersectTest(DragonXVector3& p1,real r1,DragonXVector3& p2,real r2)
{
	return (p1-p2).GetMagSquared()<(r1+r2)*(r1+r2);
}
static inline DragonXVector3 ClosestPtPointTriangle(DragonXVector3& p,DragonXVector3& a,DragonXVector3& b,DragonXVector3& c)
{
	// check if p is in vertex region outside a
	DragonXVector3 ab = b-a;
	DragonXVector3 ac = c-a;
	DragonXVector3 ap = p-a;
	real d1 = ab*ap;
	real d2 = ac*ap;
	if(d1<=0.0f&&d2<=0.0f)
	{
		return a;
	}

	// check b
	DragonXVector3 bp = p-b;
	real d3 = ab*bp;
	real d4=ac*bp;
	if(d3>=0.0f&&d4<=d3)
	{
		return b;
	}

	// check edge region ab
	real vc = (d1*d4)-(d3*d2);
	if(vc<=0.0f&&d1>=0.0f&&d3<=0.0f)
	{
		real v = d1/(d1-d3);
		return a+v*ab;
	}

	// check c
	DragonXVector3 cp = p-c;
	real d5=ab*cp;
	real d6=ac*cp;
	if(d6>=0.0f&&d5<=d6)
	{
		return c;
	}

	// check p on ac
	real vb = (d5*d2)-(d1*d6);
	if(vb<=0.0f&&d2>=0.0f&&d6<=0.0f)
	{
		real w = d2/(d2-d6);
		return a+w*ac;
	}

	// check p on bc
	real va = (d3*d6)-(d5*d4);
	if(va<=0.0f&&(d4-d3)>=0.0f&&(d5-d6)>=0.0f)
	{
		real w = (d4-d3)/((d4-d3)+(d5-d6));
		return b + w*(c-b);
	}

	// p is in the face region
	real denom = 1.0f/(va+vb+vc);
	real v = vb * denom;
	real w = vc * denom;
	return a+(ab*v)+(ac*w);
}
static inline real GetMax(real zed, real ein, real zwei)
{
	real ret=-9999999999.9f;
	if(zed>ret)
	{
		ret=zed;
	}
	if(ein>ret)
	{
		ret = ein;
	}
	if(zwei>ret)
	{
		ret = zwei;
	}
	return ret;
}
static inline real GetMin(real zed, real ein, real zwei)
{
	real ret=9999999999.9f;
	if(zed<ret)
	{
		ret=zed;
	}
	if(ein<ret)
	{
		ret = ein;
	}
	if(zwei<ret)
	{
		ret = zwei;
	}
	return ret;
}
static inline real GetMax(real zed, real ein)
{
	if(zed>ein)
	{
		return zed;
	}
	else
	{
		return ein;
	}
}
static inline real GetMin(real zed, real ein)
{
	if(zed<ein)
	{
		return zed;
	}
	else
	{
		return ein;
	}
}
inline DragonXVector3 CalcObbEdgeContactpoint(DragonXVector3& p,DragonXVector3& d,PhysicsObject* obb,real length)
{
	d.Normalize();
	real EPSILON = 0.001f;
	DragonXVector3 retPoint;
	real tmin=-999999999.9f;
	real tmax=length;
	if(abs(d.x)>EPSILON)
	{
		real ood = 1.0f / d.x;
		real t1 = (-obb->m_HalfExtents.x - p.x)*ood;
		real t2 = (obb->m_HalfExtents.x - p.x)*ood;
		if(t1>t2)
		{
			real temp = t1;
			t1=t2;
			t2=temp;
		}
		if(t1>tmin)
		{
			tmin=t1;
		}
		if (t2>tmax)
		{
			tmax=t2;
		}
	}

	if(abs(d.y)>EPSILON)
	{
		real ood = 1.0f / d.y;
		real t1 = (-obb->m_HalfExtents.y - p.y)*ood;
		real t2 = (obb->m_HalfExtents.y - p.y)*ood;
		if(t1>t2)
		{
			real temp = t1;
			t1=t2;
			t2=temp;
		}
		if(t1>tmin)
		{
			tmin=t1;
		}
		if (t2>tmax)
		{
			tmax=t2;
		}
	}

	if(abs(d.z)>EPSILON)
	{
		real ood = 1.0f / d.z;
		real t1 = (-obb->m_HalfExtents.z - p.z)*ood;
		real t2 = (obb->m_HalfExtents.z - p.z)*ood;
		if(t1>t2)
		{
			real temp = t1;
			t1=t2;
			t2=temp;
		}
		if(t1>tmin)
		{
			tmin=t1;
		}
		if (t2>tmax)
		{
			tmax=t2;
		}
	}
	retPoint = p+d*tmin;
	return retPoint;
}
static inline void clostestPtPointOBBOnObbNotIn(DragonXVector3& p,PhysicsObject* obb, DragonXVector3& q)
{
	DragonXVector3 d = p - obb->m_Position;
	q = obb->m_Position;
	DragonXVector3 he = obb->m_HalfExtents;
	//for each obb axis

	real dist = d * obb->m_TransformMatrix.GetAxisVector(0);
	// if the dist is further than the box extents clamp it
	if(dist> he.x)
	{
		dist = he.x;
	}
	if(dist < -he.x)
	{
		dist = -he.x;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(0);

	dist = d * obb->m_TransformMatrix.GetAxisVector(1);
	// if the dist is further than the box extents clamp it
	if(dist> he.y)
	{
		dist = he.y;
	}
	if(dist < -he.y)
	{
		dist = -he.y;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(1);

	dist = d * obb->m_TransformMatrix.GetAxisVector(2);
	// if the dist is further than the box extents clamp it
	if(dist> he.z)
	{
		dist = he.z;
	}
	if(dist < -he.z)
	{
		dist = -he.z;
	}
	q+=dist * obb->m_TransformMatrix.GetAxisVector(2);
	bool on=false;
	if(q.x>=he.x||q.x<=-he.x)
	{
		on=true;
	}
	if(q.y>=he.y||q.y<=-he.y)
	{
		on=true;
	}
	if(q.z>=he.z||q.z<=-he.z)
	{
		on=true;
	}
	if(!on)
	{
		DragonXVector3 dif = obb->GetPointInLocalSpace(q)-he;
		if((real)abs(dif.x)<(real)abs(dif.y)&&(real)abs(dif.x)<(real)abs(dif.z))
		{
			if(q.x<0)
			{
				q.x=obb->m_Position.x-he.x;
			}
			else
			{
				q.x=obb->m_Position.x+he.x;
			}
			return;
		}

		if((real)abs(dif.y)<(real)abs(dif.x)&&(real)abs(dif.y)<(real)abs(dif.z))
		{
			if(q.y<0)
			{
				q.y=obb->m_Position.y-he.y;
			}
			else
			{
				q.y=obb->m_Position.y+he.y;
			}
			return;
		}

		if((real)abs(dif.z)<(real)abs(dif.y)&&(real)abs(dif.z)<(real)abs(dif.x))
		{
			if(q.z<0)
			{
				q.z=obb->m_Position.z-he.z;
			}
			else
			{
				q.z=obb->m_Position.z+he.z;
			}
			return;
		}
	}
}
static inline real DistPointPlane(DragonXVector3& p, DragonXVector3& planeNormal,real planeDist)
{
	return p*planeNormal-planeDist;
}
inline bool TestTriVertsAgainstPlane(DragonXVector3& vertA,DragonXVector3& vertB,DragonXVector3& vertC,DragonXVector3& planeNormal,real planeDist,real& d0,real& d1,real& d2)
{
	d0 = DistPointPlane(vertA,planeNormal,planeDist);
	d1 = DistPointPlane(vertB,planeNormal,planeDist);
	d2 = DistPointPlane(vertC,planeNormal,planeDist);

	if(d0>0&&d1>0&&d2>0)
	{
		return false;
	}
	else if(d0<0&&d1<0&&d2<0)
	{
		return false;
	}
	else
	{
		return true;
	}
}
static inline DragonXVector3 TriEdgePointOfContact(DragonXVector3& p,DragonXVector3&q,DragonXVector3& a,DragonXVector3& b,DragonXVector3& c)
{
	DragonXVector3 pq = q-p;
	DragonXVector3 m = pq%p;
	real u = (pq*(c%b))+(m*(c-b));
	real v = (pq*(a%c))+(m*(a-c));
	real w = (pq*(b%a))+(m*(b-a));
	real denom=1.0f/(u+v+w);
	u*=denom;
	v*=denom;
	w*=denom;
	DragonXVector3 ret = u*a+v*b+w*c;
	return ret;
}
//-----------------------------------------------------------
// END LOCAL STATIC HELPER FUNCTIONS
//-----------------------------------------------------------

DragonContactRegistry::DragonContactRegistry()
{
	m_Restitution=0.0f;
	m_Limit=2500;
}
DragonContactRegistry::~DragonContactRegistry()
{
	ClearPairs();
	ClearContacts();
	ClearRayContacts();
}
// Public Functions
//=========================================
// quick test for sector ray
bool DragonContactRegistry::SectorRayCollisionDetection(DragonXVector3& spherePos,real rad,DragonXVector3& rayOrigin,DragonXVector3& rayDirection)
{
	DragonXVector3 m = rayOrigin-spherePos;
	real c = (m*m)-(rad*rad);

	if(c<=0.0f)
	{
		return true;
	}

	real b = m*rayDirection;

	if(b>0.0f)
	{
		return false;
	}

	real disc = b*b-c;

	if(disc < 0.0f)
	{
		return false;
	}
	else
	{
		return true;
	}
}
void DragonContactRegistry::ClearContacts()
{
	for(const auto DragonContact : m_Contacts)
	{
		delete DragonContact;
	}
	m_Contacts.clear();
}
void DragonContactRegistry::ClearPairs()
{
	for(const auto DragonicPair : m_ContactPairs)
	{
		delete DragonicPair;
	}
	m_ContactPairs.clear();
}
void DragonContactRegistry::GenerateContact(int collisionType,PhysicsObject*obj1,PhysicsObject*obj2)
{
	// this generates contacts based on the entities
	switch(collisionType)
	{
	case CT_SphereSphere:
		{
			GenerateContactSphereSphere(obj1,obj2);
			break;
		}
	case CT_SpherePlane:
		{
			GenerateContactSpherePlane(obj1,obj2);
			break;
		}
	case CT_SphereOBB:
		{
			GenerateContactSphereOBB(obj1,obj2);
			break;
		}
	case CT_ObbPlane:
		{
			GenerateContactOBBPlane(obj1,obj2);
			break;
		}
	case CT_ObbObb:
		{
			GenerateContactOBBOBB(obj1,obj2);
			break;
		}
	case CT_RaySphere:
		{
			GenerateContactRaySphere(obj1,obj2);
			break;
		}
	case CT_RayOBB:
		{
			GenerateContactRayObb(obj1,obj2);
			break;
		}
	case CT_MeshSphere:
		{
			GenerateContactMeshSphere(obj1,obj2);
			break;
		}
	case CT_MeshOBB:
		{
			GenerateContactMeshObb(obj1,obj2);
			break;
		}
	case CT_MeshMesh:
		{
			GenerateContactsMeshMesh(obj1,obj2);
			break;
		}
	case CT_MeshRay:
		{
			GenerateContactsMeshRay(obj1,obj2);
			break;
		}
	default:
		{
			break;
		}
	}
}
void DragonContactRegistry::GenerateContactSphereSphere(PhysicsObject* obj1,PhysicsObject* obj2)
{
	DragonContact *DC = new DragonContact();
	DragonXVector3 midLine(obj1->m_Position - obj2->m_Position);
	real size = midLine.GetMagnitude();
	DC->SetContactNormal(midLine*((1.0f)/size));
	DC->SetContactPoint(obj1->m_Position +midLine * 0.5f);
	DC->SetPenitrationDepth(obj1->m_Radius+obj2->m_Radius - size);
	DC->SetContactData(obj1,obj2,(real)m_Restitution);
	m_Contacts.push_back(DC);
}
// plane sphere
void DragonContactRegistry::GenerateContactSpherePlane(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Sphere)
	{
		DragonContact *DC = new DragonContact();
		real centerDist = obj2->GetNormal() * obj1->m_Position - obj2->m_DistanceFromOrigin;
		DragonXVector3 normal = obj2->GetNormal();
		real penetration = -centerDist;
		if(centerDist < 0)
		{
			normal.Invert();
			penetration = -penetration;
		}
		penetration+=obj1->m_Radius;
		// fill out contact data
		DC->SetContactNormal(normal);
		DC->SetPenitrationDepth(penetration);
		DC->SetContactPoint(obj1->m_Position - obj2->GetNormal() * centerDist);
		DC->SetContactData(obj1,obj2,(real)m_Restitution);
		m_Contacts.push_back(DC);
	}
	else
	{
		DragonContact *DC = new DragonContact();
		real centerDist = obj1->GetNormal() * obj2->m_Position - obj1->m_DistanceFromOrigin;
		DragonXVector3 normal = obj1->GetNormal();
		real penetration = -centerDist;
		if(centerDist < 0)
		{
			normal.Invert();
			penetration = -penetration;
		}
		penetration+=obj2->m_Radius;
		// fill out contact data
		DC->SetContactNormal(normal);
		DC->SetPenitrationDepth(penetration);
		DC->SetContactPoint(obj2->m_Position - obj1->GetNormal() * centerDist);
		DC->SetContactData(obj2,obj1,(real)m_Restitution);
		m_Contacts.push_back(DC);
	}
}
// sphere obb
void DragonContactRegistry::GenerateContactSphereOBB(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Sphere)//if object 1 is the sphere
	{
		DragonXVector3 center = obj1->m_Position;
		DragonXVector3 relCenter = obj2->m_TransformMatrix.TransformInverse(center);
		DragonXVector3 closetsPt(0.0f,0.0f,0.0f);
		real dist;
		// clamp each coordinate to the box
		dist = relCenter.x;
		if(dist > obj2->m_HalfExtents.x)
		{
			dist = obj2->m_HalfExtents.x;
		}
		if(dist < -obj2->m_HalfExtents.x)
		{
			dist = -obj2->m_HalfExtents.x;
		}
		closetsPt.x = dist;

		dist = relCenter.y;
		if(dist > obj2->m_HalfExtents.y)
		{
			dist = obj2->m_HalfExtents.y;
		}
		if(dist < -obj2->m_HalfExtents.y)
		{
			dist = -obj2->m_HalfExtents.y;
		}
		closetsPt.y = dist;

		dist = relCenter.z;
		if(dist > obj2->m_HalfExtents.z)
		{
			dist = obj2->m_HalfExtents.z;
		}
		if(dist < -obj2->m_HalfExtents.z)
		{
			dist = -obj2->m_HalfExtents.z;
		}
		closetsPt.z = dist;

		//check we are in contact last change for out
		dist = (closetsPt - relCenter).GetMagSquared();

		if(dist > obj1->m_Radius * obj1->m_Radius)
		{
			return;
		}
		else
		{
			DragonContact *DC = new DragonContact();
			DragonXVector3 closestPtWorld = obj2->m_TransformMatrix.Transform(closetsPt);
			DragonXVector3 norm = (closestPtWorld - obj1->m_Position);
			norm.Normalize();
			DC->SetContactNormal(norm);
			DC->SetContactPoint(closestPtWorld);
			DC->SetPenitrationDepth(obj1->m_Radius-sqrt(dist));
			DC->SetContactData(obj2,obj1,(real)m_Restitution);
			m_Contacts.push_back(DC);
		}
	}
	else //obj2 is the sphere and obj1 is the obb
	{
		DragonXVector3 center = obj2->m_Position;
		DragonXVector3 relCenter = obj1->m_TransformMatrix.TransformInverse(center);
		DragonXVector3 closetsPt(0.0f,0.0f,0.0f);
		real dist;
		// clamp each coordinate to the box
		dist = relCenter.x;
		if(dist > obj1->m_HalfExtents.x)
		{
			dist = obj1->m_HalfExtents.x;
		}
		if(dist < -obj1->m_HalfExtents.x)
		{
			dist = -obj1->m_HalfExtents.x;
		}
		closetsPt.x = dist;

		dist = relCenter.y;
		if(dist > obj1->m_HalfExtents.y)
		{
			dist = obj1->m_HalfExtents.y;
		}
		if(dist < -obj1->m_HalfExtents.y)
		{
			dist = -obj1->m_HalfExtents.y;
		}
		closetsPt.y = dist;

		dist = relCenter.z;
		if(dist > obj1->m_HalfExtents.z)
		{
			dist = obj1->m_HalfExtents.z;
		}
		if(dist < -obj1->m_HalfExtents.z)
		{
			dist = -obj1->m_HalfExtents.z;
		}
		closetsPt.z = dist;

		//check we are in contact last change for out
		dist = (closetsPt - relCenter).GetMagSquared();

		if(dist > obj2->m_Radius * obj2->m_Radius)
		{
			return;
		}
		else
		{
			DragonContact *DC = new DragonContact();
			DragonXVector3 closestPtWorld = obj1->m_TransformMatrix.Transform(closetsPt);
			DragonXVector3 norm = (closestPtWorld - obj2->m_Position);
			norm.Normalize();
			DC->SetContactNormal(norm);
			DC->SetContactPoint(closestPtWorld);
			DC->SetPenitrationDepth(obj2->m_Radius-sqrt(dist));
			DC->SetContactData(obj1,obj2,(real)m_Restitution);
			m_Contacts.push_back(DC);
		}
	}
}
// obb plane
void DragonContactRegistry::GenerateContactOBBPlane(PhysicsObject* obj1,PhysicsObject* obj2)
{
	// We have an intersection, so find the intersection points. We can make
	// do with only checking vertices's. If the box is resting on a plane
	// or on an edge, it will be reported as four or two contact points.

	// Go through each combination of + and - for each half-size
	static real mults[8][3] = {{1,1,1},{-1,1,1},{1,-1,1},{-1,-1,1},
	{1,1,-1},{-1,1,-1},{1,-1,-1},{-1,-1,-1}};
	DragonXVector3 cp;// this is used to generate the contact point 
	if(obj1->m_PrimitiveType==PT_OBB)//if obj1 is an obb
	{
		for(unsigned i = 0; i < 8; i++)
		{
			DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
			vertexPos.ComponentProductUpdate(obj1->m_HalfExtents);
			vertexPos = obj1->m_TransformMatrix.Transform(vertexPos);

			real vertexDist = vertexPos * obj2->GetNormal();

			if(vertexDist <= obj2->m_DistanceFromOrigin)
			{
				DragonContact *DC = new DragonContact();
				cp=obj2->GetNormal();
				cp*=(vertexDist-obj2->m_DistanceFromOrigin);
				cp+=vertexPos;
				DC->SetContactPoint(cp);
				DC->SetContactNormal(obj2->GetNormal());
				DC->SetPenitrationDepth(obj2->m_DistanceFromOrigin-vertexDist);
				DC->SetContactData(obj1,obj2,m_Restitution);
				m_Contacts.push_back(DC);
				/*if(m_vContacts.size()>=(unsigned)m_iLimit)
				{
				return;   // if we decide to limit contacts generated each frame this will be needed
				}*/
			}
		}
	}
	else//otherwise obj2 is the obb
	{
		for(unsigned i = 0; i < 8; i++)
		{
			DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
			vertexPos.ComponentProductUpdate(obj2->m_HalfExtents);
			vertexPos = obj2->m_TransformMatrix.Transform(vertexPos);

			real vertexDist = vertexPos * obj1->GetNormal();

			if(vertexDist <= obj1->m_DistanceFromOrigin)
			{
				DragonContact *DC = new DragonContact();
				cp=obj1->GetNormal();
				cp*=(vertexDist-obj1->m_DistanceFromOrigin);
				cp+=vertexPos;
				DC->SetContactPoint(cp);
				DC->SetContactNormal(obj1->GetNormal());
				DC->SetPenitrationDepth(obj1->m_DistanceFromOrigin-vertexDist);
				DC->SetContactData(obj2,obj1,m_Restitution);
				m_Contacts.push_back(DC);
				/*if(m_vContacts.size()>=(unsigned)m_iLimit)
				{
				return;
				}*/
			}
		}
	}
}
// obb obb
void DragonContactRegistry::GenerateContactOBBOBB(PhysicsObject* obj1,PhysicsObject* obj2)
{
	ObbObbContactGen(*obj1,*obj2);
}
// ray sphere 
void DragonContactRegistry::GenerateContactRaySphere(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if (obj1->m_PrimitiveType==PT_Ray)//if obj1 is the ray
	{
		DragonXVector3 m=obj1->m_Position-obj2->m_Position;
		real b = m*obj1->GetNormal();
		real c = m*m-obj2->m_Radius*obj2->m_Radius;
		if (c>0.0f&&b>0.0f)
		{
			return;
		}

		real discr = b*b-c;
		if(discr<0.0f)
		{
			return;
		}
		else
		{
			real t = -b-(real)sqrt(discr);
			if(t<0.0f)
			{
				t=0.0f;
			}		
			DragonContact* DC = new DragonContact();
			DC->SetContactData(obj1,obj2,m_Restitution);
			DC->SetPenitrationDepth(t);
			DC->SetContactPoint(DragonXVector3(obj1->m_Position+t*obj1->GetNormal()));	
			DC->SetContactNormal(obj1->GetNormal());
			m_RayContacts.push_back(DC);
		}
	} 
	else// obj2 is the ray
	{
		DragonXVector3 m=obj2->m_Position-obj1->m_Position;
		real b = m*obj2->GetNormal();
		real c = m*m-obj1->m_Radius*obj1->m_Radius;
		if (c>0.0f&&b>0.0f)
		{
			return;
		}

		real discr = b*b-c;
		if(discr<0.0f)
		{
			return;
		}
		else
		{
			real t = -b-(real)sqrt(discr);
			if(t<0.0f)
			{
				t=0.0f;
			}			
			DragonContact* DC = new DragonContact();
			DC->SetContactData(obj2,obj1,m_Restitution);
			DC->SetPenitrationDepth(t);
			DC->SetContactPoint(DragonXVector3(obj2->m_Position+t*obj2->GetNormal()));	
			DC->SetContactNormal(obj2->GetNormal());
			m_RayContacts.push_back(DC);
		}		
	}
}
// ray obb
void DragonContactRegistry::GenerateContactRayObb(PhysicsObject* obj1,PhysicsObject* obj2)
{
	real EPSILON = 0.001f;
	if (obj1->m_PrimitiveType==PT_Ray)//if obj1 is ray
	{		
		real tmin=-999999999.9f;
		real tmax=9999999999.9f;
		DragonXVector3 p = obj2->GetPointInLocalSpace(obj1->m_Position);
		DragonXVector3 d = obj2->GetDirectionInLocalSpace(obj1->GetNormal());
		if(abs(d.x)<EPSILON)// for x
		{
			if(p.x<-obj2->m_HalfExtents.x||p.x>obj2->m_HalfExtents.x)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.x;
			real t1 = (-obj2->m_HalfExtents.x - p.x)*ood;
			real t2 = (obj2->m_HalfExtents.x - p.x)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		if(abs(d.y)<EPSILON)// for y
		{
			if(p.y<-obj2->m_HalfExtents.y||p.y>obj2->m_HalfExtents.y)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.y;
			real t1 = (-obj2->m_HalfExtents.y - p.y)*ood;
			real t2 = (obj2->m_HalfExtents.y - p.y)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		if(abs(d.z)<EPSILON)// for z
		{
			if(p.z<-obj2->m_HalfExtents.z||p.z>obj2->m_HalfExtents.z)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.z;
			real t1 = (-obj2->m_HalfExtents.z - p.z)*ood;
			real t2 = (obj2->m_HalfExtents.z - p.z)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		DragonContact* DC = new DragonContact();
		DC->SetContactPoint(obj2->GetPointInWorldSpace(p+d*tmin));
		DC->SetContactNormal(obj2->GetDirectionInWorldSpace(d));
		DC->SetPenitrationDepth(tmin);
		DC->SetContactData(obj1,obj2,m_Restitution);
		m_RayContacts.push_back(DC);
	} 
	else// obj2 is the ray
	{
		real tmin=-999999999.9f;
		real tmax=9999999999.9f;
		DragonXVector3 p = obj1->GetPointInLocalSpace(obj2->m_Position);
		DragonXVector3 d = obj1->GetDirectionInLocalSpace(obj2->GetNormal());
		if(abs(d.x)<EPSILON)// for x
		{
			if(p.x<-obj1->m_HalfExtents.x||p.x>obj1->m_HalfExtents.x)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.x;
			real t1 = (-obj1->m_HalfExtents.x - p.x)*ood;
			real t2 = (obj1->m_HalfExtents.x - p.x)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		if(abs(d.y)<EPSILON)// for y
		{
			if(p.y<-obj1->m_HalfExtents.y||p.y>obj1->m_HalfExtents.y)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.y;
			real t1 = (-obj1->m_HalfExtents.y - p.y)*ood;
			real t2 = (obj1->m_HalfExtents.y - p.y)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		if(abs(d.z)<EPSILON)// for z
		{
			if(p.z<-obj1->m_HalfExtents.z||p.z>obj1->m_HalfExtents.z)
			{
				return;
			}
		}
		else
		{
			real ood = 1.0f / d.z;
			real t1 = (-obj1->m_HalfExtents.z - p.z)*ood;
			real t2 = (obj1->m_HalfExtents.z - p.z)*ood;
			if(t1>t2)
			{
				real temp = t1;
				t1=t2;
				t2=temp;
			}
			if(t1>tmin)
			{
				tmin=t1;
			}
			if (t2>tmax)
			{
				tmax=t2;
			}
			if (tmin>tmax)
			{
				return;
			}
		}
		DragonContact* DC = new DragonContact();
		DC->SetContactPoint(obj1->GetPointInWorldSpace(p+d*tmin));
		DC->SetContactNormal(obj1->GetDirectionInWorldSpace(d));
		DC->SetPenitrationDepth(tmin);
		DC->SetContactData(obj2,obj1,m_Restitution);
		m_RayContacts.push_back(DC);
	}
}
void DragonContactRegistry::FillPointFaceBoxBox(PhysicsObject &one,PhysicsObject &two,const DragonXVector3 &toCentre,unsigned best,real pen)
{
	// This method is called when we know that a vertex from
	// box two is in contact with box one.
	DragonContact *DC = new DragonContact();
	// We know which axis the collision is on (i.e. best),
	// but we need to work out which of the two faces on
	// this axis.
	DragonXVector3 normal = one.m_TransformMatrix.GetAxisVector(best);
	if (one.m_TransformMatrix.GetAxisVector(best) * toCentre > 0)
	{
		normal.Invert();
	}
	// Work out which vertex of box two we're colliding with.
	// Using toCentre doesn't work!
	DragonXVector3 vertex = two.m_HalfExtents;
	if (two.m_TransformMatrix.GetAxisVector(0) * normal < 0)//like wise changed
	{
		vertex.x = -vertex.x;
	}
	if (two.m_TransformMatrix.GetAxisVector(1) * normal < 0)//n again
	{
		vertex.y = -vertex.y;
	}
	if (two.m_TransformMatrix.GetAxisVector(2) * normal < 0)//n again
	{
		vertex.z = -vertex.z;
	}
	// Create the contact data
	DC->SetContactNormal(normal);
	DC->SetPenitrationDepth(pen);
	DC->SetContactPoint(two.m_TransformMatrix * vertex);
	DC->SetContactData(&one, &two,m_Restitution);
	m_Contacts.push_back(DC);
}
#define CHECK_OVERLAP(axis, index) \
	if (!TryAxis(one, two, (axis), toCentre, (index), pen, best)) return;
void DragonContactRegistry::ObbObbContactGen(PhysicsObject &one,PhysicsObject &two)
{
	// Find the vector between the two centers
	DragonXVector3 toCentre = two.m_Position - one.m_Position;

	// We start assuming there is no contact
	real pen = REAL_MAX;
	unsigned best = 0xffffff;

	// Now we check each axes, returning if it gives us
	// a separating axis, and keeping track of the axis with
	// the smallest penetration otherwise.
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(0), 0);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(1), 1);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(2), 2);

	CHECK_OVERLAP(two.m_TransformMatrix.GetAxisVector(0), 3);
	CHECK_OVERLAP(two.m_TransformMatrix.GetAxisVector(1), 4);
	CHECK_OVERLAP(two.m_TransformMatrix.GetAxisVector(2), 5);

	// Store the best axis-major, in case we run into almost
	// parallel edge collisions later
	unsigned bestSingleAxis = best;

	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(0), 6);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(1), 7);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(0) % two.m_TransformMatrix.GetAxisVector(2), 8);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(0), 9);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(1), 10);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(1) % two.m_TransformMatrix.GetAxisVector(2), 11);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(0), 12);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(1), 13);
	CHECK_OVERLAP(one.m_TransformMatrix.GetAxisVector(2) % two.m_TransformMatrix.GetAxisVector(2), 14);

	// Make sure we have got a result.
	assert(best != 0xffffff);

	// We now know there's a collision, and we know which
	// of the axes gave the smallest penetration. We now
	// can deal with it in different ways depending on
	// the case.
	if (best < 3)
	{
		// We've got a vertex of box two on a face of box one.
		FillPointFaceBoxBox(one, two, toCentre,best, pen);
		//fillPointFaceBoxBox(two, one, toCentre,best, pen);
	}
	else if (best < 6)
	{
		// We've got a vertex of box one on a face of box two.
		// We use the same algorithm as above, but swap around
		// one and two (and therefore also the vector between their
		// centers).
		FillPointFaceBoxBox(two, one, toCentre*-1.0f, best-3, pen);
		//fillPointFaceBoxBox(one, two,toCentre*1.0f, best-3, pen)
	}
	else
	{
		// We've got an edge-edge contact. Find out which axes
		best -= 6;
		unsigned oneAxisIndex = best / 3;
		unsigned twoAxisIndex = best % 3;
		DragonXVector3 oneAxis = one.m_TransformMatrix.GetAxisVector(oneAxisIndex);
		DragonXVector3 twoAxis = two.m_TransformMatrix.GetAxisVector(twoAxisIndex);
		DragonXVector3 axis = oneAxis % twoAxis;
		axis.Normalize();

		// The axis should point from box one to box two.
		if (axis * toCentre > 0)
		{
			axis = axis * -1.0f;
		}

		// We have the axes, but not the edges: each axis has 4 edges parallel
		// to it, we need to find which of the 4 for each object. We do
		// that by finding the point in the center of the edge. We know
		// its component in the direction of the box's collision axis is zero
		// (its a mid-point) and we determine which of the extremes in each
		// of the other axes is closest.
		DragonXVector3 ptOnOneEdge = one.m_HalfExtents;
		DragonXVector3 ptOnTwoEdge = two.m_HalfExtents;
		for (unsigned i = 0; i < 3; i++)
		{
			if (i == oneAxisIndex)
			{
				ptOnOneEdge[i] = 0;
			}
			else if (one.m_TransformMatrix.GetAxisVector(i) * axis > 0)
			{
				ptOnOneEdge[i] = -ptOnOneEdge[i];
			}

			if (i == twoAxisIndex)
			{
				ptOnTwoEdge[i] = 0;
			}
			else if (two.m_TransformMatrix.GetAxisVector(i) * axis < 0)
			{
				ptOnTwoEdge[i] = -ptOnTwoEdge[i];
			}
		}

		// Move them into world coordinates (they are already oriented
		// correctly, since they have been derived from the axes).
		ptOnOneEdge = one.m_TransformMatrix * ptOnOneEdge;
		ptOnTwoEdge = two.m_TransformMatrix * ptOnTwoEdge;

		// So we have a point and a direction for the colliding edges.
		// We need to find out point of closest approach of the two
		// line-segments.
		DragonXVector3 vertex = ContactPoint(ptOnOneEdge, oneAxis, one.m_HalfExtents[oneAxisIndex],ptOnTwoEdge, twoAxis, two.m_HalfExtents[twoAxisIndex],bestSingleAxis>2);

		// We can fill the contact.
		DragonContact *DC = new DragonContact();

		DC->SetPenitrationDepth(pen);
		DC->SetContactNormal(axis);
		DC->SetContactPoint(vertex);
		DC->SetContactData(&one, &two, m_Restitution);
		m_Contacts.push_back(DC);
	}
}
#undef CHECK_OVERLAP
// mesh sphere
void DragonContactRegistry::GenerateContactMeshSphere(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)// if obj1 is the mesh
	{
		for(auto sector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereIntersectTest(obj1->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj2->m_Position,obj2->m_Radius))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactSphereTri(obj1,obj2,tri);
				}
			}
		}
	}
	else// obj2 is mesh
	{
		for(auto sector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereIntersectTest(obj2->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj1->m_Position,obj1->m_Radius))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactSphereTri(obj2,obj1,tri);
				}
			}
		}
	}
}
// sphere tri
void DragonContactRegistry::GenerateContactSphereTri(PhysicsObject* mesh,PhysicsObject* sphere,DragonTriangle* tri)
{
	DragonXVector3 p = ClosestPtPointTriangle(sphere->m_Position,mesh->GetPointInWorldSpace(tri->GetVertA()),mesh->GetPointInWorldSpace(tri->GetVertB()),mesh->GetPointInWorldSpace(tri->GetVertC()));
	p-=sphere->m_Position;
	if (p*p<=sphere->m_Radius*sphere->m_Radius)
	{
		DragonContact *DC = new DragonContact();
		real pen = sphere->m_Radius-p.GetMagnitude();
		DC->SetPenitrationDepth(pen);
		DC->SetContactNormal(tri->GetNormal(mesh->GetPointInWorldSpace(tri->GetVertA()),mesh->GetPointInWorldSpace(tri->GetVertB()),mesh->GetPointInWorldSpace(tri->GetVertC())));
		p+=sphere->m_Position;
		DC->SetContactPoint(p);
		DC->SetContactData(mesh,sphere,(real)m_Restitution);
		m_Contacts.push_back(DC);
	}
}
// gen contact mesh obb
void DragonContactRegistry::GenerateContactMeshObb(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)// if obj1 is the mesh
	{
		for(auto sector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereIntersectTest(obj1->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj2->m_Position,obj2->m_Radius))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactObbTri(obj1,obj2,tri);
				}
			}
		}
	}
	else// obj2 is mesh
	{
		for(auto sector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereIntersectTest(obj2->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj1->m_Position,obj1->m_Radius))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactObbTri(obj2,obj1,tri);
				}
			}
		}
	}
}
// obb tri
void DragonContactRegistry::GenerateContactObbTri(PhysicsObject* mesh,PhysicsObject* obb,DragonTriangle* tri)
{
	DragonXVector3 vertA = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertA()));
	DragonXVector3 vertB = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertB()));
	DragonXVector3 vertC = obb->GetPointInLocalSpace(mesh->GetPointInWorldSpace(tri->GetVertC()));
	DragonXVector3 norm = tri->GetNormal(vertA,vertB,vertC);

	DragonXVector3 f0=vertB-vertA;

	DragonXVector3 f1=vertC-vertB;

	DragonXVector3 f2=vertA-vertC;

	DragonXVector3 tempConPoint;
	int axis = -1;
	DragonXVector3 bestAxis;
	real pen=999999999.0f;

	// one test against the tri face normal   this is the box plane test finish later
	DragonXVector3 axisfn = norm;
	bool triNormInverted=false;
	axisfn.Normalize();
	real p0 = vertA*axisfn;
	real p1 = vertB*axisfn;
	real p2 = vertC*axisfn;
	if(GetMin(p0,p1,p2)<0.0f)
	{
		triNormInverted=true;
		norm.Invert();
		axisfn.Invert();
		p0 = vertA*axisfn;
		p1 = vertB*axisfn;
		p2 = vertC*axisfn;
	}
	real tpen = TransformToAxis(*obb,axisfn)-GetMin(p0,p1);
	if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=TransformToAxis(*obb,axisfn))
	{
		if(tpen<pen&&tpen>=0.0f)
		{
			axis=0;
			bestAxis=axisfn;
			pen=tpen;
		}
	}
	else
	{
		return;
	}



	// 3 obb face normals 
	DragonXVector3 axis0 = obb->m_TransformMatrix.GetAxisVector(0);
	axis0.Normalize();
	p0 = vertA*axis0;
	p1 = vertB*axis0;
	p2 = vertC*axis0;
	if(GetMin(p0,p1,p2)<0.0f)
	{

		axis0.Invert();
		p0 = vertA*axis0;
		p1 = vertB*axis0;
		p2 = vertC*axis0;
	}
	tpen = TransformToAxis(*obb,axis0)-GetMin(p0,p1,p2);
	if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=TransformToAxis(*obb,axis0))
	{
		if(tpen<pen&&tpen>=0.0f)
		{
			if(tpen==TransformToAxis(*obb,axis0)-p0)
			{
				tempConPoint=vertA;
			}
			if(tpen==TransformToAxis(*obb,axis0)-p1)
			{
				tempConPoint=vertB;
			}
			if(tpen==TransformToAxis(*obb,axis0)-p2)
			{
				tempConPoint=vertC;
			}
			axis=1;
			bestAxis=axis0;
			pen=tpen;
		}
	}
	else
	{
		return;
	}


	DragonXVector3 axis1 = obb->m_TransformMatrix.GetAxisVector(1);
	axis1.Normalize();
	p0 = vertA*axis1;
	p1 = vertB*axis1;
	p2 = vertC*axis1;
	if(GetMin(p0,p1,p2)<0.0f)
	{

		axis1.Invert();
		p0 = vertA*axis1;
		p1 = vertB*axis1;
		p2 = vertC*axis1;
	}
	tpen = TransformToAxis(*obb,axis1)-GetMin(p0,p1,p2);
	if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=TransformToAxis(*obb,axis1))
	{
		if(tpen<pen&&tpen>=0.0f)
		{
			if(tpen==TransformToAxis(*obb,axis1)-p0)
			{
				tempConPoint=vertA;
			}
			if(tpen==TransformToAxis(*obb,axis1)-p1)
			{
				tempConPoint=vertB;
			}
			if(tpen==TransformToAxis(*obb,axis1)-p2)
			{
				tempConPoint=vertC;
			}
			axis=2;
			bestAxis=axis1;
			pen=tpen;
		}
	}
	else
	{
		return;
	}


	DragonXVector3 axis2 = obb->m_TransformMatrix.GetAxisVector(2);
	axis2.Normalize();
	p0 = vertA*axis2;
	p1 = vertB*axis2;
	p2 = vertC*axis2;
	if(GetMin(p0,p1,p2)<0.0f)
	{

		axis2.Invert();
		p0 = vertA*axis2;
		p1 = vertB*axis2;
		p2 = vertC*axis2;
	}
	tpen = TransformToAxis(*obb,axis2)-GetMin(p0,p1,p2);
	if(GetMax(-1.0f*GetMax(p0,p1,p2),GetMin(p0,p1,p2))<=TransformToAxis(*obb,axis2))
	{
		if(tpen<pen&&tpen>=0.0f)
		{
			if(tpen==TransformToAxis(*obb,axis2)-p0)
			{
				tempConPoint=vertA;
			}
			if(tpen==TransformToAxis(*obb,axis2)-p1)
			{
				tempConPoint=vertB;
			}
			if(tpen==TransformToAxis(*obb,axis2)-p2)
			{
				tempConPoint=vertC;
			}
			axis=3;
			bestAxis=axis2;
			pen=tpen;
		}
	}
	else
	{
		return;
	}


	// cross product axis tests
	DragonXVector3 axis00 = obb->m_TransformMatrix.GetAxisVector(0)%f0;
	axis00.Normalize();
	if(axis00.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis00;
		p1 = vertB*axis00;
		p2 = vertC*axis00;
		if(GetMin(p0,p1,p2)<0.0f)
		{

			axis00.Invert();
			p0 = vertA*axis00;
			p1 = vertB*axis00;
			p2 = vertC*axis00;
		}
		tpen = TransformToAxis(*obb,axis00)-GetMin(p0,p2);
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=TransformToAxis(*obb,axis00))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=4;

				bestAxis=axis00;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis01 = obb->m_TransformMatrix.GetAxisVector(0)%f1;
	axis01.Normalize();
	if(axis01.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis01;
		p1 = vertB*axis01;
		p2 = vertC*axis01;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			axis01.Invert();
			p0 = vertA*axis01;
			p1 = vertB*axis01;
			p2 = vertC*axis01;
		}
		tpen = TransformToAxis(*obb,axis01)-GetMin(p0,p2);
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=TransformToAxis(*obb,axis01))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=5;			
				bestAxis=axis01;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis02 = obb->m_TransformMatrix.GetAxisVector(0)%f2;
	axis02.Normalize();
	if(axis02.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis02;
		p1 = vertB*axis02;
		p2 = vertC*axis02;
		if(GetMin(p0,p1,p2)<0.0f)
		{	
			axis02.Invert();
			p0 = vertA*axis02;
			p1 = vertB*axis02;
			p2 = vertC*axis02;
		}
		tpen = TransformToAxis(*obb,axis02)-GetMin(p1,p2);
		if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))<=TransformToAxis(*obb,axis02))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=6;			
				bestAxis=axis02;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis10 = obb->m_TransformMatrix.GetAxisVector(1)%f0;
	axis10.Normalize();
	if(axis10.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis10;
		p1 = vertB*axis10;
		p2 = vertC*axis10;
		if(GetMin(p0,p1,p2)<0.0f)
		{

			axis10.Invert();
			p0 = vertA*axis10;
			p1 = vertB*axis10;
			p2 = vertC*axis10;
		}
		tpen=TransformToAxis(*obb,axis10)-GetMin(p0,p2);
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=TransformToAxis(*obb,axis10))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=7;		
				bestAxis=axis10;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis11 = obb->m_TransformMatrix.GetAxisVector(1)%f1;
	axis11.Normalize();
	if(axis11.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis11;
		p1 = vertB*axis11;
		p2 = vertC*axis11;
		if(GetMin(p0,p1,p2)<0.0f)
		{
			axis11.Invert();
			p0 = vertA*axis11;
			p1 = vertB*axis11;
			p2 = vertC*axis11;
		}
		tpen=TransformToAxis(*obb,axis11)-GetMin(p0,p2);
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=TransformToAxis(*obb,axis11))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=8;			
				bestAxis=axis11;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis12 = obb->m_TransformMatrix.GetAxisVector(1)%f2;
	axis12.Normalize();
	if(axis12.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis12;
		p1 = vertB*axis12;
		p2 = vertC*axis12;
		if(GetMin(p0,p1,p2)<0.0f)
		{

			axis12.Invert();
			p0 = vertA*axis12;
			p1 = vertB*axis12;
			p2 = vertC*axis12;
		}
		tpen = TransformToAxis(*obb,axis12)-GetMin(p0,p1);
		if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))<=TransformToAxis(*obb,axis12))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=9;
				bestAxis=axis12;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis20 = obb->m_TransformMatrix.GetAxisVector(2)%f0;
	axis20.Normalize();
	if(axis20.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis20;
		p1 = vertB*axis20;
		p2 = vertC*axis20;
		if(GetMin(p0,p1,p2)<0.0f)
		{

			axis20.Invert();
			p0 = vertA*axis20;
			p1 = vertB*axis20;
			p2 = vertC*axis20;
		}
		tpen=TransformToAxis(*obb,axis20)-GetMin(p0,p2);
		if(GetMax(-1.0f*GetMax(p0,p2),GetMin(p0,p2))<=TransformToAxis(*obb,axis20))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=10;			
				bestAxis=axis20;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis21 = obb->m_TransformMatrix.GetAxisVector(2)%f1;
	axis21.Normalize();
	if(axis21.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis21;
		p1 = vertB*axis21;
		p2 = vertC*axis21;
		if(GetMin(p0,p1,p2)<0.0f)
		{			
			axis21.Invert();
			p0 = vertA*axis21;
			p1 = vertB*axis21;
			p2 = vertC*axis21;
		}
		tpen=TransformToAxis(*obb,axis21)-GetMin(p0,p1);
		if(GetMax(-1.0f*GetMax(p0,p1),GetMin(p0,p1))<=TransformToAxis(*obb,axis21))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=11;			
				bestAxis=axis21;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	DragonXVector3 axis22 = obb->m_TransformMatrix.GetAxisVector(2)%f2;
	axis22.Normalize();
	if(axis22.GetMagSquared()>0.0001)
	{
		p0 = vertA*axis22;
		p1 = vertB*axis22;
		p2 = vertC*axis22;
		if(GetMin(p0,p1,p2)<0.0f)
		{		
			axis22.Invert();
			p0 = vertA*axis22;
			p1 = vertB*axis22;
			p2 = vertC*axis22;
		}
		tpen=TransformToAxis(*obb,axis22)-GetMin(p1,p2);
		if(GetMax(-1.0f*GetMax(p1,p2),GetMin(p1,p2))<=TransformToAxis(*obb,axis22))
		{
			if(tpen<pen&&tpen>=0.0f)
			{
				axis=12;			
				bestAxis=axis22;
				pen=tpen;
			}
		}
		else
		{
			return;
		}
	}

	if(axis!=-1)
	{
		if(axis==0)
		{
			DragonXVector3 Wnorm = tri->GetNormal(mesh->GetPointInWorldSpace(tri->GetVertA()),mesh->GetPointInWorldSpace(tri->GetVertB()),mesh->GetPointInWorldSpace(tri->GetVertC()));
			// Go through each combination of + and - for each half-size
			static real mults[8][3] = {{1,1,1},{-1,1,1},{1,-1,1},{-1,-1,1},
			{1,1,-1},{-1,1,-1},{1,-1,-1},{-1,-1,-1}};
			DragonXVector3 pcp;
			for(unsigned i = 0; i < 8; i++)
			{
				DragonXVector3 vertexPos(mults[i][0],mults[i][1],mults[i][2]);
				vertexPos.ComponentProductUpdate(obb->m_HalfExtents);
				//vertexPos = obb->GetPhysicsPointer()->getTransform().transform(vertexPos);
				DragonXVector3 p = ClosestPtPointTriangle(vertexPos,vertA,vertB,vertC);
				clostestPtPointOBBOnObbNotIn(obb->GetPointInWorldSpace(p),obb,pcp);				
				real pdist= p*norm;
				p=obb->GetPointInLocalSpace(pcp);
				real vdist = vertexPos*norm;
				if(vdist>=pdist)
				{
					DragonContact *DC = new DragonContact();
					DragonXVector3 conp;
					conp=Wnorm;
					conp*=pen;
					conp+=pcp;
					DC->SetContactPoint(conp);
					conp = Wnorm;
					if(triNormInverted)
					{
						conp.Invert();
					}
					DC->SetContactNormal(conp);
					DC->SetPenitrationDepth(pen);
					DC->SetContactData(mesh,obb,m_Restitution);
					m_Contacts.push_back(DC);
				}
			}
			return;
		}
		if(axis>0&&axis<4)
		{
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(obb->GetPointInWorldSpace(tempConPoint));
			DC->SetContactNormal(obb->GetDirectionInWorldSpace(bestAxis));
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh,obb,m_Restitution);
			m_Contacts.push_back(DC);
			return;
		}
		DragonXVector3 cp;
		if(axis==4||axis==7||axis==10)
		{
			cp = CalcObbEdgeContactpoint(vertA,f0,obb,f0.GetMagnitude());
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(obb->GetPointInWorldSpace(cp));
			DC->SetContactNormal(obb->GetDirectionInWorldSpace(bestAxis));
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh,obb,m_Restitution);
			m_Contacts.push_back(DC);
			return;
		}

		if(axis==5||axis==8||axis==11)
		{
			cp = CalcObbEdgeContactpoint(vertB,f1,obb,f1.GetMagnitude());
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(obb->GetPointInWorldSpace(cp));
			DC->SetContactNormal(obb->GetDirectionInWorldSpace(bestAxis));
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh,obb,m_Restitution);
			m_Contacts.push_back(DC);
			return;
		}

		if(axis==6||axis==9||axis==12)
		{
			cp = CalcObbEdgeContactpoint(vertC,f2,obb,f2.GetMagnitude());
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(obb->GetPointInWorldSpace(cp));
			DC->SetContactNormal(obb->GetDirectionInWorldSpace(bestAxis));
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh,obb,m_Restitution);
			m_Contacts.push_back(DC);
			return;
		}
		return;		
	}
}
// gen contact mesh mesh
void DragonContactRegistry::GenerateContactsMeshMesh(PhysicsObject* obj1,PhysicsObject* obj2)
{
	for(auto obj1Sector : obj1->m_CollisionMesh->m_DragonSectors)
	{
		for(auto obj2Sector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SphereSphereIntersectTest(obj1->GetPointInWorldSpace(obj1Sector->m_Position),obj1Sector->m_Radius,obj2->GetPointInWorldSpace(obj2Sector->m_Position),obj2Sector->m_Radius))
			{
				for(auto tri1 : obj1Sector->m_SectorTris)
				{
					for(auto tri2 : obj2Sector->m_SectorTris)
					{
						GenerateContactsTriTri(obj1,tri1,obj2,tri2);
					}
				}
			}
		}
	}
}
// gen contacts tri tri
void DragonContactRegistry::GenerateContactsTriTri(PhysicsObject* mesh1,DragonTriangle* tri1,PhysicsObject* mesh2,DragonTriangle* tri2)
{
	DragonXVector3 tri1A = mesh1->GetPointInWorldSpace(tri1->GetVertA());
	DragonXVector3 tri1B = mesh1->GetPointInWorldSpace(tri1->GetVertB());
	DragonXVector3 tri1C = mesh1->GetPointInWorldSpace(tri1->GetVertC());
	//tri2
	DragonXVector3 tri2A = mesh2->GetPointInWorldSpace(tri2->GetVertA());
	DragonXVector3 tri2B = mesh2->GetPointInWorldSpace(tri2->GetVertB());
	DragonXVector3 tri2C = mesh2->GetPointInWorldSpace(tri2->GetVertC());
	DragonXVector3 tri1Norm = tri1->GetNormal(tri1A,tri1B,tri1C);
	DragonXVector3 tri2Norm = tri2->GetNormal(tri2A,tri2B,tri2C);
	real du0=0.0f;
	real du1=0.0f;
	real du2=0.0f;
	real dv0=0.0f;
	real dv1=0.0f;
	real dv2 = 0.0f;
	DragonXVector3 pointOfContact;
	real pen=999999999999.9f;
	DragonXVector3 contactNorm;

	if(!TestTriVertsAgainstPlane(tri1A,tri1B,tri1C,tri2Norm,tri2Norm*tri2C,du0,du1,du2))
	{
		return;
	}

	if(!TestTriVertsAgainstPlane(tri2A,tri2B,tri2C,tri1Norm,tri1Norm*tri1C,dv0,dv1,dv2))
	{
		return;
	}	

	// now compute the edges
	DragonXVector3 tri1ba = tri1B-tri1A;
	DragonXVector3 tri1cb = tri1C-tri1B;
	DragonXVector3 tri1ac = tri1A-tri1C;
	//tri2
	DragonXVector3 tri2ba = tri2B-tri2A;
	DragonXVector3 tri2cb = tri2C-tri2B;
	DragonXVector3 tri2ac = tri2A-tri2C;

	real tpen = 99999999999.9f;
	real pu0,pu1,pu2,pv0,pv1,pv2;
	int axis = -1;
	DragonXVector3 cnAxis;
	DragonXVector3 cp;
	if(du0<pen&&du0>0.0f)
	{
		pen = du0;
		axis = 0;
		cnAxis=tri2Norm;
		cp=tri1A;
	}
	if(du1<pen&&du1>0.0f)
	{
		pen = du1;
		axis = 1;
		cnAxis=tri2Norm;
		cp=tri1B;
	}
	if(du2<pen&&du2>0.0f)
	{
		pen = du2;
		axis = 2;
		cnAxis=tri2Norm;
		cp=tri1C;
	}
	// test tri2 verts against tri 1 plane
	if(dv0<pen&&dv0>0.0f)
	{
		pen = dv0;
		axis = 3;
		cnAxis=tri1Norm;
		cp=tri2A;
	}
	if(dv1<pen&&dv1>0.0f)
	{
		pen = dv1;
		axis = 4;
		cnAxis=tri1Norm;
		cp=tri2B;
	}
	if(dv2<pen&&dv2>0.0f)
	{
		pen = dv2;
		axis = 5;
		cnAxis=tri1Norm;
		cp=tri2C;
	}
	// 9 cross product axis tests
	// set 1
	DragonXVector3 testAxis = tri1ba%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=6;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=7;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1ba%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=8;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=9;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1ba%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=10;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=11;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}
	//set2 
	testAxis = tri1cb%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=12;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=13;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1cb%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=14;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=15;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1cb%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=16;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=17;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}
	// set3
	testAxis = tri1ac%tri2ba;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=18;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=19;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1ac%tri2cb;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=20;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=21;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	testAxis = tri1ac%tri2ac;
	testAxis.Normalize();
	if(testAxis.GetMagSquared()>0.0001)
	{
		pu0=tri1A*testAxis;
		pu1=tri1B*testAxis;
		pu2=tri1C*testAxis;		

		if(GetMin(pu0,pu1,pu2)<0.0f)
		{
			testAxis.Invert();
			pu0=tri1A*testAxis;
			pu1=tri1B*testAxis;
			pu2=tri1C*testAxis;
		}
		pv0=tri2A*testAxis;
		pv1=tri2B*testAxis;
		pv2=tri2C*testAxis;

		if(GetMax(pv0,pv1,pv2)>GetMin(pu0,pu1,pu2))	
		{
			tpen=GetMax(pv0,pv1,pv2)-GetMin(pu0,pu1,pu2);
			if(tpen<pen&&tpen>0.0f)
			{
				pen=tpen;
				axis=22;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
		if(GetMin(pv0,pv1,pv2)<GetMax(pu0,pu1,pu2))	
		{
			tpen=GetMax(pu0,pu1,pu2)-GetMin(pv0,pv1,pv2);
			if(tpen<pen&&tpen>0.0f)
			{

				// note may need 2 Invert contact normal
				pen=tpen;
				axis=23;
				cnAxis=testAxis;
			}
		}
		else
		{
			return;
		}
	}

	if(axis<6)
	{
		if(axis<3)
		{
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(cp);
			DC->SetContactNormal(cnAxis);
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh2,mesh1,m_Restitution);
			m_Contacts.push_back(DC);
			return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
		}			
		else
		{
			DragonContact *DC = new DragonContact();
			DC->SetContactPoint(cp);
			DC->SetContactNormal(cnAxis);
			DC->SetPenitrationDepth(pen);
			DC->SetContactData(mesh1,mesh2,m_Restitution);
			m_Contacts.push_back(DC);
			return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
		}
	}
	else if(axis>=6&&axis<=11)
	{
		cp=TriEdgePointOfContact(tri1A,tri1B,tri2C,tri2B,tri2A);
		DragonContact *DC = new DragonContact();
		DC->SetContactPoint(cp);
		DC->SetContactNormal(cnAxis);
		DC->SetPenitrationDepth(pen);
		if(axis==7||axis==9||axis==11)
		{
			DC->SetContactData(mesh2,mesh1,m_Restitution);
		}
		else
		{
			DC->SetContactData(mesh1,mesh2,m_Restitution);
		}
		m_Contacts.push_back(DC);
		return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
	}
	else if(axis>=12&&axis<=17)
	{
		cp=TriEdgePointOfContact(tri1B,tri1C,tri2C,tri2B,tri2A);
		DragonContact *DC = new DragonContact();
		DC->SetContactPoint(cp);
		DC->SetContactNormal(cnAxis);
		DC->SetPenitrationDepth(pen);
		if(axis==13||axis==15||axis==17)
		{
			DC->SetContactData(mesh2,mesh1,m_Restitution);
		}
		else
		{
			DC->SetContactData(mesh1,mesh2,m_Restitution);
		}
		m_Contacts.push_back(DC);
		return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
	}
	else if(axis>=18&&axis<=23)
	{
		cp=TriEdgePointOfContact(tri1C,tri1A,tri2C,tri2B,tri2A);
		DragonContact *DC = new DragonContact();
		DC->SetContactPoint(cp);
		DC->SetContactNormal(cnAxis);
		DC->SetPenitrationDepth(pen);
		if(axis==19||axis==21||axis==23)
		{
			DC->SetContactData(mesh2,mesh1,m_Restitution);
		}
		else
		{
			DC->SetContactData(mesh1,mesh2,m_Restitution);
		}
		m_Contacts.push_back(DC);
		return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
	}
}

// gen contact mesh ray
void DragonContactRegistry::GenerateContactsMeshRay(PhysicsObject* obj1,PhysicsObject* obj2)
{
	if(obj1->m_PrimitiveType==PT_Mesh)// if obj1 is the mesh
	{
		for(auto sector : obj1->m_CollisionMesh->m_DragonSectors)
		{
			if(SectorRayCollisionDetection(obj1->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj2->m_Position,obj2->GetNormal()))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactsRayTri(obj1,tri,obj2);
				}
			}
		}
	}
	else// obj2 is mesh
	{
		for(auto sector : obj2->m_CollisionMesh->m_DragonSectors)
		{
			if(SectorRayCollisionDetection(obj2->GetPointInWorldSpace(sector->m_Position),sector->m_Radius,obj1->m_Position,obj1->GetNormal()))
			{
				for(auto tri : sector->m_SectorTris)
				{
					GenerateContactsRayTri(obj2,tri,obj1);
				}
			}
		}
	}
}
// tri ray contact gen
void DragonContactRegistry::GenerateContactsRayTri(PhysicsObject* mesh, DragonTriangle* tri,PhysicsObject* ray)
{
	DragonXVector3 rayNormal = ray->GetNormal();
	DragonXVector3 a=mesh->GetPointInWorldSpace(tri->GetVertA());
	DragonXVector3 b=mesh->GetPointInWorldSpace(tri->GetVertB());
	DragonXVector3 c=mesh->GetPointInWorldSpace(tri->GetVertC());
	DragonXVector3 m = rayNormal%ray->m_Position;
	real u = (rayNormal*(c%b))+(m*(c-b));
	if(u<0.0f)
	{
		return;
	}
	real v = (rayNormal*(a%c))+(m*(a-c));
	if(v<0.0f)
	{
		return;
	}
	real w = (rayNormal*(b%a))+(m*(b-a));
	if(w<0.0f)
	{
		return;
	}
	real denom=1.0f/(u+v+w);
	u*=denom;
	v*=denom;
	w*=denom;
	DragonXVector3 rayPointOfContact = u*a+v*b+w*c;
	DragonContact *DC = new DragonContact();
	DC->SetContactPoint(rayPointOfContact);
	DC->SetContactNormal(rayNormal);
	DC->SetPenitrationDepth((rayPointOfContact-ray->m_Position).GetMagnitude());
	DC->SetContactData(mesh,ray,m_Restitution);
	m_RayContacts.push_back(DC);
	return;// the 2 tris over lap completely on the same plane contact gen is any vert normal of either tri 0 pen
}
void DragonContactRegistry::ResolveContacts(DragonContactResolver* resolver,real dt)
{
	CleanContacts();
	CleanPairs();
	resolver->SetIterations(m_Contacts.size()*4);
	resolver->ResolveContacts(m_Contacts,dt);
	UpdateContactList(resolver->GetEpsilon());
}
void DragonContactRegistry::UpdateContactList(real threshold)
{
	// this makes a temp copy of all the contact pairs
	std::vector<DragonicPair*> temp=m_ContactPairs;
	// this is a copy of the individual contacts
	std::vector<DragonContact*> tempC=m_Contacts;
	// this is a copy of ray contacts
	std::vector<DragonContact*> tempR=m_RayContacts;
	// clear ray contacts
	ClearRayContacts();
	// this clears the source vector of contacts 
	ClearContacts();
	// this simply clears the vector of contact pairs
	m_ContactPairs.clear();
	// this checks if the contact pair has had its contact(s) resolved if it has it will be removed if not it will have its contacts recreated
	for(const auto DragonicPair : temp)
	{
		if(DragonicPair->m_ContactregistryType==CRT_Natural)
		{		
			for(const auto DragonContact : tempC)
			{
				if(DragonContact->GetEntity(0)==DragonicPair->m_Obj1)
				{
					if(DragonContact->GetEntity(1)==DragonicPair->m_Obj2)
					{
						if(DragonContact->GetPenitration()>=threshold)
						{
							AddNaturalContactPair(DragonicPair->m_CollisionType,DragonicPair->m_Obj1,DragonicPair->m_Obj2);
						}
					}
				}
				else if(DragonContact->GetEntity(1)==DragonicPair->m_Obj1)
				{
					if(DragonContact->GetEntity(0)==DragonicPair->m_Obj2)
					{
						if(DragonContact->GetPenitration()>=threshold)
						{
							AddNaturalContactPair(DragonicPair->m_CollisionType,DragonicPair->m_Obj2,DragonicPair->m_Obj1);
						}
					}
				}
			}
		}
		else
		{
			for(const auto DragonContact : tempR)
			{
				if(DragonContact->GetEntity(0)==DragonicPair->m_Obj1)
				{
					if(DragonContact->GetEntity(1)==DragonicPair->m_Obj2)
					{
						if(DragonContact->GetPenitration()>=0.0f)
						{
							AddRayContactPair(DragonicPair->m_CollisionType,DragonicPair->m_Obj1,DragonicPair->m_Obj2);
						}
					}
				}
				else if(DragonContact->GetEntity(1)==DragonicPair->m_Obj1)
				{
					if(DragonContact->GetEntity(0)==DragonicPair->m_Obj2)
					{
						if(DragonContact->GetPenitration()>=0.0f)
						{
							AddRayContactPair(DragonicPair->m_CollisionType,DragonicPair->m_Obj2,DragonicPair->m_Obj1);
						}
					}
				}
			}
		}
	}
	// this is no longer needed so its destroyed
	for(const auto DragonicPair : temp)
	{
		delete DragonicPair;
	}
}

int DragonContactRegistry::AddNaturalContactPair(int collisionType, PhysicsObject* obj1, PhysicsObject* obj2)
{
	if(m_ContactPairs.size()>0)
	{
		bool add = true;
		for(auto pair : m_ContactPairs)
		{
			if(pair->m_Obj1==obj1&&pair->m_Obj2==obj2)
			{
				add=false;
			}
			else if(pair->m_Obj1==obj2&&pair->m_Obj2==obj1)
			{
				add=false;
			}
		}
		if(add)
		{
			DragonicPair* p = new DragonicPair;
			p->m_CollisionType=collisionType;
			p->m_ContactregistryType=CRT_Natural;
			p->m_Obj1=obj1;
			p->m_Obj2=obj2;
			if(obj1->m_IsCollisionResolutionOn&&obj2->m_IsCollisionResolutionOn)
			{
				GenerateContact(p->m_CollisionType,p->m_Obj1,p->m_Obj2);
			}
			m_ContactPairs.push_back(p);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		DragonicPair* p = new DragonicPair;
		p->m_CollisionType=collisionType;
		p->m_ContactregistryType=CRT_Natural;
		p->m_Obj1=obj1;
		p->m_Obj2=obj2;
		if(obj1->m_IsCollisionResolutionOn&&obj2->m_IsCollisionResolutionOn)
		{
			GenerateContact(p->m_CollisionType,p->m_Obj1,p->m_Obj2);
		}
		m_ContactPairs.push_back(p);
		return 1;
	}
}
// add ray contact
int DragonContactRegistry::AddRayContactPair(int collisionType, PhysicsObject* obj1, PhysicsObject* obj2)
{
	if(m_ContactPairs.size()>0)
	{
		bool add = true;
		for(auto pair : m_ContactPairs)
		{
			if(pair->m_Obj1==obj1&&pair->m_Obj2==obj2)
			{
				add=false;
			}
			else if(pair->m_Obj1==obj2&&pair->m_Obj2==obj1)
			{
				add=false;
			}
		}
		if(add)
		{
			DragonicPair* p = new DragonicPair;
			p->m_CollisionType=collisionType;
			p->m_ContactregistryType=CRT_Ray;
			p->m_Obj1=obj1;
			p->m_Obj2=obj2;
			if(obj1->m_IsCollisionResolutionOn&&obj2->m_IsCollisionResolutionOn)
			{
				GenerateContact(p->m_CollisionType,p->m_Obj1,p->m_Obj2);
			}
			m_ContactPairs.push_back(p);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		DragonicPair* p = new DragonicPair;
		p->m_CollisionType=collisionType;
		p->m_ContactregistryType=CRT_Ray;
		p->m_Obj1=obj1;
		p->m_Obj2=obj2;
		if(obj1->m_IsCollisionResolutionOn&&obj2->m_IsCollisionResolutionOn)
		{
			GenerateContact(p->m_CollisionType,p->m_Obj1,p->m_Obj2);
		}
		m_ContactPairs.push_back(p);
		return 1;
	}
}
//*******************************************************
#include "DragonForceEngine.h"

DragonForceEngine::DragonForceEngine()
{
	m_DragonForceRegistry=new DragonForceRegistry;
}
DragonForceEngine::~DragonForceEngine()
{
	delete m_DragonForceRegistry;
}
// Public Functions
//=======================================
//when adding forces try to make them self destroying see the force generators for details
void DragonForceEngine::AddThrustToOneEntity(PhysicsObject* entity, real durationOfThrust, real DecrementOfThrust, real MagnitudeOfThrust, DragonXVector3 &direction)
{
	Thrust* fg =new Thrust(durationOfThrust,DecrementOfThrust,MagnitudeOfThrust,direction);
	m_DragonForceRegistry->Add(entity,fg);
}
// default force is a temporary force upward like a space rocket
void DragonForceEngine::AddDefaultThrustToEntity(PhysicsObject* entity)
{
	Thrust* fg = new Thrust();
	m_DragonForceRegistry->Add(entity,fg);
}
//this will add a thrust force to all the entities in this list the same parameters will be used in each thrust force
void DragonForceEngine::AddThrustToVectorOfEntities(std::vector<PhysicsObject*>& entities, real durationOfThrust, real DecrementOfThrust, real MagnitudeOfThrust, DragonXVector3 &direction)
{
	for(const auto PhysicsObject : entities)
	{
		AddThrustToOneEntity(PhysicsObject,durationOfThrust,DecrementOfThrust,MagnitudeOfThrust,direction);
	}
}
// this does basically the same thing but with default values for the thrust force
void DragonForceEngine::AddDeafaultThrustForceToVectorOfEntities(std::vector<PhysicsObject*>& entities)
{
	for (const auto PhysicsObject : entities)
	{
		AddDefaultThrustToEntity(PhysicsObject);
	}
}
// this adds gravity towards a point to one entity
// for a black hole just use a real big number for mass
void DragonForceEngine::AddGravityFromPointToEntity(PhysicsObject* entity, DragonXVector3 &positionOfGravity, real massOfGravityObject)
{
	GravityFromPoint* fg = new GravityFromPoint(positionOfGravity,massOfGravityObject);
	m_DragonForceRegistry->Add(entity,fg);
}
// this adds gravity to a point to a vector of entities
void DragonForceEngine::AddGravityFromPointToVectorOfEntites(std::vector<PhysicsObject*>& entities,DragonXVector3 &positionOfGravityObject, real massOfGravityObject)
{
	for (const auto PhysicsObject : entities)
	{
		AddGravityFromPointToEntity(PhysicsObject,positionOfGravityObject,massOfGravityObject);
	}
}

// you can thank Nicholson for this one this will apply gravity to both entities based on each entities own mass and their distance from each other
void DragonForceEngine::AddGravityBetweenEntities(PhysicsObject* entity1,PhysicsObject* entity2)
{
	GravityBetweenEntities* fg = new GravityBetweenEntities(entity1,entity2);
	GravityBetweenEntities* fg2 = new GravityBetweenEntities(entity2,entity1);
	m_DragonForceRegistry->Add(entity1,fg);
	m_DragonForceRegistry->Add(entity2,fg2);
}
// and just for insanity this will apply gravity to all entities based on distance and mass will likely have negative effects on performance
void DragonForceEngine::AddGravityBetweenAllEntitiesInVector(std::vector<PhysicsObject*>& entities)
{
	for(int i = 0 ; (unsigned)i < entities.size()-1; i++)
	{
		for (int j=i+1;(unsigned)j < entities.size();j++)
		{
			if(entities[i]->HasFiniteMass()&&entities[j]->HasFiniteMass())
			{
				AddGravityBetweenEntities(entities[i],entities[j]);
			}
		}
	}
}
// because all games need big booms this is a default explosion force added to one entity
void DragonForceEngine::AddDefaultExplosionToOneEntity(PhysicsObject* entity)
{
	Explosion* fg = new Explosion();
	m_DragonForceRegistry->Add(entity,fg);
}
// this will allow for custom explosions as a result its a long one and you will need to enter a value for every parameter but alterations can be made if needed
void DragonForceEngine::AddCustomExplosionToOneEntity(PhysicsObject* entity,DragonXVector3 &pointOfDetonation,real implosionMinRadius,
													  real implosionMaxRadius, real implosionDuration, real implosionForce,real shockWaveSpeed, real shockWaveThickness, real peakConcussiveForce,
													  real concussionDuration, real peakConvectionForce, real chimneyRadius, real chimneyHeight, real convectionDuration )
{
	Explosion* fg = new Explosion();
	fg->m_PointOfDetonation=pointOfDetonation;
	fg->m_ImplosionMinRadius=implosionMinRadius;
	fg->m_ImplosionMaxRadius=implosionMaxRadius;
	fg->m_ImplosionDuration=implosionDuration;
	fg->m_ImplosionForce=implosionForce;
	fg->m_ShockWaveSpeed=shockWaveSpeed;
	fg->m_ShockWaveThickness=shockWaveThickness;
	fg->m_PeakConcussiveForce=peakConcussiveForce;
	fg->m_ConcussionDuration=concussionDuration;
	fg->m_PeakConvectionForce=peakConvectionForce;
	fg->m_ChmineyRadius=chimneyRadius;
	fg->m_ChimneyHeight=chimneyHeight;
	fg->m_ConvectionDuration=convectionDuration;
	m_DragonForceRegistry->Add(entity,fg);
}
// this is the easiest way of making an explosion
void DragonForceEngine::AddDefaultExplosionToVectorOfEntities(std::vector<PhysicsObject*>& entities)
{
	for (const auto PhysicsObject : entities)
	{
		AddDefaultExplosionToOneEntity(PhysicsObject);
	}
}
// this is the hard way yes all values do need to be entered as for good values the default ones should be decent
//for a description of the values see the force generator
void DragonForceEngine::AddCustomExplosionToVectorOfEntities(std::vector<PhysicsObject*>& entities,DragonXVector3 &pointOfDetonation,real implosionMinRadius,
															 real implosionMaxRadius, real implosionDuration, real implosionForce,real shockWaveSpeed, real shockWaveThickness, real peakConcussiveForce,
															 real concussionDuration, real peakConvectionForce, real chimneyRadius, real chimneyHeight, real convectionDuration )
{
	for (const auto PhysicsObject : entities)
	{
		AddCustomExplosionToOneEntity(PhysicsObject,pointOfDetonation,implosionMinRadius,implosionMaxRadius,implosionDuration,
			implosionForce,shockWaveSpeed,shockWaveThickness,peakConcussiveForce,concussionDuration,
			peakConvectionForce,chimneyRadius,chimneyHeight,convectionDuration);
	}
}
void DragonForceEngine::AddDefaultAnchoredSpringForceToEntity(PhysicsObject* entity)
{
	AnchoredSpringGenerator* fg = new AnchoredSpringGenerator();
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddDefaultAnchoredSpringForceToVectorOfEntities(std::vector<PhysicsObject*>& entities)
{
	for(const auto PhysicsObject : entities)
	{
		AddDefaultAnchoredSpringForceToEntity(PhysicsObject);
	}
}
void DragonForceEngine::AddDefaultAnchoredSpringForceToEntityWithOffSet(PhysicsObject* entity,DragonXVector3& offset)
{
	AnchoredSpringGenerator* fg = new AnchoredSpringGenerator();
	fg->SetOffSet(offset);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddDefaultAnchoredSpringForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,DragonXVector3& offset)
{
	for(const auto PhysicsObject : entities)
	{
		AddDefaultAnchoredSpringForceToEntityWithOffSet(PhysicsObject,offset);
	}
}
void DragonForceEngine::AddCustomAnchoredSpringForceToEntity(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos)
{
	AnchoredSpringGenerator* fg =new AnchoredSpringGenerator(springConstant,restLength,anchorPos);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddCustomAnchoredSpringForceToVectorOfEntities(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos)
{
	for(const auto PhysicsObject : entities)
	{
		AddCustomAnchoredSpringForceToEntity(PhysicsObject,springConstant,restLength,anchorPos);
	}
}
void DragonForceEngine::AddCustomAnchoredSpringForceToEntityWithOffSet(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset)
{
	AnchoredSpringGenerator* fg =new AnchoredSpringGenerator(springConstant,restLength,anchorPos);
	fg->SetOffSet(offset);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddCustomAnchoredSpringForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset)
{
	for (const auto PhysicsObject : entities)
	{
		AddCustomAnchoredSpringForceToEntityWithOffSet(PhysicsObject,springConstant,restLength,anchorPos,offset);
	}
}
void DragonForceEngine::AddDefaultAnchoredBungeeForceToEntity(PhysicsObject* entity)
{
	AnchoredBungeeGenerator* fg=new AnchoredBungeeGenerator();
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddDefaultAnchoredBungeeForceToVectorOfEntities(std::vector<PhysicsObject*>& entities)
{
	for (const auto PhysicsObject : entities)
	{
		AddDefaultAnchoredBungeeForceToEntity(PhysicsObject);
	}
}
void DragonForceEngine::AddDefaultAnchoredBungeeForceToEntityWithOffSet(PhysicsObject* entity,DragonXVector3& offset)
{
	AnchoredBungeeGenerator* fg=new AnchoredBungeeGenerator();
	fg->SetOffSet(offset);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddDefaultAnchoredBungeeForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,DragonXVector3& offset)
{
	for (const auto PhysicsObject : entities)
	{
		AddDefaultAnchoredBungeeForceToEntityWithOffSet(PhysicsObject,offset);
	}
}
void DragonForceEngine::AddCustomAnchoredBungeeForceToEntity(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos)
{
	AnchoredBungeeGenerator* fg=new AnchoredBungeeGenerator(springConstant,restLength,anchorPos);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddCustomAnchoredBungeeForceToVectorOfEntities(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos)
{
	for (const auto PhysicsObject : entities)
	{
		AddCustomAnchoredBungeeForceToEntity(PhysicsObject,springConstant,restLength,anchorPos);
	}
}
void DragonForceEngine::AddCustomAnchoredBungeeForceToEntityWithOffSet(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset)
{
	AnchoredBungeeGenerator* fg=new AnchoredBungeeGenerator(springConstant,restLength,anchorPos);
	fg->SetOffSet(offset);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddCustomAnchoredBungeeForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset)
{
	for (const auto PhysicsObject : entities)
	{
		AddCustomAnchoredBungeeForceToEntityWithOffSet(PhysicsObject,springConstant,restLength,anchorPos,offset);
	}
}
void DragonForceEngine::AddSpringForceToAnchorOneEntityToAnother(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity)
{
	ConnectingSpringForceGenerator* fg =new ConnectingSpringForceGenerator(springConstant,restLength,AnchorEntity);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddSpringForceToAnchorMultipleEntitiesToOneEntity(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity)
{
	for (const auto PhysicsObject : entities)
	{
		AddSpringForceToAnchorOneEntityToAnother(PhysicsObject,restLength,springConstant,AnchorEntity);
	}
}
void DragonForceEngine::AddSpringForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offSetForEntity,DragonXVector3& offsetForAnchor)
{
	ConnectingSpringForceGenerator* fg =new ConnectingSpringForceGenerator(springConstant,restLength,AnchorEntity);
	fg->SetOffSetMain(offSetForEntity);
	fg->SetOffSetOther(offsetForAnchor);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddSpringForceToAnchorMultipleEntitiesToOneEntityWithOffsets(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offsetForEntities,DragonXVector3& offsetForAnchor)
{
	for (const auto PhysicsObject : entities)
	{
		AddSpringForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject,restLength,springConstant,AnchorEntity,offsetForEntities,offsetForAnchor);
	}
}
void DragonForceEngine::AddBungeeForceToAnchorOneEntityToAnother(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity)
{
	ConnectingBungeeForceGenerator* fg =new ConnectingBungeeForceGenerator(springConstant,restLength,AnchorEntity);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddBungeeForceToAnchorMultipleEntitiesToOneEntity(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity)
{
	for (const auto PhysicsObject : entities)
	{
		AddBungeeForceToAnchorOneEntityToAnother(PhysicsObject,restLength,springConstant,AnchorEntity);
	}
}
void DragonForceEngine::AddBungeeForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offSetForEntity,DragonXVector3& offsetForAnchor)
{
	ConnectingBungeeForceGenerator* fg =new ConnectingBungeeForceGenerator(springConstant,restLength,AnchorEntity);
	fg->SetOffSetMain(offSetForEntity);
	fg->SetOffSetOther(offsetForAnchor);
	m_DragonForceRegistry->Add(entity,fg);
}
void DragonForceEngine::AddBungeeForceToAnchorMultipleEntitiesToOneEntityWithOffsets(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offsetForEntities,DragonXVector3& offsetForAnchor)
{
	for (const auto PhysicsObject : entities)
	{
		AddBungeeForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject,restLength,springConstant,AnchorEntity,offsetForEntities,offsetForAnchor);
	}
}
void DragonForceEngine::AddSpringForceForConnectingTwoEntitiesTogether(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant)
{
	ConnectingSpringForceGenerator* fg1 = new ConnectingSpringForceGenerator(springConstant,restLength,entity2);
	ConnectingSpringForceGenerator* fg2 =new ConnectingSpringForceGenerator(springConstant,restLength,entity1);
	m_DragonForceRegistry->Add(entity1,fg1);
	m_DragonForceRegistry->Add(entity2,fg2);
}
void DragonForceEngine::AddSpringForceForConnectingTwoEntitiesTogetherWithOffsets(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant,DragonXVector3& offsetForEntity1,DragonXVector3& offsetForEntity2)
{
	ConnectingSpringForceGenerator* fg1 = new ConnectingSpringForceGenerator(springConstant,restLength,entity2);
	ConnectingSpringForceGenerator* fg2 =new ConnectingSpringForceGenerator(springConstant,restLength,entity1);
	fg1->SetOffSetMain(offsetForEntity1);
	fg1->SetOffSetOther(offsetForEntity2);
	fg2->SetOffSetMain(offsetForEntity2);
	fg2->SetOffSetOther(offsetForEntity1);
	m_DragonForceRegistry->Add(entity1,fg1);
	m_DragonForceRegistry->Add(entity2,fg2);
}
void DragonForceEngine::AddBungeeForceForConnectingTwoEntitiesTogether(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant)
{
	ConnectingBungeeForceGenerator* fg1 = new ConnectingBungeeForceGenerator(springConstant,restLength,entity2);
	ConnectingBungeeForceGenerator* fg2 =new ConnectingBungeeForceGenerator(springConstant,restLength,entity1);
	m_DragonForceRegistry->Add(entity1,fg1);
	m_DragonForceRegistry->Add(entity2,fg2);
}
void DragonForceEngine::AddBungeeForceForConnectingTwoEntitiesTogetherWithOffsets(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant,DragonXVector3& offsetForEntity1,DragonXVector3& offsetForEntity2)
{
	ConnectingBungeeForceGenerator* fg1 = new ConnectingBungeeForceGenerator(springConstant,restLength,entity2);
	ConnectingBungeeForceGenerator* fg2 =new ConnectingBungeeForceGenerator(springConstant,restLength,entity1);
	fg1->SetOffSetMain(offsetForEntity1);
	fg1->SetOffSetOther(offsetForEntity2);
	fg2->SetOffSetMain(offsetForEntity2);
	fg2->SetOffSetOther(offsetForEntity1);
	m_DragonForceRegistry->Add(entity1,fg1);
	m_DragonForceRegistry->Add(entity2,fg2);
}
void DragonForceEngine::UpdateForces(real dt)
{
	m_DragonForceRegistry->UpdateForces(dt);
}
//**************************************************************



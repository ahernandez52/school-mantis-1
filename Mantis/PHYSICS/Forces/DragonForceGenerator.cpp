#include "DragonForceGenerator.h"
// gravity force constants
const double GravityFromPoint::m_GravityConstant=0.00006;
const double GravityBetweenEntities::m_GravityConstant=0.06;

///////////////////////////////////////
// Gravity From point
GravityFromPoint::GravityFromPoint(const DragonXVector3 &Position, real mass)
{
	m_Position=Position;
	m_MassOfPoint=mass;
}
GravityFromPoint::~GravityFromPoint()
{
}

// Public Functions
//============================
//applies the update
bool GravityFromPoint::UpdateForce(PhysicsObject* entity, real dt)
{
	if(!entity->HasFiniteMass())
	{
		return false;
	}
	DragonXVector3 dir = m_Position-entity->m_Position;
	real dist = dir.GetMagSquared();
	dir.Normalize();
	real force = (real)m_GravityConstant*m_MassOfPoint*entity->GetMass();
	force /= dist;
	dir*=force;
	entity->AddForce(dir);
	if(force<0.0001)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//*************************************
///////////////////////////////////////

///////////////////////////////////////
// Gravity Between Entities
//creates the force
GravityBetweenEntities::GravityBetweenEntities(PhysicsObject* entity1,PhysicsObject* entity2)
{
	m_Entity1=entity1;
	m_Entity2=entity2;
}
GravityBetweenEntities::~GravityBetweenEntities()
{
}

// Public Functions
//============================
//updates the force will decide internally which one is which
bool GravityBetweenEntities::UpdateForce(PhysicsObject* entity, real dt)
{
	if(!entity->HasFiniteMass())
	{
		return false;
	}
	DragonXVector3 dir = entity->m_Position-m_Entity2->m_Position;
	real dist = dir.GetMagSquared();
	dir.Normalize();
	dir.Invert();
	real force = (real)m_GravityConstant*m_Entity2->GetMass() *entity->GetMass();
	force /= dist;
	dir*=force;
	entity->AddForce(dir);
	if(force<0.0001)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//*************************************
///////////////////////////////////////

///////////////////////////////////////
// Explosion
// creates a default explosion with sensible default values
Explosion::Explosion()
{
	// how long the explosion has been in operation
	m_TimePassed=0.0f;

	// properties for the explosion
	// public for alteration
	// this is the origin of the blast
	m_PointOfDetonation = DragonXVector3(0.0f,0.0f,0.0f);

	// radius up to which the objects will implode for stage one of the explosion
	m_ImplosionMaxRadius = 150.0f;

	// radius at which the objects are not affected by the implosion
	m_ImplosionMinRadius=75.0f;

	// implosion duration
	// the length of time spent imploding before the concussive phase begins
	m_ImplosionDuration = 0.5f;

	// implosion force should be relatively small to avoid sucking entities in past the origin of the blast n out the other side
	m_ImplosionForce = 8.0f;

	// the speed at which the shock wave is traveling this is related to
	// the thickness below in the relationship
	// thickness >= speed * minimum frame duration
	m_ShockWaveSpeed=1000.0f;

	// a shock wave applies its force over a range of distances
	// this controls how thick, faster waves require larger thicknesses
	m_ShockWaveThickness=300.0f;

	// this is the force applied to the very center of the blast
	m_PeakConcussiveForce=2000.0f;

	// length of time the concussive wave is active
	// closer we get to this the less powerful the blast
	m_ConcussionDuration=1.0f;

	// this is the peak force for the stationary objects in
	// the center of the convection chimney
	// calculations 4 this force are the same as peak concussive force
	m_PeakConvectionForce=500.0f;

	// the radius of the chimney cylinder in the xz plane
	m_ChmineyRadius=200.0f;

	// maximum height of the mushroom cloud aka chimney
	m_ChimneyHeight=500.0f;

	// this is the length of time for the chimney effect
	// this is typically the longest as the heat from the blast typically out lives the shock wave it self
	m_ConvectionDuration=2.0f;
}
Explosion::~Explosion()
{
}

// Public Functions
// applies the force to the given entity
bool Explosion::UpdateForce(PhysicsObject* entity, real dt)
{
	if(!entity->HasFiniteMass())
	{
		return true;
	}
	m_TimePassed+=dt;
	if(m_ImplosionDuration>0.0f)
	{
		m_ImplosionDuration-=dt;
		DragonXVector3 dir = entity->m_Position-m_PointOfDetonation;
		real dist=dir.GetMagnitude();
		if(dist<=m_ImplosionMaxRadius&&dist>=m_ImplosionMinRadius)
		{
			dir.Normalize();
			dir*=m_ImplosionForce;
			entity->AddForce(dir);
		}
		return false;
	}
	else if(m_ConcussionDuration>0.0f)
	{
		m_ConcussionDuration-=dt;
		DragonXVector3 dir = entity->m_Position-m_PointOfDetonation;
		real dist=dir.GetMagnitude();
		real st=m_ShockWaveSpeed*dt;
		real force =0.0f;
		if((st-(0.5f*m_ShockWaveThickness))<=dist&&(dist<st))
		{
			force=1.0f-(st-dist)/(0.5f*m_ShockWaveThickness);
			dir.Normalize();
			dir*=force*m_PeakConcussiveForce;
			entity->AddForce(dir);
		}
		else if((st<=dist)&&(dist<(st+m_ShockWaveThickness)))
		{
			force = m_PeakConcussiveForce;
			dir.Normalize();
			dir*=force*m_PeakConcussiveForce;
			entity->AddForce(dir);
		}
		else if(((st+m_ShockWaveThickness)<=dist)&& (dist < (st+1.5f*m_ShockWaveThickness)))
		{
			force = (dist-st-m_ShockWaveThickness)/m_ShockWaveThickness*0.5f;
			dir.Normalize();
			dir*=force*m_PeakConcussiveForce;
			entity->AddForce(dir);
		}
		else
		{
			force = 0.0f;
			dir.Normalize();
			dir*=force*m_PeakConcussiveForce;
			entity->AddForce(dir);
		}
		return false;
	}
	else if(m_ConvectionDuration>0.0f)
	{
		m_ConvectionDuration-=dt;
		DragonXVector3 dir = entity->m_Position-m_PointOfDetonation;
		real distxz= sqrt(dir.x*dir.x+dir.z*dir.z);
		if((distxz<m_ChmineyRadius*2)&&dir.y<m_ChimneyHeight)
		{
			real force = m_PeakConvectionForce*distxz/m_ChmineyRadius;
			dir.Normalize();
			dir*=force;
			entity->AddForce(dir);
		}
		return false;
	}
	else
	{
		return true;
	}
}
//**************************************
////////////////////////////////////////

////////////////////////////////////////
// Thrust
Thrust::Thrust()
{
	m_DurationOfThrust=5.5f;

	// this is drag or the mpg or whatever the magnitude of the thrust will be decreased by this based on dt so
	// the magnitude will be decreased by this amount every second so if mpg you need a new car
	// side note you can make this negative for an increase in thrust till the duration of the thrust force has been reached
	m_Decrement=0.5f;

	// this is the magnitude or strength of the thrust
	m_MagnitudeOfTrust=5000.0f;

	// this is the direction of the thrust
	// they are stored separately so the magnitude can be decremented
	// and then integrated into the force update by scaling the direction
	m_Direction=DragonXVector3(0.0f,1.0f,0.0f);
	m_Direction.Normalize();
}
// this is the parameterized constructor
Thrust::Thrust(real duration,real decrement,real magnitude,DragonXVector3 direction)
{
	m_DurationOfThrust=duration;
	m_Decrement=decrement;
	m_MagnitudeOfTrust=magnitude;
	m_Direction.Normalize();
}
Thrust::~Thrust()
{
}

// Public Functions
//============================
// updates the entity with the force
bool Thrust::UpdateForce(PhysicsObject* entity, real dt)
{
	if(!entity->HasFiniteMass())
	{
		return true;
	}
	if(m_DurationOfThrust>0.0f)
	{
		m_DurationOfThrust-=dt;
		m_MagnitudeOfTrust-=m_Decrement;
		m_Direction.Normalize();
		m_Direction*=m_MagnitudeOfTrust;
		entity->AddForce(m_Direction);
		return false;
	}
	else if (m_DurationOfThrust==0.0f)
	{
		m_Direction.Normalize();
		m_Direction*=m_MagnitudeOfTrust;
		entity->AddForce(m_Direction);
		return true;
	}
	else if(m_DurationOfThrust==-1.0f)
	{
		m_MagnitudeOfTrust-=m_Decrement;
		if (m_MagnitudeOfTrust>0.0f)
		{
			m_Direction.Normalize();
			m_Direction*=m_MagnitudeOfTrust;
			entity->AddForce(m_Direction);
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}
//*************************************
////////////////////////////////////////

////////////////////////////////////////
// Anchored Spring Force
AnchoredSpringGenerator::AnchoredSpringGenerator()
{
	m_SpringConstant=(real)1.5f;
	m_RestLength=(real)5.0f;
	m_AnchorPosition=DragonXVector3(0.0f,0.0f,0.0f);
	m_OffSet=DragonXVector3(0.0f,0.0f,0.0f);
}
AnchoredSpringGenerator::AnchoredSpringGenerator(real constant, real restLength, DragonXVector3& anchorPos)
{
	m_SpringConstant=constant;
	m_RestLength=restLength;
	m_AnchorPosition=anchorPos;
	m_OffSet=DragonXVector3(0.0f,0.0f,0.0f);
}
AnchoredSpringGenerator::~AnchoredSpringGenerator()
{
}
// Public Functions
//==============================
void AnchoredSpringGenerator::SetOffSet(DragonXVector3& offset)
{
	m_OffSet=offset;
}
bool AnchoredSpringGenerator::UpdateForce(PhysicsObject* entity, real dt)
{
	DragonXVector3 force;
	force=entity->m_Position;
	force-=m_OffSet;
	force -= m_AnchorPosition;

	real magnitude = force.GetMagnitude();
	magnitude = (magnitude-m_RestLength) * m_SpringConstant;

	force.Normalize();
	force*=-magnitude;
	entity->AddForce(force);
	return false;
}
//****************************************
//////////////////////////////////////////

//////////////////////////////////////////
// Anchored Bungee Force
AnchoredBungeeGenerator::AnchoredBungeeGenerator()
{
	m_SpringConstant=(real)1.5f;
	m_RestLength=(real)5.0f;
	m_AnchorPosition=DragonXVector3(0.0f,0.0f,0.0f);
	m_OffSet=DragonXVector3(0.0f,0.0f,0.0f);
}
AnchoredBungeeGenerator::AnchoredBungeeGenerator(real constant, real restLength, DragonXVector3& anchorPos)
{
	m_SpringConstant=constant;
	m_RestLength=restLength;
	m_AnchorPosition=anchorPos;
	m_OffSet=DragonXVector3(0.0f,0.0f,0.0f);
}
AnchoredBungeeGenerator::~AnchoredBungeeGenerator()
{

}
// Public Functions
//====================================
void AnchoredBungeeGenerator::SetOffSet(DragonXVector3& offset)
{
	m_OffSet=offset;
}
bool AnchoredBungeeGenerator::UpdateForce(PhysicsObject* entity, real dt)
{
	DragonXVector3 force;
	force=entity->m_Position;
	force-=m_OffSet;
	force-=m_AnchorPosition;

	real magnitude=force.GetMagnitude();
	if(magnitude<=m_RestLength)
	{
		return false;
	}
	magnitude=(magnitude-m_RestLength)*m_SpringConstant;

	force.Normalize();
	force*=-magnitude;
	entity->AddForce(force);
	return false;
}
//************************************************
//////////////////////////////////////////////////

//////////////////////////////////////////////////
// Connecting Spring Force
ConnectingSpringForceGenerator::ConnectingSpringForceGenerator(real constant, real restLength, PhysicsObject* otherEntity)
{
	m_SpringConstant=constant;
	m_RestLength=restLength;
	m_OtherEntity=otherEntity;
	m_OffsetForUpdatedEntity=DragonXVector3(0.0f,0.0f,0.0f);
	m_OffsetForOther=DragonXVector3(0.0f,0.0f,0.0f);
}
ConnectingSpringForceGenerator::~ConnectingSpringForceGenerator()
{
}
// Public Functions
//====================================
void ConnectingSpringForceGenerator::SetOffSetOther(DragonXVector3& offset)
{
	m_OffsetForOther=offset;
}
void ConnectingSpringForceGenerator::SetOffSetMain(DragonXVector3& offset)
{
	m_OffsetForUpdatedEntity=offset;
}
bool ConnectingSpringForceGenerator::UpdateForce(PhysicsObject* entity, real dt)
{
	if(m_OtherEntity==nullptr)
	{
		return true;
	}
	else
	{
		DragonXVector3 force;
		force = entity->m_Position-m_OffsetForUpdatedEntity;
		force -= (m_OtherEntity->m_Position-m_OffsetForOther);

		real magnitude = force.GetMagnitude();
		magnitude = magnitude - m_RestLength;
		magnitude*=m_SpringConstant;

		force.Normalize();
		force *= -magnitude;
		entity->AddForce(force);
		return false;
	}
}
//*************************************************
///////////////////////////////////////////////////

///////////////////////////////////////////////////
// Connecting Bungee Force
ConnectingBungeeForceGenerator::ConnectingBungeeForceGenerator(real constant, real restLength, PhysicsObject* otherEntity)
{
	m_SpringConstant=constant;
	m_RestLength=restLength;
	m_OtherEntity=otherEntity;
	m_OffsetForUpdatedEntity=DragonXVector3(0.0f,0.0f,0.0f);
	m_OffsetForOther=DragonXVector3(0.0f,0.0f,0.0f);
}
ConnectingBungeeForceGenerator::~ConnectingBungeeForceGenerator()
{
}
// Public Functions
//======================================
void ConnectingBungeeForceGenerator::SetOffSetOther(DragonXVector3& offset)
{
	m_OffsetForOther=offset;
}
void ConnectingBungeeForceGenerator::SetOffSetMain(DragonXVector3& offset)
{
	m_OffsetForUpdatedEntity=offset;
}
bool ConnectingBungeeForceGenerator::UpdateForce(PhysicsObject* entity, real dt)
{
	if(m_OtherEntity==nullptr)
	{
		return true;
	}
	else
	{
		DragonXVector3 force;
		force = entity->m_Position-m_OffsetForUpdatedEntity;
		force -= (m_OtherEntity->m_Position-m_OffsetForOther);

		real magnitude = force.GetMagnitude();
		if(magnitude <= m_RestLength)
		{
			return false;
		}
		magnitude = (magnitude-m_RestLength) * m_SpringConstant;

		force.Normalize();
		force *= -magnitude;
		entity->AddForce(force);
		return false;
	}
}
//************************************************
///////////////////////////////////////////////////
#pragma once
#include <vector>
#include "DragonForceGenerator.h"

// a registry to hold all the entities and forces that are to be applied to them

// a struct to hold a pair for registration
struct DragonForceRegistration
{
	PhysicsObject* m_Entity;
	DragonForceGenerator* m_DFG;
	~DragonForceRegistration()
	{
		delete m_DFG;
	}
};
// holds a vector of registrations
typedef std::vector<DragonForceRegistration*> DragonicRegistry;

class DragonForceRegistry
{
private:
	DragonicRegistry m_DragonRegistrations;
public:
	~DragonForceRegistry()
	{
		Clear();
	}
	// registers the given entity and force generator
	void Add(PhysicsObject* entity, DragonForceGenerator* fg);
	void CleanUp();
	// this will remove all registrations and delete all force generators
	void Clear();
	// removes given registered pair from list if no pair is found does nothing
	void Remove(PhysicsObject* entity, DragonForceGenerator* fg);
	// removes all forces associated with the given entity
	void RemoveAllFrom(PhysicsObject* entity);
	// removes the given registration from the vector of registrations
	void RemoveRegistration(DragonForceRegistration* reg);
	// removes a list of registrations from the main registry
	void RemoveRegistrations(std::vector<DragonForceRegistration*>& regs);
	// this updates all the registrations and takes in dt
	// note this will also automatically delete any forces that need to be deleted
	void UpdateForces(real dt);	
};
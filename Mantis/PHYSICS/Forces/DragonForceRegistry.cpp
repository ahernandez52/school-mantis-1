#include "DragonForceRegistry.h"

// Public Functions
//==========================
// registers the given entity and force generator
void DragonForceRegistry::Add(PhysicsObject* entity, DragonForceGenerator* fg)
{
	DragonForceRegistration* r = new DragonForceRegistration;
	r->m_Entity=entity;
	r->m_DFG=fg;
	m_DragonRegistrations.push_back(r);
}
void DragonForceRegistry::CleanUp()
{
	DragonicRegistry regsToKeep;
	for(auto DragonForceRegistration : m_DragonRegistrations)
	{
		if(DragonForceRegistration)
		{
			regsToKeep.push_back(DragonForceRegistration);
		}
	}
	m_DragonRegistrations.clear();
	m_DragonRegistrations=regsToKeep;
}
// this will remove all registrations and delete all force generators
void DragonForceRegistry::Clear()
{
	for (auto DragonForceRegistration : m_DragonRegistrations)
	{
		delete DragonForceRegistration;
		DragonForceRegistration=nullptr;
	}
	m_DragonRegistrations.clear();
}
// removes given registered pair from list if no pair is found does nothing
void DragonForceRegistry::Remove(PhysicsObject* entity, DragonForceGenerator* fg)
{
	for(auto DragonForceRegistration : m_DragonRegistrations)
	{
		if(DragonForceRegistration->m_Entity==entity&&DragonForceRegistration->m_DFG==fg)
		{
			delete DragonForceRegistration;
			DragonForceRegistration=nullptr;
		}
	}
	CleanUp();
}
// removes all forces associated with the given entity
void DragonForceRegistry::RemoveAllFrom(PhysicsObject* entity)
{
	for(auto DragonForceRegistration : m_DragonRegistrations)
	{
		if(DragonForceRegistration->m_Entity==entity)
		{
			delete DragonForceRegistration;
			DragonForceRegistration=nullptr;
		}
	}
	CleanUp();
}
// removes the given registration from the vector of registrations
void DragonForceRegistry::RemoveRegistration(DragonForceRegistration* reg)
{
	for(auto DragonForceRegistration : m_DragonRegistrations)
	{
		if(DragonForceRegistration==reg)
		{
			delete DragonForceRegistration;
			DragonForceRegistration=nullptr;
		}
	}
	CleanUp();
}
// removes a list of registrations from the main registry
void DragonForceRegistry::RemoveRegistrations(std::vector<DragonForceRegistration*>& regs)
{
	for(const auto DragonForceRegistration : regs)
	{
		RemoveRegistration(DragonForceRegistration);
	}
	CleanUp();
}
// this updates all the registrations and takes in dt
// note this will also automatically delete any forces that need to be deleted
void DragonForceRegistry::UpdateForces(real dt)
{
	DragonicRegistry regsToKeep;
	for(const auto DragonForceRegistration : m_DragonRegistrations)
	{
		if(!DragonForceRegistration->m_DFG->UpdateForce(DragonForceRegistration->m_Entity,dt))
		{
			regsToKeep.push_back(DragonForceRegistration);
		}
		else
		{
			delete DragonForceRegistration;
		}
	}
	m_DragonRegistrations.clear();
	m_DragonRegistrations=regsToKeep;
}
//************************************
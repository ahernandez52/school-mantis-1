#pragma once
#include "DragonForceRegistry.h"

class DragonForceEngine
{
private:
	DragonForceRegistry* m_DragonForceRegistry;
public:
	DragonForceEngine();
	~DragonForceEngine();	
	//when adding forces try to make them self destroying see the force generators for details
	void AddThrustToOneEntity(PhysicsObject* entity, real durationOfThrust, real DecrementOfThrust, real MagnitudeOfThrust, DragonXVector3 &direction);
	void AddDefaultThrustToEntity(PhysicsObject* entity);// default force is a temporary force upward like a space rocket
	//this will add a thrust force to all the entities in this list the same parameters will be used in each thrust force
	void AddThrustToVectorOfEntities(std::vector<PhysicsObject*>& entities, real durationOfThrust, real DecrementOfThrust, real MagnitudeOfThrust, DragonXVector3 &direction);
	// this does basically the same thing but with default values for the thrust force
	void AddDeafaultThrustForceToVectorOfEntities(std::vector<PhysicsObject*>& entities);
	// this adds gravity towards a point to one entity
	void AddGravityFromPointToEntity(PhysicsObject* entity, DragonXVector3 &positionOfGravity, real massOfGravityObject);// for a black hole just use a real big number for mass
	// this adds gravity to a point to a vector of entities
	void AddGravityFromPointToVectorOfEntites(std::vector<PhysicsObject*>& entities,DragonXVector3 &positionOfGravityObject, real massOfGravityObject);// this will be used to demo straight gravity and gravity to a point
	// you can thank Nicholson for this one this will apply gravity to both entities based on each entities own mass and their distance from each other
	void AddGravityBetweenEntities(PhysicsObject* entity1,PhysicsObject* entity2);
	// and just for insanity this will apply gravity to all entities based on distance and mass will likely have negative effects on performance
	void AddGravityBetweenAllEntitiesInVector(std::vector<PhysicsObject*>& entities);
	// because all games need big booms this is a default explosion force added to one entity
	void AddDefaultExplosionToOneEntity(PhysicsObject* entity);
	// this will allow for custom explosions as a result its a long one and you will need to enter a value for every parameter but alterations can be made if needed
	void AddCustomExplosionToOneEntity(PhysicsObject* entity,DragonXVector3 &pointOfDetonation,real implosionMinRadius,
		real implosionMaxRadius, real implosionDuration, real implosionForce,real shockWaveSpeed, real shockWaveThickness, real peakConcussiveForce,
		real concussionDuration, real peakConvectionForce, real chimneyRadius, real chimneyHeight, real convectionDuration );
	// this is the easiest way of making an explosion
	void AddDefaultExplosionToVectorOfEntities(std::vector<PhysicsObject*>& entities);
	// this is the hard way yes all values do need to be entered as for good values the default ones should be decent
	//for a description of the values see the force generator
	void AddCustomExplosionToVectorOfEntities(std::vector<PhysicsObject*>& entities,DragonXVector3 &pointOfDetonation,real implosionMinRadius,
		real implosionMaxRadius, real implosionDuration, real implosionForce,real shockWaveSpeed, real shockWaveThickness, real peakConcussiveForce,
		real concussionDuration, real peakConvectionForce, real chimneyRadius, real chimneyHeight, real convectionDuration );
	void AddDefaultAnchoredSpringForceToEntity(PhysicsObject* entity);
	void AddDefaultAnchoredSpringForceToVectorOfEntities(std::vector<PhysicsObject*>& entities);
	void AddDefaultAnchoredSpringForceToEntityWithOffSet(PhysicsObject* entity,DragonXVector3& offset);
	void AddDefaultAnchoredSpringForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,DragonXVector3& offset);
	void AddCustomAnchoredSpringForceToEntity(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos);
	void AddCustomAnchoredSpringForceToVectorOfEntities(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos);
	void AddCustomAnchoredSpringForceToEntityWithOffSet(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset);
	void AddCustomAnchoredSpringForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset);
	void AddDefaultAnchoredBungeeForceToEntity(PhysicsObject* entity);
	void AddDefaultAnchoredBungeeForceToVectorOfEntities(std::vector<PhysicsObject*>& entities);
	void AddDefaultAnchoredBungeeForceToEntityWithOffSet(PhysicsObject* entity,DragonXVector3& offset);
	void AddDefaultAnchoredBungeeForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,DragonXVector3& offset);
	void AddCustomAnchoredBungeeForceToEntity(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos);
	void AddCustomAnchoredBungeeForceToVectorOfEntities(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos);
	void AddCustomAnchoredBungeeForceToEntityWithOffSet(PhysicsObject* entity,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset);
	void AddCustomAnchoredBungeeForceToVectorOfEntitiesWithOffSet(std::vector<PhysicsObject*>& entities,real springConstant,real restLength,DragonXVector3& anchorPos,DragonXVector3& offset);
	void AddSpringForceToAnchorOneEntityToAnother(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity);
	void AddSpringForceToAnchorMultipleEntitiesToOneEntity(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity);
	void AddSpringForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offSetForEntity,DragonXVector3& offsetForAnchor);
	void AddSpringForceToAnchorMultipleEntitiesToOneEntityWithOffsets(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offsetForEntities,DragonXVector3& offsetForAnchor);
	void AddBungeeForceToAnchorOneEntityToAnother(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity);
	void AddBungeeForceToAnchorMultipleEntitiesToOneEntity(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity);
	void AddBungeeForceToAnchorOneEntityToAnotherWithOffsets(PhysicsObject* entity,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offSetForEntity,DragonXVector3& offsetForAnchor);
	void AddBungeeForceToAnchorMultipleEntitiesToOneEntityWithOffsets(std::vector<PhysicsObject*>& entities,real restLength,real springConstant,PhysicsObject* AnchorEntity,DragonXVector3& offsetForEntities,DragonXVector3& offsetForAnchor);
	void AddSpringForceForConnectingTwoEntitiesTogether(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant);
	void AddSpringForceForConnectingTwoEntitiesTogetherWithOffsets(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant,DragonXVector3& offsetForEntity1,DragonXVector3& offsetForEntity2);
	void AddBungeeForceForConnectingTwoEntitiesTogether(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant);
	void AddBungeeForceForConnectingTwoEntitiesTogetherWithOffsets(PhysicsObject* entity1,PhysicsObject* entity2,real restLength,real springConstant,DragonXVector3& offsetForEntity1,DragonXVector3& offsetForEntity2);
	void UpdateForces(real dt);
	inline DragonForceRegistry* GetForceRegistry()
	{
		return m_DragonForceRegistry;
	}
};
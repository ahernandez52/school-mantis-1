#pragma once
// collision type world means infinite mass game means not
enum collisionobjectType
{
	COT_World,
	COT_Game,
};
// the type of bounding volume or basic physics that will be applied to the object 
// be it a plane sphere or box
enum primitiveType
{
	PT_Plane,
	PT_Sphere,
	PT_OBB,
	PT_Ray,
	PT_Mesh,
};
// this is the type of collision fairly obvious i hope
enum CollisionType
{
	CT_SphereSphere,
	CT_SpherePlane,
	CT_SphereOBB,
	CT_ObbPlane,
	CT_ObbObb,
	CT_RaySphere,
	CT_RayOBB,
	CT_MeshSphere,
	CT_MeshOBB,
	CT_MeshMesh,
	CT_MeshRay,
};
// this is here in preperation of possible more advanced types of contacts such as rods and cables
enum ContactType
{
	CRT_Natural,
	CRT_Ray,
};
// these are used pretty much exclusively in octrees
enum HydraOctreeParts
{
	Top_Front_Left,		//0
	Top_Front_Right,	//1
	Top_Back_Left,		//2
	Top_Back_Right,		//3
	Bottom_Front_Left,	//4
	Bottom_Front_Right,	//5
	Bottom_Back_Left,	//6
	Bottom_Back_Right,	//7
};
#pragma once
#define DPMI DragonPhysicsManager::instance()
#include "DragonXMath.h"
#include <vector>
#include <map>

class MeshOctreePartitioner;

class DragonPhysicsEngine;

class DragonTriangle
{
private:
	real m_MaxX;
	real m_MinX;
	real m_MaxY;
	real m_MinY;
	real m_MaxZ;
	real m_MinZ;
	DragonXVector3 m_VertA;
	DragonXVector3 m_VertB;
	DragonXVector3 m_VertC;
private:
	inline void SetUp()
	{
		// set up max x
		m_MaxX=m_VertA.x;
		if(m_VertB.x>m_MaxX)
		{
			m_MaxX=m_VertB.x;
		}
		if (m_VertC.x>m_MaxX)
		{
			m_MaxX=m_VertC.x;
		}
		// set up min x
		m_MinX=m_VertA.x;
		if(m_VertB.x<m_MinX)
		{
			m_MinX=m_VertB.x;
		}
		if (m_VertC.x<m_MinX)
		{
			m_MinX=m_VertC.x;
		}
		// set up max Y
		m_MaxY=m_VertA.y;
		if(m_VertB.y>m_MaxY)
		{
			m_MaxY=m_VertB.y;
		}
		if (m_VertC.y>m_MaxY)
		{
			m_MaxY=m_VertC.y;
		}
		// set up min y
		m_MinY=m_VertA.y;
		if(m_VertB.y<m_MinY)
		{
			m_MinY=m_VertB.y;
		}
		if (m_VertC.y<m_MinY)
		{
			m_MinY=m_VertC.y;
		}
		// set up max Z
		m_MaxZ=m_VertA.z;
		if(m_VertB.z>m_MaxZ)
		{
			m_MaxZ=m_VertB.z;
		}
		if (m_VertC.z>m_MaxZ)
		{
			m_MaxZ=m_VertC.z;
		}
		// set up min z
		m_MinZ=m_VertA.z;
		if(m_VertB.z<m_MinZ)
		{
			m_MinZ=m_VertB.z;
		}
		if (m_VertC.z<m_MinZ)
		{
			m_MinZ=m_VertC.z;
		}	
	}
public:
	DragonTriangle(DragonXVector3& a,DragonXVector3& b,DragonXVector3& c)
	{
		m_VertA=a;
		m_VertB=b;
		m_VertC=c;
		SetUp();
	}
	~DragonTriangle()
	{
	}
	inline real GetMaxX()
	{
		return m_MaxX;
	}
	inline real GetMinX()
	{
		return m_MinX;
	}
	inline real GetMaxY()
	{
		return m_MaxY;
	}
	inline real GetMinY()
	{
		return m_MinY;
	}
	inline real GetMaxZ()
	{
		return m_MaxZ;
	}
	inline real GetMinZ()
	{
		return m_MinZ;
	}
	inline DragonXVector3 GetVertA()
	{
		return m_VertA;
	}
	inline DragonXVector3 GetVertB()
	{
		return m_VertB;
	}
	inline DragonXVector3 GetVertC()
	{
		return m_VertC;
	}
	inline DragonXVector3 GetNormal(DragonXVector3& vA,DragonXVector3& vB,DragonXVector3& vC)
	{
		DragonXVector3 n = ((vB-vA)%(vC-vA));
		n.Invert();
		n.Normalize();
		return n;
	}
};

class DragonSector
{
public:
	DragonSector(DragonXVector3& pos, real rad, std::vector<DragonTriangle*>& v)
	{
		m_Position=pos;
		m_Radius=rad;
		m_SectorTris=v;
	}
	~DragonSector()
	{
		m_SectorTris.clear();
	}
	DragonXVector3 m_Position;
	real m_Radius;
	std::vector<DragonTriangle*> m_SectorTris;
private:
};

class DragonCollisionMesh
{
public:
	DragonCollisionMesh()
	{
	}
	~DragonCollisionMesh()
	{
		for (auto DragonSector : m_DragonSectors)
		{			
			delete DragonSector;	
		}
		m_DragonSectors.clear();

		for (auto DragonTriangle : m_DragonTris)
		{
			delete DragonTriangle;
		}
		m_DragonTris.clear();
	}
	std::vector<DragonSector*> m_DragonSectors;
	std::vector<DragonTriangle*> m_DragonTris;
	std::string m_CollisionMeshName;
	DragonXVector3 m_HalfSizes;
	real m_Radius;
private:
};

class DragonPhysicsManager
{
private:
	DragonPhysicsManager();
	static MeshOctreePartitioner* m_MeshPartitioner;
	std::map<std::string,DragonCollisionMesh*> m_DragonCollisionMeshRegistry;
	std::vector<DragonXVector3*> GetVertexPositionsFromMesh(ID3DXMesh* mesh,DragonXVector3& scale);
	std::vector<DragonXVector3*> GetIndeciesFromMesh(ID3DXMesh* mesh);
	void GetTris(std::vector<DragonXVector3*>& verts,std::vector<DragonXVector3*>& indexes,std::vector<DragonTriangle*>& tris);
	static DragonPhysicsEngine* m_PhysicsEngine;	
public:
	static DragonPhysicsManager* instance();
	~DragonPhysicsManager();
	DragonPhysicsEngine* GetDragonPhysicsCore();
	DragonCollisionMesh* RegisterNewCollisionMesh(ID3DXMesh* mesh,std::string CollisionMeshName,DragonXVector3 scale=DragonXVector3(1.0f,1.0f,1.0f));
	DragonCollisionMesh* GetCollisionMeshByName(std::string CollisionMeshName);
};
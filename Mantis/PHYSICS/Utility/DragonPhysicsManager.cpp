#include "DragonPhysicsManager.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../DragonPhysicsEngine.h"
#include "../../GRAPHICS/Utility/D3D9Vertex.h"
#include "../Tools/DragonMeshPartitioner.h"

DragonPhysicsEngine* DragonPhysicsManager::m_PhysicsEngine = new DragonPhysicsEngine();
MeshOctreePartitioner* DragonPhysicsManager::m_MeshPartitioner = new MeshOctreePartitioner();


DragonPhysicsManager::DragonPhysicsManager()
{
}

DragonPhysicsManager::~DragonPhysicsManager()
{
	delete m_PhysicsEngine;
	delete m_MeshPartitioner;
	for(auto DragonCollisionMesh : m_DragonCollisionMeshRegistry)
	{
		delete DragonCollisionMesh.second;
	}
}

std::vector<DragonXVector3*> DragonPhysicsManager::GetVertexPositionsFromMesh(ID3DXMesh* mesh,DragonXVector3& scale)
{
	std::vector<DragonXVector3*> vPos;
	LPDIRECT3DVERTEXBUFFER9 pVBuf;
	if (SUCCEEDED(mesh->GetVertexBuffer(&pVBuf)))
	{
		VertexPNT *pVert;
		if (SUCCEEDED(pVBuf->Lock(0,0,(void **) &pVert,D3DLOCK_DISCARD)))
		{
			DWORD numVerts=mesh->GetNumVertices();
			for (int i=0;(unsigned)i<numVerts;i++)
			{
				DragonXVector3* v = new DragonXVector3();
				*v=pVert[i].pos;
				*v=v->ComponentProductDrX(scale);
				vPos.push_back(v);
			}
			pVBuf->Unlock();
		}
		pVBuf->Release();
	}
	return vPos;
}
std::vector<DragonXVector3*> DragonPhysicsManager::GetIndeciesFromMesh(ID3DXMesh* mesh)
{
	std::vector<DragonXVector3*> vIPos;
	LPDIRECT3DINDEXBUFFER9 pIBuf;
	if (SUCCEEDED(mesh->GetIndexBuffer(&pIBuf)))
	{
		WORD* pInd;
		DWORD numFaces=mesh->GetNumFaces();
		D3DINDEXBUFFER_DESC desc;
		pIBuf->GetDesc(&desc);
		if (SUCCEEDED(pIBuf->Lock(0,0,(void **) &pInd,D3DLOCK_DISCARD)))
		{
			for(int i = 0; (unsigned)i<numFaces*3;i+=3)
			{
				DragonXVector3* p = new DragonXVector3(pInd[i],pInd[i+1],pInd[i+2]);
				vIPos.push_back(p);
			}
			pIBuf->Unlock();
		}
		pIBuf->Release();
	}
	return vIPos;
}

void DragonPhysicsManager::GetTris(std::vector<DragonXVector3*>& verts,std::vector<DragonXVector3*>& indexes,std::vector<DragonTriangle*>& tris)
{
	for (int i = 0;(unsigned)i<indexes.size();i++)
	{
		DragonTriangle* t = new DragonTriangle(*verts[(unsigned)indexes[i]->x],*verts[(unsigned)indexes[i]->y],*verts[(unsigned)indexes[i]->z]);
		tris.push_back(t);
	}
	for (auto DragonXVector3 : verts)
	{
		delete DragonXVector3;
		DragonXVector3=nullptr;
	}
	verts.clear();
	for (auto DragonXVector3 : indexes)
	{
		delete DragonXVector3;
		DragonXVector3=nullptr;
	}
	indexes.clear();
}

DragonPhysicsManager* DragonPhysicsManager::instance()
{
	static DragonPhysicsManager instance;
	return &instance;
}
// convection is simple name of collision mesh _ scale for ex "BattleShipCollisionMesh_1,1,1" or "BattleShipCollisionMesh_1.5,1,2"
// note this is only for the collision mesh name graphics schem name can just be BattleShipCollisionMesh for all of the battleShipCollisionMeshes
DragonCollisionMesh* DragonPhysicsManager::RegisterNewCollisionMesh(ID3DXMesh* mesh,std::string CollisionMeshName,DragonXVector3 scale)
{
	auto result = m_DragonCollisionMeshRegistry.find(CollisionMeshName);

	if(result == m_DragonCollisionMeshRegistry.end())
	{
		ID3DXMesh* m = mesh;
		DragonCollisionMesh* d = new DragonCollisionMesh();
		// build collision Mesh
		std::vector<DragonTriangle*> tris;
		GetTris(GetVertexPositionsFromMesh(m,scale),GetIndeciesFromMesh(m),tris);
		d->m_DragonTris=tris;
		m_MeshPartitioner->Create(tris,tris.size());
		m_MeshPartitioner->GetSectors(d->m_DragonSectors,m_MeshPartitioner);
		d->m_HalfSizes=m_MeshPartitioner->m_HalfExtents;
		d->m_Radius=m_MeshPartitioner->m_HalfExtents.GetMagnitude();
		m_MeshPartitioner->Release();
		tris.clear();
		m_DragonCollisionMeshRegistry.insert(make_pair(CollisionMeshName,d));
		return d;
	}
	else
	{
		return result->second;
	}
}

DragonCollisionMesh* DragonPhysicsManager::GetCollisionMeshByName(std::string CollisionMeshName)
{
	auto result = m_DragonCollisionMeshRegistry.find(CollisionMeshName);

	if(result == m_DragonCollisionMeshRegistry.end())
	{
		return nullptr;
	}
	else
	{
		return result->second;
	}
}

DragonPhysicsEngine* DragonPhysicsManager::GetDragonPhysicsCore()
{
	return m_PhysicsEngine;
}
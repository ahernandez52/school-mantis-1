#pragma once
#include "D3D9ShaderEffect.h"

class D3D9ShaderManager
{
private:

	std::map<std::string, D3D9ShaderEffect*>	m_Effects;
	
private:

	//file creation helper functions

public:

	D3D9ShaderManager();
	~D3D9ShaderManager();

	//Usage Functions
	//=================================
	D3D9ShaderEffect*	GetShaderEffect(std::string ShaderName);
	//*********************************

	//Automated effect creation
	//=================================
	void				LoadEffectsFromFile(LPDIRECT3DDEVICE9 device);
	//*********************************

	//Manually Create Effect
	//=================================
	void				CreateNewEffect(std::string EffectName, std::string ShaderFileName, LPDIRECT3DDEVICE9 device);
	//*********************************
};
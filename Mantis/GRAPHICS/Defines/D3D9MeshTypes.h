#pragma once
//D3D9ObjectType - Typedef for type of object stored in D3D9Object member

enum D3D9ObjectType
{
	D3D9_Sphere = 0,				//Sphere Primitive
	D3D9_Cube,						//Cube Primitive
	D3D9_ShipA,						//Ship Mesh
	D3D9_Skybox						//Skybox Mesh
};
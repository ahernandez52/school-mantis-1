#pragma once
//D3D9TextureTypes - Enum for type of texture

enum D3D9TextureType
{
	D3D9Tex_Texture = 0,
	D3D9Tex_Texture2,
	D3D9Tex_Diffuse,
	D3D9Tex_Normal,
	D3D9Tex_Bump,
	D3D9Tex_Specular,
	D3D9Tex_Illumination,
};
#include "D3D9Buffer.h"

D3D9Buffer::D3D9Buffer(LPDIRECT3DDEVICE9 pDevice, UINT pWidth, UINT pHeight, UINT pLevels, WORD pUsage, D3DFORMAT pFormat, D3DPOOL pPool, HANDLE pHandle)
{
	m_pDevice = pDevice;
	m_iWidth = pWidth;
	m_iHeight = pHeight;
	m_iLevels = pLevels;
	m_wUsage = pUsage;
	m_fFormat = pFormat;
	m_pPool = pPool;
	m_pHandle = pHandle;

	//Initialize Targets
	m_pTexture = NULL;
	m_pSurface = NULL;

	//Create the Texture
	m_pDevice->CreateTexture(m_iWidth, m_iHeight, 1, m_wUsage, m_fFormat, m_pPool, &m_pTexture, NULL);

	//Peel the Surface
	m_pTexture->GetSurfaceLevel(0, &m_pSurface);

}

D3D9Buffer::~D3D9Buffer()
{
	m_pTexture->Release();
}

void D3D9Buffer::OnLost()
{
	m_pSurface->Release();
	m_pSurface = NULL;
	m_pTexture->Release();
	m_pTexture = NULL;
}

void D3D9Buffer::OnReset()
{
	//Create the Texture
	m_pDevice->CreateTexture(m_iWidth, m_iHeight, 1, m_wUsage, m_fFormat, m_pPool, &m_pTexture, NULL);

	//Peel the Surface
	m_pTexture->GetSurfaceLevel(0, &m_pSurface);
}
#include "D3D9Viewport.h"


D3D9Viewport::D3D9Viewport(LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR2 pAnchor, D3DXVECTOR2 pAnchorOff, D3DXVECTOR2 pSize, D3DXVECTOR2 pSizeOff, UINT pBuffer)
{
	m_bVisible = true;
	m_vAnchor = pAnchor;
	m_vAnchorOff = pAnchorOff;
	m_vSize = pSize;
	m_vSizeOff = pSizeOff;
	m_pDevice = pDevice;
	this->OnReset();
}

void D3D9Viewport::ToggleVisible()
{
	m_bVisible = (m_bVisible)?false:true;
#if 0
	if (m_bVisible)
		m_bVisible = false;
	else
		m_bVisible = true;
#endif
}

void D3D9Viewport::Position(D3DXVECTOR2 pAnchor, D3DXVECTOR2 pAnchorOff)
{
	m_vAnchor = pAnchor;
	m_vAnchorOff = pAnchorOff;
	this->OnReset();
}

void D3D9Viewport::Size(D3DXVECTOR2 pSize, D3DXVECTOR2 pSizeOff)
{
	m_vSize = pSize;
	m_vSizeOff = pSizeOff;
	this->OnReset();
}

bool D3D9Viewport::IsVisible()
{
	return m_bVisible;
}

D3DVIEWPORT9 D3D9Viewport::GetViewport()
{
	return m_vViewport;
}

void D3D9Viewport::OnLost()
{

}

void D3D9Viewport::OnReset()
{
	D3DVIEWPORT9 _temp;
	m_pDevice->GetViewport(&_temp);
	UINT _bufferH, _bufferW;
	_bufferH = _temp.Height;
	_bufferW = _temp.Width;

	m_vViewport.X = (DWORD)((_bufferW * m_vAnchor.x) + m_vAnchorOff.x);
	m_vViewport.Y = (DWORD)((_bufferH * m_vAnchor.y) + m_vAnchorOff.y);
	m_vViewport.Width = (DWORD)((_bufferW * m_vSize.x) + m_vSizeOff.x);
	m_vViewport.Height = (DWORD)((_bufferH * m_vSize.y) + m_vSizeOff.y);
	m_vViewport.MinZ = 0.0f;
	m_vViewport.MaxZ = 1.0f;
}
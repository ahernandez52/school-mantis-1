#pragma once

#include "D3D9Buffer.h"
#include <map>

enum BufferType
{
	BUF_QUAD = 0,				//Screen Quad Buffer
	BUF_PERLIN2DA,
	BUF_ILLUM
};

class D3D9BufferManager
{
private:
	std::map<int, D3D9Buffer*> m_mBuffers;
private:


public:

public:
	D3D9BufferManager(){}
	~D3D9BufferManager(){}

	//D3D9Buffer* CreateBuffer();

	void OnLost(){}
	void OnReset(){}

};
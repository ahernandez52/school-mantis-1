#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "D3D9Buffer.h"


class D3D9Viewport
{
private:		//Private vars
	//Dimensional defines
	D3DXVECTOR2 m_vAnchor;
	D3DXVECTOR2 m_vAnchorOff;
	D3DXVECTOR2 m_vSize;
	D3DXVECTOR2 m_vSizeOff;
	bool m_bVisible;
	D3D9Buffer* m_pBuffer;
	D3DVIEWPORT9 m_vViewport;
	LPDIRECT3DDEVICE9	m_pDevice;

private:		//Private functions

public:			//Public vars
	
public:			//Public functions
	D3D9Viewport(LPDIRECT3DDEVICE9 pDevice, D3DXVECTOR2 pAnchor, D3DXVECTOR2 pAnchorOff, D3DXVECTOR2 pSize, D3DXVECTOR2 pSizeOff, UINT pBuffer);
	~D3D9Viewport(){}
	void OnLost();
	void OnReset();

	//Setters
	void ToggleVisible();
	void Position(D3DXVECTOR2 pAnchor, D3DXVECTOR2 pAnchorOff);
	void Size(D3DXVECTOR2 pSize, D3DXVECTOR2 pSizeOff);

	//Getters
	bool IsVisible();
	D3DVIEWPORT9 GetViewport();
};
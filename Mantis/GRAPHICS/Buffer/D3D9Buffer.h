#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"


class D3D9Buffer
{
private:
	LPDIRECT3DDEVICE9	m_pDevice;

private:

	UINT m_iLevels;
	DWORD m_wUsage;
	D3DFORMAT m_fFormat;
	D3DPOOL m_pPool;
	HANDLE m_pHandle;

public:
	LPDIRECT3DTEXTURE9  m_pTexture;		
	LPDIRECT3DSURFACE9  m_pSurface;		
	UINT m_iWidth;
	UINT m_iHeight;
public:
	D3D9Buffer(LPDIRECT3DDEVICE9 pDevice, UINT pWidth, UINT pHeight, UINT pLevels, WORD pUsage, D3DFORMAT pFormat, D3DPOOL pPool, HANDLE pHandle);
	~D3D9Buffer();
	void OnLost();
	void OnReset();

};
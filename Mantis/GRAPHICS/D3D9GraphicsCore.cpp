#include "D3D9GraphicsCore.h"
#include "../FRAMEWORK/Managers/WorldManager.h"
#include "Objects\D3D9Object.h"
#include "Objects\D3D9MantisSkyBox.h"
#include "Utility\GfxStats.h"
#include "Objects\D3D9Quad.h"
#include "Gui\MantisGUI.h"
#include "../FRAMEWORK/Managers/MemoryManager.h"

D3D9GraphicsCore::D3D9GraphicsCore(LPDIRECT3DDEVICE9 device)
{
	m_bViewportShow = false;
	m_bTextures = true;
	m_bAnisotropic = false;
	m_Device = device;

	//Init Viewport
	m_pViewport.X = 300;
	m_pViewport.Y = 400;
	m_pViewport.Width = 200;
	m_pViewport.Height = 100;
	m_pViewport.MinZ = 0.0f;
	m_pViewport.MaxZ = 1.0f;

	m_ShaderManager = new D3D9ShaderManager();
	m_LightManager	= new LightManager();
	m_GfxStats		= new GfxStats();
	m_SkyBox		= new D3D9MantisSkyBox("Assets/Meshes/Skybox/project_sol_green_brown_1024.dds", 20000.0f, device);
	

	m_gfxStatsDisplay	= true;
	m_skyBoxRender		= true;

	m_DrawColor = g_Black;

	//Load Defined Shaders
	m_ShaderManager->LoadEffectsFromFile(device);

	//DirectionalLight light;

	m_lLight.ambient = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	m_lLight.diffues = D3DXCOLOR(0.5f, 0.5f, 0.0f, 1.0f);
	m_lLight.specular = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_lLight.directionWorld = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

	//Set Default Shader
	m_CurrentShader = m_ShaderManager->GetShaderEffect("UberMain");
	
	//Initialize Render Targets
	m_pBackBuffer = NULL;

	D3DVIEWPORT9 viewport;
	m_Device->GetViewport(&viewport);


	//Initialize Render Buffer
	m_pRenderBuffer = new D3D9Buffer(m_Device, 
								 viewport.Width,
                                 viewport.Height,
                                 1,
                                 D3DUSAGE_RENDERTARGET,
                                 D3DFMT_X8R8G8B8,
                                 D3DPOOL_DEFAULT,
                                 NULL);

	m_pPerlinBuffer = new D3D9Buffer(m_Device, 
								 512,
                                 512,
                                 1,
                                 D3DUSAGE_RENDERTARGET,
                                 D3DFMT_X8R8G8B8,
                                 D3DPOOL_DEFAULT,
                                 NULL);

	m_pIlluminationBuffer = new D3D9Buffer(m_Device, 
								 viewport.Width,
                                 viewport.Height,
                                 1,
                                 D3DUSAGE_RENDERTARGET,
                                 D3DFMT_X8R8G8B8,
                                 D3DPOOL_DEFAULT,
                                 NULL);

	//m_pPerlinTex = WMI->
	m_pPerlinTex = NULL;
	string fileName = "Assets/Textures/perlin512.jpg";
	HRESULT hr = D3DXCreateTextureFromFile(m_Device, fileName.c_str(), &m_pPerlinTex);
	if (FAILED(hr))
	{
		MessageBox(0, "Graphics Core - Failed to Load perlin512.jpg", 0, 0);
	}
	//Initialize quad
	m_oQuad = new D3D9Quad(m_Device);
	
	m_iBufferShow = 0;

	m_fElapsedTime = 0;

	m_vIntensity = D3DXVECTOR2(0.5, 1.0);

	//Set up dummy viewport
	D3D9Viewport* _vpTemp;
	_vpTemp = new D3D9Viewport(m_Device, D3DXVECTOR2(0.5, 0.5), D3DXVECTOR2(0, 0), D3DXVECTOR2(0.5, 0.5), D3DXVECTOR2(0, 0), 0);
	_vpTemp->ToggleVisible();
	m_vViewport.push_back(_vpTemp);

	m_fAlphaLevel = 0.0f;

	
	//Plugin Initialization
	ParticleInit();
	GUIInit();
}

D3D9GraphicsCore::~D3D9GraphicsCore()
{
	for(auto vp : m_vViewport)
		delete vp;

	delete m_pIlluminationBuffer;
	delete m_pPerlinBuffer;
	delete m_pRenderBuffer;

	delete m_ParticleMan;
	delete m_ShaderManager;
	delete m_GfxStats;
	delete m_SkyBox;
	delete m_oQuad;
	delete m_LightManager;
}

// Private Functions
//==========================================

//==========================================

void D3D9GraphicsCore::Update(float dt)
{
	m_fElapsedTime += dt;

	m_GfxStats->update(dt);
	m_oQuad->Update(dt);

	ParticleUpdate(dt);

}

void D3D9GraphicsCore::Render(D3DXMATRIX View, D3DXMATRIX Proj, D3DXVECTOR3 CameraPosition)
{
	D3DXMATRIX ViewProjection = View * Proj;
	D3DXMATRIX W, WI;
	auto GraphicsObjects = WMI->GetGraphicsSortedByType();

	//Initialize for render to texture
	m_Device->GetRenderTarget(0, &m_pBackBuffer);
	//*************************PRE RENDER BUFFER BEGIN***********************************//
	
	//Illumination buffer
	m_Device->SetRenderTarget(0, m_pIlluminationBuffer->m_pSurface);
	m_Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,  g_Black, 1.0f, 0);
	m_Device->BeginScene();
	m_CurrentShader->SetTechnique("IlluminationBuffer");

	m_CurrentShader->Begin(0);
	m_CurrentShader->BeginPass(0);
	for(auto vectorOfType : GraphicsObjects)
	{
		for(auto Object : *vectorOfType)
		{
			if (!Object->m_bShieldAlpha)// & ILL)
			{
				Object->buildTransformMatrix();
				D3DXMatrixIdentity(&W);
			
				W = W * Object->m_transformMatrix;
				m_CurrentShader->SetParamaterValue("gWorld", &W, sizeof(W));
				m_CurrentShader->SetParamaterValue("gWVP", &(W * ViewProjection), sizeof(W));

				D3DXMatrixInverse(&WI, 0, &W);
				if (Object->m_bStartShift)
				{
					m_CurrentShader->SetParamaterValue("gStretchStart", &m_fElapsedTime, sizeof(m_fElapsedTime));
					Object->m_bStartShift = false;
				}
				m_CurrentShader->SetParamaterValue("gWorldInvTrans", &WI, sizeof(WI));
				m_CurrentShader->SetParamaterValue("gTextureIllumination", &Object->m_maps[4], sizeof(Object->m_maps[4]));
				m_CurrentShader->SetBool("gIsSun", Object->m_bIsSun);
				m_CurrentShader->SetBool("gShipStretch", Object->m_bShipStretch);
				m_CurrentShader->SetBool("gShipReleaseStretch", Object->m_bShipReleaseStretch);
				m_CurrentShader->SetParamaterValue("gTexturePerlin", &m_pPerlinTex, sizeof(m_pPerlinTex));
				m_CurrentShader->SetBool("gShipStretch", Object->m_bShipStretch);
				
				//Lock in changes
				m_CurrentShader->CommitChanges();

				//Render
				Object->m_mesh->DrawSubset(0);
			}
		}
	}
	m_CurrentShader->EndPass();
	m_CurrentShader->End();


	m_Device->EndScene();


	//**************************PRE RENDER BUFFER END************************************//
	//Point the device at the quad's render surface
	m_Device->SetRenderTarget(0, m_pRenderBuffer->m_pSurface);
	
	m_Device->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, g_Black, 1.0f, 0);
	m_Device->BeginScene();
	
	if(m_skyBoxRender)
		m_SkyBox->Render(ViewProjection, CameraPosition);

	
	
	//Begin primary object render => pRenderSurface
	m_CurrentShader->SetTechnique("PhongDirLtTexTechFilled");
	m_CurrentShader->SetParamaterValue("gLight", &m_lLight, sizeof(m_lLight));
	m_CurrentShader->SetParamaterValue("gEyePosW", CameraPosition, sizeof(D3DXVECTOR3));
		
	
	
	m_CurrentShader->Begin(0);
	m_CurrentShader->BeginPass(0);

	for(auto vectorOfType : GraphicsObjects)
	{
		for(auto Object : *vectorOfType)
		{
			if (!Object->m_bShieldAlpha)// & ILL)
			{
			//Set object level uniforms

			Object->buildTransformMatrix();
			D3DXMatrixIdentity(&W);
			
			W = W * Object->m_transformMatrix;
			m_CurrentShader->SetParamaterValue("gWorld", &W, sizeof(W));
			m_CurrentShader->SetParamaterValue("gWVP", &(W * ViewProjection), sizeof(W));

			D3DXMatrixInverse(&WI, 0, &W);
			m_CurrentShader->SetParamaterValue("gWorldInvTrans", &WI, sizeof(WI));
			
			

			m_CurrentShader->SetParamaterValue("gTextureDiff", &Object->m_maps[0], sizeof(Object->m_maps[0]));
			m_CurrentShader->SetParamaterValue("gTextureBump", &Object->m_maps[1], sizeof(Object->m_maps[1]));
			m_CurrentShader->SetParamaterValue("gTextureNormal", &Object->m_maps[2], sizeof(Object->m_maps[2]));
			m_CurrentShader->SetParamaterValue("gTextureSpecular", &Object->m_maps[3], sizeof(Object->m_maps[3]));
			m_CurrentShader->SetParamaterValue("gTextureIllumination", &Object->m_maps[4], sizeof(Object->m_maps[4]));
			m_CurrentShader->SetParamaterValue("gShowBuffer", &m_iBufferShow, sizeof(int));
			m_CurrentShader->SetParamaterValue("gElapsedTime", &m_fElapsedTime, sizeof(m_fElapsedTime));
			m_CurrentShader->SetBool("gIsSun", Object->m_bIsSun);
			m_CurrentShader->SetBool("gShipStretch", Object->m_bShipStretch);
			m_CurrentShader->SetBool("gShipReleaseStretch", Object->m_bShipReleaseStretch);
			m_CurrentShader->SetBool("gDiffuseOn", m_bTextures);
			m_CurrentShader->SetBool("gAniOn", m_bAnisotropic);
			m_CurrentShader->SetParamaterValue("gTexturePerlin", &m_pPerlinTex, sizeof(m_pPerlinTex));
			m_CurrentShader->SetParamaterValue("gMtrl", &Object->m_material, sizeof(Object->m_material));
			

			//Lock in changes
			m_CurrentShader->CommitChanges();

			//Render
			Object->m_mesh->DrawSubset(0);
			}
		}
	}
	m_CurrentShader->EndPass();
	m_CurrentShader->End();
	//End Primary Object render loop


	ParticleRender();

	//Begin Alpha Render Loop
	m_CurrentShader->SetTechnique("ShieldAlpha");

	m_CurrentShader->Begin(0);
	m_CurrentShader->BeginPass(0);

	for(auto vectorOfType : GraphicsObjects)
	{
		for(auto Object : *vectorOfType)
		{
			if (Object->m_bShieldAlpha)
			{
			//Set object level uniforms

			Object->buildTransformMatrix();
			D3DXMatrixIdentity(&W);
			
			W = W * Object->m_transformMatrix;
			m_CurrentShader->SetParamaterValue("gWorld", &W, sizeof(W));
			m_CurrentShader->SetParamaterValue("gWVP", &(W * ViewProjection), sizeof(W));

			D3DXMatrixInverse(&WI, 0, &W);
			m_CurrentShader->SetParamaterValue("gWorldInvTrans", &WI, sizeof(WI));
				
			m_CurrentShader->SetParamaterValue("gTextureDiff", &Object->m_maps[0], sizeof(Object->m_maps[0]));
			m_CurrentShader->SetParamaterValue("gElapsedTime", &m_fElapsedTime, sizeof(m_fElapsedTime));

			m_CurrentShader->SetParamaterValue("gMtrl", &Object->m_material, sizeof(Object->m_material));
			m_CurrentShader->SetParamaterValue("gContact", &Object->m_vContact, sizeof(Object->m_vContact));
			m_CurrentShader->SetParamaterValue("gShieldColor", &Object->m_cShieldColor, sizeof(Object->m_cShieldColor));
			m_CurrentShader->SetParamaterValue("gShieldSize", &Object->m_fShieldSize, sizeof(Object->m_fShieldSize));
			m_CurrentShader->SetParamaterValue("gAlphaLevel", &Object->m_fShieldAlpha, sizeof(m_fAlphaLevel));
			
			//Lock in changes
			m_CurrentShader->CommitChanges();

			//Render
			Object->m_mesh->DrawSubset(0);
			}
		}
	}
	m_CurrentShader->EndPass();
	m_CurrentShader->End();


	//End Alpha Render Loop

	m_Device->EndScene();
	
	//Draw quad and perform post shader effects
	m_Device->SetRenderTarget(0, m_pBackBuffer);
	m_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, g_Black, 1.0f, 0);
	

	m_Device->BeginScene();
	m_CurrentShader->SetTechnique("PostProcess");
		m_CurrentShader->Begin(0);
			m_CurrentShader->BeginPass(0);
		
				m_CurrentShader->SetParamaterValue("gWVP", &(m_oQuad->m_mWorld), sizeof(W));
				m_CurrentShader->SetParamaterValue("gPostBuffer", &m_pRenderBuffer->m_pTexture, sizeof(m_pRenderBuffer->m_pTexture));
				m_CurrentShader->SetParamaterValue("gBufferIllumination", &m_pIlluminationBuffer->m_pTexture, sizeof(m_pIlluminationBuffer->m_pTexture));
				m_CurrentShader->SetParamaterValue("gIlluminationFactor", &m_vIntensity, sizeof(D3DXVECTOR2));
				m_CurrentShader->CommitChanges();
				
				m_oQuad->Render();
			m_CurrentShader->EndPass();
		m_CurrentShader->End();

	if(m_gfxStatsDisplay)
		m_GfxStats->display();

	//GUI Render
	GUIRender();

	m_Device->EndScene();
#if 0
	//Viewport Test
	if (m_bViewportShow)
	{
		m_Device->SetViewport(&m_pViewport);
		m_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, COL_BLACK, 1.0f, 0);
	m_Device->BeginScene();
	m_CurrentShader->SetTechnique("PostSimple");
		m_CurrentShader->Begin(0);
			m_CurrentShader->BeginPass(0);
		
				m_CurrentShader->SetParamaterValue("gWVP", &(m_oQuad->m_mWorld), sizeof(W));
				m_CurrentShader->SetParamaterValue("gPostBuffer", &m_pRenderBuffer->m_pTexture, sizeof(m_pRenderBuffer->m_pTexture));
				m_CurrentShader->CommitChanges();
				
				m_oQuad->Render();
			m_CurrentShader->EndPass();
		m_CurrentShader->End();
		m_Device->EndScene();


	}
#else
	for(auto vp : m_vViewport)
	{
		if (vp->IsVisible())
		{
			m_Device->SetViewport(&vp->GetViewport());
			m_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, g_Black, 1.0f, 0);
			m_Device->BeginScene();
			m_CurrentShader->SetTechnique("PostSimple");
				m_CurrentShader->Begin(0);
					m_CurrentShader->BeginPass(0);
		
						m_CurrentShader->SetParamaterValue("gWVP", &(m_oQuad->m_mWorld), sizeof(W));
						m_CurrentShader->SetParamaterValue("gPostBuffer", &m_pRenderBuffer->m_pTexture, sizeof(m_pRenderBuffer->m_pTexture));
						m_CurrentShader->CommitChanges();
				
						m_oQuad->Render();
					m_CurrentShader->EndPass();
				m_CurrentShader->End();
			m_Device->EndScene();

		}

	}
#endif
	m_Device->Present(0, 0, 0, 0);
	m_pBackBuffer->Release();
}

void D3D9GraphicsCore::OnLostDevice()
{
	ParticleOnLost();

	if (m_pBackBuffer)
		m_pBackBuffer->Release();
	delete m_oQuad;
	for(auto vp : m_vViewport)
	{
		vp->OnLost();
	}
	m_pIlluminationBuffer->OnLost();
	m_pPerlinBuffer->OnLost();
	m_pRenderBuffer->OnLost();

	m_pBackBuffer = NULL;

	m_CurrentShader->OnLostDevice();

	m_GfxStats->onLostDevice();
	m_SkyBox->onLostDevice();
}

void D3D9GraphicsCore::OnResetDevice()
{
	D3DVIEWPORT9 viewport;
	m_Device->GetViewport(&viewport);
	for(auto vp : m_vViewport)
	{
		vp->OnReset();
	}
	m_oQuad = new D3D9Quad(m_Device);
	
	m_pRenderBuffer->m_iWidth = (UINT)viewport.Width;
	m_pRenderBuffer->m_iHeight = (UINT)viewport.Height;
	m_pIlluminationBuffer->m_iWidth = (UINT)viewport.Width;
	m_pIlluminationBuffer->m_iHeight = (UINT)viewport.Height;

	m_pRenderBuffer->OnReset();
	m_pPerlinBuffer->OnReset();
	m_pIlluminationBuffer->OnReset();
	m_CurrentShader->OnResetDevice();
	m_GfxStats->onResetDevice();
	m_SkyBox->onResetDevice();
	ParticleOnReset();
	
}

//Interfacing Functions
void D3D9GraphicsCore::ReloadShaders()
{
	//delete m_ShaderManager;

	//m_ShaderManager = new D3D9ShaderManager();

	//m_ShaderManager->LoadEffectsFromFile(m_Device);
}

void D3D9GraphicsCore::RegisterNewParticleEffect()
{




}

void D3D9GraphicsCore::RegisterNewLight()
{

}

void D3D9GraphicsCore::SetSkyBoxRender(bool value)
{
	m_skyBoxRender = value;
}

void D3D9GraphicsCore::SetDrawColor(int r, int g, int b)
{
	m_DrawColor = D3DCOLOR_XRGB(r, g, b);
}

void D3D9GraphicsCore::SetGfxStatsRender(bool value)
{
	m_gfxStatsDisplay = value;
}

float D3D9GraphicsCore::GetFPS()
{
	return m_GfxStats->GetFPS();
}

void D3D9GraphicsCore::SetRenderFlag(int pFlag)
{
	switch (pFlag)
	{
	case GFX_NO_BLUR:
		{
			m_vIntensity = D3DXVECTOR2(0.0, 1.0);
			break;
		}
	case GFX_BLUR:
		{
			m_vIntensity = D3DXVECTOR2(0.5, 1.0);
			break;
		}
	default:
		break;
	}
}

//****************BEGIN PARTICLE WRAPPERS**********************//
void D3D9GraphicsCore::ParticleInit()
{
	m_ParticleMan = new ParticleManager();
}

void D3D9GraphicsCore::ParticleUpdate(float dt)
{
	m_ParticleMan->update(dt);
}

void D3D9GraphicsCore::ParticleRender()
{
	m_ParticleMan->Render();
}

void D3D9GraphicsCore::ParticleOnLost()
{
	m_ParticleMan->onLostDevice();
}

void D3D9GraphicsCore::ParticleOnReset()
{
	m_ParticleMan->onResetDevice();
}

ParticleEmitter* D3D9GraphicsCore::CreateEmitter(string emitterType, Vector3 pos, float duration)
{
	return m_ParticleMan->CreateEmitter(emitterType, pos, duration);
}

void D3D9GraphicsCore::DeleteEmitter(ParticleEmitter* emitter)
{
	m_ParticleMan->RemoveEmitter(emitter);
}

void D3D9GraphicsCore::ReloadParticles()
{
	delete m_ParticleMan;
	m_ParticleMan = new ParticleManager();
}
//******************END PARTICLE WRAPPERS**********************//

//********************BEGIN GUI WRAPPERS***********************//
void D3D9GraphicsCore::GUIInit()
{

}

void D3D9GraphicsCore::GUIRender()
{
	CEGUI::System::getSingleton().renderGUI();

	//Reset render states
	GUIPostRender();

}

void D3D9GraphicsCore::GUIOnLost()
{

}

void D3D9GraphicsCore::GUIOnReset()
{

}

/**
 * PreRender - Resets the device render states to their default settings
 * after the previous GUI render
 */

void D3D9GraphicsCore::GUIPostRender()
{

	m_Device->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_Device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	m_Device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);

}
//***********************END GUI WRAPPERS************************//
#include "ConsoleDelegate.h"
#include "Console.h"
#include "../../AI/AICore.h"
#include <CEGUI.h>

ConsoleDelegate::ConsoleDelegate(void)
{
	m_Console = nullptr;
	m_AICore  = nullptr;
}

ConsoleDelegate* ConsoleDelegate::Instance(void)
{
	static ConsoleDelegate ptr;
	return &ptr;
}

bool ConsoleDelegate::LinkConsole(Console* console)
{
	if (m_Console == nullptr)
	{
		m_Console = console;
		return true;
	}

	else 
		return false;
}

bool ConsoleDelegate::LinkAICore(AICore* core)
{
	if (m_AICore == nullptr)
	{
		m_AICore = core;
		return true;
	}

	else return false;
}

void ConsoleDelegate::ParseAI(std::string fileName)
{
	if (m_AICore)
	{
		m_AICore->ParseSteeringWeight(fileName);
	}
}

void ConsoleDelegate::ReloadAI(std::string fileName)
{
	if (m_AICore)
	{
		m_AICore->LoadSteeringWeight(fileName);
	}
}

void ConsoleDelegate::OutputText(const std::string& input)
{
	CEGUI::String msg(input);

	m_Console->ParseText(msg);
}

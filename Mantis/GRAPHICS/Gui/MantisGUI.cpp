#include "MantisGUI.h"
#include "../../FRAMEWORK/Input/DirectInput.h"
#include "TestBed.h"
#include "Console.h"
#include "QuestDialog.h"
#include "ConsoleDelegate.h"
#include "OptionsMenu.h"
#include "MainMenu.h"
#include "PauseMenu.h"
#include "HUD.h"
#include "DeathSplash.h"
#include "..\..\FRAMEWORK\Utility\MantisUtil.h"
#include "..\..\FRAMEWORK\App\d3dApp.h"
#include <RendererModules\Direct3D9\CEGUIDirect3D9Renderer.h>
#include <iostream>
#include "../../FRAMEWORK/Sound/Audio.h"

#pragma comment( lib, "CEGUIBase_d.lib" )
#pragma comment( lib, "CEGUIDirect3D9Renderer_d.lib" )


MantisGUI::MantisGUI(GameFramework* game) : m_GameFramework(game)
{
	//Initialize the renderer
	m_pRenderer = &CEGUI::Direct3D9Renderer::bootstrapSystem(gd3dDevice);
	
	//Get singleton
	m_pSysGUI = CEGUI::System::getSingletonPtr();

	//Create console
	m_Console = new Console();

	//Create testbed
	//m_Testbed = new TestBed(this);

	//main menu creation
	m_MainMenu = new MainMenu(this);

	//create options
	m_Options = new OptionsMenu(this);

	//pause menu creation
	m_PauseMenu = new PauseMenu(this);

	//HUD
	m_HUD = new HUD(this);

	//Death splash
	m_DeathSplash = new DeathSplash(this);
}

MantisGUI::~MantisGUI(void)
{
	/*if (m_Testbed)
	{
		delete m_Testbed;
	}*/

	if (m_Console)
	{
		delete m_Console;
	}

	if(m_MainMenu)
	{
		delete m_MainMenu;
	}

	if(m_Options)
	{
		delete m_Options;
	}

	if(m_PauseMenu)
	{
		delete m_PauseMenu;
	}

	if(m_HUD)
	{
		delete m_HUD;
	}

	if(m_DeathSplash)
	{
		delete m_DeathSplash;
	}


	if (m_pRenderer)
	{
		m_pRenderer->destroySystem();
	}
}

void MantisGUI::Init(void)
{
	//Initialize the required dirs for the DefaultResourceProvider
	CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>
		(CEGUI::System::getSingleton().getResourceProvider());

	rp->setResourceGroupDirectory("schemes", "./Assets/GuiFiles/schemes");
	rp->setResourceGroupDirectory("imagesets", "./Assets/GuiFiles/imagesets/");
	rp->setResourceGroupDirectory("fonts", "./Assets/GuiFiles/fonts/");
	rp->setResourceGroupDirectory("layouts", "./Assets/GuiFiles/layouts/");
	rp->setResourceGroupDirectory("looknfeel", "./Assets/GuiFiles/looknfeel/");
	rp->setResourceGroupDirectory("lua_scripts", "./Assets/GuiFiles/lua_scripts");

	//Set the default resource groups to be used
	CEGUI::Imageset::setDefaultResourceGroup("imagesets");
	CEGUI::Font::setDefaultResourceGroup("fonts");
	CEGUI::Scheme::setDefaultResourceGroup("schemes");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeel");
	CEGUI::WindowManager::setDefaultResourceGroup("layouts");
	CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

	//Load scheme and font files
	CEGUI::SchemeManager::getSingleton().create( "TaharezLook.scheme" );
	CEGUI::FontManager::getSingleton().create( "OCRAStd-10.font" );

	//Set default initialisation
	CEGUI::System::getSingleton().setDefaultFont( "OCRAStd-10" );
	CEGUI::System::getSingleton().setDefaultMouseCursor( "TaharezLook", "MouseArrow" );
	ShowCursor(false);

	CEGUI::System::getSingleton().setDefaultTooltip( "TaharezLook/Tooltip" );

	//Create parent window
	CEGUI::Window* myRoot = CEGUI::WindowManager::getSingleton().
		createWindow( "DefaultWindow", "_MasterRoot" );

	//Set parent window as root window
	CEGUI::System::getSingleton().setGUISheet( myRoot );

	//Initialize the console and link to delegate
	m_Console->Init();
	CDI->LinkConsole(m_Console);
	CDI->LinkAICore(GetAI());

	//Initialize testbed menu
	//m_Testbed->Init();

	m_Options->Init();
	m_Options->setVisible(false);

	m_MainMenu->Init();
	m_MainMenu->setVisible(false);

	m_PauseMenu->Init();
	m_PauseMenu->setVisible(false);

	m_HUD->Init();
	m_HUD->setVisible(false);

	m_DeathSplash->Init();
	m_DeathSplash->setVisible(false);
}

void MantisGUI::Update(float dt)
{
	//Get cursor position
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(gd3dApp->getMainWnd(), &p);

	//Inject mouse position
	CEGUI::System::getSingleton().injectMousePosition(
		(float)p.x, (float)p.y);

	CEGUI::System::getSingleton().injectTimePulse(dt);

	//Check console
	if (gRawInput->keyPressed(VK_OEM_3))
	{
		m_Console->toggleVisible();
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
	}

	m_HUD->Update(dt);
}

void MantisGUI::Draw(void)
{
	m_pSysGUI->renderGUI();
}

CEGUI::System* MantisGUI::GetGui(void)
{
	return m_pSysGUI;
}

CEGUI::Direct3D9Renderer* MantisGUI::GetRenderer(void)
{
	return m_pRenderer;
}

void MantisGUI::OnResetDevice(void)
{
	m_pRenderer->postD3DReset();
}

void MantisGUI::OnLostDevice(void)
{
	m_pRenderer->preD3DReset();
}
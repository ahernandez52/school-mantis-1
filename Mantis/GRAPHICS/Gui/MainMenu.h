#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"


/**	
 *	CEGUI Window for the Main Menu.
 */
class MainMenu
{
private:

	//Pointer to the MainMenu root window
	CEGUI::Window*	m_MainMenuWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Display bool
	bool m_Display;

	MantisGUI* m_Owner;

public:
	MainMenu(MantisGUI* owner);

	~MainMenu(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_NewGameButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_OptionsButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_QuitButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_NewGameButtonHovered(const CEGUI::EventArgs& e);

	bool Handle_OptionsButtonHovered(const CEGUI::EventArgs& e);

	bool Handle_QuitButtonHovered(const CEGUI::EventArgs& e);

	//Audio events
	void PlayClick(void);
	void PlayRollOver(void);
};
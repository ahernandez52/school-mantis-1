#include "OptionsMenu.h"
#include "../../GAMEPLAY/MainMenuState.h"
#include "PauseMenu.h"
#include "../../FRAMEWORK/Sound/Audio.h"

int OptionsMenu::m_InstanceNumber = 0;

OptionsMenu::OptionsMenu(MantisGUI* owner) : m_Owner(owner)
{
	m_OptionsMenuWindow = NULL;
	m_ChildNumber = 0;
	m_NamePrefix = "";
	m_Display = false;
	m_atMainMenu = false;
}

OptionsMenu::~OptionsMenu(void)
{
	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_OptionsMenuWindow);
	m_OptionsMenuWindow->destroy();
}

//========================================
//  Private Functions
//========================================

void OptionsMenu::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void OptionsMenu::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_expand.wav");
		m_OptionsMenuWindow->activate();
		m_OptionsMenuWindow->show();
	}
	else
	{
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_contract.wav");
		m_OptionsMenuWindow->deactivate();
		m_OptionsMenuWindow->hide();
	}
}

bool OptionsMenu::isVisible(void)
{
	return m_Display;
}

void OptionsMenu::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_contract.wav");
	}
	else
	{
		setVisible(true);
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_expand.wav");
	}
}

bool OptionsMenu::atMainMenu()
{
	return m_atMainMenu;
}

void OptionsMenu::SetMainMenuFlag(bool b)
{
	m_atMainMenu = b;
}

//========================================
//  Private Functions
//========================================

void OptionsMenu::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the OptionsMenu window and assign it to window manager
	m_OptionsMenuWindow = pWindowManager->loadWindowLayout("OptionsMenu.layout", m_NamePrefix);

	if (m_OptionsMenuWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_OptionsMenuWindow);

		//register the tabs with the tab controller
		RegisterTabs();

		//Register handlers for events
		(this)->RegisterHandlers();
	}

	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the OptionsMenu window from .layout");
	}
}

void OptionsMenu::RegisterTabs()
{
	CEGUI::WindowManager* windMgr = CEGUI::WindowManager::getSingletonPtr();
	CEGUI::TabControl* tc = static_cast<CEGUI::TabControl*>(m_OptionsMenuWindow->getChild("OptionsMenuRoot/TabControl"));
	CEGUI::String prefix = "OptionsMenu/";

	tc->addTab(windMgr->loadWindowLayout("VideoOptions.layout", prefix));
	tc->addTab(windMgr->loadWindowLayout("AudioOptions.layout", prefix));
	tc->addTab(windMgr->loadWindowLayout("GameplayOptions.layout", prefix));
	tc->addTab(windMgr->loadWindowLayout("KeybindOptions.layout", prefix));
}

void OptionsMenu::RegisterHandlers(void)
{
	m_OptionsMenuWindow->getChild(m_NamePrefix + "OptionsMenuRoot/ApplyButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&OptionsMenu::Handle_ApplyButtonPressed, this));

	m_OptionsMenuWindow->getChild(m_NamePrefix + "OptionsMenuRoot/SaveButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&OptionsMenu::Handle_SaveButtonPressed, this));

	m_OptionsMenuWindow->getChild(m_NamePrefix + "OptionsMenuRoot/CancelButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&OptionsMenu::Handle_CancelButtonPressed, this));

	RegisterVideoTabHandlers();
	RegisterAudioTabHandlers();
	RegisterGameplayTabHandlers();
	RegisterKeybindTabHandlers();
}

void OptionsMenu::RegisterVideoTabHandlers()
{
}

void OptionsMenu::RegisterAudioTabHandlers()
{
}

void OptionsMenu::RegisterGameplayTabHandlers()
{
}

void OptionsMenu::RegisterKeybindTabHandlers()
{
}

bool OptionsMenu::Handle_ApplyButtonPressed(const CEGUI::EventArgs& e)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
	
	return true;
}

bool OptionsMenu::Handle_SaveButtonPressed(const CEGUI::EventArgs& e)
{
	setVisible(false);
	
	if(atMainMenu()){
		m_Owner->m_MainMenu->setVisible(true);
	}
	else
	{
		m_Owner->m_PauseMenu->setVisible(true);
	}

	return true;
}

bool OptionsMenu::Handle_CancelButtonPressed(const CEGUI::EventArgs& e)
{
	setVisible(false);

	//SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/hud_contract.wav");

	if(atMainMenu()){
		m_Owner->m_MainMenu->setVisible(true);
	}
	else
	{
		m_Owner->m_PauseMenu->setVisible(true);
	}

	return true;
}

bool OptionsMenu::Handle_ApplyButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();
	return true;
}

bool OptionsMenu::Handle_SaveButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();
	return true;
}

bool OptionsMenu::Handle_CancelButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();
	return true;
}

void OptionsMenu::PlayClick(void)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Clicks/click3.wav");
}

void OptionsMenu::PlayRollOver(void)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Rollovers/rollover1.wav");
}
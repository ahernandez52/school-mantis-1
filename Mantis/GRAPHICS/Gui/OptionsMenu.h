#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"


/**	
 *	CEGUI Window for the Options Menu.
 */
class OptionsMenu
{
private:

	//Pointer to the OptionsMenu root window
	CEGUI::Window*	m_OptionsMenuWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//child identifiers
	int m_ChildNumber;

	//Display bool
	bool m_Display;

	bool m_atMainMenu;

	//pointer to owner
	MantisGUI* m_Owner;

public:
	OptionsMenu(MantisGUI* owner);

	~OptionsMenu(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

	/**	
	 *	Returns true if we are at the main menu.
	 *	Returns false if we are in-game.
	 */
	bool atMainMenu();

	void SetMainMenuFlag(bool b);

private:

	void CreateCEGUIWindow(void);

	/**	
	 *	Creates the separate tabs (Video/Audio/Gameplay/Keybinds) for the Options menu.
	 */
	void RegisterTabs();

	//Event handling
	void RegisterHandlers(void);

	void RegisterVideoTabHandlers();
	void RegisterAudioTabHandlers();
	void RegisterGameplayTabHandlers();
	void RegisterKeybindTabHandlers();

	bool Handle_ApplyButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_SaveButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_CancelButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_ApplyButtonHovered(const CEGUI::EventArgs& e);

	bool Handle_SaveButtonHovered(const CEGUI::EventArgs& e);

	bool Handle_CancelButtonHovered(const CEGUI::EventArgs& e);

	void PlayClick(void);

	void PlayRollOver(void);

};
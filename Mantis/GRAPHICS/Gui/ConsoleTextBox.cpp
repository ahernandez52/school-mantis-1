#include "ConsoleTextBox.h"

namespace CEGUI
{

ConsoleTextBox::ConsoleTextBox(const String& text,
	const HorizontalTextFormatting format,
	const uint item_id,
	void* const item_data,
	const bool disabled,
	const bool auto_delete) : 
	ListboxTextItem(text, item_id, item_data, disabled, auto_delete),
	m_formatting(format),
	m_formattedRenderedString(0),
	m_formattedAreaSize(0, 0)
{
}

	ConsoleTextBox::~ConsoleTextBox(void)
	{
		delete m_formattedRenderedString;
	}

	HorizontalTextFormatting ConsoleTextBox::getFormatting(void) const
	{
		return m_formatting;
	}
	
	void ConsoleTextBox::setFormatting(const HorizontalTextFormatting fmt)
	{
		if (fmt == m_formatting)
			return;

		m_formatting = fmt;
		delete m_formattedRenderedString;
		m_formattedRenderedString = 0;
		m_formattedAreaSize = Size(0, 0);
	}

	Size ConsoleTextBox::getPixelSize(void) const
	{
		if (!d_owner)
			return Size(0, 0);

		//Reparse text if needed
		if (!d_renderedStringValid)
			parseTextString();

		//Create formatter if needed
		if (!m_formattedRenderedString)
			setupStringFormatter();

		//Get size of render area
		const Size area_sz(static_cast<const Listbox*>(d_owner)->
			getListRenderArea().getSize());
		if (area_sz != m_formattedAreaSize)
		{
			m_formattedRenderedString->format(area_sz);
			m_formattedAreaSize = area_sz;
		}

		return Size(m_formattedRenderedString->getHorizontalExtent(),
					m_formattedRenderedString->getVerticalExtent());
	}

	void ConsoleTextBox::draw(GeometryBuffer& buffer,
							  const Rect& targetRect,
							  float alpha, const Rect* clipper) const
	{
		//Reparse text if needed
		if (!d_renderedStringValid)
			parseTextString();

		//Create formatter if needed
		if (!m_formattedRenderedString)
			setupStringFormatter();

	    const Size area_sz(static_cast<const Listbox*>(d_owner)->
        getListRenderArea().getSize());
		if (area_sz != m_formattedAreaSize)
		{
			m_formattedRenderedString->format(area_sz);
			m_formattedAreaSize = area_sz;
		}

		//Draw selection imagery
		if (d_selected && d_selectBrush != 0)
			d_selectBrush->draw(buffer, targetRect, clipper,
								getModulateAlphaColourRect(d_selectCols, alpha));

		//Factor the window alpha
		const ColourRect final_colours(
			getModulateAlphaColourRect(ColourRect(0xFFFFFFFF), alpha));

		//Draw the formatted text
		m_formattedRenderedString->draw(buffer, targetRect.getPosition(), 
										&final_colours, clipper);
	}

	void ConsoleTextBox::setupStringFormatter(void) const
	{
		//Delete current formatter
		delete m_formattedRenderedString;
		m_formattedRenderedString = 0;

		//Create new formatter
		switch(m_formatting)
		{
		case HTF_LEFT_ALIGNED:

			m_formattedRenderedString = 
				new LeftAlignedRenderedString(d_renderedString);
			break;

		case HTF_RIGHT_ALIGNED:

			m_formattedRenderedString = 
				new RightAlignedRenderedString(d_renderedString);
			break;

		case HTF_CENTRE_ALIGNED:

			m_formattedRenderedString = 
				new CentredRenderedString(d_renderedString);
			break;

		case HTF_JUSTIFIED:

			m_formattedRenderedString = 
				new JustifiedRenderedString(d_renderedString);
			break;

		case HTF_WORDWRAP_LEFT_ALIGNED:

			m_formattedRenderedString = 
				new RenderedStringWordWrapper
					<LeftAlignedRenderedString>(d_renderedString);
			break;

		case HTF_WORDWRAP_RIGHT_ALIGNED:

			m_formattedRenderedString = 
				new RenderedStringWordWrapper
					<RightAlignedRenderedString>(d_renderedString);
			break;

		case HTF_WORDWRAP_CENTRE_ALIGNED:

			m_formattedRenderedString = 
				new RenderedStringWordWrapper
					<CentredRenderedString>(d_renderedString);
			break;

		case HTF_WORDWRAP_JUSTIFIED:

			m_formattedRenderedString = 
				new RenderedStringWordWrapper
					<JustifiedRenderedString>(d_renderedString);
			break;
		}
	}
}
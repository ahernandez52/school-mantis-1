#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"


/**	
 *	CEGUI Window for the player death splash screen.
 */
class DeathSplash
{
private:

	//Pointer to the DeathSplash root window
	CEGUI::Window*	m_DeathSplashWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Display bool
	bool m_Display;

	MantisGUI* m_Owner;


public:
	DeathSplash(MantisGUI* owner);

	~DeathSplash(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

	/**	
	 *	Function to display the splash screen and show the score.
	 */
	void ShowDeathSplash();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_MainMenuButtonPressed(const CEGUI::EventArgs& e);

};
#include "MainMenu.h"
#include "../../GAMEPLAY/MainMenuState.h"
#include "../../FRAMEWORK/Sound/Audio.h"

int MainMenu::m_InstanceNumber = 0;

MainMenu::MainMenu(MantisGUI* owner) : m_Owner(owner)
{
	m_MainMenuWindow = NULL;
	m_NamePrefix = "";
	m_Display = false;
}

MainMenu::~MainMenu(void)
{
	SCI->ReleaseSound("Assets/Sounds/Effects/GUI/Rollovers/rollover1.wav");
	SCI->ReleaseSound("Assets/Sounds/Effects/GUI/Clicks/click3.wav");

	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_MainMenuWindow);
	m_MainMenuWindow->destroy();
}

//========================================
//  Private Functions
//========================================

void MainMenu::Init(void)
{
	//Load audio
	SCI->LoadSound("Assets/Sounds/Effects/GUI/Rollovers/rollover1.wav", false);
	SCI->LoadSound("Assets/Sounds/Effects/GUI/Clicks/click3.wav", false);

	CreateCEGUIWindow();
	setVisible(true);
}

void MainMenu::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		m_MainMenuWindow->activate();
		m_MainMenuWindow->show();
	}
	else
	{
		m_MainMenuWindow->deactivate();
		m_MainMenuWindow->hide();
	}
}

bool MainMenu::isVisible(void)
{
	return m_Display;
}

void MainMenu::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
	}
	else
	{
		setVisible(true);
		SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
	}
}

//========================================
//  Private Functions
//========================================

void MainMenu::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the MainMenu window and assign it to window manager
	m_MainMenuWindow = pWindowManager->loadWindowLayout("MainMenu.layout", m_NamePrefix);

	if (m_MainMenuWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_MainMenuWindow);

		//Register handlers for events
		(this)->RegisterHandlers();
	}

	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the MainMenu window from .layout");
	}
}

void MainMenu::RegisterHandlers(void)
{
	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/NewGameButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_NewGameButtonPressed, this));

	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/NewGameButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEnters,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_NewGameButtonHovered, this));

	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/OptionsButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_OptionsButtonPressed, this));

	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/OptionsButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEntersArea,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_OptionsButtonHovered, this));

	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/QuitButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_QuitButtonPressed, this));

	m_MainMenuWindow->getChild(m_NamePrefix + "MainMenuRoot/MainMenuBox")->getChild("MainMenuRoot/MainMenuBox/QuitButton")->subscribeEvent(
		CEGUI::PushButton::EventMouseEntersArea,
		CEGUI::Event::Subscriber(
		&MainMenu::Handle_QuitButtonHovered, this));
}

bool MainMenu::Handle_NewGameButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();

	static_cast<MainMenuState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->NewGame();
	
	return true;
}

bool MainMenu::Handle_OptionsButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();

	static_cast<MainMenuState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->Options();

	return true;
}

bool MainMenu::Handle_QuitButtonPressed(const CEGUI::EventArgs& e)
{
	PlayClick();

	static_cast<MainMenuState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->QuitGame();

	return true;
}

bool MainMenu::Handle_NewGameButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();

	return true;
}

bool MainMenu::Handle_OptionsButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();

	return true;
}

bool MainMenu::Handle_QuitButtonHovered(const CEGUI::EventArgs& e)
{
	PlayRollOver();

	return true;
}

void MainMenu::PlayClick(void)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Expands/map_submenu_popup.wav");
}

void MainMenu::PlayRollOver(void)
{
	SCI->PlaySample("Assets/Sounds/Effects/GUI/Rollovers/rollover1.wav");
}
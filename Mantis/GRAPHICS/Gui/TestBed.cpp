#include "TestBed.h"
#include "../../FRAMEWORK/GSM/GameState.h"
//<WARNING> need to include states

int TestBed::m_InstanceNumber = 0;

TestBed::TestBed(MantisGUI* owner) : m_Owner(owner)
{
	m_TestBedWindow = NULL;
	m_InstanceNumber = 0;
	m_NamePrefix = "";
	setVisible(false);
	m_TestBed = false;

}

TestBed::~TestBed(void)
{
	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_TestBedWindow);
}

//========================================
//  Private Functions
//========================================

void TestBed::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void TestBed::setVisible(bool visible)
{
	m_TestBed = visible;
}

bool TestBed::isVisible(void)
{
	return m_TestBed;
}

//========================================
//  Private Functions
//========================================

void TestBed::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the testbed window and assign it to window manager
	m_TestBedWindow = pWindowManager->loadWindowLayout("TestBed.layout", m_NamePrefix);

	if (m_TestBedWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_TestBedWindow);

		//Register handlers for events 
		(this)->RegisterHandlers();
	}

	else 
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the TestBed window from .layout");
	}
}

void TestBed::RegisterHandlers(void)
{
	//Register physics button
	m_TestBedWindow->getChild(m_NamePrefix + "TestbedRoot/PhysicsButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&TestBed::Handle_PhysicsButtonPressed, this));

	//Register the Graphics button
	m_TestBedWindow->getChild(m_NamePrefix + "TestbedRoot/GraphicsButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&TestBed::Handle_GraphicsButtonPressed, this));

	//Register the AI button
	m_TestBedWindow->getChild(m_NamePrefix + "TestbedRoot/AIButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&TestBed::Handle_AIButtonPressed, this));
}

bool TestBed::Handle_PhysicsButtonPressed(const CEGUI::EventArgs& e)
{
		
	//m_Owner->m_Framework->getGSM()->ChangeState(new ModelViewer(m_Owner->m_Framework, m_Owner->m_GraphicsInterface));
	//m_Owner->GetFramework()->getGSM()->ChangeState(new ModelViewer(m_Owner->GetFramework(), m_Owner->GetGraphics()));
	//m_Owner->GetFramework()->getGSM()->ChangeState(new PhysicsDemoState(m_Owner->GetFramework(), m_Owner->GetGraphics()));
	return true;
}

bool TestBed::Handle_GraphicsButtonPressed(const CEGUI::EventArgs& e)
{
	//m_Owner->GetFramework()->getGSM()->ChangeState(new GraphicsDemoState(m_Owner->GetFramework(), m_Owner->GetGraphics()));
	//m_Owner->GetFramework()->getGSM()->ChangeState(new GraphicsDemoState(m_Owner->GetFramework(), m_Owner->GetGraphics()));
	return true;
}

bool TestBed::Handle_AIButtonPressed(const CEGUI::EventArgs& e)
{
	//m_Owner->GetFramework()->getGSM()->ChangeState(new AIDemo2_1(m_Owner->GetFramework(), m_Owner->GetGraphics()));

	//m_Owner->GetFramework()->getGSM()->ChangeState(new AIDemo_Phase_1(m_Owner->GetFramework(), m_Owner->GetGraphics()));
	return true;
}



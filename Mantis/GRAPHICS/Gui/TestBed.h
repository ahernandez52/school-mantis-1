/**
 * TestBed - A Menu used to select between different game states dedicated to 
 * core testbeds
 */

#pragma once

#include <CEGUI.h>
//<WARNING> need to include states
#include "MantisGUI.h"


class TestBed
{
private:

	//Pointer to the TestBed root window
	CEGUI::Window*	m_TestBedWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Visibility
	bool		    m_TestBed;

	//Pointer to the owning state
	MantisGUI*		m_Owner;

public:
	TestBed(MantisGUI* owner) ;

	~TestBed(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_PhysicsButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_GraphicsButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_AIButtonPressed(const CEGUI::EventArgs& e);

};


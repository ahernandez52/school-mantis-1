#include "QuestDialog.h"
#include "../../GAMEPLAY/Classes/Quest/Quest.h"
#include "../../GAMEPLAY/Classes/Quest/QuestSystem.h"
//<WARNING> need to include states

int QuestDialog::m_InstanceNumber = 0;

QuestDialog::QuestDialog(QuestSystem* qs)
{
	m_QuestDialogWindow = NULL;
	m_InstanceNumber = 0;
	m_NamePrefix = "";
	m_Display = false;
	m_Owner = qs;
	m_CurrQuest = nullptr;
}

QuestDialog::QuestDialog(QuestSystem* qs, Quest* quest)
{
	m_QuestDialogWindow = NULL;
	m_InstanceNumber = 0;
	m_NamePrefix = "";
	m_Display = false;
	m_Owner = qs;
	m_CurrQuest = quest;
}

QuestDialog::~QuestDialog(void)
{
	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_QuestDialogWindow);
	m_QuestDialogWindow->destroy();
}

//========================================
//  Private Functions
//========================================

void QuestDialog::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void QuestDialog::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		m_QuestDialogWindow->activate();
		m_QuestDialogWindow->show();
	}
	else
	{
		m_QuestDialogWindow->deactivate();
		m_QuestDialogWindow->hide();
	}
}

bool QuestDialog::isVisible(void)
{
	return m_Display;
}

void QuestDialog::SetQuest(Quest* quest)
{
	m_CurrQuest = quest;

	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/QuestName")->setText(quest->GetName());

	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/QuestDescription")->setText(quest->GetBody());

	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/QuestObjectives")->setText(quest->GetObjectiveString());

	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/QuestRewards")->setText(quest->GetRewardString());
}

void QuestDialog::RemoveQuest()
{
	m_CurrQuest = nullptr;
}

void QuestDialog::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}
}

//========================================
//  Private Functions
//========================================

void QuestDialog::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the QuestDialog window and assign it to window manager
	m_QuestDialogWindow = pWindowManager->loadWindowLayout("QuestDialog.layout", m_NamePrefix);

	if (m_QuestDialogWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_QuestDialogWindow);

		//Register handlers for events 
		(this)->RegisterHandlers();
	}

	else 
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the QuestDialog window from .layout");
	}
}

void QuestDialog::RegisterHandlers(void)
{
	//Register Accept button
	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/AcceptQuest")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&QuestDialog::Handle_AcceptQuestButtonPressed, this));

	//Register the Decline button
	m_QuestDialogWindow->getChild(m_NamePrefix + "QuestDialogRoot/DeclineQuest")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&QuestDialog::Handle_DeclineQuestButtonPressed, this));

}

bool QuestDialog::Handle_AcceptQuestButtonPressed(const CEGUI::EventArgs& e)
{
	//Add quest to the player's quest log
	m_Owner->m_QuestLog->AddQuest(m_CurrQuest);

	//close the window afterwards
	setVisible(false);

	RemoveQuest();

	return true;
}

bool QuestDialog::Handle_DeclineQuestButtonPressed(const CEGUI::EventArgs& e)
{
	//close the window and remove the quest
	setVisible(false);

	RemoveQuest();

	return true;
}


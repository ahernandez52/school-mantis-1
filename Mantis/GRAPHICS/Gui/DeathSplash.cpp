#include "DeathSplash.h"
#include "HUD.h"
#include "../../GAMEPLAY/FinalDemoState.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"

using namespace std;

int DeathSplash::m_InstanceNumber = 0;

DeathSplash::DeathSplash(MantisGUI* owner) : m_Owner(owner)
{
	m_DeathSplashWindow = NULL;
	m_NamePrefix = "";
	m_Display = false;
}

DeathSplash::~DeathSplash(void)
{
	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_DeathSplashWindow);
	m_DeathSplashWindow->destroy();
}

//========================================
//  Private Functions
//========================================

void DeathSplash::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void DeathSplash::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		m_DeathSplashWindow->activate();
		m_DeathSplashWindow->show();
	}
	else
	{
		m_DeathSplashWindow->deactivate();
		m_DeathSplashWindow->hide();
	}
}

bool DeathSplash::isVisible(void)
{
	return m_Display;
}

void DeathSplash::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}
}

void DeathSplash::ShowDeathSplash()
{
	setVisible(true);
	m_Owner->GetFramework()->SetPause(true);
	m_Owner->m_HUD->setVisible(false);

	m_DeathSplashWindow->getChild("DeathRoot/ScoreText")->setText("Score: " + to_string(m_Owner->GetFramework()->GetGameplay()->GetPlayer()->m_Credits));
}

//========================================
//  Private Functions
//========================================

void DeathSplash::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the DeathSplash window and assign it to window manager
	m_DeathSplashWindow = pWindowManager->loadWindowLayout("DeathSplash.layout", m_NamePrefix);

	if (m_DeathSplashWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_DeathSplashWindow);

		//Register handlers for events
		(this)->RegisterHandlers();
	}

	else
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the DeathSplash window from .layout");
	}
}

void DeathSplash::RegisterHandlers(void)
{
	m_DeathSplashWindow->getChild(m_NamePrefix + "DeathRoot/MainMenuButton")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&DeathSplash::Handle_MainMenuButtonPressed, this));
}

bool DeathSplash::Handle_MainMenuButtonPressed(const CEGUI::EventArgs& e)
{
	static_cast<FinalDemoState*>(m_Owner->GetFramework()->getGSM()->GetCurrentState())->QuitMenu();
	setVisible(false);

	
	
	return true;
}

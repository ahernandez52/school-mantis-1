#pragma once

#include <CEGUI.h>
//<WARNING> need to include states
#include "MantisGUI.h"

class item;
class Inventory;

class InventoryWindow
{
	//Pointer to the InventoryWindow root window
	CEGUI::Window*	m_InventoryWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//ListBoxItem Identifier
	int		m_ItemNumber;

	//Display bool
	bool m_Display;

	Inventory*	m_ActiveInv;

	item* m_ItemDis;

public:
	InventoryWindow(Inventory* active);
	~InventoryWindow();

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	void ToggleVisible();

	void PopulateInventory();

	void DePopulateInventory();

	void setItem(item* dis);

	void removeItemFromDisplay();

	void removeItemDis();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_ActiveItemPressed(const CEGUI::EventArgs& e);

	bool Handle_DropButtonPressed(const CEGUI::EventArgs& e);
};
#include "QuestLogWindow.h"
#include "../../GAMEPLAY/Classes/Quest/Quest.h"
#include "../../GAMEPLAY/Classes/Quest/QuestLog.h"

int QuestLogWindow::m_InstanceNumber = 0;

QuestLogWindow::QuestLogWindow(QuestLog* questLog) : m_Owner(questLog)
{
	m_QuestLogWindow = NULL;
	m_ItemNumber = -1;
	m_NamePrefix = "";
	m_Display = false;
	m_CurrQuest = nullptr;
}

QuestLogWindow::~QuestLogWindow(void)
{
	for(auto connection : m_EventConnections)
	{
		connection->disconnect();
	}

	m_QuestLogWindow->removeAllEvents();

	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_QuestLogWindow);
	CEGUI::WindowManager::getSingleton().destroyWindow(m_QuestLogWindow);
}

//========================================
//  Private Functions
//========================================

void QuestLogWindow::Init(void)
{
	CreateCEGUIWindow();
	setVisible(true);
}

void QuestLogWindow::setVisible(bool visible)
{
	m_Display = visible;

	if(m_Display)
	{
		PopulateQuestLog();
		m_QuestLogWindow->activate();
		m_QuestLogWindow->show();
	}
	else
	{
		RemoveQuest();
		DePopulateQuestLog();
		m_QuestLogWindow->deactivate();
		m_QuestLogWindow->hide();
	}
}

bool QuestLogWindow::isVisible(void)
{
	return m_Display;
}

void QuestLogWindow::SetQuest(Quest* quest)
{
	m_CurrQuest = quest;

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestName")->setText(quest->GetName());

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestObjectives")->setText(quest->GetObjectiveProgressString());

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestDescription")->setText(quest->GetBody());

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestRewards")->setText(quest->GetRewardString());
}

void QuestLogWindow::RemoveQuest()
{
	m_CurrQuest = nullptr;

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestName")->setText("");

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestObjectives")->setText("");

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestDescription")->setText("");

	m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/CurrentQuestRewards")->setText("");
}

void QuestLogWindow::ToggleVisible()
{
	if(isVisible())
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}
}

void QuestLogWindow::PopulateQuestLog()
{
	//ItemListBox
	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/QuestLog"));
	listBox->setMultiselectEnabled(false);

	for(Quest* quest : m_Owner->m_vActiveQuests)
	{
		int itemNum = ++m_ItemNumber;
		CEGUI::ListboxTextItem* textItem = new CEGUI::ListboxTextItem(quest->m_Name, itemNum);
		textItem->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");

		listBox->addItem(textItem);
	}


	listBox->handleUpdatedItemData();
	RegisterHandlers();
}

void QuestLogWindow::RemoveQuestFromLog()
{
	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/QuestLog"));

	int x = m_ItemNumber;
	
	while(x > 0)
	{
		CEGUI::ListboxItem* textItem = listBox->getListboxItemFromIndex(x);

		if(textItem->getText() == m_CurrQuest->m_Name)
		{
			listBox->removeItem(textItem);
			--m_ItemNumber;
			return;
		}

		--x;
	}

	listBox->handleUpdatedItemData();
}

void QuestLogWindow::DePopulateQuestLog()
{
	//ItemListBox
	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/QuestLog"));

	while(m_ItemNumber != -1)
	{
		CEGUI::ListboxItem* textItem = listBox->getListboxItemFromIndex(m_ItemNumber);
		listBox->removeItem(textItem);

		--m_ItemNumber;
	}

	listBox->handleUpdatedItemData();
}

//========================================
//  Private Functions
//========================================

void QuestLogWindow::CreateCEGUIWindow(void)
{
	//Get a local pointer to the window manager
	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();

	//Set and increase instance id
	m_NamePrefix = ++m_InstanceNumber + "_";

	//Create the QuestLogWindow window and assign it to window manager
	m_QuestLogWindow = pWindowManager->loadWindowLayout("QuestLog.layout", m_NamePrefix);

	if (m_QuestLogWindow)
	{
		//If window succeeds attach it
		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_QuestLogWindow);

		//Register handlers for events 
		(this)->RegisterHandlers();
	}

	else 
	{
		//Log a failed event
		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the QuestLogWindow window from .layout");
	}
}

void QuestLogWindow::RegisterHandlers(void)
{
	//Register Abandon button
	m_EventConnections.push_back(
		m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/AbandonQuest")->subscribeEvent(
		CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(
		&QuestLogWindow::Handle_AbandonQuestButtonPressed, this)));

	m_EventConnections.push_back(
		m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/QuestLog")->subscribeEvent(
		CEGUI::Listbox::EventSelectionChanged,
		CEGUI::Event::Subscriber(
		&QuestLogWindow::Handle_ActiveQuestItemPressed, this)));

}

bool QuestLogWindow::Handle_AbandonQuestButtonPressed(const CEGUI::EventArgs& e)
{
	//Remove quest from player's quest book

	if(m_CurrQuest != nullptr)
	{
		m_Owner->AbandonQuest(m_CurrQuest);

		RemoveQuestFromLog();
		RemoveQuest();
	}
	
	return true;
}

bool QuestLogWindow::Handle_ActiveQuestItemPressed(const CEGUI::EventArgs& e)
{
	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_QuestLogWindow->getChild(m_NamePrefix + "QuestLogRoot/QuestLog"));

	if(listBox->getSelectedCount() == 0)
	{
		return true;
	}

	CEGUI::ListboxItem* item = listBox->getFirstSelectedItem();

	SetQuest(m_Owner->m_mActiveQuests.at(item->getText().c_str()));

	return true;
}
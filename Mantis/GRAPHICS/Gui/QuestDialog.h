#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"

class Quest;
class QuestSystem;


/**	
 *	CEGUI Window for the Quest Dialog window.
 */
class QuestDialog
{
private:

	//Pointer to the QuestDialog root window
	CEGUI::Window*	m_QuestDialogWindow;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//Display bool
	bool m_Display;

	QuestSystem* m_Owner;

	//The current quest being displayed in the window
	Quest* m_CurrQuest;

public:
	QuestDialog(QuestSystem* qs);
	QuestDialog(QuestSystem* qs, Quest* quest);

	~QuestDialog(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	/**	
	 *	Sets the specified quest onto the Dialog window.
	 */
	void SetQuest(Quest* quest);

	/**	
	 *	Removes the current quest from the Dialog window.
	 */
	void RemoveQuest();

	void ToggleVisible();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_AcceptQuestButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_DeclineQuestButtonPressed(const CEGUI::EventArgs& e);



};
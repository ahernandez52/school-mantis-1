/**
 * Console - Defines the console class which can be used to enter,
 * edit, and send text
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <CEGUI.h>

class ConsoleDelegate;

class Console
{
	//Pointer to the console root window
	CEGUI::Window*	m_ConsoleWindow;

	//Prefix given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	bool			m_Console;

public:
	Console();
	~Console();

	void Init();

	void setVisible(bool visible);
	void toggleVisible(void);
	bool isVisible(void);

private:
	void CreateCEGUIWindow();

	//Event handling
	void RegisterHandlers();
	bool Handle_SendButtonPressed(const CEGUI::EventArgs& e);
	bool Handle_TextSubmitted(const CEGUI::EventArgs& e);

	//Text parsing
	void ParseText(CEGUI::String inMsg);
	void OutputText(CEGUI::String inMsg,
		CEGUI::colour colour = CEGUI::colour(0xFFFFFFFF));

	friend class ConsoleDelegate;
};
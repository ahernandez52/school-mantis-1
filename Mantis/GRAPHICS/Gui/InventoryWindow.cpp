//#include "InventoryWindow.h"
//#include "../../GAMEPLAY/Classes/Inventory/item.h"
//#include "../../GAMEPLAY/Classes/Inventory/Inventory.h"
//
//int		InventoryWindow::m_InstanceNumber = 0;
//
//InventoryWindow::InventoryWindow(Inventory* active)
//{
//	m_InventoryWindow = NULL;
//	m_ActiveInv = active;
//	m_NamePrefix = "";
//	m_ItemNumber = -1;
//
//}
//
//InventoryWindow::~InventoryWindow()
//{
//	CEGUI::System::getSingleton().getGUISheet()->removeChildWindow(m_InventoryWindow);
//}
//
//
//void InventoryWindow::Init(void)
//{
//	CreateCEGUIWindow();
//	setVisible(true);
//}
//
//void InventoryWindow::setVisible(bool visible)
//{
//	m_Display = visible;
//
//	if(m_Display)
//	{
//		PopulateInventory();
//		m_InventoryWindow->activate();
//		m_InventoryWindow->show();
//	}
//	else
//	{
//		removeItemDis();
//		DePopulateInventory();
//		m_InventoryWindow->deactivate();
//		m_InventoryWindow->hide();
//	}
//}
//
//bool InventoryWindow::isVisible(void)
//{
//	return m_Display;
//}
//
//void InventoryWindow::ToggleVisible()
//{
//	if(isVisible())
//	{
//		setVisible(false);
//	}
//	else
//	{
//		setVisible(true);
//	}
//}
//
//void InventoryWindow::PopulateInventory()
//{
//	//ItemListBox
//	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/Inventorylist"));
//	listBox->setMultiselectEnabled(false);
//
//	for(item* stuff : m_ActiveInv->m_Stock)
//	{
//		int itemNum = ++m_ItemNumber;
//		CEGUI::ListboxTextItem* textItem = new CEGUI::ListboxTextItem(stuff->m_Name, itemNum);
//		textItem->setSelectionBrushImage("TaharezLook", "MultiListSelectionBrush");
//
//		listBox->addItem(textItem);
//	}
//
//
//	listBox->handleUpdatedItemData();
//	RegisterHandlers();
//}
//
//void InventoryWindow::DePopulateInventory()
//{
//	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/Inventorylist"));
//
//	while(m_ItemNumber != -1)
//	{
//		CEGUI::ListboxItem* textItem = listBox->getListboxItemFromIndex(m_ItemNumber);
//		listBox->removeItem(textItem);
//
//		--m_ItemNumber;
//	}
//
//	listBox->handleUpdatedItemData();
//}
//
//void InventoryWindow::setItem(item* dis)
//{
//	m_ItemDis = dis;
//
//	m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/CurrentItemName")->setText(dis->m_Name);
//}
//
//void InventoryWindow::removeItemFromDisplay()
//{
//	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/Inventorylist"));
//
//	int x = m_ItemNumber;
//	
//	while(x > 0)
//	{
//		CEGUI::ListboxItem* textItem = listBox->getListboxItemFromIndex(x);
//
//		if(textItem->getText() == m_ItemDis->m_Name)
//		{
//			listBox->removeItem(textItem);
//			--m_ItemNumber;
//			return;
//		}
//
//		--x;
//	}
//
//	listBox->handleUpdatedItemData();
//
//}
//
//void InventoryWindow::removeItemDis()
//{
//	m_ItemDis = nullptr;
//
//	m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/CurrentItemName")->setText("");
//
//	m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/CurrentItemDescription")->setText("");
//}
//
//
////========================================
////  Private Functions
////========================================
//
//void InventoryWindow::CreateCEGUIWindow(void)
//{
//	//Get a local pointer to the window manager
//	CEGUI::WindowManager* pWindowManager = CEGUI::WindowManager::getSingletonPtr();
//
//	//Set and increase instance id
//	m_NamePrefix = ++m_InstanceNumber + "_";
//
//	//Create the Inventory window and assign it to window manager
//	m_InventoryWindow = pWindowManager->loadWindowLayout("Inventory2.layout", m_NamePrefix);
//	//m_InventoryWindow = pWindowManager->loadWindowLayout("QuestLog.layout", m_NamePrefix);
//
//	if (m_InventoryWindow)
//	{
//		//If window succeeds attach it
//		CEGUI::System::getSingleton().getGUISheet()->addChildWindow(m_InventoryWindow);
//
//		//Register handlers for events 
//		(this)->RegisterHandlers();
//	}
//
//	else 
//	{
//		//Log a failed event
//		CEGUI::Logger::getSingleton().logEvent("Error: Unable to load the InventoryWindow window from .layout");
//	}
//}
//
//void InventoryWindow::RegisterHandlers(void)
//{
//	//Register handlers
//		m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/Inventorylist")->subscribeEvent(
//		CEGUI::Listbox::EventSelectionChanged,
//		CEGUI::Event::Subscriber(&InventoryWindow::Handle_ActiveItemPressed, this));
//
//		m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/DropItem")->subscribeEvent(
//		CEGUI::PushButton::EventClicked,
//		CEGUI::Event::Subscriber(
//		&InventoryWindow::Handle_DropButtonPressed, this));
//}
//
//bool InventoryWindow::Handle_ActiveItemPressed(const CEGUI::EventArgs& e)
//{
//	CEGUI::Listbox* listBox = static_cast<CEGUI::Listbox*>(m_InventoryWindow->getChild(m_NamePrefix + "InventoryRoot/Inventorylist"));
//
//	if(listBox->getSelectedCount() == 0)
//	{
//		return true;
//	}
//
//	CEGUI::ListboxItem* Item = listBox->getFirstSelectedItem();
//
//	setItem(m_ActiveInv->findItem(Item->getText().c_str()));
//
//
//	return true;
//}
//
//bool InventoryWindow::Handle_DropButtonPressed(const CEGUI::EventArgs& e)
//{
//	if(m_ItemDis != nullptr)
//	{
//		m_ActiveInv->DeleteItem(m_ItemDis->m_Name);
//
//		removeItemFromDisplay();
//		removeItemDis();
//	}
//	return true;
//}
#pragma once

#include <CEGUI.h>
#include "MantisGUI.h"

class Quest;
class QuestLog;


/**	
 *	CEGUI Window for the Player QuestLog.
 */
class QuestLogWindow
{
private:

	//Pointer to the QuestLogWindow root window
	CEGUI::Window*	m_QuestLogWindow;

	//list of event connections
	std::vector<CEGUI::Event::Connection> m_EventConnections;

	//Previx given to layout
	CEGUI::String	m_NamePrefix;

	//Identifier
	static int		m_InstanceNumber;

	//ListBoxItem Identifier
	int		m_ItemNumber;

	//Display bool
	bool m_Display;

	//owner pointer
	QuestLog* m_Owner;

	//current quest
	Quest* m_CurrQuest;

public:
	QuestLogWindow(QuestLog* questLog);

	~QuestLogWindow(void);

	void Init(void);

	void setVisible(bool visible);

	bool isVisible(void);

	/**	
	 *	Sets the current quest to the specified quest.
	 */
	void SetQuest(Quest* quest);

	/**	
	 *	Removes the current Quest from the quest window.
	 */
	void RemoveQuest();

	void ToggleVisible();

	/**	
	 *	Populates the log window with all the quests in the Player's log.
	 */
	void PopulateQuestLog();

	/**	
	 *	Removes the current quest from the log window.
	 */
	void RemoveQuestFromLog();

	/**	
	 *	Drops all quests from the log window.
	 */
	void DePopulateQuestLog();

private:

	void CreateCEGUIWindow(void);

	//Event handling
	void RegisterHandlers(void);

	bool Handle_AbandonQuestButtonPressed(const CEGUI::EventArgs& e);

	bool Handle_ActiveQuestItemPressed(const CEGUI::EventArgs& e);

};
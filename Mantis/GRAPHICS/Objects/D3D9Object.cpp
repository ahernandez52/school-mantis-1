#include "D3D9Object.h"
#include "../../FRAMEWORK/Objects/Actor.h"

D3D9Object::D3D9Object(Actor *Owner, GraphicSchematic Schematic)
{
	m_OwningActor = Owner;
	m_bIsSun = false;
	m_bShipStretch = false;
	m_bShipReleaseStretch = false;
	m_bShieldAlpha = false;
	m_fShieldSize = 0.0f;
	m_fShieldAlpha = 0.0f;


	//Read Schematic
	m_mesh			= Schematic.mesh;
	m_renderFlags	= Schematic.detailFlags;

	m_maps[0]		= Schematic.maps[0];
	m_maps[1]		= Schematic.maps[1];
	m_maps[2]		= Schematic.maps[2];
	m_maps[3]		= Schematic.maps[3];
	m_maps[4]		= Schematic.maps[4];

	m_parent		= NULL;
	m_scale			= D3DXVECTOR3(1.0f, 1.0f, 1.0f);

	m_material.ambient = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	m_material.diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_material.specular = D3DXCOLOR(0.1f, 0.1f, 0.1f, 1.0f);
	m_material.specularPower = 50.0f;

	m_bStartShift = false;
}

void D3D9Object::buildTransformMatrix()
{
	D3DXMATRIX R, T, S;
	
	D3DXQuaternionNormalize(&m_OwningActor->m_Orientation, &m_OwningActor->m_Orientation);
	D3DXMatrixRotationQuaternion(&R, &m_OwningActor->m_Orientation);

	D3DXMatrixTranslation(&T, m_OwningActor->m_Position.x, m_OwningActor->m_Position.y, m_OwningActor->m_Position.z);

	D3DXMatrixScaling(&S, m_scale.x, m_scale.y, m_scale.z);

	m_transformMatrix = S * R * T;

	
}

void D3D9Object::SetEffect(int pEffect)
{
	switch(pEffect)
	{
	case FX_WARP_START:
		{
			m_bShipStretch = true;
			m_bShipReleaseStretch = false;
			m_bStartShift = true;
			break;
		}
	case FX_WARP_STOP:
		{
			m_bShipStretch = false;
			m_bShipReleaseStretch = true;
			m_bStartShift = true;
			break;
		}
	case FX_SUN:
		{
			m_bIsSun = true;
			break;
		}
	case FX_SHIELD:
		{
			m_bShieldAlpha = true;
			break;
		}
	case FX_CONTACTRESET:
		{
			m_vContact = D3DXVECTOR3(0.0, 0.0, 0.0);
			m_fShieldSize = 0;
			break;
		}

	}
}

//Add Shield contact (must be in object space)
void D3D9Object::SetShieldContact(D3DXVECTOR3 pContact, float pSize, D3DXVECTOR4 pColor, float alpha)
{
	m_vContact = pContact;
	m_fShieldSize = pSize;
	m_cShieldColor = pColor;
	m_fShieldAlpha = alpha;
}
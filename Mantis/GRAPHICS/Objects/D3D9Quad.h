#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"


struct vert
{
	float x, y, z;	//position
	float nx, ny, nz; //vertex normal
	DWORD c;		//color
	float tu, tv;	//texture mapping
	
};

//Defaults to upright, facing negative z
//Note, do not scale if using for screen quad

class D3D9Quad
{
public:

	vert* pVertices;
	WORD* pIndices;

	IDirect3DTexture9* m_pTexture;

	D3DXVECTOR3 m_vPosition;	
	D3DXVECTOR3 m_vRotation;	
	D3DXVECTOR3 m_vScale;		
	IDirect3DDevice9* gd3dDevice;

	D3DXMATRIX m_mWorld;		//world matrix

	IDirect3DVertexBuffer9* m_vb;
	IDirect3DVertexDeclaration9* m_vd;			//only used with element9 VF
	IDirect3DIndexBuffer9* m_ib;

	D3D9Quad(IDirect3DDevice9* Device);

	void Update(double pDT)
	{
		D3DXMATRIX _rotX, _rotY, _rotZ;
		D3DXMATRIX _scale;
		D3DXMATRIX _pos;

		D3DXMatrixRotationX( &_rotX , m_vRotation.x) ;
 		D3DXMatrixRotationY( &_rotY , m_vRotation.y) ;
 		D3DXMatrixRotationZ( &_rotZ , m_vRotation.z) ;

		D3DXMatrixTranslation( &_pos , m_vPosition.x , m_vPosition.y , m_vPosition.z ) ;
 		D3DXMatrixScaling(&_scale, m_vScale.x, m_vScale.y, m_vScale.z) ;

		//m_mWorld = (_rotX * _rotY * _rotZ * _scale * _pos);
		m_mWorld = (_scale *_rotX * _rotY * _rotZ * _pos);
	}

	void Render()
	{
		gd3dDevice->SetVertexDeclaration(m_vd);
		gd3dDevice->SetStreamSource( 0, m_vb, 0, sizeof(vert) ); 
		gd3dDevice->SetIndices( m_ib );
		gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	}
	~D3D9Quad()
	{
		m_vb->Release();
		m_vd->Release();
		m_ib->Release();
	}
};

#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"

class D3D9Object
{
private:

public:


	Actor*				m_OwningActor;

	D3D9Object			*m_parent;
	ID3DXMesh			*m_mesh;
	LPDIRECT3DTEXTURE9	m_maps[5];
	
	D3DXMATRIX			m_transformMatrix;

	DWORD				m_renderFlags;

	D3DXVECTOR3			m_scale;

	MeshMaterial		m_material;

	std::string			m_schematicName;

	bool				m_bIsSun;
	bool				m_bShipStretch;
	bool				m_bShipReleaseStretch;
	bool				m_bStartShift;
	bool				m_bShieldAlpha;
	
	D3DXVECTOR3			m_vContact;			//Temp contact
	float				m_fShieldSize;
	float				m_fShieldAlpha;
	D3DXVECTOR4			m_cShieldColor;


	D3D9Object(Actor* Owner, GraphicSchematic Schematic);

	void buildTransformMatrix();

	void SetEffect(int pEffect);
	//pContact = Object Space; pSize = Radius to Vertex Check; pColor = RGBA 0.0 to 1.0
	void SetShieldContact(D3DXVECTOR3 pContact, float pSize, D3DXVECTOR4 pColor, float alpha);
};
#include "ParticleManager.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "../../FRAMEWORK/Utility/JSONParser.h"
#include "../../FRAMEWORK/Utility/Camera.h"
#include "../../FRAMEWORK/App/d3dApp.h"

using namespace std;

ParticleManager::ParticleManager()
{
	m_Active = false;
	m_Infinity = FLT_MAX;

	BuildFX();
	LoadParticleEffects();
}

ParticleManager::~ParticleManager()
{
	//delete emitters
	for(auto emitter : m_ParticleEmitters)
		delete emitter;
	
	//release Textures
	for(auto texture : m_Textures)
		texture.second->Release();

	//release Effect
	ReleaseCOM(fx);
}

void ParticleManager::BuildFX()
{
	//D3DXCreateTextureFromFile(gd3dDevice, "Assets/Particles/Textures/bolt.dds", &gh_Ptext1);  //"assets/torch.dds"
	
	ID3DXBuffer* errors = 0;
	D3DXCreateEffectFromFile(gd3dDevice, "Assets/Shaders/Particle.fx",0, 0, D3DXSHADER_DEBUG, 0, &fx, &errors);
	if( errors )
		MessageBox(0, (LPCSTR)errors->GetBufferPointer(), 0, 0);

	mhWVP				= fx->GetParameterByName(0, "gWVP");
	mhEyePosL			= fx->GetParameterByName(0, "gEyePosL");
	mhTex				= fx->GetParameterByName(0, "gTex");
	mhTime				= fx->GetParameterByName(0, "gTime");
	mhAccel				= fx->GetParameterByName(0, "gAccel");
	mhViewportHeight	= fx->GetParameterByName(0, "gViewportHeight");
	mhTech				= fx->GetTechniqueByName("SprinklerTech"); 
	//gh_Peffect1->SetTexture(mhTex, gh_Ptext1);

	fx->SetTechnique(mhTech);
}

void ParticleManager::LoadParticleEffects()
{
	JSONParser j1;
	j1.OpenFile("Assets/Particles/EmitterTypes/ParticleList.json");

	JSONObject object = j1.GetParsedObject();

	for(auto it: object.m_arrays["ParticleEffects"].m_values)
	{
		Vector3 vec3;

		//<WARNING>
		ParticleAttributes temp;
		temp.m_Type					= atoi(it.m_variables["type"].c_str());
		temp.m_AccelerationMax		= vec3.FromString(it.m_variables["acceleration"]);
		temp.m_Color				= vec3.FromString(it.m_variables["initialColor"]);
		temp.m_LifeTimeMax			= (float)atof(it.m_variables["lifeTime"].c_str());
		temp.m_MassMax				= (float)atof(it.m_variables["mass"].c_str());
		temp.m_MaxParticle			= atoi(it.m_variables["maxParticles"].c_str());
		temp.m_SizeMax				= (float)atof(it.m_variables["initialSize"].c_str());
		temp.m_TimePerParticle		= (float)atof(it.m_variables["timePerParticle"].c_str());
		temp.m_VelocityMax			= vec3.FromString(it.m_variables["initialVelocity"]);
		temp.m_TechName				= it.m_variables["techniqueName"];
		temp.m_Texture				= it.m_variables["texture"];
		temp.m_Name					= it.m_variables["name"];
		this->m_ParticleMap.insert(std::make_pair(temp.m_Name, temp));

		auto result = m_Textures.find(temp.m_Texture);
		if(result != m_Textures.end()) 
			continue;

		LPDIRECT3DTEXTURE9 tempTexture = NULL;
		D3DXCreateTextureFromFile(gd3dDevice, temp.m_Texture.c_str(), &tempTexture);
		m_Textures.insert(make_pair(temp.m_Texture, tempTexture));
	}
}

void ParticleManager::onLostDevice()
{
	fx->OnLostDevice();

	for(auto emitter : m_ParticleEmitters)
		emitter->onLostDevice();
}

void ParticleManager::onResetDevice()
{
	fx->OnResetDevice();

	for(auto emitter : m_ParticleEmitters)
		emitter->onResetDevice();
}

void ParticleManager::UpdateTimers(float dt)
{
	for(int i = m_TimedEmitters.size() - 1;
		i >= 0;
		--i)
	{
		m_TimedEmitters.at(i).first -= dt;

		if(m_TimedEmitters.at(i).first <= 0.0f)
		{
			RemoveEmitter(m_TimedEmitters.at(i).second);

			m_TimedEmitters.erase(m_TimedEmitters.begin() + i);
		}
	}
}

void ParticleManager::update(float dt)
{
	UpdateTimers(dt);

	for(auto emitter : m_ParticleEmitters)
	{
		emitter->update(dt);
	}
}

void ParticleManager::Render()
{
	D3DXVECTOR3 eyePosW = gCamera->pos();
	D3DXVECTOR3 eyePosL;

	RECT clientRect;
	GetClientRect(gd3dApp->getMainWnd(), &clientRect);

	UINT numPasses = 0;
	fx->Begin(&numPasses, 0);
	fx->BeginPass(0);

	for(auto emitter : m_ParticleEmitters)
	{
		//change variables
		fx->SetTechnique(emitter->m_Tech);
		fx->SetTexture(mhTex, emitter->m_Tex);

		D3DXVec3TransformCoord(&eyePosL, &eyePosW, &emitter->m_InvWorld);

		fx->SetValue(mhEyePosL, &eyePosL, sizeof(D3DXVECTOR3));
		fx->SetFloat(mhTime, emitter->m_Time);
		fx->SetMatrix(mhWVP,&(emitter->m_World * Matrix4(gCamera->viewProj())));
		fx->SetValue(mhAccel, emitter->m_Accel, sizeof(D3DXVECTOR3));

		fx->SetInt(mhViewportHeight, clientRect.bottom);
		
		fx->CommitChanges();

		//render
		emitter->Render();
	}

	fx->EndPass();
	fx->End();
}

void ParticleManager::RemoveEmitter(ParticleEmitter *emitter)
{
	for(auto it = m_ParticleEmitters.begin();
		it != m_ParticleEmitters.end();
		it++)
	{
		if((*it) == emitter)
		{
			delete *it;
			m_ParticleEmitters.erase(it);
			return;
		}
	}
}

ParticleEmitter* ParticleManager::CreateEmitter(std::string name, Vector3 pos, float duration)
{
	ParticleAttributes attb = m_ParticleMap.find(name)->second;

	D3DXCOLOR particleColor(attb.m_Color.x, attb.m_Color.y, attb.m_Color.z, 0);
	
	D3DXHANDLE tech = fx->GetTechniqueByName((LPCSTR)(attb.m_TechName.c_str()));

	AABB psysBox; 
	psysBox.maxPt = D3DXVECTOR3(m_Infinity, m_Infinity, m_Infinity);
	psysBox.minPt = D3DXVECTOR3(-m_Infinity, -m_Infinity, -m_Infinity);

	D3DXMATRIX psysWorld;
	D3DXMatrixTranslation(&psysWorld, 0.0f,0.0f,0.0f);

	ParticleEmitter* emitter = new ParticleEmitter(pos, 
												   attb.m_VelocityMax,
												   attb.m_SizeMax, 
												   attb.m_LifeTimeMax, 
												   attb.m_MassMax, 
												   particleColor, 
												   tech, 
												   attb.m_Color, 
												   attb.m_AccelerationMax, 
												   psysBox, 
												   attb.m_MaxParticle, 
												   attb.m_TimePerParticle, 
												   attb.m_Type);

	emitter->m_Tex = m_Textures.at(attb.m_Texture);

	m_ParticleEmitters.push_back(emitter);

	if(duration != 0.0f)
		m_TimedEmitters.push_back(make_pair(duration, emitter));
	

	return emitter;
}
#include "ParticleEmitter.h"
#include "../../FRAMEWORK/Utility/Camera.h"


ParticleEmitter::ParticleEmitter(const D3DXVECTOR3& accel, const AABB& box,int maxNumParticles,float timePerParticle): m_Accel(accel), m_Box(box), m_Time(0.0f),m_MaxNumParticles(maxNumParticles), m_TimePerParticle(timePerParticle)
{
	m_Particles.resize(maxNumParticles);
	//m_AliveParticles.resize(maxNumParticles);
	//m_DeadParticles.resize(maxNumParticles);

	for(int i = 0; i<maxNumParticles; ++i)
	{
		m_Particles[i].lifeTime = -1.0f;
		m_Particles[i].initialTime = 0.0f;
	}

	m_World		= Matrix4(Matrix4::g_Identity);
	m_InvWorld	 = Matrix4(Matrix4::g_Identity);

	m_Position = Vector3(0.0f, 0.0f, 0.0f);

	m_Yaw = 0.0f;
	m_Pitch = 0.0f;
	m_Roll = 0.0f;

	//D3DXMatrixIdentity(&m_World);
	//D3DXMatrixIdentity(&m_InvWorld);
	//Gfx_M->createP();
	
	gd3dDevice->CreateVertexBuffer(m_MaxNumParticles*sizeof(Particle),D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,0, D3DPOOL_DEFAULT, &m_VB, 0);
}

ParticleEmitter::ParticleEmitter(D3DXVECTOR3 p_pos, D3DXVECTOR3 p_vel, float p_size, float p_life, float p_mass, D3DCOLOR p_color, D3DXHANDLE tech,D3DXVECTOR3 c, const D3DXVECTOR3& accel, const AABB& box,int maxNumParticles,float timePerParticle, int type): m_Accel(accel), m_Box(box), m_Time(0.0f),m_MaxNumParticles(maxNumParticles), m_TimePerParticle(timePerParticle)
{
	m_Effect_Type = type;
	m_Particle_Pos = p_pos;
	m_Particle_Vel = p_vel;
	m_Particle_Size = p_size;
	m_Particle_LTime = p_life;
	m_Particle_Mass = p_mass;
	m_Particle_Color = p_color;
	m_Tech = tech;
	m_Particle_Color2 = c;

	m_Particles.resize(maxNumParticles);
	//m_AliveParticles.resize(maxNumParticles);
	//m_DeadParticles.resize(maxNumParticles);

	for(int i = 0; i<maxNumParticles; ++i)
	{
		m_Particles[i].lifeTime = -1.0f;
		m_Particles[i].initialTime = 0.0f;
	}

	m_World		= Matrix4(Matrix4::g_Identity);
	m_InvWorld	 = Matrix4(Matrix4::g_Identity);

	m_Position = Vector3(0.0f, 0.0f, 0.0f);

	m_Yaw = 0.0f;
	m_Pitch = 0.0f;
	m_Roll = 0.0f;

	//D3DXMatrixIdentity(&m_World);
	//D3DXMatrixIdentity(&m_InvWorld);
	//Gfx_M->createP();
	
	

	gd3dDevice->CreateVertexBuffer(m_MaxNumParticles*sizeof(Particle),D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,0, D3DPOOL_DEFAULT, &m_VB, 0);
}

ParticleEmitter::~ParticleEmitter()
{
	//release vertex buffer
	ReleaseCOM(m_VB);
}

void ParticleEmitter::initParticle(Particle& out)
{
	if(m_Effect_Type == 1)
	{
		out.initialTime			= m_Time;
		out.lifeTime			= GetRandomFloat((m_Particle_LTime*0.70f), (m_Particle_LTime*1.30f));
		out.initialSize			= GetRandomFloat((m_Particle_Size*0.75f), (m_Particle_Size*1.20f));
		GetRandomVec(out.initialVelocity);
		out.initialVelocity		= 130.0f * out.initialVelocity;
		out.mass				= GetRandomFloat((m_Particle_Mass*0.8f),(m_Particle_Mass*1.2f));
		out.initialColor		= (D3DCOLOR)(GetRandomFloat(0.5f, 1.0f)*m_Particle_Color);
		out.initialPos.x		= GetRandomFloat((m_Particle_Pos.x - 2.0f), (m_Particle_Pos.x +2.0f));
		out.initialPos.y		= m_Particle_Pos.y;
		out.initialPos.z		= GetRandomFloat((m_Particle_Pos.z - 2.0f), (m_Particle_Pos.z +2.0f));
	}else
	if(m_Effect_Type == 2)
	{
		out.initialPos = m_Particle_Pos;
		float speed = 100.0f;
		out.initialVelocity = speed* m_Particle_Vel;
		out.initialTime     = m_Time;
		out.lifeTime        = m_Particle_LTime;
		out.initialColor    = m_Particle_Color;
		out.initialSize     = m_Particle_Size;
		out.mass            = m_Particle_Mass;
	}else
	if(m_Effect_Type == 3)
	{
		out.initialPos = m_Particle_Pos;
		out.initialTime = m_Time;
		out.lifeTime = GetRandomFloat((m_Particle_LTime*0.85f), (m_Particle_LTime*1.15f));
		out.initialColor = m_Particle_Color;
		out.mass = GetRandomFloat((m_Particle_Mass*0.8f),(m_Particle_Mass*1.2f));
		out.initialSize =  GetRandomFloat((m_Particle_Size*0.8f), (m_Particle_Size*1.2f));
		out.initialVelocity.x = GetRandomFloat(-m_Particle_Vel.x,  m_Particle_Vel.x);
		out.initialVelocity.z = GetRandomFloat(-m_Particle_Vel.z,  m_Particle_Vel.z);
		out.initialVelocity.y = GetRandomFloat((m_Particle_Vel.y*0.75f),  (m_Particle_Vel.y*1.25f));
	}else
	if(m_Effect_Type == 4)
	{
		out.initialTime			= m_Time;
		out.lifeTime			= GetRandomFloat((m_Particle_LTime*0.70f), (m_Particle_LTime*1.30f));
		out.initialSize			= GetRandomFloat((m_Particle_Size*0.80f), (m_Particle_Size*1.25f));
		GetRandomVec(out.initialVelocity);
		out.initialVelocity		= 60.0f * out.initialVelocity;
		out.mass				= GetRandomFloat((m_Particle_Mass*0.8f),(m_Particle_Mass*1.2f));
		out.initialColor		= m_Particle_Color;
		out.initialPos.x		= GetRandomFloat((m_Particle_Pos.x - 2.0f), (m_Particle_Pos.x +2.0f));
		out.initialPos.y		= m_Particle_Pos.y;
		out.initialPos.z		= GetRandomFloat((m_Particle_Pos.z - 2.0f), (m_Particle_Pos.z +2.0f));
	}else
	if(m_Effect_Type == 5)
	{
		out.initialTime			= m_Time;
		out.lifeTime			= GetRandomFloat((m_Particle_LTime*0.70f), (m_Particle_LTime*1.30f));
		out.initialSize			= GetRandomFloat((m_Particle_Size*0.15f), (m_Particle_Size*1.50f));
		GetRandomVec(out.initialVelocity);
		out.initialVelocity		= 50.0f * out.initialVelocity;
		out.mass				= GetRandomFloat((m_Particle_Mass*0.8f),(m_Particle_Mass*1.2f));
		out.initialColor		= (D3DCOLOR)(GetRandomFloat(0.5f, 1.0f)*m_Particle_Color);
		out.initialPos.x		= GetRandomFloat((m_Particle_Pos.x - 2.0f), (m_Particle_Pos.x +2.0f));
		out.initialPos.y		= m_Particle_Pos.y;
		out.initialPos.z		= GetRandomFloat((m_Particle_Pos.z - 2.0f), (m_Particle_Pos.z +2.0f));
	}
	
}

void ParticleEmitter::onLostDevice()
{
	ReleaseCOM(m_VB);
}

void ParticleEmitter::onResetDevice()
{
	if(m_VB == 0)
	{
		gd3dDevice->CreateVertexBuffer(m_MaxNumParticles*sizeof(Particle),D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,0, D3DPOOL_DEFAULT, &m_VB, 0);
	}
}

void ParticleEmitter::update(float dt)
{
	m_Time += dt;

	Matrix4 R(Matrix4::g_Identity), T(Matrix4::g_Identity);
	
	R.BuildYawPitchROll(m_Yaw, m_Pitch, m_Roll);
	T.BuildTranslation(m_Position);
	m_World = R * T;
	m_InvWorld = m_World.Inverse();

	// Rebuild the dead and alive list.  Note that resize(0) does
	// not deallocate memory (i.e., the capacity of the vector does
	// not change).
	m_DeadParticles.resize(0);
	m_AliveParticles.resize(0);

	// For each particle.
	for(int i = 0; i < m_MaxNumParticles; ++i)
	{
		// Is the particle dead?
  		if( (m_Time - m_Particles[i].initialTime) > m_Particles[i].lifeTime)
		{
			m_DeadParticles.push_back(&m_Particles[i]);
			
		}
		else
		{
			if(m_Effect_Type == 5)
				m_Particles[i].initialSize *= 0.99f;
			m_AliveParticles.push_back(&m_Particles[i]);
		}
	}
	
	// A negative or zero m_TimePerParticle value denotes
	// not to emit any particles.
	if( m_TimePerParticle > 0.0f )
	{
		// Emit particles.
		static float timeAccum = 0.0f;
		timeAccum += dt;
		while( timeAccum >= m_TimePerParticle )
		{
			addParticle();
			timeAccum -= m_TimePerParticle;
		}
	}
}

void ParticleEmitter::Render()
{
	gd3dDevice->SetStreamSource(0, m_VB, 0, sizeof(Particle));
	gd3dDevice->SetVertexDeclaration(Particle::Decl);

	AABB boxWorld;
	m_Box.xform(m_World, boxWorld);
	if(isVisable(boxWorld))
	{
		Particle* p = 0;
		m_VB->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD);
		int vbIndex = 0;
		for(UINT i = 0; i < m_AliveParticles.size(); ++i)
		{
			// Copy particle to VB
			p[vbIndex] = *m_AliveParticles[i];
			++vbIndex;
		}
		m_VB->Unlock();
		if(vbIndex > 0)
		{
			gd3dDevice->DrawPrimitive(D3DPT_POINTLIST, 0, vbIndex);
		}
	}
}

void ParticleEmitter::addParticle()
{
	if(m_DeadParticles.size() > 0)
	{
		// Reinitialize a particle.
		Particle* p = m_DeadParticles.back();
		initParticle(*p);

		// move to alive
		m_DeadParticles.pop_back();
		m_AliveParticles.push_back(p);
	}
}

void ParticleEmitter::setTime(float t)
{
	m_Time = t;
}

const AABB& ParticleEmitter::getAABB() const
{
	return m_Box;
}

bool ParticleEmitter::isVisable(const AABB& box)const
{
	D3DXVECTOR3 P;
	D3DXVECTOR3 Q;
	D3DXPLANE* mFrustumPlanes = gCamera->getFPlane(0);

	for(int i = 0; i < 6; ++i)
	{
		// For each coordinate axis x, y, z...
		for(int j = 0; j < 3; ++j)
		{
			// Make PQ point in the same direction as the plane normal on this axis.
			if( mFrustumPlanes[i][j] >= 0.0f )
			{
				P[j] = box.minPt[j];
				Q[j] = box.maxPt[j];
			}
			else 
			{
				P[j] = box.maxPt[j];
				Q[j] = box.minPt[j];
			}
		}

		// If box is in negative half space, it is behind the plane, and thus, completely
		// outside the frustum.  Note that because PQ points roughly in the direction of the 
		// plane normal, we can deduce that if Q is outside then P is also outside--thus we
		// only need to test Q.
		if( D3DXPlaneDotCoord(&mFrustumPlanes[i], &Q) < 0.0f  )
			return false;
	}
	return true;
}

float ParticleEmitter::GetRandomFloat(float a, float b)
{
	if( a >= b ) // bad input
		return a;
	// Get random float in [0, 1] interval.
	float f = (rand()%10001) * 0.0001f;
		return (f*(b-a))+a;
}

void ParticleEmitter::GetRandomVec(D3DXVECTOR3& out)
{
	out.x = GetRandomFloat(-1.0f, 1.0f);
	out.y = GetRandomFloat(-1.0f, 1.0f);
	out.z = GetRandomFloat(-1.0f, 1.0f);

	// Project onto unit sphere.
	D3DXVec3Normalize(&out, &out);
}

float ParticleEmitter::getTime()
{
	return m_Time;
}
#pragma once
#include "ParticleEmitter.h"

struct ParticleAttributes
{
	int				m_Type;
	int				m_MaxParticle;
	float			m_SizeMax;
	float			m_LifeTimeMax;
	float			m_MassMax;
	float			m_TimePerParticle;
	D3DXVECTOR3		m_VelocityMax;
	D3DXVECTOR3		m_AccelerationMax;
	D3DXVECTOR3		m_Color;
	std::string		m_TechName;
	std::string		m_Texture;
	std::string		m_Name;
};


class ParticleManager
{
public:
	ParticleManager();
	~ParticleManager();

	std::map<std::string, ParticleAttributes>			m_ParticleMap;
	std::map<std::string, LPDIRECT3DTEXTURE9>			m_Textures;
	std::vector<ParticleEmitter*>						m_ParticleEmitters;
	std::vector<std::pair<float, ParticleEmitter*>>		m_TimedEmitters;

	bool				m_Active;
	float				m_Infinity;

	ID3DXEffect *fx;

	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhEyePosL;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhTime;
	D3DXHANDLE mhAccel;
	D3DXHANDLE mhViewportHeight;

public:

	void				BuildFX();
	void				LoadParticleEffects();

	void				onLostDevice();
	void				onResetDevice();

	void				UpdateTimers(float dt);
	void				update(float dt);
	void				Render();

	void				RemoveEmitter(ParticleEmitter *emitter);
	ParticleEmitter*	CreateEmitter(std::string name, Vector3 pos = Vector3(0.0f, 0.0f, 0.0f), float duration = 0.0f);
};

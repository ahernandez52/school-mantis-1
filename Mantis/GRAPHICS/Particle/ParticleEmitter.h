#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "AABB.h"
#include "../Utility/D3D9Vertex.h"

class ParticleEmitter
{
public:

	D3DXHANDLE				m_Tech;
	IDirect3DTexture9*		m_Tex;
	IDirect3DVertexBuffer9* m_VB;
	AABB					m_Box;
	D3DCOLOR				m_Particle_Color;

	//D3DXMatrixs
	Matrix4					m_World;
	Matrix4					m_InvWorld;
	Vector3					m_Position;

	//D3DXvectors
	D3DXVECTOR3				m_Accel;
	D3DXVECTOR3				m_Particle_Color2;
	D3DXVECTOR3				m_Particle_Pos;
	D3DXVECTOR3				m_Particle_Vel;

	//ints
	int 					m_Effect_Type;
	int						m_MaxNumParticles;
	
	//floats
	float					m_TimePerParticle;
	float					m_Time;
	float					m_Particle_Size;
	float					m_Particle_LTime;
	float					m_Particle_Mass;

	float					m_Yaw;
	float					m_Pitch;
	float					m_Roll;	
	float					m_Speed;
	

	//vectors 
	std::vector<Particle>	m_Particles;
	std::vector<Particle*>	m_AliveParticles;
	std::vector<Particle*>	m_DeadParticles;

	ParticleEmitter(){}	
	ParticleEmitter(const D3DXVECTOR3& accel, const AABB& box,int maxNumParticles,float timePerParticle);

	ParticleEmitter(D3DXVECTOR3 p_pos, 
					D3DXVECTOR3 p_vel, 
					float p_size, 
					float p_life, 
					float p_mass, 
					D3DCOLOR p_color, 
					D3DXHANDLE tech,
					D3DXVECTOR3 c, 
					const D3DXVECTOR3& accel, 
					const AABB& box,
					int maxNumParticles,
					float timePerParticle,
					int type);

	~ParticleEmitter();

	void			initParticle(Particle& out);

	void			onLostDevice();
	void			onResetDevice();

	void			update(float dt);
	void			Render();

	void			addParticle();
	void			setTime(float t);
	const AABB&		getAABB() const;

	bool			isVisable(const AABB& box)const;
	void			GetRandomVec(D3DXVECTOR3& out);
	float			GetRandomFloat(float a, float b);
	float			getTime();
};

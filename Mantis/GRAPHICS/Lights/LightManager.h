#pragma once
#include "../../FRAMEWORK/Utility/MantisUtil.h"

class LightManager
{
private:

	std::map<std::string, Light>	m_LightsLookup;
	std::vector<Light>				m_LightVec;
	
private:


public:

	LightManager();
	~LightManager();

};
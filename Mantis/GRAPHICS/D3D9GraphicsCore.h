#pragma once
#include "Shader\D3D9ShaderManager.h"
#include "Particle\ParticleManager.h"
#include "Lights\LightManager.h"
#include "Buffer\D3D9Buffer.h"
#include "Buffer\D3D9Viewport.h"
#include <vector>

//Defines for render texture dimensions
#define QUAD_W 800
#define QUAD_H 600

class GfxStats;
class D3D9MantisSkyBox;
class D3D9Quad;
class MantisGUI;

enum RenderFlag
{
	GFX_NO_BLUR = 0,
	GFX_BLUR,
};

class D3D9GraphicsCore
{
private:
	LPDIRECT3DDEVICE9	m_Device;

	LightManager		*m_LightManager;
	D3D9ShaderManager	*m_ShaderManager;
	D3D9ShaderEffect	*m_CurrentShader;
	GfxStats			*m_GfxStats;
	D3D9MantisSkyBox	*m_SkyBox;

	

	bool				m_gfxStatsDisplay;
	bool				m_skyBoxRender;

	D3DCOLOR			m_DrawColor;

	LPDIRECT3DSURFACE9  m_pBackBuffer;			//Back Buffer Pointer
	D3D9Quad*		    m_oQuad;

	D3D9Buffer*			m_pRenderBuffer;		//Buffer for render to quad
	D3D9Buffer*			m_pPerlinBuffer;		//Perlin noise buffer
	D3D9Buffer*			m_pIlluminationBuffer;  //Scene Illumination buffer
	


	LPDIRECT3DTEXTURE9  m_pPerlinTex;
	//Lighting Defines (temporary)
	DirectionalLight    m_lLight;
	
	
	

private:

	void BuildFX();
	void BasicRender();
	
	//Core Plugin Wrappers
	void				ParticleInit();
	void				ParticleUpdate(float dt);
	void				ParticleRender();
	void				ParticleOnLost();
	void				ParticleOnReset();

	void GUIInit();
	void GUIRender();
	void GUIOnLost();
	void GUIOnReset();
	void GUIDestroy();
	void GUIPostRender();

public:
	//Switching
	bool				m_bTextures;
	bool				m_bAnisotropic;
	int					m_iBufferShow;
	float				m_fElapsedTime;
	D3DXVECTOR2			m_vIntensity;
	bool				m_bViewportShow;
	std::vector<D3D9Viewport*>	m_vViewport;
	D3DVIEWPORT9 m_pViewport;
	float				m_fAlphaLevel;
	
	//Public Managers
	ParticleManager			*m_ParticleMan;
public:

	D3D9GraphicsCore(LPDIRECT3DDEVICE9 device);
	~D3D9GraphicsCore();
	void SetRenderFlag(int pFlag);

	void Update(float dt);
	void Render(D3DXMATRIX View, D3DXMATRIX Proj, D3DXVECTOR3 CameraPosition);

	void OnLostDevice();
	void OnResetDevice();

	//Interfacing Functions
	void ReloadShaders();
	void RegisterNewParticleEffect();
	void RegisterNewLight();
	void SetSkyBoxRender(bool value);
	void SetDrawColor(int r, int g, int b);
	void SetGfxStatsRender(bool value);
	float GetFPS();

	ParticleEmitter*	CreateEmitter(std::string emitterType, Vector3 pos = Vector3(0.0f, 0.0f, 0.0f), float duration = 0.0f);
	void				DeleteEmitter(ParticleEmitter* emitter);
	void				ReloadParticles();
};
uniform extern float4x4 gWVP;
uniform extern texture  gTex;
uniform extern float3   gEyePosL;    //camera position
uniform extern float3   gAccel;
uniform extern float    gTime;
uniform extern int      gViewportHeight;

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = POINT;
	AddressU  = CLAMP;
    AddressV  = CLAMP;
};
 
struct OutputVS
{
    float4 posH  : POSITION0;
    float4 color : COLOR0;
    float2 tex0  : TEXCOORD0; // D3D fills in for point sprites.
    float size   : PSIZE; // In pixels.
};

OutputVS FireVS(float3 posL    : POSITION0, 
                    float3 vel     : TEXCOORD0,
                    float size     : TEXCOORD1,
                    float time     : TEXCOORD2,
                    float lifeTime : TEXCOORD3,
                    float mass     : TEXCOORD4,
                    float4 color   : COLOR0)
{
	OutputVS outVS = (OutputVS)0;
	
	float t = gTime - time;
	
	posL = posL + vel*t + 0.5f * gAccel * t * t;
	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
		
	size += 8.0f*t*t;
	
	float d = distance(posL, gEyePosL);
	outVS.size = gViewportHeight*size/(1.0f + 8.0f*d);
	
	outVS.color = (1.0f - (t / lifeTime));
	
    return outVS;
}

float4 FirePS(float4 color : COLOR0, float2 tex0 : TEXCOORD0) : COLOR
{
	return color*tex2D(TexS, tex0);
}

technique FireTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 FireVS();
        pixelShader  = compile ps_2_0 FirePS();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	    SrcBlend     = One;
	    DestBlend    = One;
	    	    
	    ZWriteEnable = false;
    }
}

OutputVS SprinklerVS(float3 posL    : POSITION0, 
                     float3 vel     : TEXCOORD0,
                     float size     : TEXCOORD1,
                     float time     : TEXCOORD2,
                     float lifeTime : TEXCOORD3,
                     float mass     : TEXCOORD4,
                     float4 color   : COLOR0)
{
	OutputVS outVS = (OutputVS)0;
	
	float t = gTime - time;
	
	posL = posL + vel*t + 0.5f * gAccel * t * t;
	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
		

	float d = distance(posL, gEyePosL);
	outVS.size = gViewportHeight*size/(1.0f + 8.0f*d);

	outVS.color = color;

    return outVS;
}

float4 SprinklerPS(float4 color : COLOR0, float2 tex0 : TEXCOORD0) : COLOR
{
	return color*tex2D(TexS, tex0);
}

technique SprinklerTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 SprinklerVS();
        pixelShader  = compile ps_2_0 SprinklerPS();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	    SrcBlend     = One;
	    DestBlend    = One;
	    
	    
	    
	    // Don't write to depth buffer; that is, particles don't obscure
	    // the objects behind them.
	    ZWriteEnable = false;
    }
}


OutputVS GunVS(float3 posL    : POSITION0, 
                     float3 vel     : TEXCOORD0,
                     float size     : TEXCOORD1,
                     float time     : TEXCOORD2,
                     float lifeTime : TEXCOORD3,
                     float mass     : TEXCOORD4,
                     float4 color   : COLOR0)
{
	OutputVS outVS = (OutputVS)0;
	
	float t = gTime - time;
	
	posL = posL + vel*t + 0.5f * gAccel * t * t;
	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
		

	float d = distance(posL, gEyePosL);
	outVS.size = gViewportHeight*size/(1.0f + 8.0f*d);
	

    return outVS;;
}

float4 GunPS(float2 tex0 : TEXCOORD0) : COLOR
{
	return tex2D(TexS, tex0);
}

technique GunTech
{
    pass P0
    {
        vertexShader = compile vs_2_0 GunVS();
        pixelShader  = compile ps_2_0 GunPS();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	    SrcBlend     = One;
	    DestBlend    = One;
	    
	    ZWriteEnable = false;
    }
}
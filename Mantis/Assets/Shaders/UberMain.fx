//Mantis Graphics Core - Ubershader One
//V 1.0
//05-23-13 - Initial Creation

//=============================================================================
//Begin Defines
//=============================================================================
float2 FlareSamples[12]
=	{
		-0.326212, -0.405805,
		-0.840144, -0.073580,
		-0.695914,  0.457137,
		-0.203345,  0.620716,
		 0.962340, -0.194983,
		 0.473434, -0.480026,
		 0.519456,  0.767022,
		 0.185461, -0.893124,
		 0.507431,  0.064425,
		 0.896420,  0.412458,
		-0.321940, -0.932615,
		-0.791559, -0.597705
	};
//=============================================================================
//End Defines
//=============================================================================

//=============================================================================
//Begin Structs
//=============================================================================
struct VS_OUT_POST
{
	float2 tex0    : TEXCOORD0;
    float4 posH	   : POSITION0;
};

struct VS_OUT
{
    float4 posH    : POSITION0;
    float3 normalW : TEXCOORD0;
    float3 toEyeW  : TEXCOORD1;
    float2 tex0    : TEXCOORD2;
    float4 color   : COLOR0;
};

struct VS_IN
{
	float4 Position		: POSITION0;
	float3 Normal		: NORMAL0;
	float2 TexCoord		: TEXCOORD0;
	float4 Color		: COLOR0;
};

struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};

struct DirLight
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float3 dirW;  
};


//=============================================================================
//End Structs
//=============================================================================


//=============================================================================
//Begin Uniforms
//=============================================================================
uniform extern float4x4		gWorld;
uniform extern float4x4		gWorldInvTrans;
uniform extern float4x4		gWVP;
uniform extern texture		gTextureDiff;
uniform extern texture		gTextureNormal;
uniform extern texture		gTextureBump;
uniform extern texture		gTextureSpecular;
uniform extern texture		gTextureIllumination;
uniform extern texture		gPostBuffer;		//Render buffer for post process
uniform extern Mtrl			gMtrl;				//**
uniform extern DirLight		gLight;				//**
uniform extern float3		gEyePosW;
uniform extern float		gElapsedTime;		//Total time elapsed since core initialized

//Perlin Noise
uniform extern texture		gTexturePerlin;

//Stretch Shader
uniform extern bool			gShipStretch;
uniform extern bool			gShipReleaseStretch;
uniform extern float		gStretchStart;

//Mesh Effect Buffer Flags
uniform extern bool			gHasDiffuse;
uniform extern bool			gHasNormal;
uniform extern bool			gHasBump;
uniform extern bool		    gHasSpecular;
uniform extern bool			gHasIllumination;

uniform extern bool			gDiffuseOn;			//Diffuse Texture Enabled
uniform extern bool			gNormalOn;
uniform extern bool			gBumpOn;
uniform extern bool			gSpecOn;
uniform extern bool			gIllOn;

uniform extern int			gShowBuffer;
uniform extern bool			gAniOn;				//Anisotropic Filtering Enabled

uniform extern bool			gIsSun;

uniform extern float		gAlphaLevel;

//Shield Effect
uniform extern float3		gShieldColor;
uniform extern float		gShieldSize;

//Physics Uniforms
uniform extern float3		gContact;
uniform extern int			gContactCount;

//Post Processing Buffers/Flags
uniform extern texture		gBufferIllumination;
uniform extern float2		gIlluminationFactor;

//=============================================================================
//End Uniforms
//=============================================================================

//=============================================================================
//Begin Samplers
//=============================================================================
sampler DiffuseAniS = sampler_state
{
	Texture = <gTextureDiff>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//Texture/Diffuse Map
sampler DiffuseS = sampler_state
{
	Texture = <gTextureDiff>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//Normal Map
sampler NormalS = sampler_state
{
	Texture = <gTextureNormal>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//Bump Map
sampler BumpS = sampler_state
{
	Texture = <gTextureBump>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//Specular Map
sampler SpecularS = sampler_state
{
	Texture = <gTextureSpecular>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//Illumination Map
sampler IlluminationS = sampler_state
{
	Texture = <gTextureIllumination>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

//2D Perlin Noise
sampler Perlin2DS = sampler_state
{
	Texture = <gTexturePerlin>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

sampler IllumBufferS = sampler_state
{
	Texture = <gBufferIllumination>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = CLAMP;
   	AddressV  = CLAMP;
};

sampler PostAniS = sampler_state
{
	Texture = <gPostBuffer>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

sampler PostS = sampler_state
{
	Texture = <gPostBuffer>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};


//=============================================================================
//End Samplers
//=============================================================================

//=============================================================================
//Begin Functions
//=============================================================================
VS_OUT PhongDirLtTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{

    // Zero out our output.
	VS_OUT outVS = (VS_OUT)0;
	
	if (gIsSun)
	{
		float4 perlinTex = tex2Dlod(Perlin2DS, float4(tex0.x, tex0.y, 0, 0));
		float3 posScalar = normalL * (sin(gElapsedTime + (perlinTex.r * 10.0)) * 0.02);// * perlinTex.r;
		posL += posScalar;
		//tex0 += (gElapsedTime / 100.0f);
	}
	float timeRunning;
	timeRunning = gElapsedTime - gStretchStart;
	float ratio = timeRunning / 1.0f;
	if (ratio > 1.0f)
		ratio = 1.0f;

	if (gShipStretch)
	{
		float frontRatio = (ratio * 2.0) + 1.0;
		posL.x *= frontRatio;
		float sideRatio = ratio + 1.0;
		posL.z /= sideRatio;
		posL.y /= sideRatio;
	}
	if (gShipReleaseStretch)
	{
		ratio = 1.0 - ratio;
		float frontRatio = (ratio * 2.0) + 1.0;
		posL.x *= frontRatio;
		float sideRatio = ratio + 1.0;
		posL.z /= sideRatio;
		posL.y /= sideRatio;
	}
	// Transform normal to world space.
	outVS.normalW = mul(float4(normalL, 0.0f), gWorldInvTrans).xyz;
	
	// Transform vertex position to world space.
	float3 posW  = mul(float4(posL, 1.0f), gWorld).xyz;
	
	// Compute the unit vector from the vertex to the eye.
	outVS.toEyeW = gEyePosW - posW;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	
	// Pass on texture coordinates to be interpolated in rasterization.
	outVS.tex0 = tex0;

	// Done--return the output.
    return outVS;
}

float4 PhongDirLtTexPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	normalW = normalize(normalW);
	toEyeW  = normalize(toEyeW);
	
	// Light vector is opposite the direction of the light.
	float3 lightVecW = -gLight.dirW;
	
	// Compute the reflection vector.
	float3 r = reflect(-lightVecW, normalW);
	
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, toEyeW), 0.0f), gMtrl.specPower);
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(lightVecW, normalW), 0.0f);
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 spec = t*(gMtrl.spec*gLight.spec).rgb;
	float3 diffuse = s*(gMtrl.diffuse*gLight.diffuse).rgb;
	float3 ambient = gMtrl.ambient*gLight.ambient;
	int buffers = (int)gShowBuffer;
	float4 texColor;
	[branch] if  (buffers == 0)
	{
		texColor = tex2D(DiffuseAniS, tex0);
	}
	else if (buffers == 1)
	{
		texColor = tex2D(NormalS, tex0);
		texColor.x += 2.0f;
		texColor.y += 2.0f;
		texColor.z += 2.0f;

		texColor = mul(texColor, 0.5f);
	}
	else if (buffers == 2)
	{
		texColor = tex2D(BumpS, tex0);
		texColor.x += 2.0f;
		texColor.y += 2.0f;
		texColor.z += 2.0f;

		texColor = mul(texColor, 0.5f);
	}
	else if (buffers == 3)
	{
		texColor = tex2D(SpecularS, tex0);
		texColor.x += 2.0f;
		texColor.y += 2.0f;
		texColor.z += 2.0f;

		texColor = mul(texColor, 0.5f);
	}
	else if (buffers == 4)
	{
		texColor = tex2D(IlluminationS, tex0);
		texColor.x += 2.0f;
		texColor.y += 2.0f;
		texColor.z += 2.0f;

		texColor = mul(texColor, 0.5f);
	}
	else
		texColor = float4(1.0, 1.0, 1.0, 1.0);
	
	// Combine the color from lighting with the texture color.
	float3 color = (ambient + diffuse)*texColor.rgb + spec;
		
	// Sum all the terms together and copy over the diffuse alpha.
    return float4(color, gMtrl.diffuse.a*texColor.a);
}

float4 IlluminationBufferPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	float4 texColor = tex2D(IlluminationS, tex0);
	texColor.w = 0.0f;
	return texColor;
}

VS_OUT_POST PostVS(float3 posL : POSITION0, float2 tex0: TEXCOORD0)
{
     // Zero out our output.
	VS_OUT_POST outVS = (VS_OUT_POST)0;
    outVS.posH = mul(float4(posL,1.0), gWVP);
	outVS.tex0 = tex0;
    return outVS;
}

float4 PostPS(float2 tex0 : TEXCOORD0) : COLOR
{
	float4 illColor = tex2D(IllumBufferS, tex0);
	float4 texColor = tex2D(PostS, tex0);
	float4 Color;

	for (int i = 0; i < 12; i++)
	{
		illColor += tex2D(IllumBufferS, tex0 + ((FlareSamples[i] * 0.01)* gIlluminationFactor.x));//.xy + 0.01);//(FlareSamples[i].xy * 5.0));
	}
	illColor /= 13;
	illColor /= gIlluminationFactor.y;
	float4 finalColor = saturate(texColor * illColor) * 2.0;
	finalColor.w = 1.0f;
	//return float4(saturate(texColor));
    //return finalColor;
	//return texColor;
	//return illColor;
	//return tex2D(IllumBufferS, tex0);
	//return float4(saturate(tex2D(PostS, tex0) * illColor) * 2.0f);
	return float4((texColor + illColor) / 2.0);
}

float4 PostPSSimple(float2 tex0 : TEXCOORD0) : COLOR
{
	float4 texColor = tex2D(PostS, tex0);

	texColor.w = 1.0f;

	return texColor;
}

VS_OUT ShieldVS(float3 posL : POSITION0, float2 tex0: TEXCOORD0)
{
     // Zero out our output.
	VS_OUT outVS = (VS_OUT)0;
    outVS.posH = mul(float4(posL,1.0), gWVP);
	outVS.tex0 = tex0;
	float3 tempDiff = gContact - posL;
	//tempDiff.x = tempDiff.x - posL.x;
	tempDiff = abs(tempDiff);
	if (tempDiff.x < gShieldSize && tempDiff.y < gShieldSize && tempDiff.z < gShieldSize)// && tempDiff.y < gShieldSize && tempDiff.z < gShieldSize)
	{
		
		outVS.color = float4(gShieldColor.rgb, gAlphaLevel);
	}
	else
	{
		outVS.color = float4(1.0, 1.0, 1.0, 0.0);
	}

    return outVS;
}

float4 ShieldPS(float2 tex0 : TEXCOORD0, float4 color : COLOR0) : COLOR
{
	//float4 texColor = tex2D(DiffuseAniS, tex0);
	float4 texColor = color;

	//texColor.w = gAlphaLevel;

	return texColor;
}

//=============================================================================
//End Functions
//=============================================================================


//=============================================================================
//Begin Techniques
//=============================================================================

technique PhongDirLtTexTechFilled
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PhongDirLtTexVS();
        pixelShader  = compile ps_3_0 PhongDirLtTexPS();
    }
}

technique IlluminationBuffer
{
	pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PhongDirLtTexVS();
        pixelShader  = compile ps_3_0 IlluminationBufferPS();
    }
}

technique PostProcess
{
	pass P0
	{
		vertexShader = compile vs_3_0 PostVS();
        pixelShader  = compile ps_3_0 PostPS();
	}
}

technique PostSimple
{
	pass P0
	{
		vertexShader = compile vs_3_0 PostVS();
        pixelShader  = compile ps_3_0 PostPSSimple();
	}
}

technique ShieldAlpha
{
   pass P0
    {
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 ShieldVS();
        pixelShader  = compile ps_3_0 ShieldPS();
    }
}

//=============================================================================
//End Techniques
//=============================================================================

#include "Geometry.h"
#include <random>
#include <chrono>

const Matrix4 Matrix4::g_Identity(D3DXMATRIX(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1));
const Quaternion Quaternion::g_Identity(D3DXQUATERNION(0,0,0,1));

float RandomBinomial(void)
{
	std::default_random_engine generator;
	generator.seed((ULONG)time(NULL));
	std::binomial_distribution<int> distribution(100, 0.5);
	float x1 = (float)distribution(generator) / 100;
	float x2 = (float)distribution(generator) / 100;

	return x1 - x2;
}

float MapToPi(float rotation)
{
	if (rotation > D3DX_PI)
		return rotation - 2 * D3DX_PI;
	if (rotation < D3DX_PI)
		return rotation + 2* D3DX_PI;
	return rotation;
}


bool IntersectRaySphere(Vector3 point, Vector3 dir, Sphere s, float& t, Vector3& q)
{
	Vector3 m = point - s.m_Center;
	
	float b = D3DXVec3Dot(&m, &dir);
	float c = D3DXVec3Dot(&m, &m) - s.m_Radius * s.m_Radius;

	//Bail out if ray is outside sphere and pointing away
	if ( c > 0.0f && b > 0.0f)
	{
		return false;
	}

	//Negative discriminant correspondes to ray missing sphere 
	float discr = b*b - c;
	if (discr < 0.0f)
	{
		return false;
	}

	//Ray is intersecting
	t = -b - sqrt(discr);

	//If t is negative the ray started inside the sphere
	if (t < 0.0f)
	{
		t = 0.0f;
	}

	q = point + t * dir;

	return true;
}

void ClosestPointOnLine(Vector3 ref, Vector3 lineStart, Vector3 lineEnd, float& t, Vector3& point)
{
	Vector3 ab = lineEnd - lineStart;

	//Project c onto ab
	if (t <= 0.0f)
	{
		// c projects outside [a,b] on the a side
		t = 0.0f;
		point = lineStart;
	}
	else
	{
		float denom = D3DXVec3Dot(&ab, &ab);

		if (t >= denom)
		{
			//Something went terrible wrong
		}

		else
		{
			t = t / denom;
			point = lineStart + t * ab;
		}
	}
}

Vector3 WorldToLocal(Vector3 obj, Vector3 axis)
{
	Matrix4 objTransform;
	D3DXMatrixTranslation(&objTransform, obj.x, obj.y, obj.z);
	D3DXMatrixInverse(&objTransform, 0, &objTransform);

	Vector3 result;
	Matrix4 axisZ, worldTransform;

	D3DXMatrixTranslation(&axisZ, axis.x, axis.y, axis.z);

	worldTransform = objTransform * axisZ;

	D3DXVec3TransformCoord(&result, &obj, &worldTransform);

	return result;
}

Vector3 TransformAtoB(Vector3 posA, Vector3 posB)
{
	Vector3 newB;
	Matrix4  invA, b;

	D3DXMatrixTranslation(&invA, posA.x, posA.y, posA.z);
	D3DXMatrixTranslation(&b, posB.x, posB.y, posB.z);
	D3DXMatrixInverse(&invA, 0, &invA);

	b = invA * b;

	D3DXVec3TransformCoord(&newB, &posB, &b);

	return newB;
}

Vector3 TransformQuaternion(Vector3 vector, D3DXQUATERNION quat)
{
	//Convert the vector into a quaternion
	D3DXQUATERNION vectorAsQuat = D3DXQUATERNION(vector.x, vector.y, vector.z, 0);

	//Transform it
	vectorAsQuat = quat * vectorAsQuat * -quat;

	//Return transformed vector
	return Vector3(vectorAsQuat.x, vectorAsQuat.y, vectorAsQuat.z);
}
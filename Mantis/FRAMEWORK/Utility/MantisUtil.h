#pragma once

//Enable console header if debug is enabled
//#if defined(DEBUG) | defined(_DEBUG)

#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <iostream>

//#endif



//Containers
#include <vector>
#include <map>
#include <list>

//Project Includes
#include "Schematics.h"
#include <DxErr.h>


// Globals for convenient access.
//===============================================================
//Type Defines
typedef D3DXCOLOR Color;

//Forward Declarations
class D3DApp;
class Actor;

//External Pointers
extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;

extern Color g_White;
extern Color g_Black;
extern Color g_Cyan;
extern Color g_Red;
extern Color g_Green;
extern Color g_Blue;
extern Color g_Yellow;
extern Color g_Gray40;
extern Color g_Gray25;
extern Color g_Gray65;
extern Color g_Transparent;

extern Vector3 g_Up;
extern Vector3 g_Right;
extern Vector3 g_Forward;

extern Vector4 g_Up4;
extern Vector4 g_Right4;
extern Vector4 g_Forward4;

extern const float fOPAQUE;
extern const int iOPAQUE;
extern const float fTRANSPARENT;
extern const int iTRANSPARENT;
//***************************************************************

//Enumerations
//===============================================================
enum LightType{
	LIGHT_DIRECTION	= 0,
	LIGHT_POINT		= 1
};

enum MeshDetails{
	MAP_DIFFUSE = 0,
	MAP_NORMAL = 1,
	MAP_BUMP = 2,
	MAP_SPECULAR = 3,
	MAP_ILLUMINATION = 4,
};

enum GAMEPLAY_TYPE
{
	NO_TYPE = -1,
	PLAYER,
	PROJECTILE,
	SHIP,
	NPC,
	TRADER,
	FREIGHTER,
	WARP_PORTAL,
	TRIGGER_VOLUME,
	EVENT
};

enum EffectFlag
{
	FX_WARP_START = 0,
	FX_WARP_STOP,
	FX_SUN,
	FX_SHIELD,
	FX_CONTACTRESET,
};
//***************************************************************

//Helper Structs
//===============================================================
struct ActorContactPair
{
	Actor	*Actor1;
	Actor	*Actor2;
	Vector3  Contact;
};

// Materials and Lights Defines
//======================================
struct MeshMaterial
{
	MeshMaterial():ambient(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
		           diffuse(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
			       specular(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
			       specularPower(5.0f){}

	MeshMaterial(const D3DXCOLOR& amb,
		         const D3DXCOLOR& dif,
			     const D3DXCOLOR& spe,
			     float pwr):ambient(amb),
			                diffuse(dif),
						    specular(spe),
						    specularPower(pwr){}

	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR specular;
	float specularPower;
};

struct Light
{
	Color ambient;
	Color diffues;
	Color specular;
};

struct DirectionalLight : public Light
{
	Vector3 directionWorld;
};

struct PointLight : public Light
{
	Vector3		posW;
	float		range;
};
//**************************************


//***************************************************************

//Debugging
//===============================================================
//Safe Release
#define ReleaseCOM(x)  if(x) x->Release(); x = 0;
#define SAFE_DELETE(x) if(x) delete x; x=nullptr;

//HR
#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)                                      \
	{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
		{                                              \
			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
		}                                              \
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 


//Check for undefined value
template <class T>
bool UndefinedResult(T t)
{
	return t != t;
}
//***************************************************************
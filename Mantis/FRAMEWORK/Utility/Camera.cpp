#include "Camera.h"
//#include "../Input/DirectInput.h"
#include "../Input/RawInput.h"
#include "../Input/XBOXController.h"

//Camera* gCamera = 0;

Camera* Camera::Instance()
{
	static Camera temp;
	return &temp;
}

Camera::Camera()
{
	D3DXMatrixIdentity(&m_view);				//initialize matracies
	D3DXMatrixIdentity(&m_proj);
	D3DXMatrixIdentity(&m_viewProj);

	m_posW   = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	//camera position
	m_rightW = D3DXVECTOR3(1.0f, 0.0f, 0.0f);	//right axis from camera's position
	m_upW    = D3DXVECTOR3(0.0f, 1.0f, 0.0f);	//up, default is y
	m_lookW  = D3DXVECTOR3(0.0f, 0.0f, 1.0f);	//where camera is looking

	m_fixedLookPoint = D3DXVECTOR3(0.0f, 0.0f, 0.0f); //where the camera is fixed on

	m_speed = 25.0f;

	m_cameraMode = FREEFORM;
	
	m_axisAlligned = false;
}

Camera::~Camera()
{
	//might need to clean up
}

//////////////////////
// Camera Functions //
//////////////////////

void Camera::setMode(CameraModes newMode)
{
	m_cameraMode = newMode;
}

void Camera::setInput(CameraInput newInput)
{
	m_cameraInput = newInput;
}

void Camera::setSpeed(float speed)
{
	m_speed = speed;
}

void Camera::setFOV(float fov)
{
	m_fov = fov;
}

void Camera::setFrustum(float nearZ, float farZ)
{
	m_nearZ = nearZ;
	m_farZ = farZ;
}

void Camera::setLens(float fieldOfView, float aspectRatio, float closeZ, float farZ)
{
	m_fov = fieldOfView;
	m_nearZ = closeZ;
	m_farZ = farZ;

	D3DXMatrixPerspectiveFovLH(&m_proj, fieldOfView, aspectRatio, closeZ, farZ);
	
	//buildWorldFrustumPlanes();

	m_viewProj = m_view * m_proj;
}

void Camera::toggleAxis()
{
	m_axisAlligned = !m_axisAlligned;
}

void Camera::lookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	D3DXVECTOR3 L = target - pos;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_posW		= pos;
	m_rightW	= R;
	m_upW		= U;
	m_lookW		= L;

	buildView();

	m_viewProj = m_view * m_proj;
}

void Camera::SnapToLookAtPoint(D3DXVECTOR3 snapPoint)
{
	snapPoint -= (right() * gRawInput->mouseDX());
	snapPoint.y += gRawInput->mouseDY();

	m_posW += snapPoint;
	m_fixedLookPoint = snapPoint;

	buildThirdPerson();
}

void Camera::update(float dt)
{
	switch(m_cameraMode)
	{
	case FIXED_ROTATION:
		{
			adjustFixedCamera(dt);
			rotateFixedCamera(dt);
			break;
		}

		case ATTACHED:
		{

			//attatch to mesh
			break;
		}
		
		case FREEFORM:
		{
			adjustCamera(dt);
			rotateCamera(dt);
			break;
		}

		case STANDSTILL:
			break;
	}

	buildView();
	//buildWorldFrustumPlanes();

	m_viewProj = m_view * m_proj;
}

void Camera::onResetDevice(float w, float h)
{
	setLens(m_fov, w / h, m_nearZ, m_farZ);
}

D3DXPLANE* Camera::getFPlane(int p)
{
	return m_frustumPlanes;
}

////////////////////////
// Internal Functions //
////////////////////////

void Camera::adjustCamera(float dt)
{
	float speedSet = m_speed;

	D3DXVECTOR3 dir(0.0f, 0.0f, 0.0f);

	if(gXinput->current[0].LShoulder)
		speedSet *= 5;

	if(gXinput->current[0].LAnol.x > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
	   gXinput->current[0].LAnol.x < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
		dir = right();
		dir *= (float)gXinput->current[0].LAnol.x / 32767.0f;

		m_posW += dir * speedSet * dt;
	}

	if(gXinput->current[0].LAnol.y > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
	   gXinput->current[0].LAnol.y < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
		dir = look();
		dir *= (float)gXinput->current[0].LAnol.y/32767.0f;

		m_posW += dir * speedSet * dt;
	}
}

void Camera::rotateCamera(float dt)
{
	//yAngle
	if(gXinput->current[0].RAnol.x >XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE || 
	   gXinput->current[0].RAnol.x < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
		float yAngle = (float)gXinput->current[0].RAnol.x/32767.0f * dt;

		//yAngle *= mSensitivity;

		D3DXMATRIX matRotYAxis;
		D3DXMatrixRotationY(&matRotYAxis, yAngle);
		D3DXVec3TransformCoord(&m_rightW, &m_rightW, &matRotYAxis);
		D3DXVec3TransformCoord(&m_upW, &m_upW, &matRotYAxis);
		D3DXVec3TransformCoord(&m_lookW, &m_lookW, &matRotYAxis);
	}

	//pitch
	if(gXinput->current[0].RAnol.y > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE || 
	   gXinput->current[0].RAnol.y < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
		float pitch = -(float)gXinput->current[0].RAnol.y/32767.0f * dt;

		//pitch *= mSensitivity;

		D3DXMATRIX matRotAxis;
		D3DXMatrixRotationAxis(&matRotAxis, &m_rightW, pitch);
		D3DXVec3TransformCoord(&m_lookW, &m_lookW, &matRotAxis);
		D3DXVec3TransformCoord(&m_upW, &m_upW, &matRotAxis);
	}
}

void Camera::adjustFixedCamera(float dt)
{
	D3DXVECTOR3 direction = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_fixedPointDistance -= gRawInput->mouseDZ() * m_speed * dt;

	if(m_fixedPointDistance < 1.0f)
	{
		m_fixedPointDistance = 1.0f;
	} else {
		direction += look() * gRawInput->mouseDZ();
	}

	m_posW += direction * m_speed * dt;

	if(gRawInput->mouseButtonHeld(2))
	{
		direction -= (right() * gRawInput->mouseDX());
		direction.y += gRawInput->mouseDY();

		m_posW += direction * m_speed * dt;
		m_fixedLookPoint += direction * m_speed * dt;
	}

	buildThirdPerson();
}

void Camera::rotateFixedCamera(float dt)
{
	static float theta = m_fixedPointTheta;
	if(gRawInput->mouseButtonHeld(1))
	{
		//grab angles
		m_fixedPointTheta += gRawInput->mouseDY() / 360; //get the change in the angle for moving the mouse up and down
		m_fixedPointPhi -= gRawInput->mouseDX() / 360; //get the change in the angle for moving the mouse left and right
		
		//used to lock the "poles" of the sphere so you can't flip the camera over. You can look straight up and straight down without flipping the camera.
		if(cos(m_fixedPointTheta) < 0.0001f)
		{
			m_fixedPointTheta = theta;
		}

		theta = m_fixedPointTheta;

		buildThirdPerson();
	}
}

void Camera::buildView()
{
	D3DXVec3Normalize(&m_lookW, &m_lookW);			//normalize where camera is looking

	D3DXVec3Cross(&m_upW, &m_lookW, &m_rightW);		//cross look and right to get up
	D3DXVec3Normalize(&m_upW, &m_upW);				//normalize up

	D3DXVec3Cross(&m_rightW, &m_upW, &m_lookW);		//cross up and look to get right
	D3DXVec3Normalize(&m_rightW, &m_rightW);		//normalize right

	//build the view matrix

	float x = -D3DXVec3Dot(&m_posW, &m_rightW);		//dot pos with right to get x length
	float y = -D3DXVec3Dot(&m_posW, &m_upW);		//dot pos with up to get y length
	float z = -D3DXVec3Dot(&m_posW, &m_lookW);		//dot pos with look to get z length

	//put together the view matrix with vector axis info
	m_view(0, 0) = m_rightW.x;
	m_view(1, 0) = m_rightW.y;
	m_view(2, 0) = m_rightW.z;
	m_view(3, 0) = x;

	m_view(0, 1) = m_upW.x;
	m_view(1, 1) = m_upW.y;
	m_view(2, 1) = m_upW.z;
	m_view(3, 1) = y;

	m_view(0, 2) = m_lookW.x;
	m_view(1, 2) = m_lookW.y;
	m_view(2, 2) = m_lookW.z;
	m_view(3, 2) = z;

	m_view(0, 3) = 0.0f;
	m_view(1, 3) = 0.0f;
	m_view(2, 3) = 0.0f;
	m_view(3, 3) = 1.0f;
}

void Camera::buildThirdPerson()
{
	D3DXVECTOR3 pos;
	//figure out camera's new position based off of the mouse movement
	pos.x = m_fixedPointDistance * cos(m_fixedPointTheta) * cos(m_fixedPointPhi);
	pos.y = m_fixedPointDistance * sin(m_fixedPointTheta);
	pos.z = m_fixedPointDistance * sin(m_fixedPointPhi) * cos(m_fixedPointTheta); // * cos(mThirdPersonPhi);

	m_posW = pos + m_fixedLookPoint;
	lookAt(m_posW, m_fixedLookPoint, D3DXVECTOR3(0.0f, 1.0f, 0.0f));

	buildView();
}

void Camera::buildWorldFrustumPlanes()
{
	D3DXMATRIX VP = m_view * m_proj;

	D3DXVECTOR4 col0(VP(0, 0), VP(1, 0), VP(2, 0), VP(3, 0));
	D3DXVECTOR4 col1(VP(0, 1), VP(1, 1), VP(2, 1), VP(3, 1));
	D3DXVECTOR4 col2(VP(0, 2), VP(1, 2), VP(2, 2), VP(3, 2));
	D3DXVECTOR4 col3(VP(0, 3), VP(1, 3), VP(2, 3), VP(3, 3));

	// Planes face inward.
	m_frustumPlanes[0] = (D3DXPLANE)(col2);				// near
	m_frustumPlanes[1] = (D3DXPLANE)(col3 - col2);		// far
	m_frustumPlanes[2] = (D3DXPLANE)(col3 + col0);		// left
	m_frustumPlanes[3] = (D3DXPLANE)(col3 - col0);		// right
	m_frustumPlanes[4] = (D3DXPLANE)(col3 - col1);		// top
	m_frustumPlanes[5] = (D3DXPLANE)(col3 + col1);		// bottom

	//jnormalize planes
	for(int i = 0; i < 6; i++)
		D3DXPlaneNormalize(&m_frustumPlanes[i], &m_frustumPlanes[i]);
}

//////////////////////
// Camera Accessors //
//////////////////////

D3DXMATRIX Camera::view()
{
	return m_view;
}

D3DXMATRIX Camera::proj()
{
	return m_proj;
}

D3DXMATRIX Camera::viewProj()
{
	return m_viewProj;
}

D3DXVECTOR3 Camera::right()
{
	return m_rightW;
}

D3DXVECTOR3 Camera::up()
{
	return m_upW;
}

D3DXVECTOR3 Camera::look()
{
	return m_lookW;
}

D3DXVECTOR3& Camera::pos()
{
	return m_posW;
}

float& Camera::fixedDist()
{
	return m_fixedPointDistance;
}

D3DXVECTOR3 Camera::GetFixedPoint()
{
	return m_fixedLookPoint;
}
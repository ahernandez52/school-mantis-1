#pragma once
#include <d3d9.h>
#include <d3dx9.h>
#include <sstream>
#include <string>

//class Quaternion;



class Matrix4;

/**
	Extended D3D Vector3 Math class
*/
class Vector3 : public D3DXVECTOR3
{
public:
	Vector3(D3DXVECTOR3 &Vector3)							{ x = Vector3.x; y = Vector3.y; z = Vector3.z;				}
	Vector3() : D3DXVECTOR3()							{ }
	Vector3(const float fx, 
			const float fy, 
			const float fz)								{ x = fx; y = fy; z = fz;							}
	inline Vector3(const class Vector4 &v4);
public: 
	inline float	Length()							{ return D3DXVec3Length(this);						}
	inline float	LengthSq()							{ return D3DXVec3LengthSq(this);					}
	inline float	Dot(const Vector3 &b)				{ return D3DXVec3Dot(this, &b);						}

	inline Vector3	*Normalize()						{ return (Vector3*)D3DXVec3Normalize(this,this);	}
	inline Vector3	Cross(const Vector3 &b) const
	{
		Vector3 out;
		D3DXVec3Cross(&out, this, &b);
		return out;
	}

	Vector3 FromString(std::string line)
	{
		float results[3];
		int i = 0;

		std::string::iterator index = line.begin();
		std::string value = "";

		for(; index != line.end(); index++)
		{
			if((*index == '\"') || (*index == ' '))
				continue;

			if(*index == ',')
			{
				results[i] = (float)atof(value.c_str());
				i++;
				value = "";
				continue;
			}
			value += *index;
		}
	
		x = results[0];
		y = results[1];
		z = results[2];

		return *this;
	}

	std::string ToString()
	{
		std::ostringstream ss;

		ss << x << ", " << y << ", " << z << ",";

		return std::string(ss.str());
	}
};

/**
	Extended D3D Vector4 Math class
*/
class Vector4 : public D3DXVECTOR4
{
public:
	Vector4(D3DXVECTOR4 &vec4)							{ x = vec4.x; y = vec4.y; z = vec4.z; w = vec4.w;	}
	Vector4() : D3DXVECTOR4()							{ }
	Vector4(const float fx, 
			const float fy, 
			const float fz, 
			const float fw)								{ x = fx; y = fy; z = fz; w = fw;					}
	Vector4(const Vector3 &Vector3)						{ x = Vector3.x; y = Vector3.y; z = Vector3.z; w = 1.0f;		}
public:
	inline float	Length()							{ D3DXVec4Length(this);								}
	inline float	LengthSq()							{ D3DXVec4LengthSq(this);							}
	inline float	Dot(const Vector4 &b)				{ D3DXVec4Dot(this, &b);							}

	inline Vector4	*Normalize()						{ return (Vector4*)D3DXVec4Normalize(this,this);	}
	//cross product would be D3DXVec3Cross()
};

inline Vector3::Vector3(const Vector4 &v4) { x = v4.x; y = v4.y; z = v4.z; }

//Vector3 g_Right(1.0f, 0.0f, 0.0f);
//Vector3 g_Up(0.0f, 1.0f, 0.0f);
//Vector3 g_Forward(0.0f, 0.0f, 1.0f);

/**
	Extended D3D Quaternion Math class
*/
class Quaternion : public D3DXQUATERNION
{
public:
	Quaternion(D3DXQUATERNION &q) : D3DXQUATERNION(q) {}
	Quaternion(const float& fx,
			   const float& fy,
			   const float& fz,
			   const float& fw) : D3DXQUATERNION(fx, fy, fz, fw) {}
	Quaternion() : D3DXQUATERNION() {}

	static const Quaternion g_Identity;

public:
	void Normalize() {};
	void Slerp(const Quaternion &begin, const Quaternion &end, float cooef)
	{
		//performs spherical linear interpolation between begin and end
		//note: set cooef between 0.0f - 1.0f
		D3DXQuaternionSlerp(this, &begin, &end, cooef);
	}

	Vector3 Forward()
	{
		return Vector3( 2 * (x * z + w * y),
						2 * (y * z - w * x),
						1 - 2 * (x * x + y * y));
	}

	Vector3 Up()
	{
		return Vector3( 2 * (x * y - w * z),
						1 - 2 * (x * x + z * z),
						2 * (y * z + w * x));
	}

	Vector3 Right()
	{
		return Vector3( 1 - 2 * (y * y + z * z),
						2 * (x * y + w * z),
						2 * (x * z - w * y));
	}

	void GetAxisAngle(Vector3 &axis, float &angle)const
	{
		D3DXQuaternionToAxisAngle(this, &axis, &angle);
	}

	void BuildRotYawPitchRoll(const float yawRadians,
								const float pitchRadians,
								const float rollRadians)
	{
		D3DXQuaternionRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians);
	}

	void BuildAxisAngle(const Vector3 &axis, const float radians)
	{
		D3DXQuaternionRotationAxis(this, &axis, radians);
	}

	void Build(const Matrix4 &matrix)
	{
		//D3DXQuaternionRotationMatrix(this, &matrix); <WARNING>
	}
};

inline Quaternion operator * (const Quaternion &a, const Quaternion &b)
{
	// for rotaitons, this is exactly like concatenating
	// matrices - the new quat represents rot A follwed by rot B.
	//Quaternion out;
	//D3DXQuaternionMultiply(&out, &a, &b);
	//return out;

	return a * b;
}



/**
	Extended D3D Matrix4 Math class
*/
class Matrix4 : public D3DXMATRIX
{
public:
	Matrix4(D3DXMATRIX &matrix)							{ memcpy(&m, &matrix.m, sizeof(matrix.m));			}
	Matrix4() : D3DXMATRIX()							{ }

	static const Matrix4 g_Identity;

public:
	inline void SetPosition(Vector3 const &pos)
	{
		m[3][0] = pos.x;
		m[3][1] = pos.y;
		m[3][2] = pos.z;
		m[3][3] = 1.0f;
	}

	inline void SetPosition(Vector4 const &pos)
	{
		m[3][0] = pos.x;
		m[3][1] = pos.y;
		m[3][2] = pos.z;
		m[3][3] = pos.w;
	}

	inline Vector3 GetPosition() const
	{
		return Vector3(m[3][0], m[3][1], m[3][2]);
	}

	inline Vector3 GetDirection() const
	{
		Vector3 g_Forward(0.0f, 0.0f, 1.0f);
		Matrix4 justRot = *this;
		justRot.SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		Vector3 forward = justRot.Xform(g_Forward);
		return forward;
	}

	inline Vector4 Xform(Vector4 &v) const
	{
		Vector4 temp;
		D3DXVec4Transform(&temp, &v, this);
		return temp;
	}

	inline Vector3 Xform(Vector3 &vec) const
	{
		Vector4 temp(vec), out;
		D3DXVec4Transform(&temp, &temp, this);
		return Vector3 (out.x, out.y, out.z);
	}

	inline Matrix4 Inverse() const
	{
		Matrix4 out;
		D3DXMatrixInverse(&out, NULL, this);
		return out;
	}

	inline void BuildTranslation(const Vector3 &pos)
	{
		*this = Matrix4::g_Identity;
		m[3][0]  = pos.x; m[3][1] = pos.y; m[3][2] = pos.z;
	}

	inline void BuildTranslation(const float x, const float y, const float z)
	{
		*this = Matrix4::g_Identity;
		m[3][0] = x; m[3][1] = y; m[3][2] = z;
	}

	inline void BuildRotationX(const float radians)					{ D3DXMatrixRotationX(this, radians); }
	inline void BuildRotationY(const float radians)					{ D3DXMatrixRotationY(this, radians); }
	inline void BuildRotationZ(const float radians)					{ D3DXMatrixRotationZ(this, radians); }
	inline void BuildYawPitchROll(const float yawRadians, 
			                        const float pitchRadians, 
									const float rollRadians)
	{
		D3DXMatrixRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians); 
	}
		
	inline void BuildRotationQuat(const Quaternion &q)
	{
		D3DXMatrixRotationQuaternion(this, &q);
	}

	inline void BuildRotationLookAt(const Vector3 &eye, const Vector3 &at, const Vector3 &up)
	{
		D3DXMatrixLookAtRH(this, &eye, &at, &up);
	}


};

inline Matrix4 operator* (const Matrix4 &a, const Matrix4 &b)
{
	Matrix4 out;
	D3DXMatrixMultiply(&out, &a, &b);
	return out;
}

//Plane
/**
	Extended D3D Plane class
*/
class Plane : public D3DXPLANE
{
public:
	inline void Normalize();

	// normal faces away from you if you send in verts in counter clockwise order....
	inline void Init(const Vector3 &p0, const Vector3 &p1, const Vector3 &p2);
	bool Inside(const Vector3 &point, const float radius) const;
	bool Inside(const Vector3 &point) const;
};

inline void Plane::Normalize()
{
	float mag;
	mag = sqrt(a * a + b * b + c * c);
	a = a / mag;
	b = b / mag;
	c = c / mag;
	d = d / mag;
}

inline void Plane::Init(const Vector3 &p0, const Vector3 &p1, const Vector3 &p2)
{
	D3DXPlaneFromPoints(this, &p0, &p1, &p2);
	Normalize();
}

//Sphere
class Sphere
{
public:

	Vector3 m_Center;
	float	m_Radius;

public:
	Sphere(void){}
	Sphere(const Vector3& center)									{m_Center = center;}
	Sphere(const Vector3& center, const float& radius)				{m_Center = center; m_Radius = radius;}
	
	~Sphere(void){}
};

//View Frustum
class Frustum
{
public:
	enum Side {Near, Far, Top, RIght, Bottom, Left, NumPlanes };

	Plane m_Planes[NumPlanes];
	Vector3 m_NearClip[4];
	Vector3 m_FarClip[4];

	float m_Fov;
	float m_Aspect;
	float m_Near;
	float m_Far;

public:
	Frustum();

	bool Inside(const Vector3 &point)const;
	bool Inside(const Vector3 &point, const float radius) const;

	const Plane &Get(Side side) { return m_Planes[side]; }
	void SetFOV(float fov) { m_Fov = fov; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetAspect(float aspect){ m_Aspect = aspect; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetNear(float nearClip) { m_Near=nearClip; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetFar(float farClip) { m_Far=farClip; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void Init(const float fov, const float aspect, const float near, const float far);

	void Render();
};


//===========================================
// Math utility
//===========================================

//Random number between -1 and 1
float RandomBinomial(void);

float MapToPi(float rotation);

//Ray/sphere intersect test
bool IntersectRaySphere(Vector3 point, Vector3 dir, Sphere s, float& t, Vector3& q);

//Closest point on ray
void ClosestPointOnLine(Vector3 ref, Vector3 lineStart, Vector3 lineEnd, float& t, Vector3& point);

//Transform from world to local
Vector3 WorldToLocal(Vector3 obj, Vector3 axis);

//Transform object B into A space
Vector3 TransformAtoB(Vector3 posA, Vector3 posB);

//Transform vector by quaternion
Vector3 TransformQuaternion(Vector3 vector, D3DXQUATERNION quat);
/**
 * Audio - This class acts as a wrapper for FMOD to be easily used 
 * in the application
 *
 * Author - Jesse Dillon
 */

#pragma once

#include <map>
#include <fmod.hpp>
#include <fmod_errors.h>

#include "../Utility/MantisUtil.h"


#define SCI Sound::Instance()

class SoundObject;

class Sound
{

private:
	//FMOD members
	FMOD_VECTOR		m_CameraPos;

	//Sound map
	std::map<const char* , FMOD::Sound*> mSoundMap;

	////Object map
	//std::map<int, SoundObject*> m_ObjectMap;

	FMOD::System*	 m_System;
	FMOD_RESULT		 m_Result;
	unsigned int	 version;
	int				 numDrivers;
	FMOD_SPEAKERMODE speakerMode;
	FMOD_CAPS		 caps;
	char			 name[256];

	float			 m_DistanceFactor;
	float			 m_DopplerScale;
	float			 m_RolloffScale;

	FMOD::Channel*	 mChannel0;		//Music channel
	FMOD::Channel*	 mChannel1;		//Effect channel
	FMOD::Channel*	 mChannel2;		//Player channel
	//Channel volumes

private:
	//CTOR
	Sound(void);

public:

	~Sound(void);

	static Sound* Instance()
	{
		static Sound instance;
		return &instance;
	}

	//Initialize and shutdown methods
	void Initialize(void);
	void Shutdown(void);

	//Update
	void Update(float dt);

	//Sound functions
	void PlayMusic(const char* fileName, bool looped);
	void StopMusic(const char* fileName);

	void PlaySample(const char* fileName);
	void PlaySample(const char* fileName, float volume);
	void PlaySample3D(const char* fileName, D3DXVECTOR3 pos);
	void PlaySample3D(const char* fileName, D3DXVECTOR3 pos, float volume);

	void LoadSound(const char* fileName, bool looped);
	void LoadSound(const char* fileName, float minDistance, 
		float maxDistance, bool looped);

	void LoadMusic(const char* fileName, bool looped);

	void ReleaseSound(const char* fileName);
};

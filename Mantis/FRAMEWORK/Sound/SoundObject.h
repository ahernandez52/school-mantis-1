///**
// * SoundObject - Sound object represents a sound object in 3D space
// */
//
//#pragma once
//
//#include "../Utility/MantisUtil.h"
//#include "Audio.h"
//
//class Actor;
//
//class SoundObject
//{
//private:
//
//	Actor* m_Owner;
//
//	FMOD::Channel* m_Channel;
//
//	float m_Volume;
//
//public:
//	SoundObject(void);
//	SoundObject(Actor* owner);
//
//	~SoundObject(void);
//
//	bool RequestChannel(void);
//
//	void PlaySample(const char* fileName);
//	void LoadSample(const char* fileName, float minDist, float maxDist, bool looped);
//
//	void PlayMusic(const char* fileName, bool looped);
//	void LoadMusic(const char* fileName, bool looped);
//
//	/**
//	 * Getters
//	 */
//	
//	D3DXVECTOR3 GetPosition(void);
//	FMOD::Channel*		GetChannel(void);
//
//	Actor*		GetActor(void);
//
//	/**
//	 * Setters
//	 */
//	void SetVolume(float vol);
//
//};
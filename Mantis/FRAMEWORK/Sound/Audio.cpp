#include "Audio.h"
#include "../Utility/MantisUtil.h"
#include "../Utility/Camera.h"
#include "SoundObject.h"
#include <iostream>
#include <utility>


Sound* gSound = 0;


void ERRCHECK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		MessageBox(HWND_DESKTOP, FMOD_ErrorString(result), "Error", MB_OK | MB_ICONEXCLAMATION);
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-5);
	}
}

Sound::Sound(void)
{
	m_CameraPos.x = 0.0f;
	m_CameraPos.y = 0.0f;
	m_CameraPos.z = 0.0f;

	m_DistanceFactor = 1.0f;
	m_DopplerScale = 1.0f;
	m_RolloffScale = 1.0f;

	Initialize();
}

Sound::~Sound(void)
{
	Shutdown();
}

void Sound::Initialize(void)
{
	m_Result = FMOD::System_Create(&m_System);
	ERRCHECK(m_Result);

	/*if (version < FMOD_VERSION)
	{
		MessageBox(0, "FMOD version outdated", 0, 0);
		PostQuitMessage(0);
	}*/

	m_Result = m_System->getNumDrivers(&numDrivers);
	ERRCHECK(m_Result);

	if (numDrivers == 0)
	{
		m_Result = m_System->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		ERRCHECK(m_Result);
	}
	else
	{
		m_Result = m_System->getDriverCaps(0, &caps, 0, &speakerMode);
		ERRCHECK(m_Result);

		m_Result = m_System->setSpeakerMode(speakerMode);
		ERRCHECK(m_Result);

		if (caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			m_Result = m_System->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT,
				0, 0, FMOD_DSP_RESAMPLER_LINEAR);
			ERRCHECK(m_Result);
		}
	}

	m_Result = m_System->init(100, FMOD_INIT_NORMAL, 0);
	if (m_Result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		m_Result = m_System->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		ERRCHECK(m_Result);

		m_Result = m_System->init(100, FMOD_INIT_NORMAL, 0);
		ERRCHECK(m_Result);
	}

	/**
	 * Set the distance units
	 */
	m_Result = m_System->set3DSettings(m_DopplerScale, m_DistanceFactor, m_RolloffScale);

}

void Sound::Shutdown(void)
{
	mSoundMap.clear();

	m_System->release();
}

void Sound::Update(float dt)
{
	//Update the listener
	Vector3 camForward = gCamera->look();
	Vector3 camUp      = gCamera->up();
	Vector3 camP       = gCamera->pos();

	FMOD_VECTOR lastPos = m_CameraPos;
	FMOD_VECTOR forward = {camForward.x, camForward.y, camForward.z};
	FMOD_VECTOR up      = {camUp.x, camUp.y, camUp.z};

	FMOD_VECTOR camPos  = {camP.x, camP.y, camP.z};

	FMOD_VECTOR vel;// = {0.0f, 0.0f, 0.0f};
	vel.x = (camPos.x - lastPos.x) * dt;
	vel.y = (camPos.y - lastPos.y) * dt;
	vel.z = (camPos.z - lastPos.z) * dt;

	m_Result = m_System->set3DListenerAttributes(0, &camPos, &vel, &forward, &up);
	ERRCHECK(m_Result);

	m_System->update(); 
}

void Sound::LoadSound(const char* fileName, bool looped)
{
	FMOD::Sound* newSound;
	m_Result = m_System->createSound(fileName, FMOD_DEFAULT, 0, &newSound);
	ERRCHECK(m_Result);

	if(!looped)
	{
		m_Result = newSound->setMode(FMOD_LOOP_OFF | FMOD_2D);
	}
	else
	{
		m_Result = newSound->setMode(FMOD_LOOP_NORMAL | FMOD_2D);
	}

	ERRCHECK(m_Result);

	mSoundMap.insert(std::make_pair(fileName, newSound));
}

void Sound::LoadSound(const char* fileName, float minDistance, float maxDistance, bool looped)
{
	FMOD::Sound* newSound;
	m_Result = m_System->createSound(fileName, FMOD_DEFAULT, 0, &newSound);
	ERRCHECK(m_Result);
	m_Result = newSound->set3DMinMaxDistance(minDistance * m_DistanceFactor, maxDistance * m_DistanceFactor);
	ERRCHECK(m_Result);
	
	if (looped)
	{
		m_Result = newSound->setMode(FMOD_LOOP_NORMAL | FMOD_3D);
	}

	else
	{
		m_Result = newSound->setMode(FMOD_LOOP_OFF | FMOD_3D);
	}

	ERRCHECK(m_Result);

	mSoundMap.insert(std::make_pair(fileName, newSound));
}

void Sound::LoadMusic(const char* fileName, bool looped)
{
	FMOD::Sound* tempSound;

	if (!looped)
	{
		m_Result = m_System->createStream(fileName, FMOD_LOOP_OFF | FMOD_2D |
			FMOD_HARDWARE, 0, &tempSound);
	}
	else
	{
		m_Result = m_System->createStream(fileName, FMOD_LOOP_NORMAL | FMOD_2D |
			FMOD_HARDWARE, 0, &tempSound);
	}

	ERRCHECK(m_Result);

	mSoundMap.insert(std::make_pair(fileName, tempSound));
}

void Sound::PlaySample(const char* fileName)
{
	FMOD::Sound* tempSound;

	if (mSoundMap.find(fileName) == mSoundMap.end())
	{
		LoadSound(fileName, 10.0f, 10000.0f, false);
	}

	tempSound = mSoundMap.find(fileName)->second;

	m_Result = m_System->playSound(FMOD_CHANNEL_FREE, tempSound, 0, 
		&mChannel1);

	mChannel1->setVolume(0.5f);
}

void Sound::PlaySample(const char* fileName, float volume)
{
	FMOD::Sound* tempSound;

	if (mSoundMap.find(fileName) == mSoundMap.end())
	{
		LoadSound(fileName, false);
	}

	tempSound = mSoundMap.find(fileName)->second;

	m_Result = m_System->playSound(FMOD_CHANNEL_FREE, tempSound, 0, 
		&mChannel1);

	//Normalize volume
	if (volume > 1.0f || volume < 0.0f)
	{
		volume /= volume;
	}

	mChannel1->setVolume(volume);
}

void Sound::PlaySample3D(const char* fileName, D3DXVECTOR3 pos)
{
	FMOD::Sound* tempSound;
	
	if (mSoundMap.find(fileName) == mSoundMap.end())
	{
 		LoadSound(fileName, 50.0f, 10000.0f, false);
	}

	tempSound = mSoundMap.find(fileName)->second;
	
	FMOD_VECTOR p = {pos.x, pos.y, pos.z};
	FMOD_VECTOR v = {0.0f, 0.0f, 0.0f};

	m_Result = m_System->playSound(FMOD_CHANNEL_FREE, tempSound, 0, 
		&mChannel1);

	mChannel1->set3DAttributes(&p, &v);

}

void Sound::PlaySample3D(const char* fileName, D3DXVECTOR3 pos, float volume)
{
	FMOD::Sound* tempSound;
	
	if (mSoundMap.find(fileName) == mSoundMap.end())
	{
 		LoadSound(fileName, 50.0f, 10000.0f, false);
	}

	tempSound = mSoundMap.find(fileName)->second;
	
	FMOD_VECTOR p = {pos.x, pos.y, pos.z};
	FMOD_VECTOR v = {0.0f, 0.0f, 0.0f};

	m_Result = m_System->playSound(FMOD_CHANNEL_FREE, tempSound, 0, 
		&mChannel1);

	//Normalize volume
	if (volume > 1.0f || volume < 0.0f)
	{
		volume /= volume;
	}

	mChannel1->set3DAttributes(&p, &v);
	mChannel1->setVolume(volume);
}

void Sound::PlayMusic(const char* fileName, bool looped)
{
	auto tempSound = mSoundMap.find(fileName);

	if (tempSound == mSoundMap.end())
	{
		LoadMusic(fileName, true);//(fileName, 10.0f, 10000.0f, false);
	}

	m_Result = m_System->playSound(FMOD_CHANNEL_FREE, mSoundMap.at(fileName), 0, &mChannel0);
}

void Sound::StopMusic(const char* fileName)
{
}

void Sound::ReleaseSound(const char* fileName)
{
	auto result = mSoundMap.find(fileName);
	if (result != mSoundMap.end())
	{
		mSoundMap.find(fileName)->second->release();
		mSoundMap.erase(result);
	}
}

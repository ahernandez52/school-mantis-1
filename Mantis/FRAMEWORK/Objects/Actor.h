#pragma once
#include "../../AI/AIObject/AIObject.h"
#include "../../GRAPHICS/Objects/D3D9Object.h"
#include "../../PHYSICS/Objects/PhysicsObject.h"
#include "../../GAMEPLAY/Object/GameplayObject.h"

/**
	Actor is the primary class for the Entity based System

	Core Specific Objects:
	+ AI
	+ Graphics
	+ Physics
	+ Gameplay
*/

class Actor
{
public:

	AIObject			*AI;
	D3D9Object			*Graphics;
	PhysicsObject		*Physics;
	GameplayObject		*Gameplay;

	int ID;

	D3DXVECTOR3		m_Position;
	D3DXQUATERNION  m_Orientation;

public:

	/**
		Default constructer inssure nullptrs for you
	*/
	Actor()
	{
		AI			= nullptr;
		Graphics	= nullptr;
		Physics		= nullptr;
		Gameplay	= nullptr;

		m_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		D3DXQuaternionIdentity(&m_Orientation);

		static int nextID = 0;
		ID = nextID;
		nextID++;
	}

	/**
		Starts Actor at a given point
	*/
	Actor(D3DXVECTOR3 position)
	{
		AI			= nullptr;
		Graphics	= nullptr;
		Physics		= nullptr;
		Gameplay	= nullptr;

		m_Position = position;
		D3DXQuaternionIdentity(&m_Orientation);

		static int nextID = 0;
		ID = nextID;
		nextID++;
	}

	/**
		Starts Actor at given point with orientation
	*/
	Actor(D3DXVECTOR3 position, D3DXQUATERNION orientation)
	{
		AI			= nullptr;
		Graphics	= nullptr;
		Physics		= nullptr;
		Gameplay	= nullptr;

		m_Position = position;
		m_Orientation = orientation;

		static int nextID = 0;
		ID = nextID;
		nextID++;
	}

	/**
		Deconstructor takes care of safe deletes
	*/
	~Actor()
	{
		if(AI != nullptr)
		{
			delete AI;
		}
		if(Graphics != nullptr)
		{
			delete Graphics;
		}
		if(Physics != nullptr)
		{
			delete Physics;
		}
		if(Gameplay != nullptr)
		{
			delete Gameplay;
		}
	}
};
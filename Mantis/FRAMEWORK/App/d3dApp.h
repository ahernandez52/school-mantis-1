//=============================================================================
// d3dApp.h by Frank Luna (C) 2005 All Rights Reserved.
//
// Contains the base Direct3D application class which provides the
// framework interface for the sample applications.  Clients are to derive 
// from D3DApp, override the framework methods, and instantiate only a single
// instance of the derived D3DApp class.  At the same time, the client should
// set the global application  pointer (gd3dApp) to point to the one and only
// instance, e.g., gd3dApp = new HelloD3DApp(hInstance);
// 
//=============================================================================

#pragma once

#include "../Utility/MantisUtil.h"
#include <string>

class MantisGUI;

/**
	Primary entry point into framework
	Takes paramaters from WinMain to Initialize the
	D3D App
*/
class D3DApp
{
public:
	D3DApp(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	virtual ~D3DApp();

	HINSTANCE getAppInst();
	HWND      getMainWnd();


	virtual bool checkDeviceCaps()     { return true; }
	virtual void onLostDevice()        {}
	virtual void onResetDevice()       {}
	virtual void updateScene(float dt) {}
	virtual void drawScene()           {}

	virtual void initMainWindow();
	virtual void initDirect3D();
	virtual int run();
	virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

	void enableFullScreenMode(BOOL enable);
	bool isDeviceLost();

	POINT GetScreenSize();

	virtual MantisGUI* GetGui(void){return nullptr;}

protected:
 
	std::string mMainWndCaption;
	D3DDEVTYPE  mDevType;
	DWORD       mRequestedVP;
	

	HINSTANCE             mhAppInst;
	HWND                  mhMainWnd;
	IDirect3D9*           md3dObject;
	bool                  mAppPaused;
	D3DPRESENT_PARAMETERS md3dPP;

};

extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;
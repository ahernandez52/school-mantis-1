#pragma once
#include <vector>
#include <map>
#include "../Objects/Actor.h"

class MemoryManager;
class BattleSystem;

#define WMI WorldManager::Instance()

/**
	Singleton Interface class that grants access to memory

	can be used anywhere in the program to access core specific objects
*/

class WorldManager
{
private:

	WorldManager();

private:

	MemoryManager* m_memory;

public:
	static WorldManager* Instance();
	~WorldManager();

	BattleSystem *m_ProjectileManager;

	//Memory Utility Functions
	/**
		Initiates a cleanup routine on the current level
	*/
	void CleanUp();

	/**
		Initalize memory heaps
	*/
	void InitMemory();

	/**
		Destroys the memory before exiting the program
	*/
	void DestroyMemory();

	//void onResetDevice();
	//void onLostDevice();
	//===========================================

	//Accessor Functions
	// - Actors
	Actor*									GetActorFromId(int id);
	std::map<int, Actor*>					GetActorLookupMap();
	std::vector<Actor*>						GetActors();
	//------------

	// - Graphics
	D3D9Object*								GetGraphicFromId(int id);
	std::map<int, D3D9Object*>				GetGraphicLookupMap();
	std::vector<std::vector<D3D9Object*>*>	GetGraphicsSortedByType();
	std::vector<D3D9Object*>				GetGraphicsByShader(std::string ShaderID);
	//------------

	// - AI
	AIObject*								GetAIFromId(int id);
	std::map<int, AIObject*>				GetAILookupMap();
	std::vector<AIObject*>*					GetAIObjects();

	std::vector<AIObject*>*					GetAI(); //DEPRECATION
	//------------

	// - Physics
	std::vector<PhysicsObject*>				GetPhysicsObjects();
	//------------

	// - Schematics
	AISchematic								GetAISchematic(std::string Schematic);
	GraphicSchematic						GetGraphicsSchematic(std::string Schematic);
	PhysicsSchematic						GetPhysicsSchematic(std::string Schematic);
	//------------

	//===========================================

	//Registratoin Functions
	void RegisterNewMeshSchematic(std::string SchematicName, std::string filename, std::string details[5]); //DEPRECATION
	void RegisterNewActor(Actor* newActor);		//DEPRECATION

	void RegisterAISchematic(AISchematic Schematic);
	void RegisterGraphicSchematic(GraphicSchematic Schematic);
	void RegisterPhysicsSchematic(PhysicsSchematic Schematic);

	void RegisterGraphicObject(D3D9Object *object);
	//===========================================

	//Creation Functions
	D3D9Object* CreateD3D9ObjectFromSchematic(std::string SchematicName);//DEPRECATION

	Actor*	CreateActor(std::string AISchemName,std::string GraphicsSchemName,std::string PhysicsSchemName,std::string GameplaySchemName);
	//===========================================

	//Deletion Functions
	void DeleteActor(Actor* actor);
	void DeRegisterGraphic(D3D9Object *object);
	//===========================================

	//Helpful functions
	void LoadBoxSchematic();
	void SpawnProjectile(int weaponID, D3DXVECTOR3 shipPosition, D3DXVECTOR3 direction);
	//===========================================
};
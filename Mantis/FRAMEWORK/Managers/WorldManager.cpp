#include "WorldManager.h"
#include "MemoryManager.h"

#include "../../GAMEPLAY/Classes/Weapons/BattleSystem.h"

using namespace std;

WorldManager* WorldManager::Instance()
{
	static WorldManager temp;
	return &temp;
}

WorldManager::WorldManager()
{
	m_memory = new MemoryManager();
}


WorldManager::~WorldManager()
{
	//delete COM objects
}

//Memory Functions
//=======================================
void WorldManager::CleanUp()
{
	m_memory->CleanUp();
}

void WorldManager::InitMemory()
{
	m_memory->InitMemory();
}

void WorldManager::DestroyMemory()
{
	delete m_memory;
}

//void WorldManager::onResetDevice()
//{
//	//m_memory->onResetDevice();
//}
//
//void WorldManager::onLostDevice()
//{
//	//m_memory->onLostDevice();
//}
//***************************************

//Accessor Functions
//=======================================
// - Actors
Actor* WorldManager::GetActorFromId(int id)
{
	return m_memory->m_ActorsLookUp.at(id);
}

map<int, Actor*> WorldManager::GetActorLookupMap()
{
	return m_memory->m_ActorsLookUp;
}

vector<Actor*>	WorldManager::GetActors()
{
	return m_memory->m_Actors;
}
//------------

// - Graphics
D3D9Object* WorldManager::GetGraphicFromId(int id)
{
	return m_memory->m_GraphicsLookUp.at(id);
}

map<int, D3D9Object*> WorldManager::GetGraphicLookupMap()
{
	return m_memory->m_GraphicsLookUp;
}

vector<vector<D3D9Object*>*> WorldManager::GetGraphicsSortedByType()
{
	return m_memory->m_AllGraphicObjectsTypes;
}

vector<D3D9Object*> WorldManager::GetGraphicsByShader(string ShaderID)
{
	return m_memory->m_GraphicObjectsTypeMap.at(ShaderID);
}
//------------

// - AI
AIObject* WorldManager::GetAIFromId(int id)
{
 if(m_memory->m_AILookUp.size() == 0) return nullptr;

 return m_memory->m_AILookUp.at(id);
}

map<int, AIObject*> WorldManager::GetAILookupMap()
{
	return m_memory->m_AILookUp;
}

vector<AIObject*>* WorldManager::GetAIObjects()
{
	return &m_memory->m_AIObjects;
}

vector<AIObject*>* WorldManager::GetAI() //DEPRECATION
{
	return GetAIObjects();
}
//------------

// - Physics
vector<PhysicsObject*> WorldManager::GetPhysicsObjects()
{
	return m_memory->m_PhysicsObjects;
}
//------------

// - Schematics
AISchematic WorldManager::GetAISchematic(string Schematic)
{
	return m_memory->m_AISchematics.at(Schematic);
}

GraphicSchematic WorldManager::GetGraphicsSchematic(string Schematic)
{
	return m_memory->m_GraphicSchematic.at(Schematic);
}

PhysicsSchematic WorldManager::GetPhysicsSchematic(string Schematic)
{
	return m_memory->m_PhysicsSchematic.at(Schematic);
}
//------------
//***************************************

//Registration Functions
//=======================================
void WorldManager::RegisterNewMeshSchematic(string SchematicName, string filename, string details[5])//DEPRECATION
{
	m_memory->RegisterNewMeshSchematic(SchematicName, filename, details);
}

void WorldManager::RegisterNewActor(Actor* newActor)//DEPRECATION
{
	m_memory->AddActor(newActor);
}

void WorldManager::RegisterAISchematic(AISchematic Schematic)
{
	m_memory->RegisterAISchematic(Schematic);
}

void WorldManager::RegisterGraphicSchematic(GraphicSchematic newSchematic)
{
	m_memory->RegisterGraphicSchematic(newSchematic);
}

void WorldManager::RegisterPhysicsSchematic(PhysicsSchematic Schematic)
{
	m_memory->RegisterPhysicsSchematic(Schematic);
}

void WorldManager::RegisterGraphicObject(D3D9Object *object)
{
	m_memory->m_AllGraphicObjectsTypes.at(0)->push_back(object);
}
//***************************************

//Creation Functions
//=======================================
D3D9Object* WorldManager::CreateD3D9ObjectFromSchematic(string SchematicName)//DEPRECATION
{
	return m_memory->CreateD3D9Object(SchematicName);
}

Actor* WorldManager::CreateActor(string AISchemName, string GraphicsSchemName, string PhysicsSchemName, string GameplaySchemName)
{
	return m_memory->CreateActor(AISchemName, GraphicsSchemName, PhysicsSchemName, GameplaySchemName);
}
//***************************************

//Creation Functions
//=======================================
void WorldManager::DeleteActor(Actor* actor)
{
	m_memory->DeleteActor(actor);
}

void WorldManager::DeRegisterGraphic(D3D9Object *object)
{
	m_memory->DeRegisterGraphics(object);
}
//***************************************

//Helpful Functions
void WorldManager::LoadBoxSchematic()
{
	m_memory->LoadBoxSchematic();
}

void WorldManager::SpawnProjectile(int weaponID, D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	m_ProjectileManager->CreateProjectile(weaponID, position, direction);
}
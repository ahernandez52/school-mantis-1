#include "MemoryManager.h"
#include "../Objects/Actor.h"
#include "../../GRAPHICS/Utility/D3D9Vertex.h"
#include "../../AI/AIObject/ShipAI.h"
#include <cassert>


//MemoryManager* MemoryManager::Instance()
//{
//	static MemoryManager temp;
//	return &temp;
//}

MemoryManager::MemoryManager()
{
	m_memoryInitalized = false;
}

MemoryManager::~MemoryManager()
{
}

//memory meta functions
//======================================
void MemoryManager::InitMemory()
{
	//if the memory is initalized don't bother
	//otherwise allocate everything
	if(m_memoryInitalized) return;

	//initialize anything that needs to be done once
	InitAllVertexDeclarations();

	m_memoryInitalized = true;
}

void MemoryManager::CleanUp()
{
	 //if the memory is not initialized don't bother
	 //otherwise deallocate everything
	 if(!m_memoryInitalized) return;

	 DestroyAllVertexDeclarations();

	 //Release Components
	 //for(auto schematic : m_GraphicSchematic)
	 //{
	  //ReleaseCOM(schematic.second.mesh);
	  //delete schematic.second.BoundingBox;
	  //delete schematic.second.BoundingSphere;
	 //}

	 for(auto mesh : m_Meshes)
	 {
	  ReleaseCOM(mesh.second);
	 }

	 for(auto texture : m_Textures)
	 {
	  ReleaseCOM(texture.second);
	 }

	 m_GraphicSchematic.clear();
	 m_AISchematics.clear();
	 m_PhysicsSchematic.clear();

	 m_Meshes.clear();
	 m_Textures.clear();

	 //delete allocated memory
	 //for(auto map : m_GraphicsObjects)
	 // for(auto mesh : map.second)
	 //  delete mesh;

	for(auto actor : m_Actors)
	{
		if(actor != nullptr)
		{
			delete actor;
		}
	}

	 m_ActorsLookUp.clear();
	 m_GraphicsLookUp.clear();
	 m_AILookUp.clear();
	 m_PhysicsLookUp.clear();

	 m_Actors.clear();
	 m_GraphicsObjects.clear();
	 m_AIObjects.clear();
	 m_PhysicsObjects.clear();

	 m_GraphicObjectsTypeMap.clear();

	 m_AllGraphicObjectsTypes.clear();

	 m_memoryInitalized = false;

	 cout << "Memory Performed CleanUp" << endl;
}

void MemoryManager::IsMemoryGood()
{
	assert(m_memoryInitalized && "MEMORY IS NOT INITALIZED");
}

void MemoryManager::ReloadMemory()
{
	CleanUp();
	InitMemory();
}

void MemoryManager::Defragment()
{
	//check for empty schematic vectors
	for(auto schematicVector = m_GraphicObjectsTypeMap.begin();
		schematicVector != m_GraphicObjectsTypeMap.end();
		schematicVector++)
	{
		if(schematicVector->second.size() == 0)
			m_GraphicObjectsTypeMap.erase(schematicVector);
	}

	//rebuild iteration vector for meshes
	m_AllGraphicObjectsTypes.clear();
	for(auto schematicVector = m_GraphicObjectsTypeMap.begin();
		schematicVector != m_GraphicObjectsTypeMap.end();
		schematicVector++)
	{
		m_AllGraphicObjectsTypes.push_back(&schematicVector->second);
	}

	cout << "Memory Performed Defrag" << endl;
}
//**************************************

// Utility Functions
ID3DXMesh* MemoryManager::CalculateTBN(ID3DXMesh* mesh)
{
	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	UINT numElements = 0;
	VertexNormalMap::Decl->GetDeclaration(elems, &numElements);

	ID3DXMesh* cloneTempMesh = 0;
	mesh->CloneMesh(D3DXMESH_MANAGED, elems, gd3dDevice, &cloneTempMesh);

	ReleaseCOM(mesh);
	mesh = 0;

	D3DXComputeTangentFrameEx(
			cloneTempMesh,			  // Input mesh
			D3DDECLUSAGE_TEXCOORD, 0, // Vertex element of input tex-coords.  
			D3DDECLUSAGE_BINORMAL, 0, // Vertex element to output binormal.
			D3DDECLUSAGE_TANGENT, 0,  // Vertex element to output tangent.
			D3DDECLUSAGE_NORMAL, 0,   // Vertex element to output normal.
			0,						  // Options
			0,						  // Adjacency
			0.01f, 0.25f, 0.01f,	  // Thresholds for handling errors
			&mesh,				  // Output mesh
			0);

	ReleaseCOM(cloneTempMesh);

	return mesh;
}
//======================================

// Resource Allocation
//======================================
LPDIRECT3DTEXTURE9 MemoryManager::AllocateNewTexture(string resourceName, string resourcePath)
{
	if (resourceName == "NULL") return nullptr;

	IsMemoryGood();

	LPDIRECT3DTEXTURE9 returnTexture = nullptr;

	auto result = m_Textures.find(resourceName);

	if(result == m_Textures.end())
	{
		D3DXCreateTextureFromFile(gd3dDevice, resourcePath.c_str(), &returnTexture);

		//assert((returnTexture != NULL) && "TEXTURE NOT FOUND");

		if(returnTexture == nullptr)
			return nullptr;

		cout << "Allocated new Texture: " << endl;
		cout << "\tName: " << resourceName << endl;
		cout << "\tPath: " << resourcePath << endl << endl;

		m_Textures.insert(make_pair(resourceName, returnTexture));

		return returnTexture;
	}
	else
	{
		return result->second;
	}
}

ID3DXMesh* MemoryManager::AllocateNewMesh(string resourceName, string resourcePath)
{
	IsMemoryGood();

	ID3DXMesh* returnMesh;

	auto result = m_Meshes.find(resourceName);

	if(result == m_Meshes.end())
	{
		D3DXLoadMeshFromX(resourcePath.c_str(), D3DXMESH_SYSTEMMEM, gd3dDevice, NULL, NULL, NULL, NULL, &returnMesh);

		cout << "Allocated new Mesh: " << endl;
		cout << "\tName: " << resourceName << endl;
		cout << "\tPath: " << resourcePath << endl << endl;

		assert((returnMesh != NULL) && "MESH NOT FOUND");

		returnMesh = CalculateTBN(returnMesh);

		m_Meshes.insert(make_pair(resourceName, returnMesh));

		return returnMesh;
	}
	else
	{
		return result->second;
	}
}
//**************************************

// Schematic Management
//======================================
void MemoryManager::RegisterNewMeshSchematic(string SchematicName, string filename, string details[5])
{
	cout << "CALL TO DEPRECATED FUNCTON (RegisterNewMeshSchematic)" << endl;
}

GraphicSchematic MemoryManager::RegisterNewMeshSchematic(string SchematicName, string filename, string AbsolutePath)
{
	cout << "CALL TO DEPRECATED FUNCTON (RegisterNewMeshSchematic)" << endl;

	return GraphicSchematic();
}

//Registration
void MemoryManager::RegisterAISchematic(AISchematic Schematic)
{
	m_AISchematics.insert(make_pair(Schematic.Identifier, Schematic));
}

void MemoryManager::RegisterGraphicSchematic(GraphicSchematic Schematic)
{
	auto result = m_GraphicSchematic.find(Schematic.name);

	if(result == m_GraphicSchematic.end())
	{
		AllocateGraphicsSchematic(Schematic);
	}
}

void MemoryManager::RegisterPhysicsSchematic(PhysicsSchematic Schematic)
{
	m_PhysicsSchematic.insert(make_pair(Schematic.Identifier, Schematic));
}

//Getters
AISchematic MemoryManager::GetAISchematic(string SchematicName)
{
	return m_AISchematics.at(SchematicName);
}

GraphicSchematic MemoryManager::GetGraphicSchematic(string SchematicName)
{
	return m_GraphicSchematic.at(SchematicName);
}

PhysicsSchematic MemoryManager::GetPhysicsSchematic(string SchematicName)
{
	return m_PhysicsSchematic.at(SchematicName);
}

//Allocation
void MemoryManager::AllocateGraphicsSchematic(GraphicSchematic Schematic)
{
	IsMemoryGood();

	//GraphicSchematic schematic;
	DWORD detailFlags = 0;

	//schematic.fileName	= filename;
	//schematic.name		= SchematicName;
	
	//1 construct schematic since it does not exists
	ID3DXMesh* tempMesh = AllocateNewMesh(Schematic.fileName, "Assets\\Meshes\\" + Schematic.fileName + "\\" + Schematic.fileName +".X");
	  	
	Schematic.mesh = tempMesh;

	//Calculate Bounding volumes
	//==================================
	//VertexNormalMap *pVerts = 0;

	//BoundingSphere *Sphere	= new BoundingSphere();
	//AABB		   *box		= new AABB();

	//schematic.mesh->LockVertexBuffer(0, (void**)&pVerts);

	//D3DXComputeBoundingSphere(&pVerts[0].pos,
	//							schematic.mesh->GetNumVertices(),
	//							sizeof(VertexNormalMap),
	//							&Sphere->pos,
	//							&Sphere->radius);

	//D3DXComputeBoundingBox(&pVerts[0].pos,
	//						schematic.mesh->GetNumVertices(),
	//						sizeof(VertexNormalMap),
	//						&box->minPt,
	//						&box->maxPt);

	//schematic.mesh->UnlockVertexBuffer();

	//schematic.BoundingBox = box;
	//schematic.BoundingSphere = Sphere;
	//**********************************

	// Assign Detail Maps
	//==================================
	if(Schematic.mapNames[DIFFUSE_MAP] == "")
	{
		Schematic.maps[DIFFUSE_MAP] = AllocateNewTexture(Schematic.fileName + "_DIF.jpg", "Assets\\Meshes\\" + Schematic.fileName + "\\Diffuse\\" + Schematic.fileName + "_DIF.jpg");
		if(Schematic.maps[DIFFUSE_MAP] != nullptr)
		{
			Schematic.mapNames[DIFFUSE_MAP] = Schematic.fileName + "_DIF.jpg";
			detailFlags |= DIF;
		}
		Schematic.mapNames[DIFFUSE_MAP] = "FileNotFound";
	}
	else
	{
		Schematic.maps[DIFFUSE_MAP] = AllocateNewTexture(Schematic.fileName + "_DIF_" + Schematic.mapNames[DIFFUSE_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Diffuse\\" + Schematic.fileName + "_DIF_" + Schematic.mapNames[DIFFUSE_MAP]);
		if(Schematic.maps[DIFFUSE_MAP] != nullptr)
		{
			Schematic.mapNames[DIFFUSE_MAP] = Schematic.fileName + "_DIF_" + Schematic.mapNames[DIFFUSE_MAP];
			detailFlags |= DIF;
		}
		Schematic.mapNames[DIFFUSE_MAP] = "FileNotFound";
	}

	if(Schematic.mapNames[BUMP_MAP] == "")
	{
		Schematic.maps[BUMP_MAP] = AllocateNewTexture(Schematic.fileName + "_BMP.jpg", "Assets\\Meshes\\" + Schematic.fileName + "\\Bump\\" + Schematic.fileName + "_BMP.jpg");
		if(Schematic.maps[BUMP_MAP] != nullptr)
		{
			Schematic.mapNames[BUMP_MAP] = Schematic.fileName + "_BMP.jpg";
			detailFlags |= BMP;
		}
		Schematic.mapNames[BUMP_MAP] = "FileNotFound";
	}
	else
	{
		Schematic.maps[BUMP_MAP] = AllocateNewTexture(Schematic.fileName + "_BMP_" + Schematic.mapNames[BUMP_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Bump\\" + Schematic.fileName + "_BMP_" + Schematic.mapNames[BUMP_MAP]);
		if(Schematic.maps[BUMP_MAP] != nullptr)
		{
			Schematic.mapNames[BUMP_MAP] = Schematic.fileName + "_BMP_" + Schematic.mapNames[BUMP_MAP];
			detailFlags |= BMP;
		}
		Schematic.mapNames[BUMP_MAP] = "FileNotFound";
	}

	if(Schematic.mapNames[NORMAL_MAP] == "")
	{
		Schematic.maps[NORMAL_MAP] = AllocateNewTexture(Schematic.fileName + "_NRM.jpg", "Assets\\Meshes\\" + Schematic.fileName + "\\Normal\\" + Schematic.fileName + "_NRM.jpg");
		if(Schematic.maps[NORMAL_MAP] != nullptr)
		{
			Schematic.mapNames[NORMAL_MAP] = Schematic.fileName + "_NRM.jpg";
			detailFlags |= NRM;
		}
		Schematic.mapNames[NORMAL_MAP] = "FileNotFound";
	}
	else
	{
		Schematic.maps[NORMAL_MAP] = AllocateNewTexture(Schematic.fileName + "_NRM_" + Schematic.mapNames[NORMAL_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Normal\\" + Schematic.fileName + "_NRM_" + Schematic.mapNames[NORMAL_MAP]);
		if(Schematic.maps[NORMAL_MAP] != nullptr)
		{
			Schematic.mapNames[NORMAL_MAP] = Schematic.fileName + "_NRM_" + Schematic.mapNames[NORMAL_MAP];
			detailFlags |= NRM;
		}
		Schematic.mapNames[NORMAL_MAP] = "FileNotFound";
	}

	if(Schematic.mapNames[SPECULAR_MAP] == "")
	{
		Schematic.maps[SPECULAR_MAP] = AllocateNewTexture(Schematic.fileName + "_SPC.jpg", "Assets\\Meshes\\" + Schematic.fileName + "\\Specular\\" + Schematic.fileName + "_SPC.jpg");
		if(Schematic.maps[SPECULAR_MAP] != nullptr)
		{
			Schematic.mapNames[SPECULAR_MAP] = Schematic.fileName + "_SPC.jpg";
			detailFlags |= SPC;
		}
		Schematic.mapNames[SPECULAR_MAP] = "FileNotFound";
	}
	else
	{
		Schematic.maps[SPECULAR_MAP] = AllocateNewTexture(Schematic.fileName + "_SPC_" + Schematic.mapNames[SPECULAR_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Specular\\" + Schematic.fileName + "_SPC_" + Schematic.mapNames[SPECULAR_MAP]);
		if(Schematic.maps[SPECULAR_MAP] != nullptr)
		{
			Schematic.mapNames[SPECULAR_MAP] = Schematic.fileName + "_SPC_" + Schematic.mapNames[SPECULAR_MAP];
			detailFlags |= SPC;
		}
		Schematic.mapNames[SPECULAR_MAP] = "FileNotFound";
	}

	if(Schematic.mapNames[ILLUMINATION_MAP] == "")
	{
		Schematic.maps[ILLUMINATION_MAP] = AllocateNewTexture(Schematic.fileName + "_ILL.jpg", "Assets\\Meshes\\" + Schematic.fileName + "\\Illumination\\" + Schematic.fileName + "_ILL.jpg");
		if(Schematic.maps[ILLUMINATION_MAP] != nullptr)
		{
			Schematic.mapNames[ILLUMINATION_MAP] = Schematic.fileName + "_ILL.jpg";
			detailFlags |= ILL;
		}
		Schematic.mapNames[ILLUMINATION_MAP] = "FileNotFound";
	}
	else
	{
		Schematic.maps[ILLUMINATION_MAP] = AllocateNewTexture(Schematic.fileName + "_ILL_" + Schematic.mapNames[ILLUMINATION_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Illumination\\" + Schematic.fileName + "_ILL_" + Schematic.mapNames[ILLUMINATION_MAP]);
		if(Schematic.maps[ILLUMINATION_MAP] != nullptr)
		{
			Schematic.mapNames[ILLUMINATION_MAP] = Schematic.fileName + "_ILL_" + Schematic.mapNames[ILLUMINATION_MAP];
			detailFlags |= ILL;
		}
		Schematic.mapNames[ILLUMINATION_MAP] = "FileNotFound";
	}

#if 0

	if(Schematic.mapNames[SPECULAR_MAP] != "")
	{
		Schematic.maps[SPECULAR_MAP] = AllocateNewTexture(Schematic.mapNames[SPECULAR_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Specular\\" + Schematic.mapNames[SPECULAR_MAP]);
		detailFlags |= SPC;
	}





	if(Schematic.mapNames[ILLUMINATION_MAP] != "")
	{
		Schematic.maps[ILLUMINATION_MAP] = AllocateNewTexture(Schematic.mapNames[ILLUMINATION_MAP], "Assets\\Meshes\\" + Schematic.fileName + "\\Illumination\\" + Schematic.mapNames[ILLUMINATION_MAP]);
		detailFlags |= ILL;
	}
#endif
	//**********************************

	//assign detail map flag
	Schematic.detailFlags = detailFlags;

	//verify for duplicates and add to schematic map
	DoesExist(Schematic);

	m_GraphicSchematic.insert(make_pair(Schematic.name, Schematic));
}

//Utility
bool MemoryManager::ContentsDoMatch(GraphicSchematic Incoming, GraphicSchematic Orginal)
{
	//compare for same mesh
	if(Incoming.mesh != Orginal.mesh)		return false;

	//compre map details
	if(Incoming.maps[0] != Orginal.maps[0]) return false;
	if(Incoming.maps[1] != Orginal.maps[1])	return false;
	if(Incoming.maps[2] != Orginal.maps[2])	return false;
	if(Incoming.maps[3] != Orginal.maps[3]) return false;
	if(Incoming.maps[4] != Orginal.maps[4]) return false;

	return true;
}

void MemoryManager::DoesExist(GraphicSchematic &Incoming)
{
	//unique identifer
	static int unique = 0;

	//Check if a schematic by that name exists
	auto result = m_GraphicSchematic.find(Incoming.name);

	//if it does not then check all other schematics for content duplicates
	//if duplicate is found then return the original schematic and ignore the new one
	if(result == m_GraphicSchematic.end())
	{
		for(auto original : m_GraphicSchematic)
		{
			if(ContentsDoMatch(Incoming, original.second))
			{
				Incoming = original.second;
				return;
			}
		}

		return;
	}

	//if a duplicate name is found check for duplicate contents
	//if contents MATCH then return original schematic ignore the new one
	//if contents DO NOT MATCH generate new unique name and check for duplicates against other schematics
	if(ContentsDoMatch(Incoming, result->second))
	{
		Incoming = result->second;
		return;
	}
	else
	{
		//char buffer[10];
		//Incoming.name = Incoming.name + "_" + itoa(unique, buffer, 10);
		//unique++;

		for(auto original : m_GraphicSchematic)
		{
			if(ContentsDoMatch(Incoming, original.second))
			{
				Incoming = original.second;
				return;
			}
		}

		return;
	}
}
//**************************************

//Creation Functions
//======================================

Actor* MemoryManager::CreateActor(string AISchematicName, string GraphicsSchematicName , string PhysicsSchematicName, string GameplaySchematicName)
{
	Actor* newActor = new Actor();

	if(AISchematicName != "NULL")
		newActor->AI = CreateAIObject(newActor, AISchematicName);

	if(GraphicsSchematicName != "NULL")
		newActor->Graphics = CreateD3D9Object(newActor, GraphicsSchematicName);

	if(PhysicsSchematicName != "NULL")
		newActor->Physics = CreatePhysicsObject(newActor, PhysicsSchematicName);

	//if(GameplaySchematicName != "NULL")
	//	newActor->Gameplay = CreateGameplayObject(GameplaySchematicName);

	//Add Actor to Memory
	m_Actors.push_back(newActor);
	m_ActorsLookUp.insert(make_pair(newActor->ID, newActor));

	return newActor;
}
D3D9Object* MemoryManager::CreateD3D9Object(Actor* Owner, string GraphicsSchematicName)
{
	D3D9Object *newObject = new D3D9Object(Owner, m_GraphicSchematic.at(GraphicsSchematicName));
	string ShaderName = m_GraphicSchematic.at(GraphicsSchematicName).Shader;
	vector<D3D9Object*> ShaderSpecificVector;

	auto result = m_GraphicObjectsTypeMap.insert(make_pair(ShaderName, ShaderSpecificVector));
	
	if(result.second == false)
	{
		result.first->second.push_back(newObject);
	}
	else
	{
		result.first->second.push_back(newObject);
		m_AllGraphicObjectsTypes.push_back(&result.first->second);
	}

	return newObject;
}

AIObject* MemoryManager::CreateAIObject(Actor* Owner, string AISchematicName)
{
	ShipAI *newObject = new ShipAI(Owner, m_AISchematics.at(AISchematicName));

	m_AIObjects.push_back(newObject);
	m_AILookUp.insert(make_pair(Owner->ID, newObject));

	return newObject;
}

PhysicsObject* MemoryManager::CreatePhysicsObject(Actor* Owner, string PhysicsSchematicName)
{
	PhysicsObject *newObject = new PhysicsObject(Owner, m_PhysicsSchematic.at(PhysicsSchematicName));

	m_PhysicsObjects.push_back(newObject);

	return newObject;
}

void MemoryManager::AddActor(Actor* newActor)
{
	cout << "CALL TO DEPRECATED FUNCTON (AddActor)" << endl;
}

D3D9Object* MemoryManager::CreateD3D9Object(string Schematic)
{
	cout << "CALL TO DEPRECATED FUNCTON (CreateD3D9Object)" << endl;

	return nullptr;
}
//**************************************

// Delete Functions
//======================================
void MemoryManager::DeleteActor(Actor* actor)
{
	bool ExistsInMemory = false;
	for(vector<Actor*>::iterator it = m_Actors.begin();
		it != m_Actors.end();
		it++)
	{
		if((*it) == actor)
		{
			m_Actors.erase(it);
			ExistsInMemory = true;
			break;
		}
	}

	if(!ExistsInMemory) return;

	//De-register components
	if(actor->AI != nullptr)
	{
		DeRegisterAI(actor->AI);
	}

	if(actor->Graphics != nullptr)
	{
		DeRegisterGraphics(actor->Graphics);
	}

	if(actor->Physics != nullptr)
	{
		DeRegisterPhysics(actor->Physics);
	}

	delete actor;
}

void MemoryManager::DeRegisterAI(AIObject* ai)
{
	for(vector<AIObject*>::iterator it = m_AIObjects.begin();
		it != m_AIObjects.end();
		it++)
	{
		if((*it) == ai)
		{
			m_AIObjects.erase(it);
			return;
		}
	}
}

void MemoryManager::DeRegisterPhysics(PhysicsObject* physics)
{
	for(vector<PhysicsObject*>::iterator it = m_PhysicsObjects.begin();
		it != m_PhysicsObjects.end();
		it++)
	{
		if((*it) == physics)
		{
			m_PhysicsObjects.erase(it);
			return;
		}
	}
}

void MemoryManager::DeRegisterGraphics(D3D9Object* graphic)
{
	for(auto type : m_AllGraphicObjectsTypes)
	{
		for(vector<D3D9Object*>::iterator it = type->begin();
			it != type->end();
			it++)
		{
			if((*it) == graphic)
			{
				type->erase(it);
				return;
			}
		}
	}
}

void MemoryManager::DeleteD3D9Object(D3D9Object* mesh)
{
	auto meshVector = m_GraphicObjectsTypeMap.find(mesh->m_schematicName);

	for(auto meshIT = meshVector->second.begin();
		meshIT != meshVector->second.end();
		meshIT++)
	{
		if(*meshIT == mesh)
		{
			meshVector->second.erase(meshIT);

			if(meshVector->second.size() == 0)
				m_GraphicObjectsTypeMap.erase(meshVector);

			return;
		}
	}
}
//**************************************

void MemoryManager::LoadBoxSchematic()
{
	static bool loaded = false;

	if(loaded) return;

	IsMemoryGood();

	GraphicSchematic Schematic;

	Schematic.name = "Box";

	ID3DXMesh *tempMesh = 0;
	D3DXCreateBox(gd3dDevice, 1.0, 1.0, 1.0, &tempMesh, 0);

	Schematic.mesh = tempMesh;

	Schematic.maps[DIFFUSE_MAP] = AllocateNewTexture("default.dds", "Assets/Textures/default.dds");

	m_GraphicSchematic.insert(make_pair(Schematic.name, Schematic));

	loaded = true;
}
#pragma once
#include "../Utility/MantisUtil.h"
#include <map>

//#define MMI MemoryManager::Instance()

using namespace std;

class D3D9Object;
class AIObject;
class PhysicsObject;

class Actor;

// ========== Enumerations ==========
enum MeshType 
{
	SPHERE,
	BOX,
	CYLINDER,
};

enum MapNames 
{
	DIFFUSE_MAP,
	BUMP_MAP,
	NORMAL_MAP,
	SPECULAR_MAP,
	ILLUMINATION_MAP,
};

enum DetailFlags
{
	DIF = 0x00001,
	BMP = 0x00010,
	NRM = 0x00100,
	SPC = 0x01000,
	ILL = 0x10000,
};
// ==================================

enum ResourceType{
	GRAPHIC = 0,
};

/**
	Primary Actor and Resource management class

	Holds core specific object as well as a list of Actors

	Cableable of loading and detecting sub resources for a primary source
*/

class MemoryManager
{
friend class WorldManager;
private:

	bool m_memoryInitalized;

private:

	//Look up Tables
	map<int, Actor*>						m_ActorsLookUp;
	map<int, D3D9Object*>					m_GraphicsLookUp;
	map<int, AIObject*>						m_AILookUp;
	map<int, PhysicsObject*>				m_PhysicsLookUp;

	//Schematics
	map<string, AISchematic>				m_AISchematics;
	map<string, GraphicSchematic>			m_GraphicSchematic;
	map<string, PhysicsSchematic>			m_PhysicsSchematic;

	//Resources
	map<string, LPDIRECT3DTEXTURE9>			m_Textures;
	map<string, ID3DXMesh*>					m_Meshes;

	//Object Memory Spaces
	vector<Actor*>							m_Actors;
	vector<D3D9Object*>						m_GraphicsObjects;
	vector<AIObject*>						m_AIObjects;
	vector<PhysicsObject*>					m_PhysicsObjects;
	
	map<string, vector<D3D9Object*>>		m_GraphicObjectsTypeMap;	//organized by shaders

	vector<vector<D3D9Object*>*>			m_AllGraphicObjectsTypes;

private:

	//memory meta functions
	void				InitMemory();
	void				CleanUp();
	void				IsMemoryGood();
	void				ReloadMemory();
	void				Defragment();
	//=====================

	//Utility Functions
	ID3DXMesh*			CalculateTBN(ID3DXMesh* mesh);
	//======================================

	//Resource Allocation
	LPDIRECT3DTEXTURE9	AllocateNewTexture(string resourceName, string resourcePath);
	ID3DXMesh*			AllocateNewMesh(string resourceName, string resourcePath);
	//======================================

	//Schematic Management
	void				RegisterNewMeshSchematic(string SchematicName, string filename, string details[5]);		//Deprecate
	GraphicSchematic	RegisterNewMeshSchematic(string SchematicName, string filename,  string AbsolutePath);	//Deprecate

	//Registration
	void				RegisterAISchematic(AISchematic Schematic);
	void				RegisterGraphicSchematic(GraphicSchematic Schematic);	
	void				RegisterPhysicsSchematic(PhysicsSchematic Schematic);
	
	//Getters
	AISchematic			GetAISchematic(string SchematicName);
	GraphicSchematic	GetGraphicSchematic(string SchematicName);
	PhysicsSchematic	GetPhysicsSchematic(string SchematicName);

	//Allocation
	void				AllocateGraphicsSchematic(GraphicSchematic Schematic);

	//Utility
	bool				ContentsDoMatch(GraphicSchematic Incoming, GraphicSchematic Orginal);
	void				DoesExist(GraphicSchematic &Schematic);				
	//======================================

	// Creation Functions
	Actor*				CreateActor(string AISchematicName, string GraphicsSchematicName , string PhysicsSchematicName, string GameplaySchematicName);
	
	D3D9Object*			CreateD3D9Object(Actor* Owner, string GraphicsSchematicName);
	AIObject*			CreateAIObject(Actor* Owner, string AISchematicName);
	PhysicsObject*		CreatePhysicsObject(Actor* Owner, string PhysicsSchematicName);

	void				AddActor(Actor* newActor);			//Deprecate
	D3D9Object*			CreateD3D9Object(string Schematic);	//Deprecate
	//======================================

	// Delete Functions
	void				DeleteActor(Actor* actor);

	void				DeRegisterAI(AIObject* ai);
	void				DeRegisterPhysics(PhysicsObject* physics);
	void				DeRegisterGraphics(D3D9Object* graphic);

	void				DeleteD3D9Object(D3D9Object* mesh);
	//======================================

	//Helpful functions
	void				LoadBoxSchematic();
	//======================================
public:
	//static MemoryManager* Instance();
	MemoryManager();
	~MemoryManager();

};


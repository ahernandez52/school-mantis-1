//=============================================================================
// HelloDirect3D.cpp by Frank Luna (C) 2005 All Rights Reserved.
//
// Demonstrates Direct3D Initialization and text output using the 
// framework code.
//=============================================================================

#include "GameFramework.h"
#include "Managers\WorldManager.h"
#include "../GRAPHICS/Gui/MantisGUI.h"
#include "../FRAMEWORK/Sound/Audio.h"

//States
#include "../GAMEPLAY/AllGameStates.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	#if defined(DEBUG) | defined(_DEBUG)
	// Enable Console
		//AllocConsole();

		//HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
		//int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
		//FILE* hf_out = _fdopen(hCrt, "w");
		//setvbuf(hf_out, NULL, _IONBF, 1);
		//*stdout = *hf_out;

		//HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
		//hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
		//FILE* hf_in = _fdopen(hCrt, "r");
		//setvbuf(hf_in, NULL, _IONBF, 128);
		//*stdin = *hf_in;
	
	// Enable run-time memory check for debug builds.
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		//_CrtSetBreakAlloc(542);
	#endif

	GameFramework app(hInstance, "Project Sol Demo", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;

	RawInput ri;
	ri.Init();
	gRawInput = &ri;
	
	app.startGSM();
	
	return gd3dApp->run();
}

GameFramework::GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	srand(time_t(0));

	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}

	gXinput = new XBOXController(1);
	
	//Default Camera Settings
	gCamera->setSpeed(400);
	gCamera->setLens(D3DXToRadian(45), (float)md3dPP.BackBufferWidth /  (float)md3dPP.BackBufferHeight, 1.0f, 50000.0f);
	gCamera->fixedDist() = 100;
	gCamera->lookAt(D3DXVECTOR3(25.0f, 50.0f, -50.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	gCamera->setInput(PC_MOUSEKEYBOARD);
	gCamera->setMode(FIXED_ROTATION);

	m_paused = false;

	//Core Initalizations
	//=====================================

	//Graphics Core
	m_GraphicsCore = new D3D9GraphicsCore(gd3dDevice);

	//Gui bootstrap
	m_Gui = new MantisGUI(this);
	m_Gui->Init();

	//AI Core
	m_AICore = new AICore();
	if(!m_AICore->Initialize())
	{
		MessageBox(0, "AICore Initialize failed", 0, 0);
		PostQuitMessage(0);
	}
	m_AICore->SetPhysics(true);

	//Physics Core
	m_PhysicsCore = new DragonPhysicsEngine();

	//Gameplay Core
	m_GameplayCore = new GameplayCore();
	m_GameplayCore->Startup();

	m_GameplayCore->assignGraphics(m_GraphicsCore->m_ParticleMan);

	WMI->m_ProjectileManager = m_GameplayCore->m_BattleSystem;
	
	
	//*************************************
}

GameFramework::~GameFramework()
{
	delete GSM;

	WMI->DestroyMemory();

	delete gXinput;

	//Cores
	delete m_PhysicsCore;
	delete m_GraphicsCore;	
	delete m_AICore;
	delete m_GameplayCore;
	delete m_Gui;
}

bool GameFramework::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

bool GameFramework::isPaused()
{
	return m_paused;
}

void GameFramework::SetPause(bool b)
{
	m_paused = b;
}

void GameFramework::TogglePause()
{
	if(m_paused)
	{
		m_paused = false;
	}
	else
	{
		m_paused = true;
	}
}

void GameFramework::onLostDevice()
{
	//GSM->OnLostDevice();
	m_GraphicsCore->OnLostDevice();
}

void GameFramework::onResetDevice()
{
	gCamera->onResetDevice((float)md3dPP.BackBufferWidth, (float)md3dPP.BackBufferHeight);

	//GSM->OnResetDevice();
	m_GraphicsCore->OnResetDevice();
}

void GameFramework::startGSM()
{
	GSM = new GameStateMachine();
	GSM->StartupGSM(this, m_GraphicsCore, m_PhysicsCore, m_AICore, m_GameplayCore);

	GSM->SetLodingState(new LoadingState01());
	GSM->SetNextState(new MainMenuState());

	GSM->LoadNextState();
}

GameStateMachine* GameFramework::getGSM()
{
	return GSM;
}

MantisGUI* GameFramework::GetGui(void)
{
	return m_Gui;
}

void GameFramework::updateScene(float dt)
{
	
	//Poll Input
	EventXBOXController();

	//Update gui
	m_Gui->Update(dt);
	 
	gCamera->update(dt);

	GSM->Update(dt);

	if(!isPaused())
	{
		m_AICore->Update(dt);
	
		//m_PhysicsCore->Update(dt);

		m_GameplayCore->Update(m_PhysicsCore->GetContactPairs(), dt);

		m_GraphicsCore->Update(dt);
	}

	SCI->Update(dt);

	gRawInput->ClearInput();
}

void GameFramework::drawScene()
{
	m_GraphicsCore->Render(gCamera->view(), gCamera->proj(), gCamera->pos());
}

void GameFramework::EventXBOXController()
{
	lock.acquire();

	//check for controller pause
	if(gXinput->current[0].Start && (gXinput->previous[0].Start == false))
	{
		if(!m_paused)
		{
			m_paused = true;
			//GSM->ChangeState(new GameMenu(this));
		} else {
			m_paused = false;
			//GSM->ReverToPreviousState();
		}
	}

	//check for fullscreen toggle
	if(gXinput->current[0].Select && (gXinput->previous[0].Select == false))
		enableFullScreenMode(md3dPP.Windowed);

	gXinput->wait = false;
	
	lock.release();
}


#pragma once

class GameFramework;
class D3D9GraphicsCore;
class DragonPhysicsEngine;
class AICore;
class GameplayCore;

class GameState{
friend class GameStateMachine;
protected:

	GameFramework			*Framework;
	D3D9GraphicsCore		*Graphics;
	DragonPhysicsEngine		*Physics;
	AICore					*AI;
	GameplayCore			*Gameplay;

public:

	//GameState(){}
	//virtual ~GameState(){}

	virtual void InitializeState() = 0;
	virtual void EnterState() = 0;
	virtual void LeaveState() = 0;
    virtual void Update(float dt) = 0;
};


/**
 * BaseFactory - Defines an abstract factory which will be used as the base for 
 * all other factory systems
 */

#pragma once

#include <string>
#include "../Utility/JSONParser.h"

class BaseFactory
{
private:

	virtual void ReadSchematics(void) = 0;
	virtual void CreateActors(void)   = 0;

protected:

	JSONObject m_JSONObject;

public:

	BaseFactory(void){}
	~BaseFactory(void){}

	//Overriden build function builds the associated object
	virtual void Build(std::string filename) = 0;
};
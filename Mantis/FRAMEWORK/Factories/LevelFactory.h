#pragma once
#include <string>
#include "../Utility/JSONParser.h"
#include "../Factories/BaseFactory.h"

/**
	Level Factory responsible for loading in levels
	from JSON files
*/
class LevelFactory : public BaseFactory
{
private:

	// Private Functions
	//=====================================
	/**
		Parses a JSON file into the level
	*/
	void ReadSchematics() override;

	/**
		Prases a JSON to create Actors
	*/
	void CreateActors() override;
	//*************************************

private:

	//JSONObject held in BaseFactory  (Jesse 5-10)
	/*JSONObject	m_JSONObject;*/

public:
	LevelFactory();
	~LevelFactory();



	// Public Functions
	//=====================================
	/**
		Initiates Primary build
	*/
	void BuildLevel(std::string filename);

	//Overriden build function calls existing BuildLevel (Jesse 5-10)
	/**
		Build a specific file
	*/
	void Build(std::string filename) override;
	//*************************************
};


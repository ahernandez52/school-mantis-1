#include "LevelFactory.h"
#include "../Managers/WorldManager.h"

using namespace std;

LevelFactory::LevelFactory(void)
{
	WMI->InitMemory();
}


LevelFactory::~LevelFactory(void)
{
}

// Private Functions
//=====================================
void LevelFactory::ReadSchematics()
{
	//string type; For later use 
	string SchematicName, filename, details[5];

	for(auto schematics : m_JSONObject.m_arrays.at("Schematics").m_values)
	{
		//type = schematics.m_variables["Type"]; For later use
		filename = schematics.m_variables["fileName"];
		SchematicName = schematics.m_variables["Name"];

		auto savedTextures = schematics.m_objects["Textures"];

		details[0] = savedTextures.m_variables["Diffuse"];
		details[1] = savedTextures.m_variables["Bump"];
		details[2] = savedTextures.m_variables["Normal"];
		details[3] = savedTextures.m_variables["Specular"];
		details[4] = savedTextures.m_variables["Illumination"];

		WMI->RegisterNewMeshSchematic(SchematicName, filename, details);
	}
}

void LevelFactory::CreateActors()
{
	string SchematicToUse;
	Actor *newActor;
	D3D9Object* tempMesh;

	for(auto actors :  m_JSONObject.m_arrays.at("Actors").m_values)
	{
		newActor = new Actor();

		SchematicToUse = actors.m_variables["Schematic"];

		//<WARNING>
		Vector3 vec3;

		tempMesh = WMI->CreateD3D9ObjectFromSchematic(SchematicToUse);
		newActor->m_Position = vec3.FromString(actors.m_variables["Position"]);
		tempMesh->m_scale = vec3.FromString(actors.m_variables["Scale"]);

		newActor->Graphics = tempMesh;

		WMI->RegisterNewActor(newActor);
	}
}
//*************************************


// Public Functions
//=====================================
void LevelFactory::BuildLevel(string filename)
{
	JSONParser parser;
	parser.OpenFile(filename);

	m_JSONObject = parser.GetParsedObject();

	if(m_JSONObject.m_arrays.size() == 0) return; //if the file is empty

	ReadSchematics();
	CreateActors();

	//MMI->Defragment();
}

void LevelFactory::Build(std::string filename)  //Added (Jesse 5-10)
{
	BuildLevel(filename);
}
//*************************************
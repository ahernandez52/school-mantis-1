#pragma once

#include <Windows.h>
//#include <Xinput.h>
#include <XInput.h>

#pragma comment(lib, "XInput.lib")

class Lock
{
private:
	Lock(const Lock&);
	Lock& operator=(const Lock&);

	CRITICAL_SECTION m_cs;

public:
	Lock()
	{
		InitializeCriticalSection(&m_cs);
	}

	~Lock()
	{
		DeleteCriticalSection(&m_cs);
	}

	void acquire()
	{
		EnterCriticalSection(&m_cs);
	}

	void release()
	{
		LeaveCriticalSection(&m_cs);
	}
};

class XBOXController
{
private:
	
	XINPUT_STATE ControllerState;
	

public:

	XBOXController(int);
	~XBOXController();
	unsigned int players;

	HANDLE mThread;
	unsigned int ThreadKillState;

	XINPUT_STATE GetState();

	struct Controller{

		bool InUse;
		POINT LAnol, RAnol;
		float LTrig, RTrig;
		bool A, B, X, Y;
		bool Start, Select;
		bool LShoulder, RShoulder;
		bool LStick, RStick;
		bool DPAD_UP, DPAD_DOWN, DPAD_LEFT, DPAD_RIGHT;
	};

	Controller current[4];
	Controller previous[4];

	bool IsConnected();
	bool wait;

	void Init();
	void StartChecking();
	void StopChecking();
	void Check();
};

DWORD WINAPI HandleXBOX(void* nID);

extern XBOXController* gXinput;

static Lock lock;
//#include "D3DApp.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "RawInput.h"
#include "KeyDefs.h"
#include <windowsx.h>
#include <ctype.h>
#include <CEGUI.h>

/**
 * RawInput
 */

RawInput* gRawInput = 0;

RawInput::RawInput(void)
{
	ClearInput();
}

void RawInput::Init(void)
{
	//Initialize the keyboard
	Rid[0].usUsagePage = 1;
	Rid[0].usUsage	   = 6;
	Rid[0].dwFlags     = 0;
	Rid[0].hwndTarget  = NULL;

	//Initialize the mouse
	Rid[1].usUsagePage = 1;
	Rid[1].usUsage	   = 2;
	Rid[1].dwFlags	   = 0;
	Rid[1].hwndTarget  = NULL;

	if (RegisterRawInputDevices(Rid, 2, sizeof(RAWINPUTDEVICE)) == FALSE)
	{
		//Handle exception
		MessageBox(0, "Raw Input register failed", NULL, 0);
		PostQuitMessage(0);
	}
	
	ClearInput();
	ClearButtonHeld();

	m_keyAlt = false;
	m_keyShift = false;
	m_keyCntrl = false;

}


void RawInput::ProcessInput(const RAWINPUT* buffer)
{
	//Clear input from the last update
	ClearInput();

	if (buffer->header.dwType == RIM_TYPEMOUSE)
	{

		//Change in mouse position
		m_MouseDX = buffer->data.mouse.lLastX;
		m_MouseDY = buffer->data.mouse.lLastY;

		//Input from mouse wheel
		m_MouseDZ = (short)buffer->data.mouse.usButtonData;

		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_1_DOWN)
		{
			mMouseButtonPressed[0] = true;
			mMouseButtonHeld[0] = true;

			CEGUI::System::getSingleton().injectMouseButtonDown(
				CEGUI::MouseButton::LeftButton);
		}
		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_1_UP)
		{
			mMouseButtonReleased[0] = true;
			mMouseButtonHeld[0] = false;

			CEGUI::System::getSingleton().injectMouseButtonUp(
				CEGUI::MouseButton::LeftButton);
		}
		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_3_DOWN)
		{
			mMouseButtonPressed[2] = true;
			mMouseButtonHeld[2] = true;

			CEGUI::System::getSingleton().injectMouseButtonDown(
				CEGUI::MouseButton::MiddleButton);
		}
		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_3_UP)
		{
			mMouseButtonReleased[2] = true;
			mMouseButtonHeld[2] = false;

			CEGUI::System::getSingleton().injectMouseButtonUp(
				CEGUI::MouseButton::MiddleButton);
		}
		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_2_DOWN)
		{
			mMouseButtonPressed[1] = true;
			mMouseButtonHeld[1] = true;

			CEGUI::System::getSingleton().injectMouseButtonDown(
				CEGUI::MouseButton::RightButton);
		}
		if (buffer->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_2_UP)
		{
			mMouseButtonReleased[1] = true;
			mMouseButtonHeld[1] = false;

			CEGUI::System::getSingleton().injectMouseButtonUp(
				CEGUI::MouseButton::RightButton);
		}
	}
	
	if (buffer->header.dwType == RIM_TYPEKEYBOARD)
	{
		if (buffer->data.keyboard.Message == WM_KEYDOWN ||
			buffer->data.keyboard.Message == WM_SYSKEYDOWN)
		{
			USHORT keyCode = buffer->data.keyboard.VKey;

			m_KeyboardMake = keyCode;

			//// If printable
			//// ================
			//if (isprint(keyCode))
			//{
			//	//Handle printable keys
			//	if (!m_keyShift)
			//	{
			//		CEGUI::System::getSingleton().injectChar(keyCode + 32);
			//		return;
			//	}
			//	else
			//	{
			//		CEGUI::System::getSingleton().injectChar(keyCode);
			//		return;
			//	}
			//}

			// If not printable
			// ================

			//else
			//{
				//Handle cntrl keys
				switch(keyCode)
				{
				case VK_BACK:
					CEGUI::System::getSingleton().injectKeyDown(DIK_BACK);
					break;

				case VK_TAB:
					CEGUI::System::getSingleton().injectKeyDown(DIK_TAB);
					CEGUI::System::getSingleton().injectChar(keyCode);
					break;

				case VK_RETURN:
					CEGUI::System::getSingleton().injectKeyDown(DIK_RETURN);
					break;

				case VK_SHIFT:	//Shift key
					m_keyShift = true;
					break;

				case VK_CONTROL:	//Control key
					m_keyCntrl = true;
					break;

				case VK_MENU:	//Alt key
					m_keyAlt = true;
					break;

				case VK_PAUSE:
					CEGUI::System::getSingleton().injectKeyDown(DIK_PAUSE);
					break;

				case VK_ESCAPE:
					CEGUI::System::getSingleton().injectKeyDown(DIK_ESCAPE);
					break;

				case VK_SPACE:
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					CEGUI::System::getSingleton().injectChar(keyCode);
					break;

				case VK_PRIOR:	//Page Up Key
					CEGUI::System::getSingleton().injectKeyDown(DIK_PGUP);
					break;

				case VK_NEXT:	//Page Down Key
					CEGUI::System::getSingleton().injectKeyDown(DIK_PGDN);
					break;

				case VK_END:
					CEGUI::System::getSingleton().injectKeyDown(DIK_END);
					break;

				case VK_HOME:
					CEGUI::System::getSingleton().injectKeyDown(DIK_HOME);
					break;

				case VK_LEFT:
					CEGUI::System::getSingleton().injectKeyDown(DIK_LEFT);
					break;

				case VK_UP:
					CEGUI::System::getSingleton().injectKeyDown(DIK_UP);
					break;

				case VK_RIGHT:
					CEGUI::System::getSingleton().injectKeyDown(DIK_RIGHT);
					break;

				case VK_DOWN:
					CEGUI::System::getSingleton().injectKeyDown(DIK_DOWN);
					break;

				case VK_SELECT:
					CEGUI::System::getSingleton().injectKeyDown(DIK_MEDIASELECT);
					break;

				case VK_INSERT:
					CEGUI::System::getSingleton().injectKeyDown(DIK_INSERT);
					break;

				case VK_DELETE:
					CEGUI::System::getSingleton().injectKeyDown(DIK_DELETE);
					break;


				case VK_F1:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F1);
					break;

				case VK_F2:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F2);
					break;

				case VK_F3:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F3);
					break;

				case VK_F4:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F4);
					break;

				case VK_F5:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F5);
					break;

				case VK_F6:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F6);
					break;

				case VK_F7:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F7);
					break;

				case VK_F8:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F8);
					break;

				case VK_F9:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F9);
					break;

				case VK_F10:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F10);
					break;

				case VK_F11:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F11);
					break;

				case VK_F12:	
					CEGUI::System::getSingleton().injectKeyDown(DIK_F12);
					break;

				case VK_NUMLOCK:
					CEGUI::System::getSingleton().injectKeyDown(DIK_NUMLOCK);
					break;

				case VK_SCROLL:
					CEGUI::System::getSingleton().injectKeyDown(DIK_SCROLL);
					break;

				case VK_OEM_1:	// ';:' Key
					CEGUI::System::getSingleton().injectKeyDown(DIK_SEMICOLON);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x3B);
					else 
						CEGUI::System::getSingleton().injectChar(0X3A);
					break;

				case VK_OEM_PLUS:
					CEGUI::System::getSingleton().injectKeyDown(DIK_ADD);
					CEGUI::System::getSingleton().injectChar(0x2B);
					break;

				case VK_OEM_COMMA:
					CEGUI::System::getSingleton().injectKeyDown(DIK_COMMA);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x2C);
					else 
						CEGUI::System::getSingleton().injectChar(0x3C);
					break;

				case VK_OEM_MINUS:
					CEGUI::System::getSingleton().injectKeyDown(DIK_MINUS);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x2D);
					else 
						CEGUI::System::getSingleton().injectChar(0x5F);
					break;

				case VK_OEM_PERIOD:
					CEGUI::System::getSingleton().injectKeyDown(DIK_PERIOD);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0X2E);
					else 
						CEGUI::System::getSingleton().injectChar(0x3E);
					break;

				case VK_OEM_2:
					CEGUI::System::getSingleton().injectKeyDown(DIK_SLASH);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x2F);
					else 
						CEGUI::System::getSingleton().injectChar(0x3F);
					break;

				case VK_OEM_3:
					CEGUI::System::getSingleton().injectKeyDown(DIK_GRAVE);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x60);
					else 
						CEGUI::System::getSingleton().injectChar(0x7E);
					break;

				case VK_OEM_4:
					CEGUI::System::getSingleton().injectKeyDown(DIK_LBRACKET);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x5B);
					else 
						CEGUI::System::getSingleton().injectChar(0x7B);
					break;

				case VK_OEM_5:
					CEGUI::System::getSingleton().injectKeyDown(DIK_BACKSLASH);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0X5C);
					else
						CEGUI::System::getSingleton().injectChar(0x7C);
					break;

				case VK_OEM_6:
					CEGUI::System::getSingleton().injectKeyDown(DIK_RBRACKET);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x5D);
					else 
						CEGUI::System::getSingleton().injectChar(0x7D);
					break;

				case VK_OEM_7:
					CEGUI::System::getSingleton().injectKeyDown(DIK_APOSTROPHE);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(0x27);
					else
						CEGUI::System::getSingleton().injectChar(0x22);
					break;

				case 0X30:	// '0' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else
						CEGUI::System::getSingleton().injectChar(0x0029);
					break;

				case 0x31:	// '1' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x21);
					break;

				case 0x32:	// '2' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else
						CEGUI::System::getSingleton().injectChar(0x40);
					break;

				case 0x33:	// '3' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x23);
					break;

				case 0x34:	// '4' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x24);
					break;

				case 0x35:	// '5' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else
						CEGUI::System::getSingleton().injectChar(0x25);
					break;

				case 0x36:	// '6' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else
						CEGUI::System::getSingleton().injectChar(0x5E);
					break;

				case 0x37:	// '7' Key
					CEGUI::System::getSingleton().injectKeyDown(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x26);
					break;

				case 0x38:	// '8' Key
					CEGUI::System::getSingleton().injectChar(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x2A);
					break;

				case 0x39:	// '9' Key
					CEGUI::System::getSingleton().injectChar(keyCode);
					if (!m_keyShift)
						CEGUI::System::getSingleton().injectChar(keyCode);
					else 
						CEGUI::System::getSingleton().injectChar(0x28);
					break;


				default:
					//Handle printable keys
					if (!m_keyShift)
					{
						CEGUI::System::getSingleton().injectChar(keyCode + 32);
						return;
					}
					else
					{
						CEGUI::System::getSingleton().injectChar(keyCode);
						return;
					}
				}
			//}
		}

		if (buffer->data.keyboard.Message == WM_KEYUP ||
			buffer->data.keyboard.Message == WM_SYSKEYUP)
		{
			USHORT keyCode = buffer->data.keyboard.VKey;

			m_KeyboardBreak = keyCode;

			//Handle application specific commands
			switch(keyCode)
			{
			case 0x10:  //Shift key
				m_keyShift = false;
				return;

			case 0x11:	//Control key
				m_keyCntrl = false;
				return;

			case 0x12:	//Alt key
				m_keyAlt = false;
				return;
			}

			//Inject input to GUI
			CEGUI::System::getSingleton().injectKeyUp(keyCode);
		}
	}
}

void RawInput::ClearInput(void)
{
	//Clear mouse input
	for (int i = 0 ; i < 3 ; i++)
	{
		mMouseButtonPressed[i] = false;
		mMouseButtonReleased[i] = false;
	}

	m_MouseDX = (long)0.0f;
	m_MouseDY = (long)0.0f;
	m_MouseDZ = (long)0.0f;

	//Clear keyboard input
	memset(&m_KeyboardMake, 0, sizeof(USHORT));
	memset(&m_KeyboardBreak, 0, sizeof(USHORT));
}

void RawInput::ClearButtonHeld(void)
{
	for (int i = 0 ; i < 3 ; i++)
	{
		mMouseButtonHeld[i] = false;
	}
}

bool RawInput::keyPressed(const char* key)
{
	bool ret = (char)m_KeyboardMake == *key;
	return ret;
}

bool RawInput::keyPressed(USHORT key)
{
	return m_KeyboardMake == key;
}

bool RawInput::keyReleased(const char* key)
{
	bool ret = (char)m_KeyboardBreak == *key;
	return ret;
}

bool RawInput::keyReleased(USHORT key)
{
	return m_KeyboardBreak == key;
}
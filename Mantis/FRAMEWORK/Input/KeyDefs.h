/**
 * KeyDefs - Creates defines which can be used in switch statements with 
 * the RawInput module.  Should be expanded on in order to check for other 
 * input systems and include the basic alphanumeric keys
 *
 * Author - Jesse Dillon
 */

#pragma once


#define MK_LEFT			int 37
#define MK_RIGHT		int 39
#define MK_UP			int 38
#define MK_DOWN			int 40
#define MK_INSERT		int 45
#define MK_DELETE		int 46
#define MK_END			int 35
#define MK_PAGEDWN		int 34
#define MK_PAGEUP		int 33
#define MK_HOME			int 36
#define MK_NUMLOCK		int 144
#define MK_SCROLLLOCK	int 145
#define MK_PAUSE		int 19
#define MK_COMMA		int 188
#define MK_PERIOD		int 190
#define MK_SLASHFOR		int 191
#define MK_SLASHBACK	int 220
#define MK_APOSTROPHE	int 222
#define MK_COLON		int 186
#define MK_BRACKETOPEN	int 219
#define MK_BRACKETCLOSE	int 221
#define MK_BACKSPACE	int 8
#define MK_SPACE		int 32
#define MK_TILDE		int 192
#define MK_ESCAPE		int 27
#define MK_TAB			int 9
#define MK_ENTER		int 13
#define MK_NUMPAD0		int 96
#define MK_NUMPAD1		int 97
#define MK_NUMPAD2		int 98
#define MK_NUMPAD3		int 99 
#define MK_NUMPAD4		int 100
#define MK_NUMPAD5		int 101
#define MK_NUMPAD6		int 102
#define MK_NUMPAD7		int 103
#define MK_NUMPAD8		int 104
#define MK_NUMPAD9		int 105
#define MK_MULTIPLY		int 106
#define MK_ADD			int 107
#define MK_SEPARATOR	int 108
#define MK_SUBTRACT		int 109
#define MK_DECIMAL		int 110
#define MK_DIVIDE		int 111
#define MK_F1			int 112
#define MK_F2			int 113
#define MK_F3			int 114
#define MK_F4			int 115
#define MK_F5			int 116
#define MK_F6			int 117
#define MK_F7			int 118
#define MK_F8			int 119
#define MK_F9			int 120
#define MK_F10			int 121
#define MK_F11			int 122
#define MK_F12			int 123



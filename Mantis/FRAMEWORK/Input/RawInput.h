/**
 * Input - Input defines DInput to poll input devices
 * <NOTE> Expand to include RAWInput as well
 *
 * Author - Jesse Dillon
 */

#pragma once

/**
 * Not using Direct Input, but this header defines RawInput structs
 */

//Define the Direct Input version
#define DIRECTINPUT_VERSION 0x800

#include <dinput.h>	

/**
 * RAW Input
 */

class RawInput
{
private:

	//Modifiers
	bool m_keyAlt;
	bool m_keyShift;
	bool m_keyCntrl;

	//Mouse position change
	long m_MouseDX;
	long m_MouseDY;
	long m_MouseDZ;

	//Mouse state
	bool mMouseButtonPressed[3];
	bool mMouseButtonReleased[3];
	bool mMouseButtonHeld[3];

	//Keyboard state
	USHORT m_KeyboardMake;
	USHORT m_KeyboardBreak;

	//Buffer size for allocation
	UINT m_BufferSize;

	//Input buffer
	LPBYTE m_RawBuffer;

private:

	RawInput(const RawInput& rhs);
	RawInput& operator = (const RawInput& rhs);

public:
	RawInput(void);

	void Init(void);

	void ProcessInput(const RAWINPUT* rMouse);

	//Input devices for keyboard[0] and mouse[1]
	RAWINPUTDEVICE Rid[2];

	//Mouse accessors
	bool mouseButtonPressed(int n){return mMouseButtonPressed[n];}
	bool mouseButtonReleased(int n){return mMouseButtonReleased[n];}
	bool mouseButtonHeld(int n){return mMouseButtonHeld[n];}

	float mouseDX(void){return (float)m_MouseDX;}
	float mouseDY(void){return (float)m_MouseDY;}
	float mouseDZ(void){return (float)m_MouseDZ;}
	
	void ClearInput(void);
	void ClearButtonHeld(void);

	bool keyPressed(const char* key);
	bool keyPressed(USHORT key);

	bool keyReleased(const char* key);
	bool keyReleased(USHORT key);
};

//Globally defined input
extern RawInput* gRawInput;	


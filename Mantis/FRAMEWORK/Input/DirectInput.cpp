////=============================================================================
//// DirectInput.cpp by Frank Luna (C) 2005 All Rights Reserved.
////=============================================================================
//
//#include "../Utility/MantisUtil.h"
//#include "DirectInput.h"
//#include "../App/d3dApp.h"
//
//DirectInput* gDInput = 0;
//
//DirectInput::DirectInput(DWORD keyboardCoopFlags, DWORD mouseCoopFlags)
//{
//	ZeroMemory(mKeyboardState, sizeof(mKeyboardState));
//	ZeroMemory(&mMouseState, sizeof(mMouseState));
//
//	HR(DirectInput8Create(gd3dApp->getAppInst(), DIRECTINPUT_VERSION, 
//		IID_IDirectInput8, (void**)&mDInput, 0));
//
//	HR(mDInput->CreateDevice(GUID_SysKeyboard, &mKeyboard, 0));
//	HR(mKeyboard->SetDataFormat(&c_dfDIKeyboard));
//	HR(mKeyboard->SetCooperativeLevel(gd3dApp->getMainWnd(), keyboardCoopFlags));
//	HR(mKeyboard->Acquire());
//
//	HR(mDInput->CreateDevice(GUID_SysMouse, &mMouse, 0));
//	HR(mMouse->SetDataFormat(&c_dfDIMouse2));
//	HR(mMouse->SetCooperativeLevel(gd3dApp->getMainWnd(), mouseCoopFlags));
//	HR(mMouse->Acquire());
//}
//
//DirectInput::~DirectInput()
//{
//	ReleaseCOM(mDInput);
//	mKeyboard->Unacquire();
//	mMouse->Unacquire();
//	ReleaseCOM(mKeyboard);
//	ReleaseCOM(mMouse);
//}
//
//void DirectInput::poll()
//{
//	// Poll keyboard.
//	HRESULT hr = mKeyboard->GetDeviceState(sizeof(mKeyboardState), (void**)&mKeyboardState); 
//	if( FAILED(hr) )
//	{
//		// Keyboard lost, zero out keyboard data structure.
//		ZeroMemory(mKeyboardState, sizeof(mKeyboardState));
//
//		 // Try to acquire for next time we poll.
//		hr = mKeyboard->Acquire();
//	}
//
//	// Poll mouse.
//	hr = mMouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&mMouseState); 
//	if( FAILED(hr) )
//	{
//		// Mouse lost, zero out mouse data structure.
//		ZeroMemory(&mMouseState, sizeof(mMouseState));
//
//		// Try to acquire for next time we poll.
//		hr = mMouse->Acquire(); 
//	}
//
//	POINT temp;
//	GetCursorPos(&temp);
//	ScreenToClient(gd3dApp->getMainWnd(), &temp);
//
//	mousePos = temp;
//}
//
//bool DirectInput::keyDown(char key)
//{
//	return (mKeyboardState[key] & 0x80) != 0;
//}
//
//bool DirectInput::keyPressed(int Key)
//{
//	bool relevantState;
//
//	mKeyboardCurrentState[Key] = ((mKeyboardState[Key] & 0x80) != 0);
//	
//	relevantState = (mKeyboardCurrentState[Key] != mKeyboardLastState[Key]) && (mKeyboardLastState[Key] == 0);
//	
//	mKeyboardLastState[Key] = ((mKeyboardState[Key] & 0x80) != 0);
//
//	return relevantState;
//}
//
//bool DirectInput::mouseButtonDown(int button)
//{
//	return (mMouseState.rgbButtons[button] & 0x80) != 0;
//}
//
//float DirectInput::mouseDX()
//{
//	return (float)mMouseState.lX;
//}
//
//float DirectInput::mouseDY()
//{
//	return (float)mMouseState.lY;
//}
//
//float DirectInput::mouseDZ()
//{
//	return (float)mMouseState.lZ;
//}
//
//POINT DirectInput::mousePosition()
//{
//	return mousePos;
//}
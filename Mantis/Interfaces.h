#pragma once
/*
	Interfaces.h
	Defines Interface classes for engine components
*/

class CoreInterface
{
	virtual void VOnReset(){}
	virtual void VOnLost(){}
};
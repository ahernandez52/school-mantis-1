#include "AICore.h"
#include "AIObject\AIObject.h"
#include "AIObject\ShipAI.h"
#include "Steering\SteeringBehavior.h"

#include "States/FighterAttack.h"
#include "States/FighterEvade.h"

#include "Messaging\PostOffice.h"
#include "../FRAMEWORK/Managers/WorldManager.h"
#include "../FRAMEWORK/Utility/JSONParser.h"

#include <assert.h>


AICore::AICore(void)
{

}

AICore::~AICore(void)
{

}

void AICore::Update(float dt)
{

	//Pump message system
	POST->Update(dt);

	//Update actors
	//=======================
	m_Objects = WMI->GetAI();

	for (const auto actor : GetShips())
	{

		actor->Update(dt, m_Objects);

	}

	//=======================
	
}

bool AICore::PhysicsOn()
{
	return m_PhysOn;
}

void AICore::SetPhysics(bool PhysicsOn)
{
	m_PhysOn = PhysicsOn;
}

bool AICore::Initialize(void)
{
	return true;
}

std::vector<ShipAI*> AICore::GetShips(void)
{
	std::vector<ShipAI*> ships;

	for (const auto& obj : *m_Objects)
	{
		if (obj->GetAIType() == AI_SHIP)
		{
			ships.push_back((ShipAI*)obj);
		}
	}

	return ships;
}

std::vector<ShipAI*> AICore::GetFaction(FACTION faction)
{
	std::vector<ShipAI*> ships;

	for (const auto& obj : *m_Objects)
	{
		if (obj->GetAIType() == AI_SHIP)
		{
			if (((ShipAI*)obj)->GetFaction()->GetFaction() == faction)
			{
				ships.push_back((ShipAI*)obj);
			}
		}
	}

	return ships;
}

std::vector<ShipAI*> AICore::GetShipType(SHIPTYPE type)
{
	std::vector<ShipAI*> ships;

	for (const auto& obj : *m_Objects)
	{
		if (obj->GetAIType() == AI_SHIP)
		{
			if (((ShipAI*)obj)->GetShipType()->GetShipType() == type)
			{
				ships.push_back((ShipAI*)obj);
			}
		}
	}

	return ships;
}

std::vector<ShipAI*> AICore::GetFactionShipType(FACTION faction, SHIPTYPE type)
{
	std::vector<ShipAI*> ships;

	for (const auto& obj : *m_Objects)
	{
		if (obj->GetAIType() == AI_SHIP)
		{
			if ((((ShipAI*)obj)->GetFaction()->GetFaction() == faction) &&
				(((ShipAI*)obj)->GetShipType()->GetShipType() == type))
			{
				ships.push_back((ShipAI*)obj);
			}
		}
	}

	return ships;
}

void AICore::CreateAvoidanceWall(D3DXVECTOR3 topLeft, D3DXVECTOR3 topRight, D3DXVECTOR3 botLeft, D3DXVECTOR3 botRight, D3DXVECTOR3 normal)
{
	//m_pSteering->CreateWall(topLeft, topRight, botLeft, botRight, normal);
}

void AICore::ParseSteeringWeight(std::string fileName)
{
	////Create buffer and values
	//float seek, flee, arrive, wall, obs, pursue, align, sep, coh, wander;
	//seek = m_pSteering->getWeightSeek();
	//flee = m_pSteering->getWeightFlee();
	//arrive = m_pSteering->getWeightArrive();
	//wall = m_pSteering->getWeightwall();
	//obs = m_pSteering->getWeightObstacle();
	//pursue = m_pSteering->getWeightPursue();
	//align = m_pSteering->getWeightAlign();
	//sep = m_pSteering->getWeightSeparate();
	//coh = m_pSteering->getWeightCohesive();
	//wander = m_pSteering->getWeightWander();

	//char buffer [sizeof(float) * 8 + 1];

	//JSONParser parser;

	//parser.CreateNewFile(fileName);

	//sprintf_s(buffer, "%f", seek);
	//parser.CreateVariable("seek", buffer);

	//sprintf_s(buffer, "%f", flee);
	//parser.CreateVariable("flee", buffer);

	//sprintf_s(buffer, "%f", arrive);
	//parser.CreateVariable("arrive", buffer);

	//sprintf_s(buffer, "%f", wall);
	//parser.CreateVariable("wall", buffer);

	//sprintf_s(buffer, "%f", obs);
	//parser.CreateVariable("obstacle", buffer);

	//sprintf_s(buffer, "%f", pursue);
	//parser.CreateVariable("pursue", buffer);

	//sprintf_s(buffer, "%f", align);
	//parser.CreateVariable("align", buffer);

	//sprintf_s(buffer, "%f", sep);
	//parser.CreateVariable("separation", buffer);

	//sprintf_s(buffer, "%f", coh);
	//parser.CreateVariable("cohesive", buffer);

	//sprintf_s(buffer, "%f", wander);
	//parser.CreateVariable("wander", buffer);

	//parser.CloseFile();
}

void AICore::LoadSteeringWeight(std::string fileName)
{
	//JSONParser parser;

	//int error = parser.OpenFile(fileName);

	//switch(error)
	//{
	//case 1:
	//	MessageBox(0, "AI Steering weights invalid", 0, 0);
	//	PostQuitMessage(0);
	//	break;

	//case 2:
	//	MessageBox(0, "AI Steering weights not found", 0, 0);
	//	PostQuitMessage(0);
	//	break;

	//default:
	//	break;
	//}

	//JSONObject weights = parser.GetParsedObject();

	////Values
	//float seek, flee, arrive, wall, obs, pursue, align, sep, coh, wander;

	//std::stringstream(weights.m_variables["seek"]) >> seek;
	//std::stringstream(weights.m_variables["flee"]) >> flee;
	//std::stringstream(weights.m_variables["arrive"]) >> arrive;
	//std::stringstream(weights.m_variables["wall"]) >> wall;
	//std::stringstream(weights.m_variables["obstacle"]) >> obs;
	//std::stringstream(weights.m_variables["pursue"]) >> pursue;
	//std::stringstream(weights.m_variables["align"]) >> align;
	//std::stringstream(weights.m_variables["separation"]) >> sep;
	//std::stringstream(weights.m_variables["cohesive"]) >> coh;
	//std::stringstream(weights.m_variables["wander"]) >> wander;

	//m_pSteering->setWeightSeek(seek);
	//m_pSteering->setWeightFlee(flee);
	//m_pSteering->setWeightArrive(arrive);
	//m_pSteering->setWeightWall(wall);
	//m_pSteering->setWeightObstacle(obs);
	//m_pSteering->setWeightPursue(pursue);
	//m_pSteering->setWeightAlign(align);
	//m_pSteering->setWeightSeparate(sep);
	//m_pSteering->setWeightCohesive(coh);
	//m_pSteering->setWeightWander(wander);
}
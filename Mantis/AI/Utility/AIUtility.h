#pragma once

#include "..\AIObject\ShipAI.h"



//===========================================
// Influence Map Utility
//===========================================

/**
 * InfleEle - struct used to order map elements by value while keeping reference to index
 */
struct InflEle
{
	int index;
	float val;

	InflEle(int i, float v): index(i), val(v) {}
};

/**
 * Overloaded operators to order InflEle within set
 */
inline bool operator == (const InflEle& ele1, const InflEle& ele2)
{
	return ((ele1.index == ele2.index) && (ele1.val == ele2.val));
}

inline bool operator < (const InflEle& ele1, const InflEle& ele2)
{
	if (ele1 == ele2)
	{
		return false;
	}

	else return (ele1.val < ele2.val);
}

inline bool operator > (const InflEle& ele1, const InflEle& ele2)
{
	if (ele1 == ele2)
	{
		return false;
	}

	else return (ele1.val > ele2.val);
}

/**
 * Check for proximity using radius treating 1-D array as 2-D
 */
//bool IsTooClose(int index1, int index2, int rowSize, int radius);

//
////Return the 3 highest, separated elements in the map
//int* GetHighestNodes(InfluenceMap map, int proximityRadius);


//===========================================
// AI Utility
//===========================================

Actor* GetClosestActor(Vector3 point);

AIObject* GetClosestAIObject(Vector3 point);

ShipAI* GetClosestShip(Vector3 point);

ShipAI* GetClosestEnemy(Vector3 point, FACTION myFaction);

ShipAI* GetClosestFriendly(Vector3 point, FACTION myFaction);


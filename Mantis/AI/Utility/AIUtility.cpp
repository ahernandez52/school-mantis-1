#include "AIUtility.h"
#include "../AIObject/AIObject.h"
#include "../AIObject/ShipAI.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include <cassert>

//bool IsTooClose(int index1, int index2, int rowSize, int radius)
//{
//	assert(radius > 0);
//
//	//Test without checking for array bounds
//	for (int k = -radius; k <= radius; k++)
//	{
//		for (int j = -radius; j <= radius; j++)
//		{
//			//If index 2 == index 1 + radius*Rowsize + radius
//			if (index2 == index1 + k*rowSize + j)
//				return true;
//		}
//	}
//}
//
//int* GetHighestNodes(InfluenceMap map, int proximityRadius)
//{
//	std::set<InflEle> sorted;
//
//	//Sort the map indices in the set by value
//	for (int i = 0 ; i < map.GetNumCells() ; i++)
//	{
//		sorted.insert(InflEle(i, map.GetValueByIndex(i)));
//	}
//
//	//Push the top 10 onto a vector for iteration
//	std::vector<InflEle> highest;
//	std::set<InflEle>::const_iterator ele = sorted.begin();
//
//	for (int i = 0 ; i < 10 ; i++)
//	{
//		ele++;
//		highest.push_back(*ele);
//	}
//
//	//Compare index by other nodes and remove lowest value close nodes
//
//	for (std::vector<InflEle>::iterator index = highest.begin();
//		 index != std::prev(highest.end()); 
//		 ++index)
//	{
//		for (std::vector<InflEle>::iterator iter1 = std::next(index) ; 
//			iter1 != highest.end(); 
//			++iter1)
//		{
//			if (IsTooClose(index->index, iter1->index, map.GetCols(), proximityRadius) 
//				&& (highest.size() > 3))
//			{
//				//Nodes are too close remove lowest and break control to outer loop
//				if(index < iter1)
//				{
//					highest.erase(index);
//				}
//				else
//				{
//					highest.erase(iter1);
//				}
//
//				//Transfer control
//				break;
//
//			}	//Else continue
//
//		}	//End inner traversal
//
//	}	//End outer traversal
//
//	//Return the three integers
//	int ret[3];
//
//	for (int i = 0 ; i < 3; i++)
//	{
//		ret[i] = highest.at(i).index;
//	}
//	
//	return ret;
//}

Actor* GetClosestActor(Vector3 point)
{
	std::vector<Actor*> actors = WMI->GetActors();

	if(actors.size() == 0)
	{
		return nullptr;
	}

	Actor* closestActor = actors.front();

	for(auto actor : actors)
	{
		if( D3DXVec3LengthSq(&( closestActor->m_Position - point )) > D3DXVec3LengthSq(&( actor->m_Position - point )) )
		{
			closestActor = actor;
		}
	}

	return closestActor;
}

AIObject* GetClosestAIObject(Vector3 point)
{
	std::vector<AIObject*>* objects = WMI->GetAIObjects();

	if(objects->size() == 0)
	{
		return nullptr;
	}

	AIObject* closestAI = objects->front();

	for(auto AI : *objects)
	{
		if( D3DXVec3LengthSq(&( closestAI->GetPosition() - point )) > D3DXVec3LengthSq(&( AI->GetPosition() - point )) )
		{
			closestAI = AI;
		}
	}

	return closestAI;
}

ShipAI* GetClosestShip(Vector3 point)
{
	std::vector<AIObject*>* ships = WMI->GetAIObjects();

	if(ships->size() == 0)
	{
		return nullptr;
	}

	AIObject* closestShip = ships->front();

	for(auto ship : *ships)
	{
		if(ship->GetAIType() == AI_SHIP)
		{
			if( D3DXVec3LengthSq(&( closestShip->GetPosition() - point )) > D3DXVec3LengthSq(&( ship->GetPosition() - point )) )
			{
				closestShip = ship;
			}
		}
	}

	return static_cast<ShipAI*>(closestShip);
}

ShipAI* GetClosestEnemy(Vector3 point, FACTION myFaction)
{
	std::vector<AIObject*>* ships = WMI->GetAIObjects();

	if(ships->size() == 0)
	{
		return nullptr;
	}

	AIObject* closestShip = ships->front();

	for(auto ship : *ships)
	{
		if(ship->GetAIType() == AI_SHIP)
		{
			if(static_cast<ShipAI*>(ship)->GetFaction()->GetFaction() != myFaction)
			{
				if( D3DXVec3LengthSq(&( closestShip->GetPosition() - point )) > D3DXVec3LengthSq(&( ship->GetPosition() - point )) )
				{
					closestShip = ship;
				}
			}
		}
	}

	return static_cast<ShipAI*>(closestShip);
}

ShipAI* GetClosestFriendly(Vector3 point, FACTION myFaction)
{
	std::vector<AIObject*>* ships = WMI->GetAIObjects();

	if(ships->size() == 0)
	{
		return nullptr;
	}

	AIObject* closestShip = ships->front();

	for(auto ship : *ships)
	{
		if(ship->GetAIType() == AI_SHIP)
		{
			if(static_cast<ShipAI*>(ship)->GetFaction()->GetFaction() == myFaction)
			{
				if( D3DXVec3LengthSq(&( closestShip->GetPosition() - point )) > D3DXVec3LengthSq(&( ship->GetPosition() - point )) )
				{
					closestShip = ship;
				}
			}
		}
	}

	return static_cast<ShipAI*>(closestShip);
}


/**
 * AICore - This class acts as a manager between the AI systems used for the project
 */

#pragma once

#include <vector>
#include "AIObject\AIFaction.h"
#include "AIObject\AIShipType.h"
#include "AIObject\AIObject.h"

//Forward declarations
//class MapCreator;
class SteeringBehavior;
class ShipAI;
class Player;

class AICore
{
	//Used for object updates
	bool m_PhysOn;

	//Steering algorithms
	SteeringBehavior*	   m_pSteering;

	//Pointer to all AI objects
	std::vector<AIObject*>* m_Objects;

	//Pointer to the player
	Player* m_Player;

public:
	//CTOR 
	AICore();

	~AICore(void);

	//Initialize
	bool Initialize(void);

	//Update method
	void Update(float dt);

	//Setters
	void SetPhysics(bool);

	//Accessors
	bool PhysicsOn();

	//Steering 
	SteeringBehavior*	getSteering(void);

	//Container calls
	std::vector<ShipAI*> GetShips(void);
	std::vector<ShipAI*> GetFaction(FACTION faction);
	std::vector<ShipAI*> GetShipType(SHIPTYPE type);
	std::vector<ShipAI*> GetFactionShipType(FACTION faction, SHIPTYPE type);

	void CreateAvoidanceWall(D3DXVECTOR3 topLeft, D3DXVECTOR3 topRight, D3DXVECTOR3 botLeft, D3DXVECTOR3 botRight, D3DXVECTOR3 normal);


	//Console interaction//

	//Parse steering weights to file
	void ParseSteeringWeight(std::string fileName);
	void LoadSteeringWeight(std::string fileName);

};
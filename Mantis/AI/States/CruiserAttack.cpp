#include "CruiserMove.h"
#include "CruiserAttack.h"
#include "CruiserEvade.h"

#include "..\AIObject\ShipAI.h"
#include "..\AIObject\AIShipType.h"
#include "..\Messaging\PostOffice.h"
#include <random>
#include <chrono>
#include "..\Fuzzy\FuzzySystem.h"
#include "..\..\FRAMEWORK\GameFramework.h"
#include "..\Influence\InfluenceSystem.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"
#include "..\Utility\AIUtility.h"

void CruiserAttack::Initialize(ShipAI* ship)
{
}

void CruiserAttack::Update(ShipAI* ship, float dt)
{
}

void CruiserAttack::LeaveState(ShipAI* ship)
{
}

void CruiserAttack::HandleMessage(ShipAI* ship, Message* m)
{
}
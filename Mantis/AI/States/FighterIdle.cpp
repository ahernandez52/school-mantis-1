#include "FighterIdle.h"
#include "FighterMove.h"
#include "FighterAttack.h"
#include "FighterEvade.h"

#include "../AIObject/ShipAI.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GAMEPLAY/Object/Player.h"
#include "../Messaging/PostOffice.h"
#include "../Steering/SteeringBehavior.h"

#include "../Utility/AIUtility.h"

#include <random>
#include <chrono>

void FighterIdle::Initialize(ShipAI* ship)
{
	m_Player = nullptr;
}

void FighterIdle::Update(ShipAI* ship, float dt)
{
}

void FighterIdle::LeaveState(ShipAI* ship)
{
}

void FighterIdle::HandleMessage(ShipAI* ship, Message* m)
{
	switch(m->message)
	{
	case NO_MESSAGE:

		//Do nothing

		break;

	case SELF_UPDATE:

		ship->SetUpdate(true);

		break;

	case ATTACK:
		
		ship->ChangeState(FighterAttack::Instance());

		break;

	case EVADE:

		ship->ChangeState(FighterEvade::Instance());

		break;

	case HIT:

		ship->ChangeState(FighterEvade::Instance());

		break;

	default:

		//Do nothing

		break;
	}
}

void FighterIdle::SetPlayer(Actor* player)
{
	m_Player = player;
}

Actor* FighterIdle::GetPlayer(void)
{
	return m_Player;
}
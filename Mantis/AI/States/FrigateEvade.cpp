#include "FrigateEvade.h"
#include "FrigateAttack.h"
#include "FrigateMove.h"


#include "..\AIObject\ShipAI.h"
#include "..\Messaging\PostOffice.h"
#include <random>
#include <chrono>
#include "..\Steering\SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "..\Influence\InfluenceSystem.h"
#include "..\Utility\AIUtility.h"


void FrigateEvade::Initialize(ShipAI* ship)
{

}

void FrigateEvade::Update(ShipAI* ship, float dt)
{

}

void FrigateEvade::LeaveState(ShipAI* ship)
{
}

void FrigateEvade::HandleMessage(ShipAI* ship, Message* m)
{

}
/**
 * BaseState - Declares an abstract state from which all other states inherit
 *
 * Author - Jesse Dillon
 */

#pragma once 

#include "..\Messaging\Message.h"

template <class owner_type>
class BaseState
{
public:
	virtual ~BaseState(void) {}

	virtual void Initialize(owner_type*)	= 0;
	virtual void Update(owner_type*, float dt)	= 0;
	virtual void LeaveState(owner_type*)	= 0;

	virtual void HandleMessage(owner_type*, Message* m){}

};
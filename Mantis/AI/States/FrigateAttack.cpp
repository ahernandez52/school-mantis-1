#include "FrigateEvade.h"
#include "FrigateAttack.h"
#include "FrigateMove.h"


#include "../AIObject/ShipAI.h"
#include "../Messaging/PostOffice.h"
#include <random>
#include <chrono>
#include "../Steering/SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../Influence/InfluenceSystem.h"
#include "../Utility/AIUtility.h"


void FrigateAttack::Initialize(ShipAI* ship)
{

}

void FrigateAttack::Update(ShipAI* ship, float dt)
{

}

void FrigateAttack::LeaveState(ShipAI* ship)
{

}

void FrigateAttack::HandleMessage(ShipAI* ship, Message* m)
{

}
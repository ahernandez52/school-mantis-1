#pragma once

#include "BaseState.h"

class ShipAI;

class FrigateAttack : public BaseState<ShipAI>
{
private:
	FrigateAttack() {}

public:
	static FrigateAttack* Instance()
	{
		static FrigateAttack instance;
		return &instance;
	}

	~FrigateAttack(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;

};
#include "FrigateEvade.h"
#include "FrigateAttack.h"
#include "FrigateMove.h"

#include "..\AIObject\ShipAI.h"
#include "..\Messaging\PostOffice.h"
#include <random>
#include <chrono>
#include "..\Steering\SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "..\Influence\InfluenceSystem.h"
#include "..\Utility\AIUtility.h"


void FrigateMove::Initialize(ShipAI* ship)
{

}

void FrigateMove::Update(ShipAI* ship, float dt)
{

}

void FrigateMove::LeaveState(ShipAI* ship)
{
}

void FrigateMove::HandleMessage(ShipAI* ship, Message* m)
{

}
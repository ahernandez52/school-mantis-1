#pragma once

#include "BaseState.h"

class ShipAI;

class CruiserEvade : public BaseState<ShipAI>
{
private:
	CruiserEvade() {}

public:
	static CruiserEvade* Instance()
	{
		static CruiserEvade instance;
		return &instance;
	}

	~CruiserEvade(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;
	
};
/** 
 * Fighter idle - Don't do anything
 */

#pragma once

#include "BaseState.h"

class ShipAI;
class Actor;

class FighterIdle : public BaseState<ShipAI>
{
private:
	FighterIdle() {}

	Actor* m_Player;

public:

	static FighterIdle* Instance()
	{
		static FighterIdle instance;
		return &instance;
	}

	~FighterIdle(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;

	void SetPlayer(Actor* player);
	Actor* GetPlayer(void);
};
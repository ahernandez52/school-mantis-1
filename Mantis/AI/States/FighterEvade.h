/**
 * FighterEvade - A state defining the fighter action of "bugging out" of combat.  
 */

#pragma once

#include "BaseState.h"

class ShipAI;
class Actor;
class Vector3;

class FighterEvade : public BaseState<ShipAI>
{
private:
	FighterEvade() : m_EvadeTimer(3.0f) {}

	Actor* m_Player;

	float m_EvadeTimer;

	Vector3 RandomTarget(ShipAI* ship);		//Return random target for straffing run completion

public:
	static FighterEvade* Instance()
	{
		static FighterEvade instance;
		return &instance;
	}
	
	~FighterEvade(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	/**
	 * Handle messages sent to this system
	 */
	void HandleMessage(ShipAI* ship, Message* m) override;

	void SetPlayer(Actor* player);
	Actor* GetPlayer(void);

};
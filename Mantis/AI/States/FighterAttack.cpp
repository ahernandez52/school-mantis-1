#include "FighterAttack.h"
#include "FighterEvade.h"
#include "FighterMove.h"

#include "../AIObject/ShipAI.h"
#include "../Messaging/PostOffice.h"
#include "../Steering/SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GAMEPLAY/Object/Player.h"

#include "../../GAMEPLAY/Object/Ship.h"

#include "../Utility/AIUtility.h"

#include <random>
#include <chrono>



void FighterAttack::Initialize(ShipAI* ship)
{
	//Pursue player
	ship->GetSteering()->SetTargetAgentPursue(GetPlayer()->AI);
	ship->GetSteering()->PursueOn();

	//Offset pursue to follow


	////Wander (LIGHT)
	//ship->GetSteering()->WanderOn();
	//ship->GetSteering()->SetWanderRadius(50.0f);
	//ship->GetSteering()->SetWanderOffset(100.0f);
	//ship->GetSteering()->SetWanderJitter(5.0f);
	//ship->GetSteering()->SetWeightWander(0.2f);

	//Avoid obstacles
	//ship->GetSteering()->ObstacleAvoidanceOn();
	
	//FaceForward 3d
	ship->GetSteering()->FaceForward3DOn();
	//ship->GetSteering()->FacePoint3DOn();

	//Straff approach is on
	ship->SetStraff(true);
	ship->SetSlide(false);
}

void FighterAttack::Update(ShipAI* ship, float dt)
{
	ShipAI* agent = ship;
	Actor* player = m_Player;

	agent->GetSteering()->SetTargetPoint3D(player->m_Position);

	float outerRadius = 1500.0f;
	float innerRadius = 500.0f;

	//If hit, handle event
	if (agent->GetHit())
	{
		agent->SetHit(false);
		Message m(agent->GetId(), agent->GetId(), 0.0f, HIT);
	}
	
	//Get spatial information
	D3DXVECTOR3 relPos = player->m_Position - agent->GetPosition();
	float distanceSq = D3DXVec3Length(&relPos);

	float viewDistanceSq = agent->GetSteering()->GetViewDistance();
	//viewDistanceSq *= viewDistanceSq;

	Quaternion agentOrientation = agent->GetCurrentOrientation();
	Quaternion playerOrientation = player->m_Orientation;

	D3DXVECTOR3 playerHeading = playerOrientation.Forward();
	D3DXVECTOR3 agentHeading = -agentOrientation.Forward();

	D3DXVec3Normalize(&relPos, &relPos);
	D3DXVec3Normalize(&playerHeading, &playerHeading);
	D3DXVec3Normalize(&agentHeading, &agentHeading);

	/**
	 * Where : 
	 *		theta = angle between agent heading and player position
	 *		phi   = angle between agent heading and player heading
	 */

	float theta = acosf(D3DXVec3Dot(&relPos, &agentHeading));
	float phi   = acosf(D3DXVec3Dot(&agentHeading, &playerHeading));

	if (theta != theta)
	{
		theta = 0.0f;
	}

	if (UndefinedResult(phi))
	{
		std::cout << "Uh oh phi" << std::endl;
	}

	//If target is behind the agent within range
	if (theta > D3DX_PI/2 && 
		viewDistanceSq > distanceSq)
	{
		//If target is heading at the agent
		if (phi < D3DX_PI / 8)
		{
			//Send evasion message
			Message* m = new Message(agent->GetId(), agent->GetId(), 0.0f, EVADE, m_Player);
			POST->AddMail(m);
		}
	}

	//If within the outer radius
	if (distanceSq < outerRadius)
	{
		ship->GetSteering()->FaceForward3DOff();
		ship->GetSteering()->FacePoint3DOn();
	}

	//If within the inner radius
	if (distanceSq < innerRadius)
	{
		/**
		 * Aim at the player
		 */
		//agent->GetSteering()->FaceForward3DOff();
		//agent->GetSteering()->FacePoint3DOn();

		/**
		 * Create a random point on a unit sphere, project ahead, and set seek target
		 */
		Vector3 straffTarget;
		straffTarget.x = RandomBinomial();
		straffTarget.y = RandomBinomial();
		straffTarget.z = RandomBinomial();
		straffTarget.Normalize();
		straffTarget *= 200;	//Radius 200

		//Calculate the projection vector
		Vector3 projection = agentHeading;
		projection *= 1000;		//Project 500

		//Calculate target
		projection += straffTarget;

		//Set seek target
		agent->GetSteering()->SetTargetSeek(projection);

		/**
		 * Approach is now false, and we need the agent to seek new target
		 */

		agent->SetStraff(false);

		Message* m = new Message(agent->GetId(), agent->GetId(), 0.0f, MOVE);
 		POST->AddMail(m);
	}

	//If target is within the firing solution
	if (theta < D3DX_PI / 8 && 
		distanceSq < 2500.0f)
	{
		//Firing command
		((Ship*)agent->GetSelf()->Gameplay)->Fire(agent->GetPosition(), agentHeading);
	}
}

void FighterAttack::LeaveState(ShipAI* ship)
{
	//Turn off all algorithms
	ship->GetSteering()->SteeringOff(); 
}

void FighterAttack::HandleMessage(ShipAI* ship, Message* m)
{
	switch(m->message)
	{
	case NO_MESSAGE:
		{
		//Do nothing

		break;
		}
	case SELF_UPDATE:
		{
		//ship->SetUpdate(true);

		break;
		}
	case ATTACK:
		{
		//Do nothing

		break;
		}
	case EVADE:
		{

		//ship->ChangeState(FighterEvade::Instance());

		break;
		}
	case HIT:
		{

		//ship->ChangeState(FighterEvade::Instance());

		break;
		}
	case MOVE:
		{
		ship->ChangeState(FighterMove::Instance());

		break;
		}
	default:
		{
		//Do nothing

		break;
		}
	}
}

void FighterAttack::SetPlayer(Actor* player)
{
	m_Player = player;
}

Actor* FighterAttack::GetPlayer(void)
{
	return m_Player;
}
#include "CruiserMove.h"
#include "CruiserAttack.h"
#include "CruiserEvade.h"

#include "..\AIObject\ShipAI.h"
#include "..\AIObject\AIShipType.h"
#include "..\Messaging\PostOffice.h"
#include <random>
#include <chrono>
#include "..\Fuzzy\FuzzySystem.h"
#include "..\..\FRAMEWORK\GameFramework.h"
#include "..\Influence\InfluenceSystem.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"
#include "..\Utility\AIUtility.h"

void CruiserMove::Initialize(ShipAI* ship)
{
}

void CruiserMove::Update(ShipAI* ship, float dt)
{

}

void CruiserMove::LeaveState(ShipAI* ship)
{
}

void CruiserMove::HandleMessage(ShipAI* ship, Message* m)
{

}
#pragma once

#include "BaseState.h"

class ShipAI;

class CruiserAttack : public BaseState<ShipAI>
{
private:
	CruiserAttack() {}

public:
	static CruiserAttack* Instance()
	{
		static CruiserAttack instance;
		return &instance;
	}

	~CruiserAttack(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;
	
};
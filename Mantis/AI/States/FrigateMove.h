#pragma once

#include "BaseState.h"

class ShipAI;

class FrigateMove : public BaseState<ShipAI>
{
private:
	FrigateMove() {}

public:
	static FrigateMove* Instance()
	{
		static FrigateMove instance;
		return &instance;
	}

	~FrigateMove(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;

};
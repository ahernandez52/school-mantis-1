/**
 * StateMachine - Declares a templated state machine which can be used for all program FSM's
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "BaseState.h"

template <class owner_type>
class StateMachine
{
public:
	owner_type* mOwner;

	BaseState<owner_type>*	m_Current;
	BaseState<owner_type>*	m_Previous;

	StateMachine(owner_type* owner) : mOwner(owner),
		m_Current(0), m_Previous(0)
	{
	}

	virtual ~StateMachine(void)
	{
	}

	void SetCurrent(BaseState<owner_type>* s)
	{
		m_Current = s;
		m_Current->Initialize(mOwner);
	}

	void SetPrevious(BaseState<owner_type>* s)
	{
		m_Previous = s;
	}

	void ChangeState(BaseState<owner_type>* newState)
	{
		m_Current->LeaveState(mOwner);
		m_Previous = m_Current;
		m_Current = newState;
		m_Current->Initialize(mOwner);
	}

	void RevertState(void)
	{
		ChangeState(m_Previous);
	}

	virtual void Update(float dt)
	{
		if (m_Current)
		{
			m_Current->Update(mOwner, dt);
		}
	}

	void HandleMessage(Message* m)
	{
		m_Current->HandleMessage(mOwner, m);
	}
};
#include "FighterMove.h"
#include "FighterAttack.h"
#include "FighterEvade.h"

#include "../AIObject/ShipAI.h"
#include "../Messaging/PostOffice.h"
#include "../Steering/SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GAMEPLAY/Object/Player.h"

#include"../Utility/AIUtility.h"

#include <random>
#include <chrono>

void FighterMove::Initialize(ShipAI* ship)
{
	//Arrive target position
	ship->GetSteering()->SeekOn();

	////Wander (LIGHT)
	//ship->GetSteering()->WanderOn();
	//ship->GetSteering()->SetWanderRadius(50.0f);
	//ship->GetSteering()->SetWanderOffset(100.0f);
	//ship->GetSteering()->SetWanderJitter(5.0f);
	//ship->GetSteering()->SetWeightWander(0.2f);

	//Obstacle avoidance on
	//ship->GetSteering()->ObstacleAvoidanceOn();

	//Face player
	ship->GetSteering()->FaceForward3DOn();

}

void FighterMove::Update(ShipAI* ship, float dt)
{
	//Update face point target
	ship->GetSteering()->SetTargetPoint3D(GetPlayer()->m_Position);

	ShipAI* agent = ship;
	Actor* player = m_Player;

	float playerRadius = 500.0f * 500.0f;
	float outerRadius = 500.0f * 500.0f;
	float innerRadius = 100.0f * 100.0f;

	//Get spatial information
	D3DXVECTOR3 relTargetPos = agent->GetSteering()->GetTargetSeek() - agent->GetPosition();
	float distanceSq = D3DXVec3LengthSq(&relTargetPos);

	D3DXVECTOR3 relPos = player->m_Position - agent->GetPosition();

	Quaternion playerOrientation = player->m_Orientation;
	Quaternion agentOrientation = agent->GetCurrentOrientation();

	D3DXVECTOR3 playerHeading = playerOrientation.Forward();
	D3DXVECTOR3 agentHeading  = -agentOrientation.Forward();

	D3DXVec3Normalize(&relTargetPos, &relTargetPos);
	D3DXVec3Normalize(&relPos, &relPos);
	D3DXVec3Normalize(&playerHeading, &playerHeading);
	D3DXVec3Normalize(&agentHeading, &agentHeading);

	/**
	 * Where : 
	 *		theta = angle between agent heading and player position
	 *		phi   = angle between agent heading and player heading
	 */

	float theta = acosf(D3DXVec3Dot(&relPos, &agentHeading));

	if (D3DXVec3LengthSq(&relPos) > playerRadius)
	{
		agent->GetSteering()->FacePoint3DOn();
		//agent->GetSteering()->FaceForward3DOn();
	}

	if (distanceSq < innerRadius)
	{
		//agent->GetSteering()->SeekOff();
		//agent->GetSteering()->PursueOn();
		//agent->SetStraff(true);

		Message* m = new Message(agent->GetId(), agent->GetId(), 0.0f, ATTACK);
		POST->AddMail(m);
	}

	//If target is within the firing solution
	if (theta < D3DX_PI / 8 &&
		distanceSq < 2500.0f * 2500.0f)
	{
		//Firing command
		((Ship*)agent->GetSelf()->Gameplay)->Fire(agent->GetPosition(), agentHeading);
	}
}

void FighterMove::LeaveState(ShipAI* ship)
{
	ship->GetSteering()->SteeringOff();
}

void FighterMove::HandleMessage(ShipAI* ship, Message* m)
{
	switch(m->message)
	{
	case NO_MESSAGE:

		//Do nothing

		break;

	case SELF_UPDATE:
		//ship->SetUpdate(true);

		break;

	case ATTACK:
		
		ship->ChangeState(FighterAttack::Instance());

		break;

	case EVADE:

		//ship->ChangeState(FighterEvade::Instance());

		break;

	case HIT:

		//ship->ChangeState(FighterEvade::Instance());

		break;

	default:

		//Do nothing

		break;
	}
}

void FighterMove::SetPlayer(Actor* player)
{
	m_Player = player;
}

Actor* FighterMove::GetPlayer(void)
{
	return m_Player;
}
/**
 * Fighter Move - Move a fighter to a position
 * Does not attack unless target moves into firing solution
 **/

#pragma once 

#include "BaseState.h"

class ShipAI;
class Actor;

class FighterMove : public BaseState<ShipAI>
{
private:
	FighterMove() {}

	Actor* m_Player;
public:
	static FighterMove* Instance()
	{
		static FighterMove instance;
		return &instance;
	}
	
	~FighterMove(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;
	
	/**
	 * Handle messages sent to this system
	 */
	void HandleMessage(ShipAI* ship, Message* m) override;

	void SetPlayer(Actor* player);
	Actor* GetPlayer(void);
};
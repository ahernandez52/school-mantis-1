#pragma once

#include "BaseState.h"

class ShipAI;

class CruiserMove : public BaseState<ShipAI>
{
private:
	CruiserMove() {}

public:
	static CruiserMove* Instance()
	{
		static CruiserMove instance;
		return &instance;
	}

	~CruiserMove(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;
	
};
#pragma once

#include "BaseState.h"

class ShipAI;

class FrigateEvade : public BaseState<ShipAI>
{
private:
	FrigateEvade() {}

public:
	static FrigateEvade* Instance()
	{
		static FrigateEvade instance;
		return &instance;
	}

	~FrigateEvade(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	void HandleMessage(ShipAI* ship, Message* m) override;

};
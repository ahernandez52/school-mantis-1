#include "FighterEvade.h"
#include "FighterAttack.h"
#include "FighterMove.h"

#include "../AIObject/ShipAI.h"
#include "../Messaging/PostOffice.h"
#include "../Steering/SteeringBehavior.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GAMEPLAY/Object/Player.h"

#include"..\Utility\AIUtility.h"

#include <random>
#include <chrono>



void FighterEvade::Initialize(ShipAI* ship)
{
	//Evade player
	ship->GetSteering()->SetTargetAgentEvade(GetPlayer()->AI);
	ship->GetSteering()->SetTargetFlee(GetPlayer()->m_Position);
	ship->GetSteering()->FleeOn();

	//Wander (HEAVY)
	ship->GetSteering()->WanderOn();
	ship->GetSteering()->SetWanderRadius(90.0f);
	ship->GetSteering()->SetWanderOffset(100.0f);
	ship->GetSteering()->SetWanderJitter(5.0f);
	ship->GetSteering()->SetWeightWander(1.5f);

	//Avoid obstacles
	ship->GetSteering()->ObstacleAvoidanceOn();

	//Face forward
	ship->GetSteering()->FaceForward3DOn();
}

void FighterEvade::Update(ShipAI* ship, float dt)
{
	ShipAI* agent = ship;
	Actor* player = m_Player;

	//Get spatial information
	D3DXVECTOR3 relPos = player->m_Position - agent->GetPosition();
	float distanceSq = D3DXVec3LengthSq(&relPos);
	float viewDistanceSq = agent->GetSteering()->GetViewDistance();
	viewDistanceSq *= viewDistanceSq;

	Quaternion agentOrientation = agent->GetCurrentOrientation();
	Quaternion playerOrientation = player->m_Orientation;

	D3DXVECTOR3 playerHeading = playerOrientation.Forward();
	D3DXVECTOR3 agentHeading = -agentOrientation.Forward();

	D3DXVec3Normalize(&relPos, &relPos);
	D3DXVec3Normalize(&playerHeading, &playerHeading);
	D3DXVec3Normalize(&agentHeading, &agentHeading);

	/**
	 * Where : 
	 *		theta = angle between agent heading and player position
	 *		phi   = angle between agent heading and player heading
	 */

	float theta = acosf(D3DXVec3Dot(&relPos, &agentHeading));
	float phi   = acosf(D3DXVec3Dot(&agentHeading, &playerHeading));


	//If target is behind the agent within range
	if (theta > D3DX_PI /2)
	{
		//If target is heading at the agent
		if (phi < D3DX_PI / 4)
		{
			m_EvadeTimer -= dt;

			if (m_EvadeTimer <= 0.0f)
			{
				//Create a random position towards which we should bank
				ship->GetSteering()->SetWanderTarget(RandomTarget(ship));
				m_EvadeTimer = 3.0f;
			}
		}
	}

	//If target is in front or out of range
	if (theta < D3DX_PI / 4 ||
		viewDistanceSq < distanceSq)
	{
		//Send attack message
		Message* m = new Message(agent->GetId(), agent->GetId(), 0.0f, ATTACK);
		POST->AddMail(m);
	}

	//Update the flee target
	ship->GetSteering()->SetTargetFlee(GetPlayer()->m_Position);
}

void FighterEvade::LeaveState(ShipAI* ship)
{
	ship->GetSteering()->SteeringOff();
}

void FighterEvade::HandleMessage(ShipAI* ship, Message* m)
{
	switch(m->message)
	{
	case NO_MESSAGE:

		break;

	case SELF_UPDATE:

		ship->SetUpdate(true);

		break;

	case ATTACK:

		ship->ChangeState(FighterAttack::Instance());
		
		break;

	case EVADE:

		break;

	case MOVE:

		ship->ChangeState(FighterMove::Instance());

	case HIT:

		break;

	default:

		break;
	}
}

void FighterEvade::SetPlayer(Actor* player)
{
	m_Player = player;
}

Actor* FighterEvade::GetPlayer(void)
{
	return m_Player;
}

Vector3 FighterEvade::RandomTarget(ShipAI* ship)
{
		Vector3 target;
		target.x = RandomBinomial();
		target.y = RandomBinomial();
		target.z = RandomBinomial();
		target.Normalize();

		target *= ship->GetSteering()->GetWanderRadius();

		return target;
}
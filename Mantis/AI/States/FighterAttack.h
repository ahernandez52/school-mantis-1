/*
 * FighterAttack - Attack state moves a fighter to a position with the 
 * intention of attacking if enemies are in the area
 */

#pragma once 

#include "BaseState.h"


class ShipAI;
class Actor;

class FighterAttack : public BaseState<ShipAI>
{
private:
	FighterAttack() {}

	Actor* m_Player;	//Pointer to the player

public:
	static FighterAttack* Instance()
	{
		static FighterAttack instance;
		return &instance;
	}
	
	~FighterAttack(void) override {}

	void Initialize(ShipAI* ship) override;
	void Update(ShipAI* ship, float dt) override;
	void LeaveState(ShipAI* ship) override;

	/**
	 * Handle messages sent to this system
	 */
	void HandleMessage(ShipAI* ship, Message* m) override;

	void SetPlayer(Actor* player);	//Set m_Player
	Actor* GetPlayer(void);			//Get m_Player

};
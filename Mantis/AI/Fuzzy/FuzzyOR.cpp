#include "FuzzyOR.h"

FuzzyOR::FuzzyOR(const FuzzyOR& FOR){
	for(const auto &f : FOR.m_Terms){
		m_Terms.push_back(f->Clone());
	}
}

FuzzyOR::FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
}

FuzzyOR::FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
	m_Terms.push_back(term3->Clone());
}

FuzzyOR::FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3, FuzzyTerm* term4){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
	m_Terms.push_back(term3->Clone());
	m_Terms.push_back(term4->Clone());
}

FuzzyOR::~FuzzyOR(){
	for(unsigned int x = 0; x < m_Terms.size(); x++){
		FuzzyTerm* temp = m_Terms[x];
		m_Terms[x] = 0;
		delete temp;
	}
	m_Terms.clear();
}

void FuzzyOR::ClearDOM(){
	for(auto &f : m_Terms)
		f->ClearDOM();
}

float FuzzyOR::GetDOM()const{
	float largest = FLT_MIN;

	for(const auto &f : m_Terms){
		if(f->GetDOM() > largest)
			largest = f->GetDOM();
	}

	return largest;
}
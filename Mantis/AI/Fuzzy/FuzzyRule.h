#pragma once

#include "FuzzyTerm.h"
#include "FuzzyAND.h"
#include "FuzzyOR.h"
#include "FuzzyProxy.h"


/**	\brief FuzzyRule Class
 *	
 *	The FuzzyRule class holds pointers to it's antecedent and consequent that describes the rule.
 */
class FuzzyRule
{
public:
	FuzzyTerm* m_Antecedent;
	FuzzyTerm* m_Consequent;
public:
	FuzzyRule(FuzzyTerm* ant, FuzzyTerm* con);
	~FuzzyRule();

	float GetDOM();
};

#pragma once

#include "FuzzyMemberFunction.h"


/**	
 *	FuzzyMemberFunction class that defines the a Triangle function.
 */
class FuzzyMemberFunction_Triangle : public FuzzyMemberFunction
{
private:
	float m_Left;
	float m_Center;
	float m_Right;

public:
	FuzzyMemberFunction_Triangle();
	FuzzyMemberFunction_Triangle(float left, float center, float right);
	~FuzzyMemberFunction_Triangle();

	void SetLeft(float n) override final {m_Left = n;}
	void SetCenter(float n) override final {m_Center = n;}
	void SetRight(float n) override final {m_Right = n;}

	void SetDOM(float n) override final {m_DOM = n;}
	void ClearDOM() override final {m_DOM = 0;}

	float CalculateDOM(float num) override final;
	float DefuzzifyDOM(float num) override final;

	float ReturnLeft() override final {return m_Left;}
	float ReturnCenter() override final {return m_Center;}
	float ReturnRight() override final {return m_Right;}

	float GetDOM() override final {return m_DOM;}
};

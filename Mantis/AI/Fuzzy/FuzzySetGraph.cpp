#include "FuzzySetGraph.h"
#include "FuzzyProxy.h"

FuzzySetGraph::FuzzySetGraph(void){}

FuzzySetGraph::~FuzzySetGraph(void)
{
	ClearGraph();
}

void FuzzySetGraph::DetermineDOM(float num)
{
	for(const auto &f : Functions){
		f->CalculateDOM(num);
	}
}

float FuzzySetGraph::CalculateDefuzzifiedValue()
{
	float val = 0;
	int divide = 0;

	for(const auto &f : Functions){
		val += f->DefuzzifyDOM(f->GetDOM());
		divide++;
	}
	val /= divide;
	return val;
}

std::vector<float>* FuzzySetGraph::DetermineDOM(float num, std::vector<float>* DOMs)
{

	for(const auto &f : Functions){
		DOMs->push_back(f->CalculateDOM(num));
	}

	return DOMs;
}

FuzzyProxy FuzzySetGraph::AddFunction_LHShoulder(float left, float center, float right)
{
	FuzzyMemberFunction_LHShoulder* temp = new FuzzyMemberFunction_LHShoulder(left, center, right);
	Functions.push_back(temp);
	FuzzyProxy proxy(temp);
	return proxy;
}

FuzzyProxy FuzzySetGraph::AddFunction_RHShoulder(float left, float center, float right)
{
	FuzzyMemberFunction_RHShoulder* temp = new FuzzyMemberFunction_RHShoulder(left, center, right);
	Functions.push_back(temp);
	FuzzyProxy proxy(temp);
	return proxy;
}

FuzzyProxy FuzzySetGraph::AddFunction_Triangle(float left, float center, float right)
{
	FuzzyMemberFunction_Triangle* temp = new FuzzyMemberFunction_Triangle(left, center, right);
	Functions.push_back(temp);
	FuzzyProxy proxy(temp);
	return proxy;
}

void FuzzySetGraph::ClearGraph()
{
	for(auto &f : Functions){
		delete f;
	}
	Functions.clear();
}
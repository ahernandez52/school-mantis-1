#include "FuzzyMemberFunction_LHShoulder.h"

FuzzyMemberFunction_LHShoulder::FuzzyMemberFunction_LHShoulder(){
	m_Left = 0.0f;
	m_Center = 0.0f;
	m_Right = 0.0f;
}

FuzzyMemberFunction_LHShoulder::FuzzyMemberFunction_LHShoulder(float left, float center, float right){
	m_Left = left;
	m_Center = center;
	m_Right = right;
}

FuzzyMemberFunction_LHShoulder::~FuzzyMemberFunction_LHShoulder(){}

float FuzzyMemberFunction_LHShoulder::CalculateDOM(float num){
	//check if num is on the flat part of the shoulder
	if(num <= m_Center)
		m_DOM = 1.0f;
	//check if num is not on the function anywhere
	else if(num >= m_Right)
		m_DOM = 0.0f;
	//otherwise calculate num on the line
	else{
		//line p1(m_Center, 1) p2(m_Right, 0)
		//get slope of the line
		float slope = -1 / (m_Right - m_Center);
		//get y intercept of the line
		float b = -(slope * m_Right);
		//return DOM from num
		m_DOM = (slope * num) + b;
	}
	return m_DOM;
}

float FuzzyMemberFunction_LHShoulder::DefuzzifyDOM(float num){
	if(num == 1)
		return (m_Left + m_Center) / 2;
	else if(num == 0)
		return (m_Left + m_Right) / 2;
	else{
		//line p1(m_Center, 1) p2(m_Right, 0)
		//get slope of the line
		float slope = -1 / (m_Right - m_Center);
		//get y intercept of the line
		float b = -(slope * m_Right);
		//return DOM from num
		float right = (num - b) / slope;

		return (m_Left + right) / 2;
	}
}

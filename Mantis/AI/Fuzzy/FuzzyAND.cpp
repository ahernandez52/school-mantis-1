#include "FuzzyAND.h"

FuzzyAND::FuzzyAND(const FuzzyAND& FAND){
	for(const auto &f : FAND.m_Terms){
		m_Terms.push_back(f->Clone());
	}
}

FuzzyAND::FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
}

FuzzyAND::FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
	m_Terms.push_back(term3->Clone());
}

FuzzyAND::FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3, FuzzyTerm* term4){
	m_Terms.push_back(term1->Clone());
	m_Terms.push_back(term2->Clone());
	m_Terms.push_back(term3->Clone());
	m_Terms.push_back(term4->Clone());
}

FuzzyAND::~FuzzyAND(){
	for(unsigned int x = 0; x < m_Terms.size(); x++){
		FuzzyTerm* temp = m_Terms[x];
		m_Terms[x] = 0;
		delete temp;
	}
	m_Terms.clear();
}

void FuzzyAND::ClearDOM(){
	for(auto &f : m_Terms)
		f->ClearDOM();
}

float FuzzyAND::GetDOM()const{
	float smallest = FLT_MAX;

	for(const auto &f : m_Terms){
		if(f->GetDOM() < smallest)
			smallest = f->GetDOM();
	}

	return smallest;
}
#include "FuzzyMemberFunction_RHShoulder.h"

FuzzyMemberFunction_RHShoulder::FuzzyMemberFunction_RHShoulder(){
	m_Left = 0.0f;
	m_Center = 0.0f;
	m_Right = 0.0f;
}

FuzzyMemberFunction_RHShoulder::FuzzyMemberFunction_RHShoulder(float left, float center, float right){
	m_Left = left;
	m_Center = center;
	m_Right = right;
}

FuzzyMemberFunction_RHShoulder::~FuzzyMemberFunction_RHShoulder(){}

float FuzzyMemberFunction_RHShoulder::CalculateDOM(float num){
	//check if num is on the flat part of the shoulder
	if(num >= m_Center)
		m_DOM = 1.0f;
	//check if num is not on the function anywhere
	else if(num <= m_Left)
		m_DOM = 0.0f;
	//otherwise calculate num on the line
	else{
		//line p1(m_Left, 0) p2(m_Center, 1)
		//get slope of line	
		float slope = 1 / (m_Center - m_Left);
		//get y intercept of the line
		float b = -(slope * m_Left);
		//return DOM from num
		m_DOM = (slope * num) + b;
	}
	return m_DOM;
}

float FuzzyMemberFunction_RHShoulder::DefuzzifyDOM(float num){
	if(num == 1){
		return (m_Center + m_Right) / 2;
	}
	else if(num == 0){
		return (m_Left + m_Right) / 2;
	}
	else{
		//line
		//get slope of line	
		float slope = 1 / (m_Center - m_Left);
		//get y intercept of the line
		float b = -(slope * m_Left);
		//return DOM from num
		float left = (num - b) / slope;

		return (left + m_Right) / 2;
	}
}

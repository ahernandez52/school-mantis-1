#pragma once

#include "FuzzyTerm.h"
#include "FuzzyMemberFunction.h"


/**	\brief FuzzyProxy Class
 *	
 *	The FuzzyProxy class acts as a proxy for the FuzzyMemberFunction it has a pointer to.
 *	It uses the proxy pattern to make easier the process of determining the result of a FuzzyRule.
 */
class FuzzyProxy : public FuzzyTerm
{
private:
	FuzzyMemberFunction* m_Function;
public:
	FuzzyProxy(FuzzyMemberFunction* function):m_Function(function){}
	~FuzzyProxy() override final {}

	FuzzyProxy* Clone()const override final {return new FuzzyProxy(*this);}

	void ClearDOM() override final {m_Function->ClearDOM();}

	float GetDOM()const override final {return m_Function->GetDOM();}

	void DeFuzzHelper(float n) override final
	{
		if(n > m_Function->GetDOM())
			m_Function->SetDOM(n);
	}
};
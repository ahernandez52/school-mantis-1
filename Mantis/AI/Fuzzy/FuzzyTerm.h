#pragma once


/**	
 *	Pure virtual FuzzyTerm class for handling Fuzzy Rules.
 */
class FuzzyTerm
{
public:
	virtual ~FuzzyTerm(){}

	virtual FuzzyTerm* Clone()const = 0;

	virtual void ClearDOM(){}
	
	virtual float GetDOM()const = 0;

	virtual void DeFuzzHelper(float num){}
};

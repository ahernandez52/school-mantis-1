#include "FuzzyMemberFunction_Triangle.h"

FuzzyMemberFunction_Triangle::FuzzyMemberFunction_Triangle(){
	m_Left = 0.0f;
	m_Center = 0.0f;
	m_Right = 0.0f;
}

FuzzyMemberFunction_Triangle::FuzzyMemberFunction_Triangle(float left, float center, float right){
	m_Left = left;
	m_Center = center;
	m_Right = right;
}

FuzzyMemberFunction_Triangle::~FuzzyMemberFunction_Triangle(){}

float FuzzyMemberFunction_Triangle::CalculateDOM(float num){
	//if the num is at the senter point, DOM is 1
	if(num == m_Center)
		m_DOM = 1.0f;
	//if num is not on the function, DOM is 0
	else if(num < m_Left || num > m_Right)
		m_DOM = 0.0f;
	//else calculate the two lines
	else if(num < m_Center){
		//line p1(m_Left, 0) p2(m_Center, 1)
		//get slope of line	
		float slope = 1 / (m_Center - m_Left);
		//get y intercept of the line
		float b = -(slope * m_Left);
		//return DOM from num
		m_DOM = (slope * num) + b;
	}
	else{
		//line p1(m_Center, 1) p2(m_Right, 0)
		//get slope of the line
		float slope = -1 / (m_Right - m_Center);
		//get y intercept of the line
		float b = -(slope * m_Right);
		//return DOM from num
		m_DOM = (slope * num) + b;
	}
	return m_DOM;
}

float FuzzyMemberFunction_Triangle::DefuzzifyDOM(float num){
	if(num == 1)
		return m_Center;
	else if(num == 0)
		return (m_Left + m_Right) / 2;
	else{
		//line 1
		//get slope of line	
		float slope = 1 / (m_Center - m_Left);
		//get y intercept of the line
		float b = -(slope * m_Left);
		//return DOM from num
		float left = (num - b) / slope;

		//line 2
		//get slope of the line
		slope = -1 / (m_Right - m_Center);
		//get y intercept of the line
		b = -(slope * m_Right);
		//return DOM from num
		float right = (num - b) / slope;

		return (left + right) / 2;
	}
}

#pragma once

#include <vector>
#include "FuzzyTerm.h"


/**	\brief FuzzyOR Class
 *	
 *	The FuzzyOR class is used to calculate the OR value of multiple FuzzyTerms.
 */
class FuzzyOR : public FuzzyTerm
{
private:
	std::vector<FuzzyTerm*> m_Terms;
public:
	FuzzyOR(const FuzzyOR& FOR);
	FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2);
	FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3);
	FuzzyOR(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3, FuzzyTerm* term4);

	~FuzzyOR() override final;

	void ClearDOM() override final;
	void DeFuzzHelper(float n) override final {}
	float GetDOM()const override final;
	FuzzyTerm* Clone()const override final {return new FuzzyOR(*this);}
};
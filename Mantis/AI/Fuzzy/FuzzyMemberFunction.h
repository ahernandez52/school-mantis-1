#pragma once


/**	
 *	Pure virtual base class for the FuzzyMemberFunctions.
 */
class FuzzyMemberFunction
{
protected:
	float m_DOM;
public:
	FuzzyMemberFunction(){ m_DOM = 0.0f; }
	~FuzzyMemberFunction(){}

	virtual void SetLeft(float n){}
	virtual void SetCenter(float n){}
	virtual void SetRight(float n){}

	virtual void SetDOM(float n){m_DOM = n;}
	virtual void ClearDOM(){m_DOM = 0;}

	virtual float CalculateDOM(float num) = 0;
	virtual float DefuzzifyDOM(float num) = 0;

	virtual float ReturnLeft(){return 0.0f;}
	virtual float ReturnCenter(){return 0.0f;}
	virtual float ReturnRight(){return 0.0f;}

	virtual float GetDOM(){return m_DOM;}
};

#pragma once

#include "FuzzySetGraph.h"
#include "FuzzyRule.h"
#include <map>
#include <string>


/**	\brief FuzzySystem Class
 *	
 *	The FuzzySystem class creates and handles all the items required of in a Fuzzy system.
 *	It creates the Rules and Graphs, and can calculate the final crisp result.
 */
class FuzzySystem
{
public:
	std::map<std::string, FuzzySetGraph*> m_Graphs;
	std::vector<FuzzyRule*> m_Rules;
public:
	FuzzySystem();
	~FuzzySystem();

	//creates a fuzzy set graph and returns a pointer to that graph
	FuzzySetGraph* CreateSetGraph(std::string FuzzySet);
	//adds a fuzzy set graph to the graph list
	void AddSetGraph(std::string FuzzySet, FuzzySetGraph* graph);
	//create fuzzy rule with antecedent and consequent and returns a pointer to that rule
	FuzzyRule* CreateRule(FuzzyTerm* antecedent, FuzzyTerm* consequent);
	//adds a fuzzy rule to the rule list
	void AddRule(FuzzyRule* rule);

	//fuzzify number with specific set graph
	void FuzzifyNumber(std::string FuzzySetKey, float num);

	//apply rule logic
	void DoRuleLogic();
	
	//defuzzication || DesirabilitySetKey is the string used to identify the Desirability Set Graph on creation
	float DefuzzifyNum(std::string DesirabilitySetKey);

	//resets all DOMs in the graphs
	void ResetSystem();

	//releases all pointers
	void Release();
};


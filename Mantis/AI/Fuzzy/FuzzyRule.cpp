#include "FuzzyRule.h"

FuzzyRule::FuzzyRule(FuzzyTerm* ant, FuzzyTerm* con)
{
	m_Antecedent = ant->Clone();
	m_Consequent = con->Clone();
}

FuzzyRule::~FuzzyRule()
{
	delete m_Antecedent;
	delete m_Consequent;
}

float FuzzyRule::GetDOM()
{
	return m_Antecedent->GetDOM();
}
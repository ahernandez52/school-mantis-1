#pragma once

#include <vector>
#include "FuzzyTerm.h"


/**	\brief FuzzyAND Class
 *	
 *	The FuzzyAND class is used to calculate the AND value of multiple FuzzyTerms.
 */
class FuzzyAND : public FuzzyTerm
{
private:
	std::vector<FuzzyTerm*> m_Terms;
public:
	FuzzyAND(const FuzzyAND& FAND);
	FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2);
	FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3);
	FuzzyAND(FuzzyTerm* term1, FuzzyTerm* term2, FuzzyTerm* term3, FuzzyTerm* term4);

	~FuzzyAND() override final;

	void ClearDOM() override final;
	void DeFuzzHelper(float n) override final {}
	float GetDOM()const override final;
	FuzzyTerm* Clone()const override final {return new FuzzyAND(*this);}
};
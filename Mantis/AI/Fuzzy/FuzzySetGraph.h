#pragma once

#include "FuzzyMemberFunction.h"
#include "FuzzyMemberFunction_LHShoulder.h"
#include "FuzzyMemberFunction_RHShoulder.h"
#include "FuzzyMemberFunction_Triangle.h"
#include "FuzzyProxy.h"
#include <vector>


/**	\brief FuzzySetGraph Class
 *	
 *	The FuzzySetGraph class creates, and holds the FuzzyMemberFunction inherited objects.
 *	It calculates the DOM for each FuzzyMemberFunction it holds.
 */
class FuzzySetGraph
{
public:
	std::vector<FuzzyMemberFunction*> Functions;
public:
	FuzzySetGraph(void);
	~FuzzySetGraph(void);

	void DetermineDOM(float num);
	float CalculateDefuzzifiedValue();
	std::vector<float>* DetermineDOM(float num, std::vector<float>* DOMs);

	FuzzyProxy AddFunction_LHShoulder(float left, float center, float right);
	FuzzyProxy AddFunction_RHShoulder(float left, float center, float right);
	FuzzyProxy AddFunction_Triangle(float left, float center, float right);

private:
	void ClearGraph();
};

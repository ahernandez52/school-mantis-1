#include "FuzzySystem.h"

FuzzySystem::FuzzySystem(){}

FuzzySystem::~FuzzySystem()
{
	Release();
}

FuzzySetGraph* FuzzySystem::CreateSetGraph(std::string FuzzySet)
{
	FuzzySetGraph* g = new FuzzySetGraph();
	m_Graphs.emplace(FuzzySet, g);
	return g;
}

void FuzzySystem::AddSetGraph(std::string FuzzySet, FuzzySetGraph* graph)
{
	m_Graphs.emplace(FuzzySet, graph);
}

FuzzyRule* FuzzySystem::CreateRule(FuzzyTerm* antecedent, FuzzyTerm* consequent)
{
	FuzzyRule* r = new FuzzyRule(antecedent, consequent);
	m_Rules.push_back(r);
	return r;
}

void FuzzySystem::AddRule(FuzzyRule* rule)
{
	m_Rules.push_back(rule);
}

void FuzzySystem::FuzzifyNumber(std::string FuzzySetKey, float num)
{
	//fuzzify a number and the DOMs are put into the fuzzyset
	m_Graphs.find(FuzzySetKey)->second->DetermineDOM(num);
}

void FuzzySystem::DoRuleLogic()
{
	//reset all consequents
	for(auto &r : m_Rules)
		r->m_Consequent->ClearDOM();

	//fill the fuzzy associative matrix and keep track of the highest value for each consequent
	for(const auto &r : m_Rules)
		r->m_Consequent->DeFuzzHelper(r->GetDOM());
}

float FuzzySystem::DefuzzifyNum(std::string DesirabilitySetKey)
{
	DoRuleLogic();

	return m_Graphs.find(DesirabilitySetKey)->second->CalculateDefuzzifiedValue();
}

void FuzzySystem::ResetSystem()
{
	for(auto &r : m_Rules){
		r->m_Antecedent->ClearDOM();
		r->m_Consequent->ClearDOM();
	}
}

void FuzzySystem::Release(){
	for(auto it = m_Graphs.begin(); it != m_Graphs.end(); it++)
	{
		FuzzySetGraph* temp = it->second;
		it->second = 0;
		delete temp;
	}
	for(unsigned int x = 0; x < m_Rules.size(); x++)
	{
		FuzzyRule* temp = m_Rules[x];
		m_Rules[x] = 0;
		delete temp;
	}

	m_Graphs.clear();
	m_Rules.clear();
}
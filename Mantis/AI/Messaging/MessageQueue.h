#pragma once

/**	\brief MessageQueue Class
 *	
 *	The MessageQueue class acts as a wrapper for a forward_list that keeps track of all the current AI messages.
 *	The forward_list acts as a priority queue, but allows us to iterate through it.
 *	The list is sorted by Message::timeUntilDelivery smallest to largest.
 */

#include "Message.h"
#include <list>
#include <forward_list>


class MessageQueue
{
private:
	std::forward_list<Message*> q;
	int qSize;

public:
	MessageQueue();
	~MessageQueue();

	/**
	 *	Add a pre-existing message to the queue
	 */
	void AddMessage(Message* m);

	/**	
	 *	Returns a pointer to the message at the front of the queue.
	 */
	Message* GetFront();

	/**	
	 *	Returns a pointer to the message at the front of the queue and removes the message from the queue.
	 */
	Message* TakeFront();

	/**	
	 *	Removes the message at the front of the queue and then deletes it.
	 */
	void RemoveFront();

	/**	
	 *	Clears the queue of all messages and deletes the messages.
	 */
	void ClearQueue();

	/**	
	 *	Updates each message's timeUntilDelivery.
	 */
	void UpdateMessageTime(float dt);

	/**	
	 *	Returns the size of the queue.
	 */
	int GetSize();

	/**	
	 *	Returns true if the queue is empty.
	 */
	bool IsEmpty();

	/**	
	 *	Removes any messages in the queue that has their "toID" variable as ID.
	 */
	void RemoveMessagesWithToID(int ID);
};


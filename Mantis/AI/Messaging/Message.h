#pragma once

/**	
 *	Enumeration for what the message does.
 */

enum MESSAGE
{
	NO_MESSAGE = -1,

	SELF_UPDATE = 0,
	ATTACK = 1,
	EVADE = 2, 
	HIT = 3, 
	MOVE = 4
};

/**	
 *	Message Class
 */

class Message
{
public:
	int fromID;				///< ID of the actor that sent the message.
	int toID;				///< ID of the actor to receive the message.
	float timeToDelivery;	///< Time until the message is delivered.
	MESSAGE message;		///< Enum of what the message tells the actor to do.
	void* p;				///< Void pointer for any other required information.

	Message();
	Message(int from, int to, float timeUntilDelivery, MESSAGE mess);
	Message(int from, int to, float timeUntilDelivery, MESSAGE mess, void* voidp);
	~Message();
};


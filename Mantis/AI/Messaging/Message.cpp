#include "Message.h"


Message::Message()
{
	fromID = -1;
	toID = -1;
	timeToDelivery = -1;
	message = NO_MESSAGE;
	p = 0;
}

Message::Message(int from, int to, float timeUntilDelivery, MESSAGE mess)
{
	fromID = from;
	toID = to;
	timeToDelivery = timeUntilDelivery;
	message = mess;
	p = 0;
}

Message::Message(int from, int to, float timeUntilDelivery, MESSAGE mess, void* voidp)
{
	fromID = from;
	toID = to;
	timeToDelivery = timeUntilDelivery;
	message = mess;
	p = voidp;
}

Message::~Message()
{
}

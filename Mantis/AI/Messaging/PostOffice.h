#pragma once

/**	\brief PostOffice Class
 *	
 *	The PostOffice class is a singleton that handles messages between AI objects.
 */

#include "MessageQueue.h"

#define POST PostOffice::Instance()

class PostOffice
{
public:
	MessageQueue post;

private:

	PostOffice();

public:
	static PostOffice* Instance(void);
	
	~PostOffice();

	/**
	 *	Add a pre-existing message to the queue
	 */
	void AddMail(Message* m);

	/**
	 *	\brief General Update method of update the Post Office
	 *
	 *	Update(dt) method decrements each message's time until Delivery. It will also send out any messages that need to be sent.
	 */
	void Update(float dt);

	/**
	 *	Sends all messages out that are needed to be sent.
	 */
	void SendMail();

	/**
	 *	Delivers a message to it's recipient.
	 */
	void DeliverMessage(Message* m);

	/**
	 *	Deletes any message that has a "ToID" variable of int id.
	 */
	void DeleteMessagesWithToID(int id);

	/**
	 *	Clears out the Post Office.
	 */
	void ClearPostOffice();
};


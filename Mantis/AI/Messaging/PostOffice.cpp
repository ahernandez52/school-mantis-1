#include "PostOffice.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"
#include "..\..\FRAMEWORK\Objects\Actor.h"
#include "..\AIObject\AIObject.h"


PostOffice::PostOffice()
{
}

PostOffice* PostOffice::Instance(void)
{
	//memory leak prevention
	//static PostOffice* ptr = new PostOffice();
	static PostOffice temp;
	return &temp;
}

PostOffice::~PostOffice()
{
}

void PostOffice::AddMail(Message* m)
{
	post.AddMessage(m);
}

void PostOffice::Update(float dt)
{
	post.UpdateMessageTime(dt);

	SendMail();
}

void PostOffice::SendMail()
{
	if(post.IsEmpty())
		return;

	while(post.GetFront()->timeToDelivery <= 0)
	{
		DeliverMessage(post.TakeFront());

		if(post.IsEmpty())
			break;
	}
}

void PostOffice::DeliverMessage(Message* m)
{
	//==========================================
	//WMI finds the entity 
	AIObject* a = WMI->GetAIFromId(m->toID); //<ALERT> Change to WMI call

	//send the entity  m->message or the message
	if (a)
	{
		a->HandleMessage(m);
	}
	

	//delete message after use
	delete m;
	//==========================================	
}

void PostOffice::DeleteMessagesWithToID(int id)
{
	post.RemoveMessagesWithToID(id);
}

void PostOffice::ClearPostOffice()
{
	post.ClearQueue();
}
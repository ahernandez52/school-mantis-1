#include "AIObject.h"
#include "../../GAMEPLAY/Object/Player.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"

AIObject::AIObject()
{
	m_Self = nullptr;  

	m_CurrentVelocity = Vector3(0,0,0);
	m_DesiredVelocity = Vector3(0,0,0);

	m_CurrentRotation = Vector3(0, 0, 0);
	m_DesiredRotation = Vector3(0, 0, 0);
}

AIObject::AIObject(Actor* owner)
{
	m_Self = owner;

	m_CurrentVelocity = Vector3(0,0,0);
	m_DesiredVelocity = Vector3(0,0,0);

	m_CurrentRotation = Vector3(0, 0, 0);
	m_DesiredRotation = Vector3(0, 0, 0);
}

AIObject::AIObject(Actor* owner, AISchematic schematic)
{
	m_Self = owner;

	m_CurrentVelocity = Vector3(0,0,0);
	m_DesiredVelocity = Vector3(0,0,0);

	m_CurrentRotation = Vector3(0, 0, 0);
	m_DesiredRotation = Vector3(0, 0, 0);

}

AIObject::~AIObject()
{
}

//==========
// Actor Functions
//==========

void AIObject::Update(float dt, std::vector<AIObject*>* entities)
{
	if (m_CanUpdate)
	{
		DoFuzzyLogic();
	}
}

void AIObject::DoFuzzyLogic()
{
}

void AIObject::HandleMessage(Message* m)
{
}

//==========
// Utility Functions
//==========

//Set
void AIObject::SetPosition(Vector3 pos)
{
	m_Self->m_Position = pos;
}

void AIObject::SetCurrentOrientation(Quaternion quat)
{
	m_Self->m_Orientation = quat;
}

void AIObject::SetCurrentVelocity(Vector3 vel)
{
	m_CurrentVelocity = vel;
}

void AIObject::SetDesiredVelocity(Vector3 vel)
{
	m_DesiredVelocity = vel;
}

void AIObject::SetCurrentRotation(Vector3 rot)
{
	m_CurrentRotation = rot;
}

void AIObject::SetDesiredRotation(Vector3 rot)
{
	m_DesiredRotation = rot;
}

void AIObject::SetAIType(AI_TYPE type)
{
	m_ObjectType = type;
}

//==========
// Get Functions
//==========
Vector3 AIObject::GetPosition()
{
	return m_Self->m_Position;
}

Vector3 AIObject::GetCurrentVelocity()
{
	return m_CurrentVelocity;
	//return m_Self->Physics->m_LinearVelocity;  //For use with physics entity
}

Vector3 AIObject::GetDesiredVelocity()
{
	return m_DesiredVelocity;
}

Vector3 AIObject::GetPlayerVelocity(Player* p)
{
	return p->GetVelocity();
}

float AIObject::GetPlayerMaxSpeed(Player* p)
{
	return p->m_MaxSpeed;
}

Vector3 AIObject::GetCurrentRotation(void)
{
	return m_CurrentRotation;
}

Vector3 AIObject::GetDesiredRotation(void)
{
	return m_DesiredRotation;
}

Quaternion AIObject::GetCurrentOrientation(void)
{
	return m_Self->m_Orientation;
}

Actor* AIObject::GetSelf()
{
	return m_Self;
}

int AIObject::GetId()
{
	return m_Self->ID;
}

AI_TYPE AIObject::GetAIType(void)
{
	return m_ObjectType;
}

bool AIObject::CanUpdate(void)
{
	return m_CanUpdate;
}
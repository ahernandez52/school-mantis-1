#include "FrigateAI.h"
#include "..\Messaging\PostOffice.h"

#include "..\Fuzzy\FuzzySystem.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"
#include "ShipAI.h"


FrigateAI::FrigateAI(ShipAI* actor, SHIPTYPE ship) : AIShipType(actor, ship)
{
}

FrigateAI::~FrigateAI()
{
}

void FrigateAI::Update(float dt)
{
	
}

void FrigateAI::SurveyBattlefield(void)
{
	
}

void FrigateAI::AddSubordinate(ShipAI* peon)
{
	m_Subordinates.push_back(peon);
}
#include "AIShipType.h"
#include "ShipAI.h"

AIShipType::AIShipType(ShipAI* actor, SHIPTYPE ship) : AIInterface(actor)
{
	m_ShipType = ship;
}

AIShipType::~AIShipType(void)
{
}

void AIShipType::Update(float dt)
{
}

void AIShipType::DoFuzzyLogic(void)
{
}

void AIShipType::SurveyBattlefield(void)
{
}

void AIShipType::AddSubordinate(ShipAI* peon)
{
	m_Subordinates.push_back(peon);
}

SHIPTYPE AIShipType::GetShipType(void)
{
	return m_ShipType;
}

std::vector<ShipAI*>* AIShipType::GetSubordinates(void)
{
	return &m_Subordinates;
}

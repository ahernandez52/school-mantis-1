#include "CruiserAI.h"
#include "..\Messaging\PostOffice.h"
#include "..\Fuzzy\FuzzySystem.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"
#include "ShipAI.h"

CruiserAI::CruiserAI(ShipAI* actor, SHIPTYPE ship) : AIShipType(actor, ship)
{
}

CruiserAI::~CruiserAI(void)
{
}

void CruiserAI::Update(float dt)
{
	
}

void CruiserAI::SurveyBattlefield(void)
{
	
}

void CruiserAI::AddSubordinate(ShipAI* peon)
{
	m_Subordinates.push_back(peon);
}
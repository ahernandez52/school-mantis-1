#pragma once


#include "../../FRAMEWORK/Utility/MantisUtil.h"


class Actor;
class Player;
class Message;
struct AISchematic;

/**
 * AI_TYPE - Enum class for AI typecasting
 */
enum AI_TYPE
{
	AI_UNDECLARED,
	AI_INACTIVE,
	AI_SHIP, 
	AI_OBSTACLE,
	AI_FACTION,
	AI_MISSILE,
	AI_TURRET, 
	AI_PLAYER
};

/**
 * AIObject - Base AIObject from which all other AI objects derive
 */
class AIObject
{
protected:
	Vector3		m_CurrentVelocity;
	Vector3		m_DesiredVelocity;

	Vector3     m_CurrentRotation;
	Vector3     m_DesiredRotation;
	
	Actor* m_Self;

	bool m_CanUpdate;	

	AI_TYPE	m_ObjectType;

public:
	AIObject();
	AIObject(Actor* owner);
	AIObject(Actor* owner, AISchematic schematic);
	virtual ~AIObject();

	/**
	 * Virtual update
	 * Accepts dt and vector of entites to be used by steering behaviors
	 */
	virtual void Update(float dt, std::vector<AIObject*>* entities);

	/**
	 * Virtual fuzzy logic 
	 * To be used for fuzzy logic in future iterations
	 */
	virtual void DoFuzzyLogic();

	/**
	 * Virtual message handling
	 * Passes message to agent's FSM
	 */
	virtual void HandleMessage(Message* m);

	/**
	 * Setters
	 */
	void SetPosition(Vector3 pos);
	void SetCurrentOrientation(Quaternion q);

	void SetCurrentVelocity(Vector3 vel);
	void SetDesiredVelocity(Vector3 vel);

	void SetCurrentRotation(Vector3 vel);
	void SetDesiredRotation(Vector3 vel);

	void SetAIType(AI_TYPE type);
	virtual void SetSize(void){}

	/**
	 * Getters
	 */
	Vector3 GetPosition();
	Quaternion GetCurrentOrientation();

	Vector3 GetCurrentVelocity();
	Vector3 GetDesiredVelocity();

	Vector3 GetCurrentRotation();
	Vector3 GetDesiredRotation();

	Vector3 GetPlayerVelocity(Player* p);
	float	GetPlayerMaxSpeed(Player* p);

	float GetSize(void){return 10.0f;}

	Actor* GetSelf();

	int GetId();

	AI_TYPE GetAIType(void);

	/**
	 * Deprecated function for AICore load balancing
	 */
	bool CanUpdate(void);
};
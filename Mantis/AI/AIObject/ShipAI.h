/**
 * ShipAI - Derived AIActor reserved for ships on the playing field
 */

#pragma once

#include "AIObject.h"
#include "../States/StateMachine.h"
#include "AIShipType.h"
#include "AIFaction.h"

class CombatInterface;
class Formation;
class SteeringBehavior;

/**
 * ShipAI
 * Main AI class for ship based AI
 */
class ShipAI : public AIObject
{
private:

	//Faction interface
	AIFaction*	m_Faction;

	//Shiptype interface
	AIShipType*	m_ShipType;

	//Combat interface
	CombatInterface* m_CombatInterface;

	//StateMachine
	StateMachine<ShipAI>* m_FSM;

private:

	float   m_Size;						//Ship radius
	float   m_MaxRotation;				//Maximum turning radius
	float   m_MaxAngularAcceleration;	//Maximum angular acceleration
	float   m_MaxSpeed;					//Maximum speed

	Vector3 m_MaxForce;					//Maximum velocity


	Actor* m_Commander;					//Commanding actor

	SteeringBehavior* m_Steering;		//Steering 

	//Ship specific components
	Formation* m_Formation;				//Formation

	//AI related members
	bool	m_IsHit;					//Deprecated
	bool	m_StraffApproach;			//Determines if the ship is on a straffing run
	bool	m_PowerSliding;				//Determines if the ship is facing independently of direciton

public:

	ShipAI(void);
	ShipAI(Actor* owner);
	ShipAI(Actor* owner, AISchematic schematic);
	virtual ~ShipAI(void);

	void Update(float dt, std::vector<AIObject*>* entities) override;
	void DoFuzzyLogic(void) override;
	void HandleMessage(Message* m) override;

	virtual void SurveyBattlefield(void);

public:

	//============================
	//Component functions
	//============================

	//Get faction interface
	AIFaction*	GetFaction(void);

	//Get shiptype interface
	AIShipType*	GetShipType(void);

	//Set faction interface
	void SetFaction(FACTION faction);

	//Set shiptype interface
	void SetShipType(SHIPTYPE ship);

	//Change the current state
	void ChangeState(BaseState<ShipAI>* newState);

	//Set the current state
	void SetState(BaseState<ShipAI>* newState);



	//============================
	//Utility setters
	//============================

	void SetUpdate(bool b);

	void SetMaxRotation(float maxRotation);
	void SetMaxAngularAcceleration(float accel);
	void SetMaxSpeed(float maxSpeed);
	void SetMaxForce(Vector3 maxForce);
	void SetSize(float size);

	void SetFormation(Formation* f);

	void RemoveFormation();

	//Set IsHit
	void SetHit(bool t){m_IsHit = t;}
	void SetStraff(bool approach){m_StraffApproach = approach;}
	void SetSlide(bool sliding){m_PowerSliding = sliding;}

	//============================
	//Utility getters
	//============================

	float GetMaxRotation();
	float GetMaxAngularAcceleration();
	float GetMaxSpeed();
	float GetSize();

	Vector3 GetMaxForce();

	bool GetHit(void){return m_IsHit;}
	bool GetStraff(void){return m_StraffApproach;}
	bool GetSlide(void){return m_PowerSliding;}

	SteeringBehavior* GetSteering(void){return m_Steering;}

	AIObject* GetTargetActor();

	Formation* GetFormation();

};
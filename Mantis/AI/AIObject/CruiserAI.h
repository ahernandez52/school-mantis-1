/**
 * CruiserAI - Defines cruiser AI 
 */

#pragma once

#include "ShipAI.h"

/**
 * CruiserAI - Determines cruiser specific AI 
 */
class CruiserAI : public AIShipType
{
private:

public:
	CruiserAI(ShipAI* actor, SHIPTYPE ship);
	~CruiserAI();

	void Update(float dt) override;

	void SurveyBattlefield(void) override;

	void AddSubordinate(ShipAI* peon) override;
};
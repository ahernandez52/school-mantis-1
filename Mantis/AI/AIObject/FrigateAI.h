/**
 * FrigateAI - Defines frigate specific AI
 */

#pragma once


#include "AIShipType.h"

/**
 * FrigateAI - Defines frigate specific AI
 */
class FrigateAI : public AIShipType
{
private:


public:
	FrigateAI(ShipAI* actor, SHIPTYPE ship);
	~FrigateAI();

	void Update(float dt) override;
	void SurveyBattlefield(void) override;

	void AddSubordinate(ShipAI* peon) override;

};
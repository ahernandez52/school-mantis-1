/**
 * AIInterface - Pure virtual Base Interface for AI Actors.  Faction and shiptype 
 * interfaces will be derived from AIInterface
 */

#pragma once 

class AIObject;

/**
 * AIInterface - Base interface class used for AI sub modules
 */
class AIInterface
{
protected:

	/**
	 * Pointer to the owning actor
	 */
	AIObject*	m_Actor;

public:

	AIInterface(AIObject* actor) : m_Actor(actor) {}
	~AIInterface(void){}

	virtual void Update(float dt) = 0;
	virtual void DoFuzzyLogic(void) = 0;
};
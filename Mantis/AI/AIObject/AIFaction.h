/**
 * AIFaction - Faction interface for the AIActor
 */

#pragma once

#include "AIInterface.h"

/**
 * FACTION - Enum used to differentiate friendly and foe
 */
enum FACTION
{
	UNDECLARED = 0,
	PHALANX    = 1,
	ORDER      = 2,
	TAAC       = 3,
	PIRATE     = 4,
	PLAYER_FAC = 5
};

/**
 * AIFaction - Faction interface used to perform faction specific updates or decisions
 */
class AIFaction : public AIInterface
{
private:

	FACTION m_Faction;

public:

	AIFaction(AIObject* actor, FACTION faction);
	~AIFaction(void);

	virtual void Update(float dt) override;
	virtual void DoFuzzyLogic(void) override;

	FACTION GetFaction(void);
};
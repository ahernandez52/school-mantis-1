/**
 * CombatInterface - Deprecated system for weapons management
 */

#pragma once

#include "AIInterface.h"

/**
 * CombatInterface
 * Deprecated interface for weapons management
 */
class CombatInterface : public AIInterface
{
public:
	AIObject* m_Target;

public:
	CombatInterface(AIObject* actor);
	~CombatInterface();

	void Update(float dt) override;
	void DoFuzzyLogic() override;

	// Set the target
	void SetTarget(AIObject* target);
	// Get the target
	AIObject* GetTarget();
	// clear the target
	void ClearTarget();
};
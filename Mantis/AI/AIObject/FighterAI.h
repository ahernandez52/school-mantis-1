/**
 * FighterAI - Defines fighter AI 
 */

#pragma once


#include "AIShipType.h"

/**
 * FighterAI - Determines fighter specific AI 
 */
class FighterAI : public AIShipType
{
private:


public:
	FighterAI(ShipAI* actor, SHIPTYPE ship);
	~FighterAI();

	void Update(float dt) override;
	void SurveyBattlefield(void) override;

	void AddSubordinate(ShipAI* peon) override;

};
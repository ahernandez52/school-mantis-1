#include "../../FRAMEWORK/Objects\Actor.h"
#include "ShipAI.h"
#include "AIObject.h"
#include "AIFaction.h"
#include "AIShipType.h"
#include "CruiserAI.h"
#include "FighterAI.h"
#include "FrigateAI.h"
#include "../States/CruiserMove.h"
#include "../States/FrigateMove.h"
#include "../States/FighterMove.h"
#include "../States/FighterIdle.h"

#include "CombatInterface.h"
#include "../Influence/InfluenceSystem.h"
#include "../Messaging/PostOffice.h"
#include <random>
#include "../Steering/SteeringBehavior.h"

ShipAI::ShipAI(void) : AIObject()
{
	m_Faction  = NULL;
	m_ShipType = NULL;
	m_CombatInterface = new CombatInterface(this);
	m_FSM = new StateMachine<ShipAI>(this);

	m_Size   = 3;
	m_MaxRotation = D3DX_PI / 2;
	m_MaxAngularAcceleration = D3DX_PI / 4;
	m_MaxSpeed = 10.0f;
	m_MaxForce = Vector3(0, 0, 200);

	m_IsHit = false;
	m_StraffApproach = false;

	m_Formation = nullptr;

	m_Steering = new SteeringBehavior(this);
}

ShipAI::ShipAI(Actor* owner) : AIObject(owner)
{
	m_Faction = NULL;
	m_ShipType = NULL;
	m_CombatInterface = new CombatInterface(this);
	m_FSM = new StateMachine<ShipAI>(this);

	m_Size   = 3;
	m_MaxRotation = D3DX_PI / 2;
	m_MaxAngularAcceleration = D3DX_PI / 4;
	m_MaxSpeed = 10.0f;
	m_MaxForce = Vector3(0, 0, 200);

	m_Formation = nullptr;

	m_IsHit = false;
	m_StraffApproach = false;

	m_Steering = new SteeringBehavior(this);

	m_CanUpdate = true;
}

ShipAI::ShipAI(Actor* owner, AISchematic schematic) : AIObject(owner, schematic)
{
	m_CombatInterface = new CombatInterface(this);
	m_FSM = new StateMachine<ShipAI>(this);

	m_Faction = nullptr;
	m_ShipType = nullptr;

	m_Formation = nullptr;

	m_IsHit = false;
	m_StraffApproach = false;

	m_Steering = new SteeringBehavior(this);

	if(schematic.Faction == "PHALANX")
	{
		SetFaction(PHALANX);
		//ISI->RegisterInfluence(PHALANX, this);
	}

	if(schematic.Faction == "ORDER")
	{
		SetFaction(ORDER);
		//ISI->RegisterInfluence(ORDER, this);
	}

	if(schematic.Faction == "TAAC")
	{
		SetFaction(TAAC);
		//ISI->RegisterInfluence(TAAC, this);
	}

	if(schematic.Faction == "PIRATE")
	{
		SetFaction(PIRATE);
		//ISI->RegisterInfluence(PIRATE, this);
	}

	if(schematic.Faction == "UNDECLARED")
		SetFaction(UNDECLARED);

	if(schematic.AIType == "CRUISER")
	{
		SetShipType(CRUISER);
	}

	if(schematic.AIType == "FRIGATE")
	{
		SetShipType(FRIGATE);
	}

	if(schematic.AIType == "FIGHTER")
	{
		SetShipType(FIGHTER);
	}

	if(schematic.AIType == "PLAYER")
	{
		SetShipType(PLAYER_SHIP);
	}

	if (m_ObjectType == AI_PLAYER)
	{
		m_CanUpdate = false;
		return;
	}
	m_CanUpdate = true;
	POST->AddMail(new Message(GetId(), GetId(), 1, SELF_UPDATE));
}

ShipAI::~ShipAI(void)
{
	if (m_Faction != nullptr)
	{
		//Remove from influence
		//ISI->RemoveInfluence(m_Faction->GetFaction(), this);

		delete m_Faction;
		m_Faction = nullptr;
	}
	
	if (m_ShipType != nullptr)
	{
		delete m_ShipType;
		m_ShipType = nullptr;
	}

	if (m_CombatInterface != nullptr)
	{
		delete m_CombatInterface;
		m_CombatInterface = nullptr;
	}

	if (m_Steering != nullptr)
	{
		delete m_Steering;
	}

	delete m_FSM;
}

void ShipAI::Update(float dt, std::vector<AIObject*>* entities)
{
	if (m_ObjectType == AI_PLAYER)
		return;

	if (m_FSM)
	{
		m_FSM->Update(dt);
	}

	/**
	 * Linear Velocity update
	 */

	//Update velocity
	D3DXVECTOR3 desiredVelocity;

	//Calculate acting forces
	desiredVelocity = m_Steering->GetLinearVelocity(entities);

	////Send to physics
	//m_Self->Physics->AddForce(desiredVelocity);

	//Calculate acceleration
	D3DXVECTOR3 acceleration = desiredVelocity * (float)(1.0 / m_Self->Physics->GetMass());

	//Update velocity
	m_CurrentVelocity += acceleration * dt;

	//Truncate velocity
	if (D3DXVec3LengthSq(&m_CurrentVelocity) > m_MaxSpeed * m_MaxSpeed)
	{
		D3DXVec3Normalize(&m_CurrentVelocity, &m_CurrentVelocity);
		m_CurrentVelocity *= m_MaxSpeed;
	}

	//Update position
	m_Self->m_Position += m_CurrentVelocity * dt;

	/**
	 * Angular Velocity update
	 */
	
	//Update angular velocity
	angular_output w;

	w = m_Steering->GetAngularVelocity();

	D3DXVECTOR3 desiredRotation = w.rotation;

	D3DXVECTOR3 angAccel = desiredRotation;

	m_CurrentRotation = desiredRotation;

	if (D3DXVec3LengthSq(&m_CurrentRotation) > m_MaxRotation * m_MaxRotation)
	{
		D3DXVec3Normalize(&m_CurrentRotation, &m_CurrentRotation);
		m_CurrentRotation *= m_MaxRotation;
	}

	m_Self->m_Orientation += D3DXQUATERNION(m_CurrentRotation.x, m_CurrentRotation.y, m_CurrentRotation.z, 0) * dt;

	//m_Self->m_Orientation += D3DXQUATERNION(w.rotation.x, w.rotation.y, w.rotation.z, 0) * dt;

}

void ShipAI::DoFuzzyLogic(void)
{
}

void ShipAI::HandleMessage(Message* m)
{
	m_FSM->HandleMessage(m);
}

void ShipAI::SurveyBattlefield(void)
{
	m_ShipType->SurveyBattlefield();
}

//============================
//Component functions
//============================

AIFaction* ShipAI::GetFaction(void)
{
	return m_Faction;
}

AIShipType*	ShipAI::GetShipType(void)
{
	return m_ShipType;
}

void ShipAI::SetFaction(FACTION faction)
{
	if (m_Faction == nullptr)
	{
		m_Faction = new AIFaction(this, faction);
		// Set this ent to be associated with its own faction map
		switch(m_Faction->GetFaction())
		{
		case UNDECLARED:
			break;
		case PHALANX:
			//MCI->ModifyRegInflMapName(this->GetId(), "phalanx");
			break;
		case ORDER:
			//MCI->ModifyRegInflMapName(this->GetId(), "order");
			break;
		case TAAC:
			//MCI->ModifyRegInflMapName(this->GetId(), "taac");
			break;
		case PIRATE:
			//MCI->ModifyRegInflMapName(this->GetId(), "pirate");
			break;
		case PLAYER_FAC:
			break;
		default:
			break;
		}
	}
}

void ShipAI::SetShipType(SHIPTYPE ship)
{
	switch(ship)
	{
	default:
		break;
	case CRUISER:
		{
			if (!m_ShipType)
			{
				m_ShipType = new CruiserAI(this, ship);
			}

			m_Size   = 40;
			m_MaxRotation = D3DX_PI / 2;
			m_MaxAngularAcceleration = D3DX_PI / 2;
			m_MaxSpeed = 20.0f;
			m_MaxForce = Vector3(0, 0, 200);

			SetAIType(AI_SHIP);

			m_FSM->SetCurrent(CruiserMove::Instance());
			break;
		}
	case FRIGATE:
		{
			if (!m_ShipType)
			{
				m_ShipType = new FrigateAI(this, ship);
			}

			m_Size   = 20;
			m_MaxRotation = D3DX_PI / 2;
			m_MaxAngularAcceleration = D3DX_PI / 2;
			m_MaxSpeed = 100.0f;
			m_MaxForce = Vector3(0, 0, 200);

			SetAIType(AI_SHIP);

			m_FSM->SetCurrent(FrigateMove::Instance());
			break;
		}
	case FIGHTER:
		{
			if (!m_ShipType)
			{
				m_ShipType = new FighterAI(this, ship);
			}

			m_Size   = 10;
			m_MaxRotation = D3DX_PI / 4;
			m_MaxAngularAcceleration = D3DX_PI / 2;
			m_MaxSpeed = 250.0f;
			m_MaxForce = Vector3(0, 0, 75);

			SetAIType(AI_SHIP);

			m_FSM->SetCurrent(FighterIdle::Instance());
			break;
		}
	case PLAYER_SHIP:
		{
			SetAIType(AI_PLAYER);
			break;
		}
		break;
	}
}

void ShipAI::ChangeState(BaseState<ShipAI>* newState)
{
	m_FSM->ChangeState(newState);
}

void ShipAI::SetState(BaseState<ShipAI>* newState)
{
	m_FSM->SetCurrent(newState);
}


void ShipAI::SetMaxRotation(float maxRotation)
{
	m_MaxRotation = maxRotation;
}

void ShipAI::SetMaxAngularAcceleration(float accel)
{
	m_MaxAngularAcceleration = accel;
}

void ShipAI::SetMaxSpeed(float maxSpeed)
{
	m_MaxSpeed = maxSpeed;
}

void ShipAI::SetMaxForce(Vector3 maxForce)
{
	m_MaxForce = maxForce;
}

void ShipAI::SetUpdate(bool b)
{
	m_CanUpdate = b;
}


void ShipAI::SetSize(float Size)
{
	m_Size = Size;
}

void ShipAI::SetFormation(Formation* f)
{
	m_Formation = f;
}

void ShipAI::RemoveFormation(void)
{
	m_Formation = nullptr;
}

float ShipAI::GetMaxRotation()
{
	return m_MaxRotation;
}

float ShipAI::GetMaxAngularAcceleration(void)
{
	return m_MaxAngularAcceleration;
}

float ShipAI::GetMaxSpeed()
{
	return m_MaxSpeed;
}

float ShipAI::GetSize()
{
	return m_Size;
}

Vector3 ShipAI::GetMaxForce()
{
	return m_MaxForce;
}


Formation* ShipAI::GetFormation()
{
	return m_Formation;
}

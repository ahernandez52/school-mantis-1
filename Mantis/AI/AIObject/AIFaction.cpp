#include "AIFaction.h"

AIFaction::AIFaction(AIObject* actor, FACTION faction) : AIInterface(actor)
{
	m_Faction = faction;
}

AIFaction::~AIFaction(void)
{
}

void AIFaction::Update(float dt)
{
}

void AIFaction::DoFuzzyLogic(void)
{
}


FACTION AIFaction::GetFaction(void)
{
	return m_Faction;
}
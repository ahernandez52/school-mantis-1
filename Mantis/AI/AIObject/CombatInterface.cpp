#include "CombatInterface.h"

CombatInterface::CombatInterface(AIObject* actor) : AIInterface(actor)
{
	m_Target = nullptr;
}

CombatInterface::~CombatInterface()
{
	m_Target = nullptr;
}

void CombatInterface::Update(float dt)
{
}

void CombatInterface::DoFuzzyLogic()
{
}

void CombatInterface::SetTarget(AIObject* target)
{
	m_Target = target;
}

AIObject* CombatInterface::GetTarget()
{
	if(m_Target)
	{
		return m_Target;
	}
	else
	{
		return nullptr;
	}
}

void CombatInterface::ClearTarget()
{
	m_Target = nullptr;
}
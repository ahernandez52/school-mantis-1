/**
 * AIShipType - Deprecated ship type <Functionality moved to gameplay class Ship
 */

#pragma once

//#include "..\..\FRAMEWORK\Utility\MantisUtil.h"
#include "AIInterface.h"
#include <vector>

class ShipAI;

/**
 * SHIPTYPE enum
 * Used for AI ship typecasting
 */
enum SHIPTYPE
{
	UNKNOWN     = 0,
	FIGHTER     = 1, 
	FRIGATE     = 2, 
	CRUISER     = 3,
	PLAYER_SHIP = 4
};

/**
 * AIShipType - Deprecated interface for ship parameters
 */
class AIShipType : public AIInterface
{
protected:

	SHIPTYPE m_ShipType;

	/**
	 * Subordinates will take commands from higher level agents
	 */
	std::vector<ShipAI*> m_Subordinates;

public:

	AIShipType(ShipAI* actor, SHIPTYPE ship);
	~AIShipType(void);

	virtual void Update(float dt) override;
	virtual void DoFuzzyLogic(void) override;


	/**
	 * Virtual SurveyBattlefield 
	 * Makes runtime strategic decisions using influence and fuzzy logic
	 */
	virtual void SurveyBattlefield(void);

	/**
	 * Adds a subordinate AI to m_Subordinates
	 */
	virtual void AddSubordinate(ShipAI* peon);

	SHIPTYPE GetShipType(void);

	/**
	 * Return the list of subordinates
	 */
	virtual std::vector<ShipAI*>* GetSubordinates(void);
};
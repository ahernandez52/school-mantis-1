#include "ControlInfluenceMap.h"
#include "..\AIObject\AIObject.h"

ControlInfluenceMap::~ControlInfluenceMap()
{
	//Release the map array
	if(m_map)
	{
		delete m_map;
	}

	// Check if empty
	if(m_registeredObjects.size() == 0)
		return;

	// Cycle through and delete each pointer
	RegObjList::iterator listObj;
	for(listObj = m_registeredObjects.begin(); 
		listObj != m_registeredObjects.end();
		++listObj){
			delete (*listObj);
	}

	m_registeredObjects.clear();
}

void ControlInfluenceMap::Update(float dt)
{
    //bail out if nobody to update
    if(m_registeredObjects.size() == 0)
        return;
    
    //clear out map
    memset(m_map,0,m_NumCels*sizeof(float));
    
	//Stamp objects
	for (const auto& obj : m_registeredObjects)
	{
		StampInfluenceGradient(obj->m_pAIObj->GetPosition(), 10);
	}
  //  //stamp obj locations
  //  RegObjList::iterator listObj;
  //  for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
  //  {
  //      //only care about "control" objects, not miscellaneous
		//if((*listObj)->m_iObjType == OT_MISC)
  //          continue;

  //      if((*listObj)->m_iObjType == OT_FRIENDLY)
  //          StampInfluenceGradient((*listObj)->m_pAIObj->GetPosition(), 16);
  //      else if((*listObj)->m_iObjType == OT_BULLET)
  //          StampInfluenceGradient((*listObj)->m_pAIObj->GetPosition(), 8);
  //      else
  //          StampInfluenceGradient((*listObj)->m_pAIObj->GetPosition(), -(((*listObj)->m_pAIObj->m_size)/2));
  //      (*listObj)->m_vLastPosition = (*listObj)->m_pAIObj->GetPosition();
  //  }
}

void ControlInfluenceMap::RegisterGameObj(AIObject* aiObj)
{
    int sizeX,sizeZ;
    sizeX = sizeZ = 1;
    
    RegObj* temp;
    temp = new RegObj;
    temp->m_pAIObj      = aiObj;
    temp->m_iObjSizeX     = sizeX;
    temp->m_iObjSizeZ     = sizeZ;
    temp->m_vLastPosition = aiObj->GetPosition();
    temp->m_bStamped      = false;

// Validating AIObject types
    //if(aiObj->m_type == AIObject::OBJ_SHIP || aiObj->m_type == AIObject::OBJ_SAUCER)
    //    temp->m_iObjType = OT_FRIENDLY;
    //else if(aiObj->m_type == AIObject::OBJ_BULLET)
    //    temp->m_iObjType = OT_BULLET;
    //else if(aiObj->m_type == AIObject::OBJ_ASTEROID)
    //    temp->m_iObjType = OT_ENEMY;
    //else
    //    temp->m_iObjType = OT_MISC;

    m_registeredObjects.push_back(temp);
}

//void ControlInfluenceMap::DrawTheInfluence()
//{
//    glPushMatrix();
//    glDisable(GL_LIGHTING);
//    glTranslatef(0,0,0);
//    glEnable(GL_BLEND);                                 
//    glBlendFunc(GL_ONE, GL_ONE);                    
//    for(int i=0;i<m_numCels;i++)
//    {
//        if(m_map[i])
//        {
//            int y = i / m_dataSizeY;
//            int x = i - y*m_dataSizeY;
//            float color = m_map[i]/16.0f;
//            if(color > 0)
//                glColor3f(0,0,color);
//            else
//                glColor3f(-color,0,0);
//            glBegin(GL_POLYGON);
//            glVertex3f(x*m_celResX,          y*m_celResY,          0);
//            glVertex3f(x*m_celResX,          y*m_celResY+m_celResY,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY+m_celResY,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY,          0);
//            glEnd();
//        }
//    }
//    glDisable(GL_BLEND);    
//    glEnable(GL_LIGHTING);
//    glPopMatrix();
//}

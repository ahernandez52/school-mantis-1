/**
 * InfluenceMap - Base influence map
 */


#pragma once


#include <vector>
#include <list>
#include <map>
#include "D3dx9math.h"

class AIObject;

/**
 * RegObj - Placeholder object for registered objects
 */
struct RegObj{
	AIObject*	m_pAIObj;		
	int			m_iObjSizeX;
	int			m_iObjSizeZ;
	int			m_iObjType;
	D3DXVECTOR3	m_vLastPosition;
	bool		m_bStamped;
};


typedef std::list<RegObj*> RegObjList;	


/**
 * InfluenceMap - Base Influence Map
 */
class InfluenceMap
{
public:

	InfluenceMap(void);
	InfluenceMap(int type);
	~InfluenceMap();

	virtual void Initialize(int sizeX, int sizeY, int worldSizeX, int worldSizeY);
	virtual void Reset();

	virtual void Update(float dt);


	virtual void RegisterGameObject(AIObject* aiObj);
	virtual void RemoveGameObject(AIObject* aiObj);

	virtual void StampInfluenceShape(D3DXVECTOR3& location, int sizeX, int sizeY, int influenceValue);
	virtual void StampInfluenceGradient(D3DXVECTOR3& location, int initValue);

	int			SumInfluenceShape(D3DXVECTOR3& location, int sizeX, int sizeY);
	int			GetInfluenceValue(D3DXVECTOR3& location);
	int			GetInfluenceValue(int x, int y);

	const int   GetNumCells(void) {return m_NumCels;}
	const int   GetRows(void) {return m_DataSizeZ;}
	const int   GetCols(void) {return m_DataSizeX;}
	const int   GetWorldX(void) {return m_WorldSizeX;}
	const int   GetWorldZ(void) {return m_WorldSizeZ;}

	float		GetValueByIndex(int index) {return m_map[index];}

	void	ConvertPositionToGrid(D3DXVECTOR3& location, int gridX, int gridY);
	void	ConvertGridToPosition(int gridX, int gridY, D3DXVECTOR3& location);
	void	SetType(int type)			{}
	void DoBoundsCheck(int& x, int& y);
	bool IsOutOfBounds(int x, int y);

	//=========================================
	// Overloaded operators for map algorithms
	//=========================================

	InfluenceMap operator + (InfluenceMap&) const;
	InfluenceMap operator - (InfluenceMap&) const;

	InfluenceMap operator * (InfluenceMap&) const;
	void operator = (InfluenceMap&);
	
	void AbsoluteValue(InfluenceMap* pOut, InfluenceMap* pIn);

	//=========================================
	// Map types
	//=========================================
    enum
    {
        IM_NONE,
        IM_OCCUPANCE,
        IM_CONTROL,
        IM_BITWISE, 
		IM_THREAT,
		IM_VULNERABILITY,
		IM_TENSION
    };

protected:
	float*	m_map;
	RegObjList	m_registeredObjects;

	int		m_DataSizeX;
	int		m_DataSizeZ;
	int		m_NumCels;
	int		m_WorldSizeX;
	int		m_WorldSizeZ;
	float	m_CelResX;
	float	m_CelResZ;
	int		m_InfluenceType;

	bool	m_DrawInfluence;

private:
};


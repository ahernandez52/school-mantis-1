#include "InfluenceMap.h"
#include <algorithm>
#include "..\AIObject\AIObject.h"

InfluenceMap::InfluenceMap(void)
{
}

InfluenceMap::InfluenceMap(int type) : m_InfluenceType(type)
{
}

InfluenceMap::~InfluenceMap()
{
	//Release the map array
	if(m_map)
	{
		delete m_map;
	}

	// Check if empty
	if(m_registeredObjects.size() == 0)
		return;

	// Cycle through and delete each pointer
	RegObjList::iterator listObj;
	for(listObj = m_registeredObjects.begin(); 
		listObj != m_registeredObjects.end();
		++listObj){
			delete (*listObj);
	}
	m_registeredObjects.clear();


}

void InfluenceMap::Update(float dt)
{
	//Check if empty
	if(m_registeredObjects.empty())
	{
		return;
	}
}

void InfluenceMap::Initialize(int sizeX, int sizeZ, int wSizeX, int wSizeZ)
{
	// Assign sizes of row & col
    m_DataSizeX     = sizeX;
    m_DataSizeZ     = sizeZ;
    m_NumCels       = m_DataSizeX*m_DataSizeZ;
    m_map           = new float[m_NumCels];

    //clear out the map
	// Allocate memory block for this pointer
    memset(m_map, 0, 100 * sizeof(float));
    
	// Assign world sizes and get map resolution
    m_WorldSizeX    = wSizeX;
    m_WorldSizeZ    = wSizeZ;
    m_CelResX       = (float)(m_WorldSizeX / m_DataSizeX);
    m_CelResZ       = (float)(m_WorldSizeZ / m_DataSizeZ);
}

void RemoveAll(RegObj* object)
{
    delete object;
}

void InfluenceMap::Reset()
{
    //clear out the map
    memset(m_map,0,m_NumCels * sizeof(int));
    
    //get rid off all the objects
    if(m_registeredObjects.size() == 0)
        return;
    std::for_each(m_registeredObjects.begin(),m_registeredObjects.end(),RemoveAll);
    m_registeredObjects.clear();
}

void InfluenceMap::RegisterGameObject(AIObject* aiObj)
{
	int sizeZ,sizeX;
    sizeX = sizeZ = 1;
    
    RegObj* temp;
    temp = new RegObj;
    temp->m_pAIObj      = aiObj;
    temp->m_iObjSizeX     = sizeX;
    temp->m_iObjSizeZ    = sizeZ;
	temp->m_vLastPosition = aiObj->GetPosition();
    temp->m_bStamped      = false;
    m_registeredObjects.push_back(temp);
}

void InfluenceMap::RemoveGameObject(AIObject* aiObj)
{
    if(m_registeredObjects.size() == 0)
        return;

    RegObjList::iterator listObj;
    for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
    {
        RegObj* temp = *listObj;
        if((*listObj)->m_pAIObj == aiObj)
        {
            m_registeredObjects.erase(listObj);
            delete temp;
            return;
        }
    }
}

void InfluenceMap::StampInfluenceShape(D3DXVECTOR3& location, int sizeX, int sizeZ, int influenceValue)
{
    float gridX = location.x / m_CelResX;
    float gridZ = location.z / m_CelResZ;
    
    int startX = (int)gridX - sizeX / 2;

    if(startX < 0) 
	{
		startX += m_DataSizeX;
	}

    int startZ = (int)(gridZ - sizeZ / 2);

    if(startZ < 0) 
	{
		startZ += m_DataSizeZ;
	}
    
    for(int z = startZ; z < startZ + sizeZ; z++)
    {
        for(int x = startX;x < startX + sizeX; x++)
        {
            m_map[(z % m_DataSizeZ) * m_DataSizeX + (x % m_DataSizeX)] += influenceValue;
        }
    }
}

void InfluenceMap::ConvertPositionToGrid(D3DXVECTOR3& location, int gridX, int gridZ)
{
	gridX = (int)(location.x/ m_CelResX);
	gridZ = (int)(location.z/ m_CelResZ);
}

void InfluenceMap::ConvertGridToPosition(int gridX, int gridY, D3DXVECTOR3& location)
{
	//location.Set(gridX*m_celResX,gridY*m_celResY,0.0f);
}

int InfluenceMap::GetInfluenceValue(D3DXVECTOR3& location)
{
    int gridX = (int)(location.x / m_CelResX);
    int gridZ = (int)(location.z / m_CelResZ);
	DoBoundsCheck(gridX, gridZ);
	return (int)m_map[gridZ * m_DataSizeX + gridX];
}

void InfluenceMap::DoBoundsCheck(int& x, int& z)
{
	//bounds check the params
	if(x >= m_DataSizeX)
		x %= m_DataSizeX;
	while(x < 0)
		x += m_DataSizeX;
	if(z >= m_DataSizeZ)
		z %= m_DataSizeZ;
	while(z < 0)
		z += m_DataSizeZ;
}

bool InfluenceMap::IsOutOfBounds(int x, int z)
{
	//bounds check the params
	if(x >= m_DataSizeX)
		return true;
	if(x < 0)
		return true;
	if(z >= m_DataSizeZ)
		return true;
	if(z < 0)
		return true;

	return false;
}

InfluenceMap InfluenceMap::operator+(InfluenceMap& rhs) const
{
	InfluenceMap ret;
	ret.Initialize(m_DataSizeX, m_DataSizeZ, m_WorldSizeX, m_WorldSizeZ);

	if (m_NumCels != rhs.m_NumCels)
	{
		//The maps are not the same size, return empty map
		return ret;
	}

	ret.Initialize(m_DataSizeX, m_DataSizeZ, m_WorldSizeX, m_WorldSizeZ);

	for (int i = 0 ; i < m_NumCels ; i++)
	{
		ret.m_map[i] = m_map[i] + rhs.m_map[i];
	}

	return ret;
}

InfluenceMap InfluenceMap::operator-(InfluenceMap& rhs) const
{
	InfluenceMap ret;

	if (m_NumCels != rhs.m_NumCels)
	{
		//The maps are not the same size, return empty map
		return ret;
	}

	ret.Initialize(m_DataSizeX, m_DataSizeZ, m_WorldSizeX, m_WorldSizeZ);

	for (int i = 0 ; i < m_NumCels ; i++)
	{
		ret.m_map[i] = m_map[i] - rhs.m_map[i];
	}

	return ret;
}

InfluenceMap InfluenceMap::operator*(InfluenceMap& rhs) const
{
	InfluenceMap ret;

	if (m_NumCels != rhs.m_NumCels)
	{
		//The maps are not the same size, return empty map
		return ret;
	}

	ret.Initialize(m_DataSizeX, m_DataSizeZ, m_WorldSizeX, m_WorldSizeZ);

	for (int i = 0 ; i < m_NumCels ; i++)
	{
		ret.m_map[i] = m_map[i] * rhs.m_map[i];
	}

	return ret;
}

void InfluenceMap::operator=(InfluenceMap& rhs)
{
	for (int i = 0 ; i < m_NumCels ; i++)
	{
		m_map[i] = rhs.m_map[i];
	}
}
void InfluenceMap::AbsoluteValue(InfluenceMap* pOut, InfluenceMap* pIn)
{
	*pOut = *pIn;

	for (int i = 0 ; i < pOut->m_NumCels ; i++)
	{
		pOut->m_map[i] = pIn->m_map[i];
	}
}

int InfluenceMap::GetInfluenceValue(int x, int z)
{
	DoBoundsCheck(x,z);
	int retVal = (int)m_map[z*m_DataSizeX+x];
	return retVal;
}

int InfluenceMap::SumInfluenceShape(D3DXVECTOR3& location,int sizeX,int sizeZ)
{
    int sum = 0;
    int gridX = (int)(location.x/ m_CelResX);
    int gridZ = (int)(location.z/ m_CelResZ);
    
    int startX = gridX - sizeX/2;
    if(startX < 0)
	{
		startX += m_DataSizeX;
	}

    int startZ = gridZ - sizeZ/2;

    if(startZ < 0)
	{
		startZ += m_DataSizeZ;
	}
    
    for(int z = startZ; z < startZ + sizeZ; z++)
    {
        for(int x = startX; x < startX + sizeX; x++)
        {
            sum += (int)m_map[(z % m_DataSizeZ) * m_DataSizeX + (x % m_DataSizeX)];
        }
    }
    return sum;
}

void InfluenceMap::StampInfluenceGradient(D3DXVECTOR3& location, int initValue)
{
	if (location)
	{
		int gridX = (int)((location.x + m_WorldSizeX/2) / m_CelResX);
		int gridZ = (int)((location.z + m_WorldSizeZ/2) / m_CelResZ);
    
		int stopDist = abs(initValue);//*0.75f;//*(m_DataSizeX/32);
		int halfStopDist = max(1, stopDist / 2);
		int startX = gridX - halfStopDist;

		if(startX < 0) 
		{
			startX += m_DataSizeX;
		}

		int startZ = gridZ - halfStopDist;

		if(startZ < 0) 
		{
			startZ += m_DataSizeZ;
		}
    
		for(int z = startZ; z < startZ + stopDist; z++)
		{
			for(int x = startX; x < startX + stopDist; x++)
			{
				int value;

				int distX = abs(x - (startX + halfStopDist));
				int distZ = abs(z - (startZ + halfStopDist));

				value = initValue * (halfStopDist - max(distX,distZ)) / halfStopDist;
				m_map[(z % m_DataSizeZ) * m_DataSizeX + (x % m_DataSizeX)] += value;
			}
		}
	}
}

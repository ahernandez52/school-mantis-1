#pragma once

#include "InfluenceMap.h"

class OccupanceInfluenceMap : public InfluenceMap
{
public:
    //constructor/functions
    OccupanceInfluenceMap():InfluenceMap(IM_OCCUPANCE){}
    ~OccupanceInfluenceMap();
    virtual void Update(float dt);
    virtual void RegisterGameObj(AIObject* aiObj);
    virtual void RemoveGameObj(AIObject* aiObj);
    //virtual void DrawTheInfluence();
};
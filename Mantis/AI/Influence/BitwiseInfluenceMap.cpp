#include "BitwiseInfluenceMap.h"
#include "..\AIObject\AIObject.h"

void BitwiseInfluenceMap::Update(float dt)
{
    //bail out if nobody to update
    if(m_registeredObjects.size() == 0)
        return;
    
    //clear out map
    memset(m_map,0,m_NumCels * sizeof(int));
    
    //stamp new data
    RegObjList::iterator listObj;
    for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
    {
        RegObj* temp = *listObj;
        //have to update the bits, since you can change direction continuously
        temp->m_iObjType = (char)temp->m_iObjType | GetVelocityDirectionMask(temp->m_pAIObj);
        //StampInfluenceShape(m_map,(*listObj)->m_pAIObj->GetPosition(),(*listObj)->m_iObjSizeX,(*listObj)->m_iObjSizeZ,(*listObj)->m_iObjType);
        (*listObj)->m_bStamped = true;
        (*listObj)->m_vLastPosition = (*listObj)->m_pAIObj->GetPosition();
    }
}

//---------------------------------------------------------
void BitwiseInfluenceMap::RegisterGameObj(AIObject* aiObject)
{
 //   int sizeX,sizeY;
 //   if(aiObject->m_size <4)
 //   {
 //       sizeX = m_iDataSizeX/16;// 2;
 //       sizeY = m_iDataSizeY/16;// 2;
 //   }
 //   else if(aiObject->m_size<11)
 //   {
 //       sizeX = m_iDataSizeX/10;//3;
 //       sizeY = m_iDataSizeY/10;//3;
 //   }
 //   else if(aiObject->m_size<33)
 //   {
 //       sizeX = m_iDataSizeX/8;//4;
 //       sizeY = m_iDataSizeY/8;//4;
 //   }
 //   else if(aiObject->m_size <49)
 //   {
 //       sizeX = m_iDataSizeX/5;//6;
 //       sizeY = m_iDataSizeX/5;//6;
 //   }
 //   else if(aiObject->m_size <65)
 //   {
 //       sizeX = m_iDataSizeX/4;//8;
 //       sizeY = m_iDataSizeX/4;//8;
 //   }
 //   else
 //   {
 //       sizeX = m_iDataSizeX/3;//10;
 //       sizeY = m_iDataSizeX/3;//10;
 //   }
 //   
 //   //set minimum size of 1 in each direction
 //   sizeX = max(1,sizeX);
 //   sizeY = max(1,sizeY);

 //   
 //   RegObj* temp;
 //   temp = new RegObj;
 //   temp->m_iObjType = aiObject->m_type;
 //   temp->m_iObjType |= GetVelocityDirectionMask(aiObject);

 //   temp->m_pAIObj      = aiObject;
 //   temp->m_iObjSizeX     = sizeX;
 //   temp->m_iObjSizeY     = sizeY;
	//temp->m_vLastPosition = aiObject->GetPosition();
 //   temp->m_bStamped      = false;
 //   m_registeredObjects.push_back(temp);
}

//---------------------------------------------------------
int BitwiseInfluenceMap::GetVelocityDirectionMask(AIObject* aiObj)
{
 //   //set up the information bits
 //   //    bottom byte is the object type
 //   //    top byte is the velocity direction
 //   int velDir = 0;
	//if(aiObj->GetCurrentVelocity().x > 0)
 //   velDir |= DIR_RIGHT;
 //   else if (aiObj->GetCurrentVelocity().x < 0)
 //   velDir |= DIR_LEFT;
 //   if(aiObj->GetCurrentVelocity().y > 0)
 //   velDir |= DIR_UP;
 //   else if (aiObj->GetCurrentVelocity().y < 0)
 //   velDir |= DIR_DOWN;
 //   return velDir<<8;
	return (int)1;
}

//---------------------------------------------------------
//void BitwiseInfluenceMap::DrawTheInfluence()
//{
//    glPushMatrix();
//    glDisable(GL_LIGHTING);
//    glTranslatef(0,0,0);
//    glEnable(GL_BLEND);									
//    glBlendFunc(GL_ONE, GL_ONE);					
//    for(int i=0;i<m_numCels;i++)
//    {
//        if(m_map[i])
//        {
//            int y = i / m_iDataSizeY;
//            int x = i - y*m_iDataSizeY;
//            //determine color for type
//            D3DXVECTOR3 color(0,0,0);
//            for(int index = 0;index<8;index++)
//            {
//                int bitset = (m_map[i] & (1<<index));
//                if(bitset)
//                    color += colorArray[index];
//            }
//            glColor3f(color.x(),color.y(),color.z());
//            glBegin(GL_POLYGON);
//            glVertex3f(x*m_celResX,          y*m_celResY,               0);
//            glVertex3f(x*m_celResX,          y*m_celResY+m_celResY*0.5f,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY+m_celResY*0.5f,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY,               0);
//            glEnd();
//
//            color = D3DXVECTOR3(0,0,0);
//            //get colors for direction
//            int direction = m_map[i]>>8;
//            if(direction & DIR_LEFT)
//                color = colorArray[COLOR_SILVER];//left
//            if(direction & DIR_RIGHT)
//                color = colorArray[COLOR_PURPLE];//right
//            glColor3f(color.x(),color.y(),color.z());
//            glBegin(GL_POLYGON);
//            glVertex3f(x*m_celResX,               y*m_celResY+m_celResY*0.5f,0);
//            glVertex3f(x*m_celResX,               y*m_celResY+m_celResY,     0);
//            glVertex3f(x*m_celResX+m_celResX*0.5f,y*m_celResY+m_celResY,     0);
//            glVertex3f(x*m_celResX+m_celResX*0.5f,y*m_celResY+m_celResY*0.5f,0);
//            glEnd();
//            
//            color = D3DXVECTOR3(0,0,0);
//            if(direction & DIR_UP)
//                color = colorArray[COLOR_OLIVE];//up
//            if(direction & DIR_DOWN)
//                color = colorArray[COLOR_TEAL];//down
//            
//            glColor3f(color.x(),color.y(),color.z());
//            glBegin(GL_POLYGON);
//            glVertex3f(x*m_celResX+m_celResX*0.5f,y*m_celResY+m_celResY*0.5f,0);
//            glVertex3f(x*m_celResX+m_celResX*0.5f,y*m_celResY+m_celResY,     0);
//            glVertex3f(x*m_celResX+m_celResX,     y*m_celResY+m_celResY,     0);
//            glVertex3f(x*m_celResX+m_celResX,     y*m_celResY+m_celResY*0.5f,0);
//            glEnd();
//        }
//    }
//    glDisable(GL_BLEND);	
//    glEnable(GL_LIGHTING);
//    glPopMatrix();
//}

//---------------------------------------------------------
void BitwiseInfluenceMap::StampInfluenceShape(int* pMap,D3DXVECTOR3& location,int sizeX, int sizeZ, int value, bool undo)
{
    int gridX = (int)(location.x / m_CelResX);
    int gridZ = (int)(location.z / m_CelResZ);
    
    int startX = gridX - sizeX/2;
    if(startX < 0) startX += m_DataSizeX;
    int startZ = gridZ - sizeZ/2;
    if(startZ < 0) startZ += m_DataSizeZ;
    
    for(int z = startZ; z < startZ + sizeZ; z++)
    {
        for(int x = startX;x<startX + sizeX;x++)
        {
            if(undo)
				pMap[(z % m_DataSizeZ) * m_DataSizeZ + (x % m_DataSizeX)] &= ~value;
            else
                pMap[(z % m_DataSizeZ) * m_DataSizeZ + (x % m_DataSizeX)] |= value;
        }
    }
}

//---------------------------------------------------------
int BitwiseInfluenceMap::GetInfluenceType(int* pMap,D3DXVECTOR3& location)
{
    int gridX = (int)(location.x / m_CelResX);
    int gridZ = (int)(location.z / m_CelResZ);
    return pMap[gridX,gridZ] & 0x0f;
}

//---------------------------------------------------------
int BitwiseInfluenceMap::GetInfluenceDirection(int* pMap,D3DXVECTOR3& location)
{
    int gridX = (int)(location.x / m_CelResX);
    int gridZ = (int)(location.z / m_CelResZ);
    return pMap[gridX, gridZ] >> 8;
}
/**
 * BitwiseInfluenceMap
 */

#pragma once

#include "InfluenceMap.h"

/**
 * BitwiseInfluenceMap - Deprecated
 * Creates and maintains an influence map of bit values
 */
class BitwiseInfluenceMap : public InfluenceMap
{
public:
    //constructor/functions
    BitwiseInfluenceMap():InfluenceMap(IM_BITWISE){}
    ~BitwiseInfluenceMap();

    virtual void Update(float dt);
    virtual void RegisterGameObj(AIObject* object);

    virtual void StampInfluenceShape(int* pMap,D3DXVECTOR3& location,int sizeX,int sizeY, int value, bool undo = false);
    
	int GetVelocityDirectionMask(AIObject* aiObj);
    int  GetInfluenceType(int* pMap,D3DXVECTOR3& location);
    int  GetInfluenceDirection(int* pMap,D3DXVECTOR3& location);
};

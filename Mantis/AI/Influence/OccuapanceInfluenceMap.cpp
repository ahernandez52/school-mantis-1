#include "OccupanceInfluenceMap.h"
#include "..\AIObject\AIObject.h"

void OccupanceInfluenceMap::Update(float dt)
{
    //bail out if nobody to update
    if(m_registeredObjects.size() == 0)
        return;
 
    //clear out map
//    memset(m_map,0,m_numCels*sizeof(int));
    
    RegObjList::iterator listObj;
    //unstamp old locations
    for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
    {
        if((*listObj)->m_bStamped)
            StampInfluenceShape((*listObj)->m_vLastPosition,(*listObj)->m_iObjSizeX,(*listObj)->m_iObjSizeZ, -1);
    }
    
    //stamp new locations
    for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
    {
        RegObj* temp = *listObj;
        StampInfluenceShape((*listObj)->m_pAIObj->GetPosition(),(*listObj)->m_iObjSizeX,(*listObj)->m_iObjSizeZ, 1);
        (*listObj)->m_bStamped = true;
        (*listObj)->m_vLastPosition = (*listObj)->m_pAIObj->GetPosition();
    }
}

void OccupanceInfluenceMap::RemoveGameObj(AIObject* aiObj)
{
    if(m_registeredObjects.size() == 0)
        return;
    
    RegObjList::iterator listObj;
    for(listObj=m_registeredObjects.begin();listObj!=m_registeredObjects.end();++listObj)
    {
        RegObj* temp = *listObj;
        if((*listObj)->m_pAIObj == aiObj)
        {
            if((*listObj)->m_bStamped)
                StampInfluenceShape((*listObj)->m_vLastPosition,(*listObj)->m_iObjSizeX,(*listObj)->m_iObjSizeZ, -1);
            m_registeredObjects.erase(listObj);
            delete temp;
            return;
        }
    }
    
}

void OccupanceInfluenceMap::RegisterGameObj(AIObject* aiObj)
{
 //   int sizeX,sizeY;
 //   if(aiObj->m_size <4)
 //   {
 //       sizeX = m_iDataSizeX/16;
 //       sizeY = m_iDataSizeY/16;
 //   }
 //   else if(aiObj->m_size<11)
 //   {
 //       sizeX = m_iDataSizeX/10;
 //       sizeY = m_iDataSizeY/10;
 //   }
 //   else if(aiObj->m_size<33)
 //   {
 //       sizeX = m_iDataSizeX/8;
 //       sizeY = m_iDataSizeY/8;
 //   }
 //   else if(aiObj->m_size <49)
 //   {
 //       sizeX = m_iDataSizeX/5;
 //       sizeY = m_iDataSizeX/5;
 //   }
 //   else if(aiObj->m_size <65)
 //   {
 //       sizeX = m_iDataSizeX/4;
 //       sizeY = m_iDataSizeX/4;
 //   }
 //   else
 //   {
 //       sizeX = m_iDataSizeX/3;
 //       sizeY = m_iDataSizeX/3;
 //   }
 //   
 //   //set minimum size of 1 in each direction
 //   sizeX = max(1,sizeX);
 //   sizeY = max(1,sizeY);
 //   
 //   RegObj* temp;
 //   temp = new RegObj;
 //   temp->m_pAIObj      = aiObj;
 //   temp->m_iObjSizeX     = sizeX;
 //   temp->m_iObjSizeY     = sizeY;
	//temp->m_vLastPosition = aiObj->GetPosition();
 //   temp->m_bStamped      = false;
 //   m_registeredObjects.push_back(temp);
}

//void OccupanceInfluenceMap::DrawTheInfluence()
//{
//    glPushMatrix();
//    glDisable(GL_LIGHTING);
//    glTranslatef(0,0,0);
//    glEnable(GL_BLEND);                                 
//    glBlendFunc(GL_ONE, GL_ONE);                    
//    for(int i=0;i<m_numCels;i++)
//    {
//        if(m_map[i])
//        {
//            int y = i / m_iDataSizeY;
//            int x = i - y*m_iDataSizeY;
//            float grayscale = m_map[i]/10.0f;
//            glColor3f(grayscale,grayscale,grayscale);
//            glBegin(GL_POLYGON);
//            glVertex3f(x*m_celResX,          y*m_celResY,          0);
//            glVertex3f(x*m_celResX,          y*m_celResY+m_celResY,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY+m_celResY,0);
//            glVertex3f(x*m_celResX+m_celResX,y*m_celResY,          0);
//            glEnd();
//        }
//    }
//    glDisable(GL_BLEND);    
//    glEnable(GL_LIGHTING);
//    glPopMatrix();
//}

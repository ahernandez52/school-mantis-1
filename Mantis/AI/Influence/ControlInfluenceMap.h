/**
 * ControlInfluenceMap
 */

#pragma once

#include "InfluenceMap.h"

/**
 * ControlInfluenceMap
 */
class ControlInfluenceMap : public InfluenceMap
{
public:

    ControlInfluenceMap():InfluenceMap(IM_CONTROL){}
    ~ControlInfluenceMap();

	/**
	 * Update registered objects
	 */
    virtual void Update(float dt);

	/**
	 * Register a new object
	 */
    virtual void RegisterGameObj(AIObject* aiObj);

};

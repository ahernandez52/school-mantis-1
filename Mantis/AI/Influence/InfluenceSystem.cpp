#include "InfluenceSystem.h"
#include "InfluenceMap.h"
#include "ControlInfluenceMap.h"
#include <iostream>

InfluenceSystem::InfluenceSystem(void)
{
}

InfluenceSystem* InfluenceSystem::Instance(void)
{
	static InfluenceSystem* ptr = new InfluenceSystem();
	return ptr;
}

InfluenceSystem::~InfluenceSystem(void)
{
	//If maps are empty bail out
	if (m_FactionMaps.empty())
	{
		return;
	}

	else 
	{
		ShutDown();
	}
}

void InfluenceSystem::Initialize(float worldX, float worldZ, int gridX, int gridZ)
{
	//Create the control maps and insert them using FACTION as key
	ControlInfluenceMap* phalanx = new ControlInfluenceMap();
	phalanx->Initialize(gridX, gridZ, (int)worldX, (int)worldZ);

	ControlInfluenceMap* order = new ControlInfluenceMap();
	order->Initialize(gridX, gridZ, (int)worldX, (int)worldZ);

	ControlInfluenceMap* taac = new ControlInfluenceMap();
	taac->Initialize(gridX, gridZ, (int)worldX, (int)worldZ);

	ControlInfluenceMap* pirates = new ControlInfluenceMap();
	pirates->Initialize(gridX, gridZ, (int)worldX, (int)worldZ);

	RegisterMap(PHALANX, phalanx);
	RegisterMap(ORDER, order);
	RegisterMap(TAAC, taac);
	RegisterMap(PIRATE, pirates);
}

void InfluenceSystem::Update(float dt)
{
	//Iterate through maps, stamping registered objects
	for (const auto& map : m_InfluenceMaps)
	{
		if (map)
		{
			map->Update(dt);
		}
	}
}

void InfluenceSystem::ShutDown(void)
{
	//If maps are empty bail out
	if (m_FactionMaps.empty())
	{
		return;
	}

	//Delete all the influence maps currently in use
	for (auto& map : m_InfluenceMaps)
	{
		InfluenceMap* temp = map;
		map = nullptr;
		delete temp;
	}
	m_InfluenceMaps.clear();
	m_FactionMaps.clear();
}

bool CheckNodeForInclusion(std::list<Node> nodes, InfluenceMap map, int element)
{
	//Check if element is greater than nodes in list
	return true;
}

std::list<Node> InfluenceSystem::PollInfluence(influence_algorithm usage,
											   FACTION thisFaction,
											   FACTION otherFaction)
{
	//Create the return list
	std::list<Node> nodes;

	////Pull the maps for use
	//InfluenceMap* client = m_FactionMaps.find(thisFaction)->second;
	//InfluenceMap* request = m_FactionMaps.find(otherFaction)->second;

	//InfluenceMap* result = new InfluenceMap();
	//result->Initialize(10, 10, 1000.0f, 1000.0f);

	////Determine which algorithm to use
	//switch (usage)
	//{
	//default:
	//	{
	//		break;
	//	}

	//case INF_CONTROL:
	//	{
	//		//Set result to the client control map
	//		result = client;
	//		break;
	//	}

	//case INF_INFLUENCE:
	//	{
	//		//Set result to influence map of client and opponent
	//		result = GetInfluence(client, request);
	//		break;
	//	}

	//case INF_TENSION:
	//	{
	//		//Set result to tension map of client and opponent
	//		result = GetTension(client, request);
	//		break;
	//	}

	//case INF_THREAT:
	//	{
	//		//Set result to threat map of client
	//		result = GetThreat(client, request);
	//		break;
	//	}

	//case INF_VULNERABILITY:
	//	{
	//		//Set result to vulnerability map of opponent
	//		result = GetVulnerability(client, request);
	//		break;
	//	}

	//}

	//Interpret map and push back nodes


	//Return node list
	return nodes;
}

void InfluenceSystem::RegisterMap(FACTION faction, InfluenceMap* map)
{
	m_FactionMaps.insert(std::make_pair(faction, map));
	m_InfluenceMaps.push_back(map);
}

void InfluenceSystem::RegisterInfluence(FACTION faction, AIObject* object)
{
	m_FactionMaps.find(faction)->second->RegisterGameObject(object);
}

void InfluenceSystem::RemoveInfluence(FACTION faction, AIObject* object)
{
	m_FactionMaps.find(faction)->second->RemoveGameObject(object);
}

void InfluenceSystem::ClearInfluence(void)
{
	for (auto& map : m_InfluenceMaps)
	{
		map->Reset();
	}
}

void InfluenceSystem::PrintInfluence(FACTION faction)
{
	InfluenceMap* temp = m_FactionMaps.find(faction)->second;

	switch(faction)
	{
	case PHALANX:
		{
			std::cout << "Phalanx occupance is " << std::endl;
			break;
		}

	case ORDER:
		{
			std::cout << "Order occupance is " << std::endl;
		}

	default:
		break;
	}
	//for (int i = 0 ; i < 100 ; i++)
	//{
	//	std::cout << "Cell number " << i << " holds influence of " << temp->GetValueByIndex(i) << std::endl;
	//}
	for (int j = 0 ; j < 10 ; j++)
	{
		for (int k = 0 ; k < 10 ; k++)
		{
			std::cout << temp->GetValueByIndex( (j * 10) + k ) ;
			std::cout << "_____" ;
		}

		std::cout << std::endl;
	}
}

void InfluenceSystem::PrintAggregateInfluence(FACTION client, FACTION request)
{
	InfluenceMap* temp = GetInfluence(m_FactionMaps.find(client)->second,
		m_FactionMaps.find(request)->second);

	switch(client)
	{
	case PHALANX:
		{
			std::cout << "Phalanx influence is " << std::endl;
			break;
		}

	case ORDER:
		{
			std::cout << "Order influence is " << std::endl;
		}

	default:
		break;
	}

	for (int j = 0 ; j < 10 ; j++)
	{
		for (int k = 0 ; k < 10 ; k++)
		{
			std::cout << temp->GetValueByIndex( (j * 10) + k ) ;
			std::cout << "_____" ;
		}

		std::cout << std::endl;
	}

	delete temp;
}

void InfluenceSystem::PrintThreat(FACTION client, FACTION request)
{
	InfluenceMap* temp = GetThreat(m_FactionMaps.find(client)->second,
		m_FactionMaps.find(request)->second);

	switch(client)
	{
	case PHALANX:
		{
			std::cout << "Phalanx threat is " << std::endl;
			break;
		}

	case ORDER:
		{
			std::cout << "Order threat is " << std::endl;
		}

	default:
		break;
	}

	for (int j = 0 ; j < 10 ; j++)
	{
		for (int k = 0 ; k < 10 ; k++)
		{
			std::cout << temp->GetValueByIndex( (j * 10) + k ) ;
			std::cout << "_____" ;
		}

		std::cout << std::endl;
	}

	delete temp;
}

void InfluenceSystem::PrintVulnerability(FACTION client, FACTION request)
{
	InfluenceMap* temp = GetVulnerability(m_FactionMaps.find(client)->second,
		m_FactionMaps.find(request)->second);

	switch(client)
	{
	case PHALANX:
		{
			std::cout << "Phalanx vulnerability is at" << std::endl;
			break;
		}

	case ORDER:
		{
			std::cout << "Order vulnerability is  at" << std::endl;
		}

	default:
		break;
	}

	for (int j = 0 ; j < 10 ; j++)
	{
		for (int k = 0 ; k < 10 ; k++)
		{
			std::cout << temp->GetValueByIndex( (j * 10) + k ) ;
			std::cout << "_____" ;
		}

		std::cout << std::endl;
	}

	delete temp;
}

void InfluenceSystem::PrintTension(FACTION client, FACTION request)
{
	InfluenceMap* temp = GetTension(m_FactionMaps.find(client)->second,
		m_FactionMaps.find(request)->second);

	std::cout << "Tension is " << std::endl;

	for (int j = 0 ; j < 10 ; j++)
	{
		for (int k = 0 ; k < 10 ; k++)
		{
			std::cout << temp->GetValueByIndex( (j * 10) + k ) ;
			std::cout << "_____" ;
		}

		std::cout << std::endl;
	}

	delete temp;
}

//============================
// Private Map creations
//============================
InfluenceMap* InfluenceSystem::GetInfluence(InfluenceMap* client, InfluenceMap* request)
{
	InfluenceMap* ret = new InfluenceMap();
	ret->Initialize(client->GetCols(), client->GetRows(), client->GetWorldX(), client->GetWorldZ());
	*ret = *client - *request;
	return ret;
}

InfluenceMap* InfluenceSystem::GetTension(InfluenceMap* client, InfluenceMap* request)
{
	InfluenceMap* ret = new InfluenceMap();
	ret->Initialize(client->GetCols(), client->GetRows(), client->GetWorldX(), client->GetWorldZ());
	*ret	= *client + *request;
	return ret;
}

InfluenceMap* InfluenceSystem::GetThreat(InfluenceMap* client, InfluenceMap* request)
{
	return GetVulnerability(request, client);
}

InfluenceMap* InfluenceSystem::GetVulnerability(InfluenceMap* client, InfluenceMap* request)
{
	//Vulnerability = Tension map - AbsoluteValue(Influence map)
	InfluenceMap* tension = new InfluenceMap();
	InfluenceMap* infl = new InfluenceMap();
	InfluenceMap* ret = new InfluenceMap();

	ret->Initialize(client->GetCols(), client->GetRows(), client->GetWorldX(), client->GetWorldZ());
	infl->Initialize(client->GetCols(), client->GetRows(), client->GetWorldX(), client->GetWorldZ());
	tension->Initialize(client->GetCols(), client->GetRows(), client->GetWorldX(), client->GetWorldZ());

	tension = GetTension(client, request);
	infl = GetInfluence(client, request);
	infl->AbsoluteValue(infl, infl);
	*ret = *tension - *infl;

	return ret;
}
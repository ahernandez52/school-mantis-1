#include "AIFactory.h"
#include "..\Messaging\PostOffice.h"
#include "..\..\FRAMEWORK\Managers\WorldManager.h"

Actor* AIFactory::CreateAI(D3DXVECTOR3 position, FACTION f, SHIPTYPE s)
{
	Actor* tempActor = new Actor(position);

	//====================
	// AIObject Creation
	//====================

	ShipAI* tempAI = new ShipAI(tempActor);
	tempAI->SetFaction(f);
	tempAI->SetShipType(s);
	tempAI->SetAIType(AI_SHIP);
	tempAI->SetMaxSpeed(40.0f);

	//====================
	// Graphics Object Creation
	//====================

	GraphicSchematic graphics;
	PhysicsSchematic physics;

	switch(s)
	{
	case FIGHTER:
		{
			graphics.fileName = "small_fighter_1";				//folder name
			graphics.name = "small_fighter";						//schematic ID
			graphics.Shader = "UberShaderOne";					//shader
			//graphics.mapNames[0] = "small_fighter_1_DIF.jpg";	//specify detail maps
			//graphics.mapNames[1] = "small_fighter_1_BMP.jpg";
			//graphics.mapNames[2] = "";
			//graphics.mapNames[3] = "small_fighter_1_SPEC.jpg";
			//graphics.mapNames[4] = "small_fighter_1_ILL.jpg";

			physics.Identifier = "Heavy";				//specify identifier
			physics.Type = 1;							// (1 is sphere type. Will be enumerated)
			physics.Mass = 2.0f;						//set mass
			physics.Moveable = true;					//is moveable
			physics.Radius = 30.0f;						//BV Sphere radius = 30

			//set the state of the AI object
			//tempAI->SetState(FighterIdle::Instance());
			tempAI->SetSize(1);
			tempAI->SetMaxRotation(D3DX_PI/4);
			tempAI->SetMaxAngularAcceleration(D3DX_PI/4);
			tempAI->SetMaxForce(Vector3(0, 0, 200));
			break;
		}
	case FRIGATE:
		{
			graphics.fileName = "frigate_1";				//folder name
			graphics.name = "frigate";					//schematic ID
			graphics.Shader = "UberShaderOne";					//shader
			//graphics.mapNames[0] = "frigate_1_DIF.jpg";	//specify detail maps
			//graphics.mapNames[1] = "frigate_1_BMP.jpg";
			//graphics.mapNames[2] = "frigate_1_NRM.jpg";
			//graphics.mapNames[3] = "frigate_1_SPEC.jpg";
			//graphics.mapNames[4] = "frigate_1_ILL.jpg";

			physics.Identifier = "Heavy";				//specify identifier
			physics.Type = 1;							// (1 is sphere type. Will be enumerated)
			physics.Mass = 2.0f;						//set mass
			physics.Moveable = true;					//is moveable
			physics.Radius = 30.0f;						//BV Sphere radius = 30

			//set the state of the AI object
			//tempAI->SetState(FrigateIdle::Instance());
			tempAI->SetMaxSpeed(20.0f);
			tempAI->SetSize(3);
			break;
		}
	case CRUISER:
		{
			graphics.fileName = "battle_cruiser_1";				//folder name
			graphics.name = "battle_cruiser";					//schematic ID
			graphics.Shader = "UberShaderOne";					//shader
			//graphics.mapNames[0] = "battle_cruiser_1_DIF.jpg";	//specify detail maps
			//graphics.mapNames[1] = "battle_cruiser_1_BMP.jpg";
			//graphics.mapNames[2] = "battle_cruiser_1_NRM.jpg";
			//graphics.mapNames[3] = "battle_cruiser_1_SPEC.jpg";
			//graphics.mapNames[4] = "battle_cruiser_1_ILL.jpg";

			physics.Identifier = "Heavy";				//specify identifier
			physics.Type = 1;							// (1 is sphere type. Will be enumerated)
			physics.Mass = 2.0f;						//set mass
			physics.Moveable = true;					//is moveable
			physics.Radius = 30.0f;						//BV Sphere radius = 30

			//set the state of the AI object
			//tempAI->SetState(CruiserIdle::Instance());
			tempAI->SetMaxSpeed(10.0f);
			tempAI->SetSize(5);
			break;
		}
	case UNKNOWN:
		{
			graphics.fileName = "crystal_1";				//folder name
			graphics.name = "crystal";					//schematic ID
			graphics.Shader = "UberShaderOne";					//shader
			//graphics.mapNames[0] = "crystal_1_DIF.jpg";	//specify detail maps
			//graphics.mapNames[1] = "";
			//graphics.mapNames[2] = "";
			//graphics.mapNames[3] = "";
			//graphics.mapNames[4] = "";



			//set the state of the AI object
			//tempAI->SetState(CruiserIdle::Instance());
			tempAI->SetMaxSpeed(0.0f);
			tempAI->SetSize(1);
			break;
		}
		break;
	}
	//WMI->RegisterNewMeshSchematic(graphics);
	WMI->RegisterNewMeshSchematic(graphics.name, graphics.fileName,graphics.mapNames); 
	WMI->RegisterGraphicSchematic(graphics);
	WMI->RegisterPhysicsSchematic(physics);

	//====================

	tempActor = WMI->CreateActor("NULL", "NULL", "NULL", "NULL");

	tempActor->AI = tempAI;

	tempActor->Graphics = WMI->CreateD3D9ObjectFromSchematic(graphics.name);
	tempActor->Graphics->m_OwningActor = tempActor;

	if(s == FRIGATE)
		tempActor->Graphics->m_scale = D3DXVECTOR3(0.4f,0.4f,0.4f);
	if(s == FIGHTER)
		tempActor->Graphics->m_scale = D3DXVECTOR3(0.3f,0.3f,0.3f);
	if(s == UNKNOWN)
		tempActor->Graphics->m_scale = D3DXVECTOR3(0.5f,0.5f,0.5f);
	//====================

	// send the first message to self update
	//Add a message to the Post -     FROM            -   TO                 - TIME - MESSAGE
	POST->AddMail(new Message(tempActor->ID, tempActor->ID,     0,    SELF_UPDATE));

	return tempActor;
}
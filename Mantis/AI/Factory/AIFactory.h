/**
 * AIFactory - Factory to create AI related Actors
 */

#pragma once

#include "..\AIObject\ShipAI.h"
#include "..\..\FRAMEWORK\Objects\Actor.h"
#include "..\AIObject\AIFaction.h"
#include "..\AIObject\AIShipType.h"

/**
 * AIFactory - Factory to create AI related Actors
 */
class AIFactory
{
public:

public:
	AIFactory() {}
	~AIFactory() {}

	/**
	 * CreateAI 
	 * Returns an actor with an attacked AIObject
	 */
	Actor* CreateAI(D3DXVECTOR3 position, FACTION f, SHIPTYPE s);
};
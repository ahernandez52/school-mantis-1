#pragma once


#include "../../FRAMEWORK/Utility/MantisUtil.h"

using namespace std;

class Actor;

class Formation
{
private:
	Vector3 Position;	//leaders position, leader is IDFillingPos[0]
	Quaternion Rotation;
	vector<Vector3> PositionFromCenter;
	//vector<int> IDFillingPos;
	vector<Actor*> m_ActorsinPos;
public:
	Formation(Vector3 position){Position = position;Rotation = Quaternion(0,0,0,1); PositionFromCenter.push_back(Vector3(0,0,0)); m_ActorsinPos.push_back(nullptr);}

	void AddPosition(Vector3 OffsetFromPosition);
	void UpdatePositions();
	
//	void AddVehicle(int id, int position);
	void AddVehicle(Actor* actor, int PositionInFormation);
	Actor* RemoveVehicle(Actor* actor);
	Actor* Leader();
	int size(){return m_ActorsinPos.size();}
	Vector3 GetLeaderPosition(){return Position;}
	vector<Vector3> GetOffsets(){return PositionFromCenter;}

	void ChangeToFormation(Formation newFormation);

	Vector3 GetPositionOfActor(Actor* actor);
	~Formation(void);
};

class FormationTypes
{
private:
	vector<Formation> formations;
public:
	FormationTypes();
	enum Types{V, T, OneWing, TwoRows, X};
	Formation GetFormationType(Types type){return formations[type];}
};
#include "Formation.h"
#include "../AIObject/AIObject.h"
#include "../../FRAMEWORK/Managers/WorldManager.h"
#include "../AIObject/ShipAI.h"
#include "../../FRAMEWORK/Utility/MantisUtil.h"

void RotateOnOrigin(Vector3* pt, Quaternion rotation)
{
	D3DXMATRIX mat;

/*	D3DXMatrixRotationX(&mat,angle.x);
	D3DXMatrixRotationY(&mat,angle.y);
	D3DXMatrixRotationZ(&mat,angle.z);*/
	D3DXMatrixRotationQuaternion(&mat,&rotation);

	D3DXVec3TransformCoord(pt,pt,&mat);
}


/*void Formation::AddVehicle(int id, int position)
{
	if(position>(int)PositionFromCenter.size()-1 || position<0)
		return; 

	if(IDFillingPos[position] != -1)
		return;
	if(position == 0)
	{
		Quaternion temp = WMI->GetAIFromId(id)->GetCurrentOrientation();
		Rotation=temp*Rotation;
	}

	for(UINT i = 0; i<IDFillingPos.size();i++)
	{ 
		if(id==IDFillingPos[i])
		{
			IDFillingPos[i] = -1;
		}
	}
	IDFillingPos[position] = id;
	((ShipAI*)WMI->GetAIFromId(id))->SetFormation(this);/*((Aircraft*) EntityMgr->GetEntityFromID(id))->AddFormation(this);  <ALERT> Change to WMI call*/
//}

void Formation::AddVehicle(Actor* actor, int position)
{
	if(position>(int)PositionFromCenter.size()-1 || position<0)
		return; 
	if(m_ActorsinPos[position] != nullptr || m_ActorsinPos[position] != NULL)
		return;
	if(position == 0)
	{
		Quaternion temp = ((ShipAI*)actor->AI)->GetCurrentOrientation();
		Rotation=temp*Rotation;
	}
	for(UINT i = 0; i<m_ActorsinPos.size();i++)
	{ 
		if(actor==m_ActorsinPos[i])
		{
			m_ActorsinPos[i] = nullptr;
		}
	}
	m_ActorsinPos[position] = actor;
	((ShipAI*)actor->AI)->SetFormation(this);/*((Aircraft*) EntityMgr->GetEntityFromID(id))->AddFormation(this);  <ALERT> Change to WMI call*/
}

Actor* Formation::Leader()
{
	if(m_ActorsinPos[0]!= nullptr)
		return m_ActorsinPos[0];
	else
	{
		for(UINT i = 0; i<m_ActorsinPos.size();i++)
		{
			if(m_ActorsinPos[i] != nullptr)
			{
				Actor* temp = RemoveVehicle(m_ActorsinPos[i]);
				AddVehicle(temp,0);
				return m_ActorsinPos[0];
			}
		}
		return nullptr;
	}
}

void Formation::AddPosition(Vector3 position)
{
	for(UINT i = 0; i<PositionFromCenter.size();i++)
	{
		if(position == PositionFromCenter[i])
			return;
	}

	PositionFromCenter.push_back(position);
	m_ActorsinPos.push_back(nullptr);
}

Actor* Formation::RemoveVehicle(Actor* actor)
{
	for(UINT i = 0; i<m_ActorsinPos.size();i++)
	{
		if(m_ActorsinPos[i] == actor)
		{
			m_ActorsinPos[i] = nullptr;
			((ShipAI*)actor->AI)->RemoveFormation();/*((Aircraft*)EntityMgr->GetEntityFromID(id))->RemoveFormation(); <ALERT> Change to WMI call*/
		}
	}
	return nullptr;
}



Vector3 Formation::GetPositionOfActor(Actor* actor)
{
	
		for(UINT i = 0; i<m_ActorsinPos.size();i++)
		{
			if(actor == m_ActorsinPos[i])
			{
				Vector3 newPosition= PositionFromCenter[i]; /*PositionFromCenter[i]*((Vehicle*)EntityMgr->GetEntityFromID(id))->GetSize()*2; <ALERT> Change to WMI call*/
				//float temp = abs(1-D3DXVec3Length(&(((ShipAI*)Leader()->AI)->GetCurrentVelocity()/((ShipAI*)Leader()->AI)->GetMaxSpeed())))*Leader()->AI->GetCurrentVelocity();
				//newPosition.z -= temp*newPosition.z;
				newPosition *=52;
				//Quaternion leaderRotation = WMI->GetActorFromId(IDFillingPos[0])->m_Orientation; /*EntityMgr->GetEntityFromID(IDFillingPos[0])->GetOrientation();<ALERT> Change to WMI call*/
				RotateOnOrigin(&newPosition,Rotation);
				newPosition +=Position+Leader()->AI->GetCurrentVelocity();
				return newPosition;
			}
		}
		return Vector3(0,0,0); /*((Vehicle*)EntityMgr->GetEntityFromID(id))->GetPosition();<ALERT> Change to WMI call*/
}

void Formation::UpdatePositions()
{
	if(m_ActorsinPos[0]!= nullptr)
	{
		Position = Leader()->AI->GetPosition();
		Rotation = Leader()->m_Orientation; 
	}
} 

void Formation::ChangeToFormation(Formation newFormation)
{
	PositionFromCenter = newFormation.GetOffsets();
	if(newFormation.GetOffsets().size()<m_ActorsinPos.size())
	{
		vector<Actor*> temp = m_ActorsinPos;
		for(unsigned int i=PositionFromCenter.size();i<temp.size();i++)
		{
			RemoveVehicle(temp[i]);
		}
		m_ActorsinPos.clear();
		
		for(unsigned int i = 0; i<newFormation.GetOffsets().size(); i++)
		{
			m_ActorsinPos.push_back(temp[i]);
		}

	}
	if(newFormation.GetOffsets().size()>m_ActorsinPos.size())
	{
		int difference = newFormation.GetOffsets().size()-m_ActorsinPos.size();
		for(int i = 0; i<difference; i++)
		{
			m_ActorsinPos.push_back(nullptr);
		}
	}
}

Formation::~Formation(void)
{
}

FormationTypes::FormationTypes()
{
	Formation Vee(Vector3(0,0,0));

	Vee.AddPosition(Vector3(1.5,0,1.5));
	Vee.AddPosition(Vector3(3,0,3));
	Vee.AddPosition(Vector3(-1.5,0,1.5));
	Vee.AddPosition(Vector3(-3,0,3));

	formations.push_back(Vee);

	Formation T(Vector3(0,0,0));
	T.AddPosition(Vector3(0,0,2));
	T.AddPosition(Vector3(0,0,4));
	T.AddPosition(Vector3(2,0,0));
	T.AddPosition(Vector3(-2,0,0));

	formations.push_back(T);



	Formation oneWing(Vector3(0,0,0));
	oneWing.AddPosition(Vector3(2,0,0));

	formations.push_back(oneWing);

	Formation twoRows(Vector3(0,0,0));
	twoRows.AddPosition(Vector3( -2,0,0));
	twoRows.AddPosition(Vector3( 2,0,0));
	twoRows.AddPosition(Vector3( 4,0,0));
	twoRows.AddPosition(Vector3(0,2,-2));
	twoRows.AddPosition(Vector3( -2,2,-2));
	twoRows.AddPosition(Vector3( 2,2,-2));
	twoRows.AddPosition(Vector3( 4,2,-2));
	formations.push_back(twoRows);

	Formation X(Vector3(0,0,0));
	X.AddPosition(Vector3(1.5,0,1.5));
	X.AddPosition(Vector3(-1.5,0,1.5));
	X.AddPosition(Vector3(1.5,0,-1.5));
	X.AddPosition(Vector3(-1.5,0,-1.5));
	formations.push_back(X);
}
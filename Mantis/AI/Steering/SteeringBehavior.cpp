#include "SteeringBehavior.h"
#include "../AIObject/ShipAI.h"
#include "../Utility/AIUtility.h"
#include "../../FRAMEWORK/Objects/Actor.h"
#include "../../GAMEPLAY/Object/Player.h"
#include "../../FRAMEWORK/Utility/JSONParser.h"


class Wall
{
};

const float MinBoxLength = 3.0f;

SteeringBehavior::SteeringBehavior(void) : 
	m_Feelers(3),
	m_ViewDistance(10.0f),
	m_EvadeDistanceSq(100.0f),
	m_BoxLength(100.0f),
	m_WanderJitter(2.0f),
	m_WanderRadiusY(30.0f),
	m_WanderRadiusXZ(100.0f),
	m_WeightSeek(1.0f),
	m_WeightFlee(1.0f),
	m_WeightArrive(2.0f),
	m_WeightPursue(3.0f),
	m_WeightOffsetPursue(3.0f),
	m_WeightEvade(0.75f),
	m_WeightWander(0.1f),
	m_WeightObstacleAvoidance(3.0f),
	m_WeightWallAvoidance(8.0f),
	m_WeightInterpose(3.0f),
	m_WeightHide(3.0f),
	m_WeightAlignment(1.0f),
	m_WeightSeparation(4.0f),
	m_WeightCohesion(3.0f),
	m_WeightFollowPath(2.0f),
	m_WeightFacePoint2D(1.0f),
	m_WeightFacePoint3D(1.0f),
	m_WeightFaceForward2D(1.0f),
	m_WeightFaceForward3D(1.0f),
	m_SummingMethod(PRIORITIZED), 
	m_Deceleration(slow)
{
	//Set values to default
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosSeek	= Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosFlee	= Vector3(0.0f, 0.0f, 0.0f);

	m_TargetPoint2D	= D3DXVECTOR2(0.0f, 0.0f);
	m_TargetPoint3D	= Vector3(0.0f, 0.0f, 0.f);

	m_BaseOrientation = D3DXQUATERNION(0.0f, 0.0f, 0.0f, 1.0f);

	m_Offset		= Vector3(0.0f, 0.0f, 0.0f);

	//Set random wander target
	float theta = RandomBinomial();// * (2 * D3DX_PI);
	m_WanderTarget = Vector3(m_WanderRadiusXZ * cos(theta),
								 m_WanderRadiusY * sin(theta),
								 m_WanderRadiusXZ * cos(theta));

	m_WanderOffset = 200.0f;

	D3DXVec3Normalize(&m_WanderTarget, &m_WanderTarget);

	m_WanderProjection = Vector3(0.0f, 0.0f, 0.0f);
	
	m_AgentEvade = nullptr;
	m_AgentPursue = nullptr;

	SteeringOff();
}

SteeringBehavior::SteeringBehavior(ShipAI* agent) : 
	m_Agent(agent), 
	m_Feelers(3),
	m_ViewDistance(800.0f),
	m_EvadeDistanceSq(200.0f),
	m_BoxLength(150.0f),
	m_WanderJitter(10.0f),
	m_WanderRadiusY(30.0f),
	m_WanderRadiusXZ(80.0f),
	m_WeightSeek(1.2f),
	m_WeightFlee(1.0f),
	m_WeightArrive(3.0f),
	m_WeightPursue(1.0f),
	m_WeightOffsetPursue(3.0f),
	m_WeightEvade(0.75f),
	m_WeightWander(0.25f),
	m_WeightObstacleAvoidance(1.2f),
	m_WeightWallAvoidance(8.0f),
	m_WeightInterpose(3.0f),
	m_WeightHide(3.0f),
	m_WeightAlignment(1.0f),
	m_WeightSeparation(4.0f),
	m_WeightCohesion(3.0f),
	m_WeightFollowPath(2.0f),
	m_SummingMethod(PRIORITIZED), 
	m_Deceleration(slow)
{
	//Set values to default
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosSeek	= Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosFlee	= Vector3(0.0f, 0.0f, 0.0f);

	m_TargetPoint2D	= D3DXVECTOR2(0.0f, 0.0f);
	m_TargetPoint3D	= Vector3(0.0f, 0.0f, 0.f);

	m_BaseOrientation = D3DXQUATERNION(0, 0, 0, 1);

	m_Offset		= Vector3(0.0f, 0.0f, 0.0f);

	//Set random wander target
	float theta = RandomBinomial() * (2 * D3DX_PI);
	m_WanderTarget = Vector3(m_WanderRadiusXZ * cos(theta),
								 m_WanderRadiusY * sin(theta),
								 m_WanderRadiusXZ * cos(theta));
	D3DXVec3Normalize(&m_WanderTarget, &m_WanderTarget);

	m_WanderOffset = 80.0f;

	m_WanderProjection = Vector3(0.0f, 0.0f, 0.0f);
	
	m_AgentEvade = nullptr;
	m_AgentPursue = nullptr;

	SteeringOff();
}

SteeringBehavior::SteeringBehavior(ShipAI* agent, std::string paramFile) :
	m_Agent(agent),
	m_Feelers(3),
	m_SummingMethod(PRIORITIZED), 
	m_Deceleration(fast)
{
	//Set values to default
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosSeek	= Vector3(0.0f, 0.0f, 0.0f);
	m_TargetPosFlee	= Vector3(0.0f, 0.0f, 0.0f);

	m_TargetPoint2D	= D3DXVECTOR2(0.0f, 0.0f);
	m_TargetPoint3D	= Vector3(0.0f, 0.0f, 0.f);

	m_BaseOrientation = D3DXQUATERNION(0, 0, 0, 1);

	m_Offset		= Vector3(0.0f, 0.0f, 0.0f);

	//Set random wander target
	float theta = RandomBinomial() * (2 * D3DX_PI);
	m_WanderTarget = Vector3(m_WanderRadiusXZ * cos(theta),
								 m_WanderRadiusY * sin(theta),
								 m_WanderRadiusXZ * cos(theta));
	D3DXVec3Normalize(&m_WanderTarget, &m_WanderTarget);

	m_WanderOffset = 50.0f;

	m_WanderProjection = Vector3(0.0f, 0.0f, 0.0f);
	
	m_AgentEvade = nullptr;
	m_AgentPursue = nullptr;

	SteeringOff();

	//Load parameters from file
	LoadParamsFromFile(paramFile);
}

SteeringBehavior::~SteeringBehavior(void)
{
}




/**
 * Steering Behaviors - Linear Velocity Based
 */




//Seek
Vector3 SteeringBehavior::Seek(Vector3 targetPos)
{
	Vector3 desiredVelocity = targetPos - m_Agent->GetPosition();

	D3DXVec3Normalize(&desiredVelocity, &desiredVelocity);
	desiredVelocity *= m_Agent->GetMaxSpeed();

	return desiredVelocity - m_Agent->GetCurrentVelocity();
}

//Flee
Vector3 SteeringBehavior::Flee(Vector3 targetPos)
{
	Vector3 desiredVelocity = m_Agent->GetPosition() - targetPos;


	//Only flee if the targetPos is within range
	float panicDist = 200.0f * 200.0f;

	Vector3 distance = m_Agent->GetPosition() - targetPos;

	if (D3DXVec3LengthSq(&distance) > panicDist)
	{
		return Vector3(0.0f, 0.0f, 0.0f);
	}

	D3DXVec3Normalize(&desiredVelocity, &desiredVelocity);
	desiredVelocity *= m_Agent->GetMaxSpeed();

	return desiredVelocity - m_Agent->GetCurrentVelocity();
}

//Arrive
Vector3 SteeringBehavior::Arrive(Vector3 targetPos, Deceleration deceleration)
{
	Vector3 ToTarget = targetPos - m_Agent->GetPosition();

	//Calculate distance squared to target
	float distance = D3DXVec3Length(&ToTarget);

	if (distance > D3DX_16F_EPSILON)
	{
		//Tune deceleration
		float decelTuning = 0.3f;

		//Calculate speed required to reach the destination
		float speed = distance / ((float)deceleration * decelTuning);

		//Clip speed
		float agentSpeed = m_Agent->GetMaxSpeed();
		speed = min(agentSpeed, speed);

		//Calculate desired force
		Vector3 desiredVelocity = ToTarget * speed / distance;

		return desiredVelocity - m_Agent->GetCurrentVelocity();
	}

	return Vector3(0.0f, 0.0f, 0.0f);
}

//Pursue
Vector3 SteeringBehavior::Pursue(AIObject* target)
{
	Vector3 toTarget = target->GetPosition() - m_Agent->GetPosition();

	double RelativeHeading = D3DXVec3Dot(&target->GetPosition(), &m_Agent->GetPosition());

	if (RelativeHeading < -0.95)
	{
		return Seek(target->GetPosition());
	}

	
	//Look ahead time is inversely proportional to the summed velocity
	Vector3 targetPos;

	if (target->GetAIType() != AI_PLAYER)
	{
		float LookAheadTime = D3DXVec3Length(&toTarget) / (m_Agent->GetMaxSpeed() + ((ShipAI*)target)->GetMaxSpeed());

		targetPos = target->GetPosition() + target->GetCurrentVelocity() * LookAheadTime;
	}
	else
	{
		float playerMax = ((Player*)target->GetSelf()->Gameplay)->m_MaxSpeed;
		Vector3 playerVel = ((Player*)target->GetSelf()->Gameplay)->GetVelocity();
		float LookAheadTime = D3DXVec3Length(&toTarget) / (m_Agent->GetMaxSpeed() + playerMax);

		targetPos = target->GetPosition() +  playerVel * LookAheadTime;
	}

	return Arrive(targetPos, slow);
}

//Offset pursue
Vector3 SteeringBehavior::OffsetPursue(AIObject* target, Vector3 offset)
{
	//Put offset into target's local space
	Vector3 worldOffset = TransformAtoB(offset, target->GetPosition());

	Vector3 toOffset = worldOffset - m_Agent->GetPosition();

	float toTarget = toOffset.Length() / (m_Agent->GetMaxSpeed() + target->GetCurrentVelocity().Length());

	return Arrive(worldOffset + target->GetCurrentVelocity() * toTarget, fast);
}

//Evade
Vector3 SteeringBehavior::Evade(AIObject* target)
{
	//Get relative position
	Vector3 toTarget = target->GetPosition() - m_Agent->GetPosition();

	float LookAheadTime;

	//Flee from predicted position
	Vector3 predictedPos;

	if (target->GetAIType() != AI_PLAYER)
	{
		LookAheadTime = D3DXVec3Length(&toTarget) / (m_Agent->GetMaxSpeed() + ((ShipAI*)target)->GetMaxSpeed());
		predictedPos = target->GetPosition() + target->GetCurrentVelocity() * LookAheadTime;
	}
	else
	{
		Vector3 playerVel = ((Player*)target->GetSelf()->Gameplay)->GetVelocity() ;
		LookAheadTime = D3DXVec3Length(&toTarget) / (m_Agent->GetMaxSpeed() + ((ShipAI*)target)->GetMaxSpeed());
		predictedPos = target->GetPosition() + playerVel * LookAheadTime;
	}
	
	return Flee(predictedPos);
}

//Wander
Vector3 SteeringBehavior::Wander(void)
{
	//Update wander direction
	m_WanderTarget.x += RandomBinomial() * m_WanderJitter;
	m_WanderTarget.y += RandomBinomial() * m_WanderJitter;
	m_WanderTarget.z += RandomBinomial() * m_WanderJitter;
	D3DXVec3Normalize(&m_WanderTarget, &m_WanderTarget);
	m_WanderTarget *= m_WanderRadiusXZ;

	//Calculate the projection vector
	Vector3 target = m_Agent->GetCurrentOrientation().Forward();
	D3DXVec3Normalize(&target, &target);
	target *= -m_WanderOffset;

	//Calculate the target 
	target += m_WanderTarget + m_Agent->GetPosition();

	m_WanderProjection = target;

	return Seek(target);
}

//Obstacle avoidance
Vector3 SteeringBehavior::ObstacleAvoidance(const std::vector<AIObject*>* obstacles)
{
	//Get agent heading
	Vector3 heading = -m_Agent->GetCurrentOrientation().Forward();
	D3DXVec3Normalize(&heading, &heading);

	//Calculate detection box length
	float BoxLength = m_BoxLength + (D3DXVec3Length(&m_Agent->GetCurrentVelocity()) /
		 m_Agent->GetMaxSpeed()) * m_BoxLength;

	//Tag all vehicles that are within range of the detection box
	AIObject* closestObs = nullptr;
	float distanceSq;
	float DistToClosestIntersect = FLT_MAX;
	Vector3 intersection;

	for (const auto& obstacle : *obstacles)
	{
		if (obstacle->GetAIType() == AI_OBSTACLE ||
			obstacle->GetAIType() == AI_SHIP ||
			obstacle->GetAIType() == AI_PLAYER &&
			obstacle != m_Agent)
		{
			//Check if it is within range
			distanceSq = D3DXVec3LengthSq(&(m_Agent->GetPosition() - obstacle->GetPosition()));

			if (distanceSq < BoxLength * BoxLength)
			{
				//Get relative position to obstacle
				Vector3 direction = obstacle->GetPosition() - m_Agent->GetPosition();
				D3DXVec3Normalize(&direction, &direction);

				//Check angle to object
				float angleToObject = acosf(D3DXVec3Dot(&heading, &direction));

				//If object is ahead of agent consider further
				if (angleToObject < D3DX_PI / 2)
				{
					//Turn obstacle into sphere
					Sphere obs;
					obs.m_Center = obstacle->GetPosition();
					obs.m_Radius = obstacle->GetSize();

					float t;
					Vector3 intersectPoint;

					//Perform ray/sphere intersect test
					if (IntersectRaySphere(m_Agent->GetPosition(), 
						heading, obs, t, intersectPoint))
					{
						std::cout << "Look out for that obstacle!" << std::endl;

						float DistToIntersect = abs(D3DXVec3Length(&(m_Agent->GetPosition() - intersectPoint)));

						if (DistToIntersect < DistToClosestIntersect)
						{
							DistToClosestIntersect = DistToIntersect;
							closestObs = obstacle;
							intersection = intersectPoint;
						}
	
						std::cout << "It is " << DistToClosestIntersect << " away!" << std::endl;
					}//End ray/sphere test

				}//End angle test

			}//End distance test

		}//End type check

	}//End group evaluation

	if (closestObs != nullptr)
	{
		float multiplier = 1.0f + (m_BoxLength - DistToClosestIntersect) / m_BoxLength;
		float t;
		Vector3 lateralForce, normalizedLatForce, steeringForce;

		//Calculate the closest point on heading to center of circle
		ClosestPointOnLine(closestObs->GetPosition(), m_Agent->GetPosition(), intersection, 
			t, lateralForce);

		//If the agent is heading directly into the obstacle, apply force to up vector
		float angle = acos(D3DXVec3Dot(&heading, D3DXVec3Normalize(&normalizedLatForce, &lateralForce)));
		if (D3DX_PI - angle < D3DX_PI / 8)
		{
			steeringForce = m_Agent->GetCurrentOrientation().Forward() * D3DXVec3Length(&lateralForce);
			return steeringForce;
		}

		steeringForce = lateralForce * 3.0f - closestObs->GetPosition();

		//steeringForce *= multiplier;

		return steeringForce;
	}

	else
	{
		return Vector3(0, 0, 0);
	}
}

//Wall avoidance
Vector3 SteeringBehavior::WallAvoidance(const std::vector<Wall*>& walls)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Interpose
Vector3 SteeringBehavior::Interpose(const Vector3* targetA, const Vector3* targetB)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Hide
Vector3 SteeringBehavior::Hide(const AIObject* target, const std::vector<Actor*>& obstacles)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Separation
Vector3 Separation(const std::vector<AIObject*>& neighbors)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Cohesion
Vector3 Cohesion(const std::vector<AIObject*>& neighbors)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Alignment
Vector3 Alignment(const std::vector<AIObject*>& neighbors)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}

//Follow path
Vector3 FollowPath(Vector3 target)
{
	return Vector3(0.0f, 0.0f, 0.0f);
}




/**
 * Steering Behaviors - Angular Velocity Based - 2D
 */


//float MapToPi(float rotation)
//{
//	if (rotation > D3DX_PI)
//		return rotation - 2 * D3DX_PI;
//	if (rotation < D3DX_PI)
//		return rotation + 2* D3DX_PI;
//	return rotation;
//}



//Align 2D
float SteeringBehavior::Align2D(float orientation)
{
	float rotation = orientation; // Use agent rotation for 2D      float rotation = orientation - m_Agent->GetCurrentRotation();

	//Find the shortest path to desired rotation
	rotation = MapToPi(rotation);

	//Calculate the magnitude of rotation change
	float rotationAngle = fabs(rotation);

	//Bail out if no change
	if (rotationAngle < D3DX_16F_EPSILON)
	{
		return 0.0f;
	}

	float targetRotation;
	float slowRadius = D3DX_PI / 2;

	//Rotate at full speed
	if (rotationAngle > slowRadius)
	{
		targetRotation = m_Agent->GetMaxRotation();
	}

	//Rotate slowly
	else
	{
		targetRotation = m_Agent->GetMaxRotation() * rotationAngle / slowRadius;
	}

	//Calculate final target rotation
	targetRotation *= rotation / rotationAngle;

	float angularAcceleration = targetRotation; // Use agent current rotation for 2D - m_Agent->GetCurrentRotation();
	angularAcceleration /= (float)0.1; // Where 0.1 is the time in which we want to reach 
							   // the target rotation

	//If desired velocity > MaxRotation normalize
	if (angularAcceleration > m_Agent->GetMaxAngularAcceleration())
	{
		angularAcceleration /= angularAcceleration;
		angularAcceleration *= m_Agent->GetMaxAngularAcceleration();
	}

	return angularAcceleration;
}

//Face 2D
float SteeringBehavior::FacePoint2D(void)
{
	D3DXVECTOR2 direction, agentPos;
	agentPos = D3DXVECTOR2(m_Agent->GetPosition().x, m_Agent->GetPosition().z);

	direction = m_TargetPoint2D - agentPos;

	if (D3DXVec2LengthSq(&direction) < D3DX_16F_EPSILON)
	{
		return 0.0f;
	}

	//Calculate angle
	float angle = atan2f(-direction.x, direction.y);

	return Align2D(angle);
}

//Face Forward 2D
float SteeringBehavior::FaceForward2D(void)
{
	if (D3DXVec3LengthSq(&m_Agent->GetCurrentVelocity()) < D3DX_16F_EPSILON)
	{
		return 0.0f;
	}

	//Calculate target angle
	float angle = atan2f(-m_Agent->GetCurrentVelocity().x, m_Agent->GetCurrentVelocity().z);

	//Align to angle
	return Align2D(angle);
}




/**
 * Steering Behaviors - Angular Velocity Based - 3D
 */




//Align3D
angular_output SteeringBehavior::Align3D(void)
{
	angular_output result;
	D3DXQUATERNION q, sInv;
	D3DXQuaternionInverse(&sInv, &m_Agent->GetCurrentOrientation());

	q = sInv * m_TargetOrientation;
	D3DXQuaternionNormalize(&q, &q);

	//Debug
	if (UndefinedResult(q))
	{
		std::cout << "Undefined result in SteeringBehavior::Align3D line 437" << std::endl;
	}

	//Split quaternion into angle and axis

	//Angle
	if (q.w > 1.0f || q.w < -1.0f)
	{
		q.w = q.w / fabs(q.w);
	}
	
	float theta = 2 * acosf(q.w);

	//Debug
	if (UndefinedResult(theta))
	{
		theta = 0.0f;
	}

	//Axis
	Vector3 a = Vector3((1/(sinf(theta/2)))*q.x, (1/(sinf(theta/2)))*q.y, 
								(1/(sinf(theta/2)))*q.z);
	D3DXVec3Normalize(&a, &a);

	//Debug
	if (UndefinedResult(a))
	{
		a = Vector3(0.0f, 0.0f, 0.0f);
	}


	//==========================================
	//		   Find the Rotation Vector
	//==========================================


	//Now we need to find the proper rotation (angular acceleration)
	float rotation = theta;

	//Implemented as in Align2D
	rotation = MapToPi(rotation);

	float rotationSize = fabs(rotation);

	//Bail out if no change
	if (rotationSize < D3DX_16F_EPSILON)
	{
		rotation = 0.0f;
	}

	float targetRotation;
	float slowRadius = D3DX_PI;

	//If we are there
	if (rotationSize < (D3DX_PI / 8))
	{
		targetRotation = 0.0f;
	}

	//Rotate at full speed
	if (rotationSize > slowRadius)
	{
		targetRotation = m_Agent->GetMaxRotation();
	}

	//Rotate slowly
	else
	{
		targetRotation = m_Agent->GetMaxRotation() * rotationSize / slowRadius;
	}

	//Calculate final target rotation
	if (rotationSize != 0)
	{
		targetRotation *= rotation / rotationSize;
	}

	else
	{
		targetRotation = 0.0f;
	}

	//Debug
	if (UndefinedResult(targetRotation))
	{
		targetRotation = 0.0f;
	}

	float steeringAngular = targetRotation; // - 0.0f;
	steeringAngular /= (float)0.3; //Where 0.3 is timeToTarget


	//Normalize the value
	float angularAcceleration = abs(steeringAngular);
	if (angularAcceleration > m_Agent->GetMaxAngularAcceleration())
	{
		steeringAngular /= steeringAngular;
		steeringAngular *= m_Agent->GetMaxAngularAcceleration();
	}

	//Set theta to steeringAngular (Output new rotation)
	float w = steeringAngular;

	//Debug
	if (UndefinedResult(w))
	{
		w = 0.0f;
	}

	//Get the target rotation vector
	Vector3 rot(a.x*w, a.y*w, a.z*w);

	if (UndefinedResult(rot))
	{
		rot = Vector3(0.0f, 0.0f, 0.0f);
	}

	//Set rotational acceleration to output
	result.rotation = rot;

	//==========================================
	//		 Align to Rotation Axis Vector
	//==========================================

	//Get cross product of rotation vector and baseZorientation
	Vector3 r, Zb, rotAxis;
	Zb = TransformQuaternion(Vector3(0, 0, 1), m_BaseOrientation);
	D3DXVec3Normalize(&Zb, &Zb);
	D3DXVec3Normalize(&rotAxis, &rot);

	float angle = acosf(D3DXVec3Dot(&Zb, &rotAxis));

	if (angle > 0)
	{
		D3DXVec3Cross(&r,&Zb, &rotAxis);
	}

	if (angle < 0)
	{
		D3DXVec3Cross(&r, &rotAxis, &Zb);
	}

	D3DXQUATERNION bInv, p;
	D3DXQuaternionInverse(&bInv, &m_BaseOrientation);

	p = D3DXQUATERNION(sinf(angle/2)*r.x, sinf(angle/2)*r.y, 
		sinf(angle/2)*r.z, cosf(angle/2));

	D3DXQuaternionNormalize(&p, &p);

	q = bInv * p;

	if (sinf(angle) != 0)
	{
		D3DXQuaternionNormalize(&q, &q);
		result.orientation = q;
	}

	else
	{
		Vector3 Zt = TransformQuaternion(Vector3(0, 0, 1), q);
		D3DXVec3Normalize(&Zt, &Zt);

		if (Zb == Zt)
			result.orientation = m_BaseOrientation;
		else 
			result.orientation = -m_BaseOrientation;
	}

	return result;
}

//Face point
angular_output SteeringBehavior::FacePoint3D(void)
{
	//Calculate target to delegate to align
	Vector3 direction = m_TargetPoint3D - m_Agent->GetPosition();

	//If zero direction, make no change
	if (D3DXVec3LengthSq(&direction) < D3DX_16F_EPSILON)
	{
		angular_output w;
		w.orientation = m_BaseOrientation;
		w.rotation = Vector3(0.0f, 0.0f, 0.0f);
		return w;
	}

	D3DXVec3Normalize(&direction, &direction);
	D3DXQUATERNION q = CalculateOrientation(direction);

	if (UndefinedResult(q))
	{
		q = m_BaseOrientation;
	}

	m_TargetOrientation = q;

	return Align3D();
}

//Face Forward
angular_output SteeringBehavior::FaceForward3D(void)
{
	//Get velocity vector
	Vector3 direction = m_Agent->GetCurrentVelocity();

	D3DXVec3Normalize(&direction, &direction);

	D3DXQUATERNION q;

	//If zero direction, make no change
	if (D3DXVec3LengthSq(&direction) < D3DX_16F_EPSILON)
	{
		D3DXQuaternionIdentity(&q); 
		angular_output w;
		w.orientation = m_BaseOrientation;
		w.rotation = Vector3(0.0f, 0.0f, 0.0f);
		return w;
	}

	q = CalculateOrientation(direction);

	//Debug
	if (UndefinedResult(q))
	{
		std::cout << "Undefined result logged at SteeringBehavior::CalculateOrientation in FaceForward3D" << std::endl;
	}

	m_TargetOrientation = q;

	return Align3D();
}

//Calculate Orientation
D3DXQUATERNION SteeringBehavior::CalculateOrientation(Vector3 vector)
{
	Vector3 BaseZ; // = Vector3(0.0f, 0.0f, 1);

	//Get the base vector along Z axis
	BaseZ = TransformQuaternion(Vector3(0, 0, 1), D3DXQUATERNION(0, 0, 0, 1));  

	//Vector3 dir = vector - m_Agent->GetPosition();
	//D3DXVec3Normalize(&dir, &dir);

	if (BaseZ == vector)
		return m_BaseOrientation;
	
	if (BaseZ == -vector)
	{
		D3DXQUATERNION bInv;
		D3DXQuaternionInverse(&bInv, &m_BaseOrientation);
		return bInv;
	}

	//Find the minimum rotation from base to target using vector product
	Vector3 change;
	
	//Find the angle and axis
	Vector3 normal;
	D3DXVec3Normalize(&normal, &vector);

	float angle = acosf(D3DXVec3Dot(&BaseZ, &normal));

	D3DXVec3Cross(&change, &BaseZ, &vector);

	Vector3 axis = change;

	D3DXVec3Normalize(&axis, &axis);

	//Pack the axis and angle into a quaternion
	return D3DXQUATERNION(sinf(angle/2)*axis.x, sinf(angle/2)*axis.y,
						  sinf(angle/2)*axis.z, cosf(angle/2));
}




/**
 * Force accumulation
 */




bool SteeringBehavior::AccumulateForce(Vector3& SteeringTotal, Vector3 forceToAdd)
{
	//Calculate current magnitude
	float MagnitudeSoFar = D3DXVec3Length(&SteeringTotal);
	
	//Calculate available magnitude
	float MagnitudeRemaining = m_Agent->GetMaxForce().z - MagnitudeSoFar;

	if (MagnitudeRemaining <= 0)
	{
		return false;
	}

	//Calculate force to add
	double MagnitudeToAdd = D3DXVec3Length(&forceToAdd);

	if (MagnitudeToAdd < MagnitudeRemaining)
	{
		SteeringTotal += forceToAdd;
	}

	else
	{
		D3DXVec3Normalize(&forceToAdd, &forceToAdd);
		SteeringTotal += forceToAdd * MagnitudeRemaining;
	}

	return true;
}

Vector3 SteeringBehavior::GetLinearVelocity(std::vector<AIObject*>* objects)
{
	switch(m_SummingMethod)
	{
	case PRIORITIZED:
		return Prioritized(objects);
		
	case WEIGHTED_AVERAGE:
		return WeightedSum(objects);

	case DITHERED:
		return Dithering(objects);

	default:
		return Prioritized(objects);
	}
}

angular_output SteeringBehavior::GetAngularVelocity(void)
{
	if (isAlign3DOn())
	{
		return Align3D();
	}

	if (isFacePoint3DOn())
	{
		return FacePoint3D();
	}

	if (isFaceForward3DOn())
	{
		return FaceForward3D();
	}

	else 
		return FaceForward3D();
}




/**
 * Summing methods
 */




Vector3 SteeringBehavior::Prioritized(std::vector<AIObject*>* objects)
{
	//Clear steering force
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);

	//Create new force
	Vector3 force;

	//Step through each movement type and calculate the force to add
	if (isWallAvoidanceOn())
	{
		//force = WallAvoidance( walls ) * m_WeightWallAvoidance

		//if (!AccumulateForce(m_SteeringForce, force))
		//{
		//	return m_SteeringForce;
		//}
	}

	if (isObstacleAvoidanceOn())
	{
		force = ObstacleAvoidance( objects ) * m_WeightObstacleAvoidance;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}

	if (isEvadeOn())
	{
		force = Evade(m_AgentEvade) * m_WeightEvade;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}

	if (isFleeOn())
	{
		force = Flee(m_TargetPosFlee) * m_WeightFlee;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}

	if (isWanderOn())
	{
		force = Wander() * m_WeightWander;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}

	if (isSeekOn())
	{
		force = Seek(m_TargetPosSeek) * m_WeightSeek;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}

	if (isArriveOn())
	{
		force = Arrive(m_TargetPosSeek, m_Deceleration) * m_WeightArrive;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}
	
	if (isPursueOn())
	{
		force = Pursue(m_AgentPursue) * m_WeightPursue;

		if (!AccumulateForce(m_SteeringForce, force))
		{
			return m_SteeringForce;
		}
	}


	//If separation, alignment, or cohesion is on, get movers in range
	if (isSeparationOn() || isAlignmentOn() || isCohesionOn())
	{
		//Tag movers in range
	}

	if (isCohesionOn())
	{
		//force = Cohesion( GetAgents ) * m_WeightCohesion;

		//if (!AccumulateForce(m_SteeringForce, force))
		//{
		//	return m_SteeringForce;
		//}
	}

	if (isSeparationOn())
	{
		//force = Separation( GetAgents ) * m_WeightSeparation;

		//if (!AccumulateForce(m_SteeringForce, force))
		//{
		//	return m_SteeringForce;
		//}
	}

	if (isAlignmentOn())
	{
		//force = Separation( GetAgents ) * m_WeightSeparation;

		//if (!AccumulateForce(m_SteeringForce, force))
		//{
		//	return m_SteeringForce;
		//}
	}

	return m_SteeringForce;
}

Vector3 SteeringBehavior::WeightedSum(std::vector<AIObject*>* objects)
{
	//Clear steering force
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);

	if (isWallAvoidanceOn())
	{
		//m_SteeringForce += WallAvoidance( walls ) * m_WeightWallAvoidance;
	}

	if (isObstacleAvoidanceOn())
	{
		m_SteeringForce += ObstacleAvoidance( objects ) * m_WeightObstacleAvoidance;
	}

	if (isEvadeOn())
	{
		m_SteeringForce += Evade(m_AgentEvade) * m_WeightEvade;
	}

	if (isSeparationOn())
	{
		//m_SteeringForce += Separation( neighbors ) * m_WeightSeparation;
	}

	if (isAlignmentOn())
	{
		//m_SteeringForce += Alignment( neighbors ) * m_WeightAlignment;
	}

	if (isCohesionOn())
	{
		//m_SteeringForce += Cohesion(neighbors) * m_WeightCohesion;
	}

	if (isWanderOn())
	{
		m_SteeringForce += Wander() * m_WeightWander;
	}

	if (isSeekOn())
	{
		m_SteeringForce += Seek(m_TargetPosSeek) * m_WeightSeek;
	}

	if (isFleeOn())
	{
		m_SteeringForce += Flee(m_TargetPosFlee) * m_WeightFlee;
	}

	if (isArriveOn())
	{
		m_SteeringForce += Arrive(m_TargetPosSeek, m_Deceleration) * m_WeightArrive;
	}

	if (isPursueOn())
	{
		m_SteeringForce += Pursue(m_AgentPursue) * m_WeightPursue;
	}

	if (isOffsetPursueOn())
	{
		//m_SteeringForce += OffsetPur
	}

	if (isInterposeOn())
	{
		m_SteeringForce += Interpose(&m_AgentEvade->GetPosition(), &m_AgentPursue->GetPosition()) * m_WeightInterpose;
	}

	if (isHideOn())
	{
		//m_SteeringForce += Hide(&m_AgentEvade->GetPosition(), obstacles) * m_WeightHide;
	}

	if (isFollowPathOn())
	{
		//Follow path
	}

	//Truncate the force to max force
	D3DXVec3Normalize(&m_SteeringForce, &m_SteeringForce);
	m_SteeringForce *= m_Agent->GetMaxForce().z;

	return m_SteeringForce;
}

Vector3 SteeringBehavior::Dithering(std::vector<AIObject*>* objects)
{
	//Clear steering force
	m_SteeringForce = Vector3(0.0f, 0.0f, 0.0f);

	return m_SteeringForce;
}




/**
 * Targeting
 */




void SteeringBehavior::SetTargetSeek(Vector3 target)
{
	m_TargetPosSeek = target;
}

void SteeringBehavior::SetTargetFlee(Vector3 target)
{
	m_TargetPosFlee = target;
}

void SteeringBehavior::SetTargetAgentEvade(AIObject* evade)
{
	m_AgentEvade = evade;
}

void SteeringBehavior::SetTargetAgentPursue(AIObject* pursue)
{
	m_AgentPursue = pursue;
}




/**
 * Utilities
 */




//Steering off
void SteeringBehavior::SteeringOff(void)
{
	m_Separation = false;
	m_Cohesion = false;
	m_Alignment = false;
	m_Wander = false;
	m_ObstacleAvoidance = false;
	m_WallAvoidance = false;
	m_Seek = false;
	m_Flee = false;
	m_Arrive = false;
	m_Pursue = false;
	m_OffsetPursue = false;
	m_Interpose = false;
	m_Hide = false;
	m_Evade = false;
	m_Flocking = false;
	m_FollowPath = false;
	m_Align2D = false;
	m_Align3D = false;
	m_FacePoint2D = false;
	m_FacePoint3D = false;
	m_FaceForward2D = false;
	m_FaceForward3D = false;
}

void SteeringBehavior::LoadParamsFromFile(std::string paramFile)
{
	////Create parser
	//JSONParser parser;

	//int error = parser.OpenFile(paramFile);

	//switch(error)
	//{
	//case 1:
	//	MessageBox(0, "Steering params invalid", "AI", 0);
	//	PostQuitMessage(0);
	//	break;

	//case 2:
	//	MessageBox(0, "Steering params not found", "AI", 0);
	//	PostQuitMessage(0);
	//	break;
	//	
	//default:
	//	break;
	//}

	//JSONObject params = parser.GetParsedObject();

	////Parameters
	//std::stringstream(params.m_variables["seek"]) >> m_WeightSeek;
	//std::stringstream(params.m_variables["flee"]) >> m_WeightFlee;
	//std::stringstream(params.m_variables["arrive"]) >> m_WeightArrive;
	//std::stringstream(params.m_variables["pursue"]) >> m_WeightPursue;
	//std::stringstream(params.m_variables["evade"]) >> m_WeightEvade;
	//std::stringstream(params.m_variables["offset"]) >> m_WeightOffsetPursue;
	//std::stringstream(params.m_variables["interpose"]) >> m_WeightInterpose;
	//std::stringstream(params.m_variables["hide"]) >> m_WeightHide;
	//std::stringstream(params.m_variables["separation"]) >> m_WeightSeparation;
	//std::stringstream(params.m_variables["cohesion"]) >> m_WeightCohesion;
	//std::stringstream(params.m_variables["alignment"]) >> m_WeightAlignment;
	//std::stringstream(params.m_variables["obstacle"]) >> m_WeightObstacleAvoidance;
	//std::stringstream(params.m_variables["wall"]) >> m_WeightWallAvoidance;


	//std::stringstream(params.m_variables["viewDist"]) >> m_ViewDistance;
	//std::stringstream(params.m_variables["evadeDistSq"]) >> m_EvadeDistanceSq;
	//std::stringstream(params.m_variables["boxLength"]) >> m_BoxLength;
	////std::stringstream(params.m_variables["feelerLength"]) >> m_FeelerLength;
	//std::stringstream(params.m_variables["wanderJitter"]) >> m_WanderJitter;
	//std::stringstream(params.m_variables["wanderRadius"]) >> m_WanderRadiusY;
	//m_WanderRadiusXZ = m_WanderRadiusY / 2.0f;
	////std::stringstream(params.m_variables["wanderDist"]) >> m_WanderDistance;
}
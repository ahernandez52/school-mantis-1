/**
 * SteeringBehavior - Defines necessary members and methods for dynamic movement algorithms
 * and blending methods for returning desired linear and angular velocities
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "../../FRAMEWORK/Utility/Geometry.h"
#include <vector>

class Actor;
class ShipAI;
class AIObject;
class Wall;

struct angular_output
{
	D3DXQUATERNION orientation;
	Vector3    rotation;
};

class SteeringBehavior
{
public:

	enum summing_method 
	{
		WEIGHTED_AVERAGE,
		PRIORITIZED,
		DITHERED
	};

private:
	//Owning agent
	ShipAI* m_Agent;

	//Desired velocity
	Vector3	m_SteeringForce;

	//Target positions
	Vector3 m_TargetPosSeek;
	Vector3 m_TargetPosFlee;

	//Target orientation positions
	D3DXVECTOR2 m_TargetPoint2D;
	Vector3 m_TargetPoint3D;

	D3DXQUATERNION m_TargetOrientation;

	//Base orientation (Used for 3D orientation)
	D3DXQUATERNION m_BaseOrientation;

	//Tracking entities
	AIObject*	m_AgentEvade;
	AIObject*	m_AgentPursue;

	//Current position on wander circle
	Vector3 m_WanderTarget;
	Vector3 m_WanderProjection;

	//Wander values
	float		m_WanderOffset;		//Offset from agent
	float		m_WanderRadiusY;	
	float		m_WanderRadiusXZ;
	float		m_WanderJitter;		//Should be specifically less than 1/sqrt(3) to avoid zero vector

	//Feelers
	std::vector<Vector3> m_Feelers;

	//Detection box length
	float		m_BoxLength;

	//Blending method
	summing_method m_SummingMethod;

	//Blending weights
	float	m_WeightSeparation;
	float	m_WeightCohesion;
	float	m_WeightAlignment;
	float	m_WeightWander;
	float	m_WeightObstacleAvoidance;
	float	m_WeightWallAvoidance;
	float	m_WeightSeek;
	float	m_WeightFlee;
	float	m_WeightArrive;
	float	m_WeightPursue;
	float	m_WeightOffsetPursue;
	float	m_WeightInterpose;
	float	m_WeightHide;
	float	m_WeightEvade;
	float	m_WeightFollowPath;
	float	m_WeightFacePoint2D;
	float	m_WeightFaceForward2D;
	float	m_WeightFacePoint3D;
	float	m_WeightFaceForward3D;

	//Boolean values to set movement algorithms		<ALERT> Change to flags when ready
	bool		m_Separation;
	bool		m_Cohesion;
	bool		m_Alignment;
	bool		m_Wander;
	bool		m_ObstacleAvoidance;
	bool		m_WallAvoidance;
	bool		m_Seek;
	bool		m_Flee;
	bool		m_Arrive;
	bool		m_Pursue;
	bool		m_OffsetPursue;
	bool		m_Interpose;
	bool		m_Hide;
	bool		m_Evade;
	bool		m_Flocking;
	bool		m_FollowPath;
	bool		m_Align2D;
	bool		m_Align3D;
	bool		m_FacePoint2D;
	bool		m_FaceForward2D;
	bool		m_FacePoint3D;
	bool		m_FaceForward3D;

	//Agent's vision
	float		m_ViewDistance;

	//Evade distance (Squared)
	float		m_EvadeDistanceSq;

	//Offset
	Vector3	m_Offset;

	//Deceleration
	enum Deceleration{slow = 3, normal = 2, fast = 1};

	Deceleration	m_Deceleration;

protected:

public:


public:
	SteeringBehavior(void);
	SteeringBehavior(ShipAI* agent);
	SteeringBehavior(ShipAI* agent, std::string paramFile);

	void SetAgent(ShipAI* agent){m_Agent = agent;}

	~SteeringBehavior(void);

	//Update forces
	Vector3 GetLinearVelocity(std::vector<AIObject*>* objects);
	angular_output GetAngularVelocity(void);

	//Blending methods
	Vector3	Prioritized(std::vector<AIObject*>* objects);
	Vector3 WeightedSum(std::vector<AIObject*>* objects);
	Vector3 Dithering(std::vector<AIObject*>* objects);		// NYI

	Vector3 PrioritizedDirection(void);
	Vector3 WeightedSumDirection(void);

	float		PrioritizedAngularAcceleration(void);
	float		WeightedSumAngularAcceleration(void);

	//Set target
	void SetTargetSeek(const Vector3 target);
	void SetTargetFlee(const Vector3 target);

	void SetTargetAgentEvade(AIObject* evader);
	void SetTargetAgentPursue(AIObject* pursuer);

	void SetTargetPoint2D(D3DXVECTOR2 p){m_TargetPoint2D = p;}
	void SetTargetPoint3D(Vector3 p){m_TargetPoint3D = p;}

	void SetTargetAlignment(D3DXQUATERNION q){m_TargetOrientation = q;}

	//Set wander
	void SetWanderOffset(float offset){m_WanderOffset = offset;}
	void SetWanderRadius(float radius){m_WanderRadiusXZ = radius;}
	void SetWanderJitter(float jitter){m_WanderJitter = jitter;}

	//Get the steering force
	Vector3 SteeringForce() const {return m_SteeringForce;}

	//Create feelers
	void CreateFeelers(void);

	//Check force accumulation
	bool AccumulateForce(Vector3& steeringTotal, Vector3 forceToAdd);

	/**
	 * Steering Behaviors - Linear Velocity
	 */

	//Seek
	Vector3 Seek(Vector3 targetPos);

	//Flee
	Vector3 Flee(Vector3 targetPos);

	//Arrive
	Vector3 Arrive(Vector3 targetPos, Deceleration deceleration);

	//Pursue
	Vector3 Pursue(AIObject* target);

	//Offset pursue
	Vector3 OffsetPursue(AIObject* target, Vector3 offset);

	//Evade
	Vector3 Evade(AIObject* target);

	//Wander
	Vector3 Wander(void);

	//Obstacle Avoidance
	Vector3 ObstacleAvoidance(const std::vector<AIObject*>* obstacles);

	//Wall avoidance
	Vector3 WallAvoidance(const std::vector<Wall*>& walls);

	//Interpose
	Vector3 Interpose(const Vector3* targetA, const Vector3* targetB);

	//Hide
	Vector3 Hide(const AIObject* target, const std::vector<Actor*>& obstacles);

	//Separation
	Vector3 Separation(const std::vector<AIObject*>& neighbors);

	//Cohesion
	Vector3 Cohesion(const std::vector<AIObject*>& neighbors);

	//Alignment
	Vector3 Alignment(const std::vector<AIObject*>& neighbors);

	//Follow path
	Vector3 FollowPath(Vector3 target);

	/**
	 * Steering Behaviors - Angular Velocity 2D
	 */

	//Align2D
	float Align2D(float orientation);

	//Face point
	float FacePoint2D(void);

	//Face forward
	float FaceForward2D (void);

	/**
	 * Steering Behaviors - Angular Velocity 3D
	 */

	//Align3D
	angular_output Align3D(void);	//Uses m_TargetOrientation

	//Vector3 Align3DRotation(D3DXQUATERNION t);

	//Face Point
	angular_output FacePoint3D(void);

	//Face Forward
	angular_output FaceForward3D(void);

	//Calculate Orienataion
	D3DXQUATERNION CalculateOrientation(Vector3 vector);

	/**
	 * Boolean setters for movement
	 */

	void SeekOn(void){m_Seek = true;}
	void FleeOn(void){m_Flee = true;}
	void ArriveOn(void){m_Arrive = true;}
	void WanderOn(void){m_Wander = true;}
	void PursueOn(void){m_Pursue = true;}
	void EvadeOn(void){m_Evade = true;}
	void CohesionOn(void){m_Cohesion = true;}
	void SeparationOn(void){m_Separation = true;}
	void AlignmentOn(void){m_Alignment = true;}
	void ObstacleAvoidanceOn(void){m_ObstacleAvoidance = true;}
	void WallAvoidanceOn(void){m_WallAvoidance = true;}
	void InterposeOn(void){m_Interpose = true;}
	void HideOn(void){m_Hide = true;}
	void OffsetPursueOn(void){m_OffsetPursue = true;}
	void FollowPathOn(void){m_FollowPath = true;}
	void FlockingOn(void){CohesionOn(); SeparationOn(); AlignmentOn();}
	void Align2DOn(void){m_Align2D = true;}
	void Align3DOn(void){m_Align3D = true;}
	void FacePoint2DOn(void){m_FacePoint2D = true; m_FaceForward2D = false;}
	void FaceForward2DOn(void){m_FaceForward2D = true; m_FacePoint2D = false;}
	void FacePoint3DOn(void){m_FacePoint3D = true; m_FaceForward3D = false; m_Align3D = false;}
	void FaceForward3DOn(void){m_FaceForward3D = true; m_FacePoint3D = false; m_Align3D = false;}

	void SeekOff(void){m_Seek = false;}
	void FleeOff(void){m_Flee = false;}
	void ArriveOff(void){m_Arrive = false;}
	void WanderOff(void){m_Wander = false;}
	void PursueOff(void){m_Pursue = false;}
	void EvadeOff(void){m_Evade = false;}
	void CohesionOff(void){m_Cohesion = false;}
	void SeparationOff(void){m_Separation = false;}
	void AlignmentOff(void){m_Alignment = false;}
	void ObstacleAvoidanceOff(void){m_ObstacleAvoidance = false;}
	void WallAvoidanceOff(void){m_WallAvoidance = false;}
	void InterposeOff(void){m_Interpose = false;}
	void HideOff(void){m_Hide = false;}
	void OffsetPursueOff(void){m_OffsetPursue = false;}
	void FollowPathOff(void){m_FollowPath = false;}
	void FlockingOff(void){CohesionOn(); SeparationOn(); AlignmentOn();}
	void Align2DOff(void){m_Align2D = false;}
	void Align3DOff(void){m_Align3D = false;}
	void FacePoint2DOff(void){m_FacePoint2D = false;}
	void FaceForward2DOff(void){m_FaceForward2D = false;}
	void FacePoint3DOff(void){m_FacePoint3D = false;}
	void FaceForward3DOff(void){m_FaceForward3D = false;}

	bool isSeekOn(void){return m_Seek;}
	bool isFleeOn(void){return m_Flee;}
	bool isArriveOn(void){return m_Arrive;}
	bool isWanderOn(void){return m_Wander;}
	bool isPursueOn(void){return m_Pursue;}
	bool isEvadeOn(void){return m_Evade;}
	bool isCohesionOn(void){return m_Cohesion;}
	bool isSeparationOn(void){return m_Separation;}
	bool isAlignmentOn(void){return m_Alignment;}
	bool isObstacleAvoidanceOn(void){return m_ObstacleAvoidance;}
	bool isWallAvoidanceOn(void){return m_WallAvoidance;}
	bool isInterposeOn(void){return m_Interpose;}
	bool isHideOn(void){return m_Hide;}
	bool isOffsetPursueOn(void){return m_OffsetPursue;}
	bool isFollowPathOn(void){return m_FollowPath;}
	bool isFlockingOn(void){CohesionOn(); SeparationOn(); AlignmentOn();}
	bool isAlign2DOn(void){return m_Align2D;}
	bool isAlign3DOn(void){return m_Align3D;}
	bool isFacePoint2DOn(void){return m_FacePoint2D;}
	bool isFaceForward2DOn(void){return m_FaceForward2D;}
	bool isFacePoint3DOn(void){return m_FacePoint3D;}
	bool isFaceForward3DOn(void){return m_FaceForward3D;}

	void SetWeightSeek(float w){m_WeightSeek = w;}
	void SetWeightFlee(float w){m_WeightFlee = w;}
	void SetWeightArrive(float w){m_WeightArrive = w;}
	void SetWeightWander(float w){m_WeightWander = w;}
	void SetWeightPursue(float w){m_WeightPursue = w;}
	void SetWeightEvade(float w){m_WeightEvade = w;}
	void SetWeightCohesion(float w){m_WeightCohesion = w;}
	void SetWeightSeparation(float w){m_WeightSeparation = w;}
	void SetWeightAlignment(float w){m_WeightAlignment = w;}
	void SetWeightObstacleAvoidance(float w){m_WeightObstacleAvoidance = w;}
	void SetWeightWallAvoidance(float w){m_WeightWallAvoidance = w;}
	void SetWeightInterpose(float w){m_WeightInterpose = w;}
	void SetWeightHide(float w){m_WeightHide = w;}
	void SetWeightOffsetPursue(float w){m_WeightOffsetPursue = w;}
	void SetWeightFollowPath(float w){m_WeightFollowPath = w;}


	//Turn steering off
	void SteeringOff(void);

	/**
	 * Getters
	 */

	//View distance
	float GetViewDistance(void){return m_ViewDistance;}

	//Return target agents
	AIObject*	GetPursueAgent(void){return m_AgentPursue;}
	AIObject*   GetEvadeAgent(void){return m_AgentEvade;}

	//Return target positions
	Vector3 GetTargetFlee(void){return m_TargetPosFlee;}
	Vector3 GetTargetSeek(void){return m_TargetPosSeek;}

	//Return target orientation positions
	D3DXVECTOR2 GetTargetPoint2D(void){return m_TargetPoint2D;}
	Vector3 GetTargetPoint3D(void){return m_TargetPoint3D;}

	D3DXQUATERNION GetTargetAlignment(void){return m_TargetOrientation;}

	Vector3 GetWanderTarget(void){return m_WanderProjection;}
	float   GetWanderOffset(void){return m_WanderOffset;}
	float   GetWanderJitter(void){return m_WanderJitter;}
	float   GetWanderRadius(void){return m_WanderRadiusXZ;}

	void SetWanderTarget(Vector3 target){m_WanderProjection = target;}

	//Load parameters from file
	void LoadParamsFromFile(std::string paramFile);
};